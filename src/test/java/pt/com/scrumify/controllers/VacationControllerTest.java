package pt.com.scrumify.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.entities.VacationView;
import pt.com.scrumify.helpers.ConstantsHelper;

@DisplayName("Test of vacations controller")
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class VacationControllerTest {
   @Autowired
   private MockMvc mvc;
   
   @Test
   @UnitTest
   @Order(1)
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_VACATIONS))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(2)
   @MockScrumifyUser(username = "anonymous")
   public void createByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_VACATIONS_CREATE))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(3)
   @MockScrumifyUser(username = "projectdirector")  
   public void createByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_VACATIONS_CREATE))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_VACATIONS_NEW))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATION, isA(VacationView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATION, hasProperty("id", is(nullValue()))))
         ;
   }
   
   @Test
   @UnitTest
   @Order(4)
   @MockScrumifyUser(username = "developer3")  
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 2;
      
      mvc.perform(get(ConstantsHelper.MAPPING_VACATIONS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_VACATIONS_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, hasSize(EXPECTED_SIZE)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, hasItem(allOf(hasProperty("id", is(1))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, hasItem(allOf(hasProperty("id", is(2))))))
         ;
   }
   
   @Test
   @UnitTest
   @Order(5)
   @MockScrumifyUser(username = "developer2")
   public void deleteByNotAllowedUser() throws Exception {
      
      mvc.perform(get(ConstantsHelper.MAPPING_VACATIONS_DELETE + ConstantsHelper.MAPPING_SLASH + 2))
         .andExpect(status().isForbidden())
         ;
   }
}