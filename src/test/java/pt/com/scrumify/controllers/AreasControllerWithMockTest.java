package pt.com.scrumify.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.services.AreaService;
import pt.com.scrumify.helpers.ConstantsHelper;

@AutoConfigureMockMvc
@DisplayName("Test of areas controller")
@SpringBootTest
public class AreasControllerWithMockTest {
   @Autowired
   private MockMvc mvc;

   @MockBean
   AreaService mock;

   @DisplayName("Test of areas save mapping (post) by not allowed user (projectlead1)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer1")  
   public void saveByNotAllowedUser() throws Exception {
      String content = "id=0&" +
                       "abbreviation=" + RandomStringUtils.random(4, true, false) + "&" +
                       "name=" + RandomStringUtils.random(50, true, false);
      
      when(mock.save(any(Area.class))).thenReturn(new Area());
            
      mvc.perform(post(ConstantsHelper.MAPPING_AREAS_SAVE)
                  .content(content)
                  .with(csrf())
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                  .accept(MediaType.APPLICATION_FORM_URLENCODED))
         .andExpect(status().isForbidden())
         .andReturn()
         ;
   }

   @DisplayName("Test of areas save mapping (post) by allowed user (projectdirector)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void saveByAllowedUser() throws Exception {
      String content = "id=0&" +
                       "abbreviation=" + RandomStringUtils.random(4, true, false) + "&" +
                       "name=" + RandomStringUtils.random(50, true, false);
      
      when(mock.save(any(Area.class))).thenReturn(new Area());
            
      mvc.perform(post(ConstantsHelper.MAPPING_AREAS_SAVE)
                  .content(content)
                  .with(csrf())
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                  .accept(MediaType.APPLICATION_FORM_URLENCODED))
         .andExpect(status().isFound())
         .andExpect(redirectedUrl(ConstantsHelper.MAPPING_AREAS))
         .andReturn()
         ;
   }

   @DisplayName("Test of areas save mapping (post) by allowed user (projectdirector) with errors (abbreviation is null and areaName is null)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void saveByAllowedUserWithErrorsNull() throws Exception {
      String content = "id=0&" +
                       "abbreviation=&" +
                       "name=";
      
      when(mock.save(any(Area.class))).thenReturn(new Area());
            
      mvc.perform(post(ConstantsHelper.MAPPING_AREAS_SAVE)
                  .content(content)
                  .with(csrf())
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                  .accept(MediaType.APPLICATION_FORM_URLENCODED))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_AREAS_UPDATE))
         .andExpect(model().attributeHasFieldErrors(ConstantsHelper.VIEW_ATTRIBUTE_AREA, "abbreviation"))
         .andExpect(model().attributeHasFieldErrors(ConstantsHelper.VIEW_ATTRIBUTE_AREA, "name"))
         .andReturn()
         ;
   }

   @DisplayName("Test of areas save mapping (post) by allowed user (projectdirector) with errors (abbreviation length > 4 and areaName > 50)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void saveByAllowedUserWithErrorsLength() throws Exception {
      String content = "id=0&" +
                       "abbreviation=" + RandomStringUtils.random(5, true, false) + "&" +
                       "name=" + RandomStringUtils.random(51, true, false);
      
      when(mock.save(any(Area.class))).thenReturn(new Area());
            
      mvc.perform(post(ConstantsHelper.MAPPING_AREAS_SAVE)
                  .content(content)
                  .with(csrf())
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                  .accept(MediaType.APPLICATION_FORM_URLENCODED))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_AREAS_UPDATE))
         .andExpect(model().attributeHasFieldErrors(ConstantsHelper.VIEW_ATTRIBUTE_AREA, "abbreviation"))
         .andExpect(model().attributeHasFieldErrors(ConstantsHelper.VIEW_ATTRIBUTE_AREA, "name"))
         .andReturn()
         ;
   }
}