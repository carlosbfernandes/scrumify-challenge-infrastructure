package pt.com.scrumify.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.database.services.AreaService;
import pt.com.scrumify.entities.AreaView;
import pt.com.scrumify.helpers.ConstantsHelper;

@AutoConfigureMockMvc
@DisplayName("Test of areas controller")
@SpringBootTest
public class AreasControllerTest {
   @Autowired
   private MockMvc mvc;
   @Autowired
   AreaService service;

   @DisplayName("Test of areas create mapping (get) by not allowed user (projectdirector)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")
   public void createNotByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_AREAS_CREATE))
         .andExpect(status().isForbidden())
         ;
   }

   @DisplayName("Test of areas create mapping (get) by allowed user (admin)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "admin")  
   public void createByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_AREAS_CREATE))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_AREAS_CREATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, isA(AreaView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, hasProperty("id", is(0))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, hasProperty("abbreviation", is(emptyOrNullString()))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, hasProperty("name", is(emptyOrNullString()))))
         ;
   }

   @DisplayName("Test of areas list mapping (get) by not allowed user (developer1)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer1")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_AREAS))
         .andExpect(status().isForbidden())
         ;
   }

   @DisplayName("Test of areas list mapping (get) by allowed user (projectlead2)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectlead2")  
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 3;
      
      mvc.perform(get(ConstantsHelper.MAPPING_AREAS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_AREAS_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, hasSize(EXPECTED_SIZE)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, hasItem(allOf(hasProperty("id", is(2)),
                                                                                          hasProperty("abbreviation", is("AGI")),
                                                                                          hasProperty("name", is("Área de Gestão de Impostos"))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, hasItem(allOf(hasProperty("id", is(1)),
                                                                                          hasProperty("abbreviation", is("AGCI")),
                                                                                          hasProperty("name", is("Área de Gestão de Contribuintes e Inspeção"))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, hasItem(allOf(hasProperty("id", is(3)),
                                                                                          hasProperty("abbreviation", is("AJFF")),
                                                                                          hasProperty("name", is("Área de Justiça e Fluxos Financeiros"))))))
         ;
   }

   @DisplayName("Test of areas update mapping (get) by not allowed user (projectlead2)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer2")
   public void updateByNotAllowedUser() throws Exception {
      int AREA = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_AREAS_UPDATE + ConstantsHelper.MAPPING_SLASH + AREA))
         .andExpect(status().isForbidden())
         ;
   }

   @DisplayName("Test of areas update mapping (get) by allowed user (projectdirector)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void updateByAllowedUser() throws Exception {
      int AREA = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_AREAS_UPDATE + ConstantsHelper.MAPPING_SLASH + AREA))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_AREAS_UPDATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, isA(AreaView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, hasProperty("id", is(greaterThan(0)))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, hasProperty("abbreviation", is("AGCI"))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, hasProperty("name", is("Área de Gestão de Contribuintes e Inspeção"))))
         ;
   }
}