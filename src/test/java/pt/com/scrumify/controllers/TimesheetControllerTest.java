package pt.com.scrumify.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.helpers.ConstantsHelper;

@DisplayName("Test of timesheets controller")
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TimesheetControllerTest {
   @Autowired
   private MockMvc mvc;
   
   @Test
   @UnitTest
   @Order(1)
   @MockScrumifyUser(username = "anonymous")
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_TIMESHEET))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(2)
   @MockScrumifyUser(username = "projectdirector")  
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 2;
      LocalDate today = LocalDate.now();
      
      mvc.perform(get(ConstantsHelper.MAPPING_TIMESHEET + "/" + today.getYear() + "/" + today.getMonthValue()))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_TIMESHEET_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, hasSize(EXPECTED_SIZE)))
         ;
   }
   
   
}