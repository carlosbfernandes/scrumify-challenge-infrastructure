package pt.com.scrumify.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.entities.ApplicationView;
import pt.com.scrumify.helpers.ConstantsHelper;

@DisplayName("Test of teams controller")
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ApplicationsControllerTest {
   @Autowired
   private MockMvc mvc;
   
   @Test
   @UnitTest
   @Order(1)
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_APPLICATIONS))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(2)
   @MockScrumifyUser(username = "anonymous")
   public void createByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_APPLICATIONS_CREATE))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(3)
   @MockScrumifyUser(username = "projectdirector")  
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 2;
      
      mvc.perform(get(ConstantsHelper.MAPPING_APPLICATIONS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_APPLICATIONS_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, hasSize(EXPECTED_SIZE)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, hasItem(allOf(hasProperty("id", is(1)),
                                                                                          hasProperty("name", is("Application A")),
                                                                                          hasProperty("fqdn", is("application-a.ritta.local"))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, hasItem(allOf(hasProperty("id", is(2)),
                                                                                          hasProperty("name", is("Application B")),
                                                                                          hasProperty("fqdn", is("application-b.ritta.local"))))))
         ;
   }
   
   @Test
   @UnitTest
   @Order(4)
   @MockScrumifyUser(username = "projectdirector")  
   public void createByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_APPLICATIONS_CREATE))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_APPLICATIONS_CREATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, isA(ApplicationView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, hasProperty("id", is(0))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, hasProperty("description", is(emptyOrNullString()))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, hasProperty("taxonomy", is(emptyOrNullString()))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, hasProperty("fqdn", is(emptyOrNullString()))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, hasProperty("name", is(emptyOrNullString()))))
         ;
   }
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "submitter")
   public void updateByNotAllowedUser() throws Exception {
      int application = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_APPLICATIONS_UPDATE + ConstantsHelper.MAPPING_SLASH + application))
         .andExpect(status().isForbidden())
         ;
   }
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void updateByAllowedUser() throws Exception {
      int application = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_APPLICATIONS_UPDATE + ConstantsHelper.MAPPING_SLASH + application))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_APPLICATIONS_CREATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, isA(ApplicationView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, hasProperty("id", is(greaterThan(0)))))
         ;
   }
}