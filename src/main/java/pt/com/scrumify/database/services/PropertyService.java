package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Property;

public interface PropertyService {
   Property getOne(Integer id);
   Property save(Property property);
   List<Property> getAll();
   List<Property> getByGroup(String group);
   List<Property> getPropertyGroupNotIn(List<Integer> id, String group);
   
}