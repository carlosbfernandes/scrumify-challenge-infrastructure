package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.TypeOfWork;

public interface TypeOfWorkService {
   TypeOfWork getById(Integer id);
   List<TypeOfWork> getAll();
   TypeOfWork save(TypeOfWork typeOfWork);
}