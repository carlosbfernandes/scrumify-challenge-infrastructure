package pt.com.scrumify.database.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.CompensationDayRepository;
import pt.com.scrumify.database.repositories.TimesheetDayRepository;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class CompensationDayServiceImpl implements CompensationDayService {
   
   @Autowired
   private CompensationDayRepository repository;
   @Autowired
   private TimeService timesService;  
   @Autowired
   private TimesheetDayRepository timesheetDaysRepository;

   @Override
   public CompensationDay getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public void save(CompensationDay compensationDay) {
      compensationDay.setDay(timesService.getbyDate(compensationDay.getDay().getDate()));
      
      if(compensationDay.getId() == 0 || (repository.getOne(compensationDay.getId()).getDay() == compensationDay.getDay() || repository.findByDay(compensationDay.getDay()) == null)){
         repository.save(compensationDay);
      }
   }

   @Override
   public List<CompensationDay> getAll() {
      return repository.findAll();
   }
   
   @Override
   public Map<Integer, List<CompensationDay>> getAllInMap() {
      List<CompensationDay> compensatioDays= repository.findAllByOrderByDayDateAsc();
      Map<Integer, List<CompensationDay>> hashcompensatioDays = new HashMap<>();
      for(CompensationDay compensatioDay : compensatioDays){
            if(hashcompensatioDays.containsKey(compensatioDay.getDay().getYear().getId())){
               hashcompensatioDays.get(compensatioDay.getDay().getYear().getId()).add(compensatioDay);
            }else{ 
               List<CompensationDay> lista = new ArrayList<>();
               lista.add(compensatioDay);
               hashcompensatioDays.put(compensatioDay.getDay().getYear().getId(), lista);
            }
         }
      TreeMap<Integer, List<CompensationDay>> sorted = new TreeMap<>(); 
      sorted.putAll(hashcompensatioDays); 
      
      return sorted;
   }
   
   @Override
   public List<CompensationDay> getByYearAndMonth(Year year, Integer month) {
      return repository.findByDayYearAndDayMonth(year, month);
   }
   
   @Override
   public void delete(CompensationDay compensationDay){
      TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(compensationDay.getDay(), ResourcesHelper.getResource());
      if(timesheetDay != null){
         timesheetDay.setHoursCompensationDay(0);
         timesheetDaysRepository.save(timesheetDay);
      }
       repository.delete(compensationDay);
   }

   
   @Override
   public List<CompensationDay> listdays(CompensationDay compensationDay) {
      List<CompensationDay> compensationDays = new ArrayList<>();
      List<Time> days = timesService.getBetweenDates(compensationDay.getStartDay() , compensationDay.getEndDay());
      for (Time day : days) {
         if(!day.isCompensationDay() && !day.isHoliday() && day.getDayOfWeek()<=5 ){
            CompensationDay temp = new CompensationDay();
            temp.setDay(day);
            temp.setName(compensationDay.getName());
            compensationDays.add(temp);       
         }
      }      
      return compensationDays;
   }
   
   @Override
   public void saveAll(List<CompensationDay> compensations) {
      for(CompensationDay compensation : compensations){
         TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(compensation.getDay(), ResourcesHelper.getResource());
         if(timesheetDay != null){
            timesheetDay.setHoursCompensationDay(compensation.getDay().getDayOfWeek() <= 4 ? 9 : 7);
            timesheetDaysRepository.save(timesheetDay);
         }
         if(repository.findByDay(compensation.getDay()) == null){
            repository.save(compensation);
         }
      }
   }

   @Override
   public Long getResourceHoursByTimesheet(Integer year, Integer month, Integer fortnight, Schedule schedule) {
      Long hours = repository.getHours(year, month, fortnight, schedule);
      return hours != null ? hours : 0 ;
   }   
}