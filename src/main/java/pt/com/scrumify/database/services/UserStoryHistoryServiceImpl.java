package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.UserStoryHistory;
import pt.com.scrumify.database.repositories.UserStoryHistoryRepository;

@Service
public class UserStoryHistoryServiceImpl implements UserStoryHistoryService {

   @Autowired
   private UserStoryHistoryRepository repository;
   

   @Override
   public UserStoryHistory save(UserStoryHistory userStoryHistory) {
      return repository.saveAndFlush(userStoryHistory);
   }
   
   @Override
   public List<UserStoryHistory> getUserStoryHistorybyUserStory(Integer iduserStory) {
      return repository.findByUserStoryIdOrderByLastUpdateDesc(iduserStory);
   }
   

}