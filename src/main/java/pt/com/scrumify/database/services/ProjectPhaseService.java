package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.ProjectPhase;

public interface ProjectPhaseService {
   ProjectPhase getOne(Integer id);
   List<ProjectPhase> getAll();
   List<ProjectPhase> getByTechnicalAssistance(boolean technicalAssistance);
}