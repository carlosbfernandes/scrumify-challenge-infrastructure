package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ContractProperty;
import pt.com.scrumify.database.repositories.ContractPropertyRepository;

@Service
public class ContractPropertyServiceImpl implements ContractPropertyService {
   @Autowired
   private ContractPropertyRepository contractsPropertiesRepository;

   @Override
   public ContractProperty getOne(int idContract, int idProperty){
      return contractsPropertiesRepository.findByPkContractIdAndPkPropertyId(idContract, idProperty);
   }
   
   @Override
   public ContractProperty getByPropertyNameAndContract(Integer contract, String property){
      return contractsPropertiesRepository.findByPkPropertyNameAndPkContractId(property, contract);
   }
   
   @Override
   public void  delete(ContractProperty property){
       contractsPropertiesRepository.delete(property);
   }

   @Override
   public ContractProperty save(ContractProperty property) {
      return contractsPropertiesRepository.save(property);
   }
}