package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Year;

public interface YearService {
   Year getOne(Integer id);
   Year save(Year year);
	
   List<Year> getAll();
   List<Year> getAllActive();
   List<Year> getByHoldiays();
   List<Year> getByCompensationDays();
   
   boolean yearsExist(List<Integer> years);
   Year getFirstYear();
   Year getFirstActiveYear();
   Year getLastYear();
   Year getLastActiveYear();   
}