package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.ContractStatus;


public interface ContractStatusService {
   List<ContractStatus> getAllStatus();
   List<ContractStatus> getByIds(int[] ids);

}