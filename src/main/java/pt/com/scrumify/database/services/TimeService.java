package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;

public interface TimeService {
   Time getOne(String id);
   Time getbyDate(Date date);
   Time getToday();   
   Time save(Time time);
	
   List<Time> getBetween(Time di, Time df);
   
   List<Time> getAll();
   List<Time> getByYear(Integer year);
   List<Time> getMonths();
   List<Time> getYears();
   List<Time> findByYearAndMonth(Year year, Integer month);
   List<Time> getBetweenDates(Date startDay,Date endDay);

}