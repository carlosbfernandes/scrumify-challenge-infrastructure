package pt.com.scrumify.database.services;

import java.util.List;
import java.util.Map;

import pt.com.scrumify.database.entities.Holiday;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Year;

public interface HolidayService {
   Holiday getOne(Integer id);
   Holiday save(Holiday holiday);
	
   List<Holiday> getAll();
   List<Holiday> getByYearAndMonth(Year year, Integer month);
   void delete(Holiday holiday);
   Map<Integer, List<Holiday>> getAllInMap();
   
   Long getResourceHoursByTimesheet(Integer year, Integer month, Integer fortnight, Schedule schedule);
}