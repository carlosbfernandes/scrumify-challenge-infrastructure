package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ScheduleSetting;
import pt.com.scrumify.database.repositories.ScheduleSettingRepository;

@Service
public class ScheduleSettingServiceImpl implements ScheduleSettingService {
   
   @Autowired
   private ScheduleSettingRepository repository;
   @Autowired
   private TimeService timesService;  

   @Override
   public ScheduleSetting getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public ScheduleSetting save(ScheduleSetting setting) {
      
      setting.setStartingDay(timesService.getbyDate(setting.getStartingDay().getDate()));
      setting.setEndingDay(timesService.getbyDate(setting.getEndingDay().getDate()));
      
      return repository.save(setting);
   }

   @Override
   public void delete(ScheduleSetting setting) {
      repository.delete(setting);
   }

   @Override
   public List<ScheduleSetting> getAll() {
      return repository.findAll();
   }   
}