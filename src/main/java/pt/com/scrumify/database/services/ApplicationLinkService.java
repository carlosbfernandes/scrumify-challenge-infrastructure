package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.ApplicationLink;

public interface ApplicationLinkService {
   void delete(ApplicationLink link);
   List<ApplicationLink> getByApplicationAndEnvironment(int application, int environment);
   ApplicationLink getOne(int link, int application, int environment);
   ApplicationLink save(ApplicationLink link);
}