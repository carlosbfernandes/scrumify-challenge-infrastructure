package pt.com.scrumify.database.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.repositories.EpicRepository;
import pt.com.scrumify.database.repositories.WorkItemStatusRepository;
import pt.com.scrumify.database.repositories.TypeOfWorkItemRepository;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;

@Service
public class EpicServiceImpl implements EpicService {
   @Autowired
   private EpicRepository repository;
   @Autowired
   private WorkItemStatusRepository statusRepository;
   @Autowired
   private TypeOfWorkItemRepository typeRepository;   
   @Autowired
   private UserStoryService userstoriesService; 
   @Autowired
   private WorkItemService workitemsService;

   @Override
   public Epic getOne(Integer id) {
      Epic epic = repository.getOne(id);

      if(!epic.getUserStories().isEmpty()) {
         Long timeSpent = epic.getUserStories().stream().flatMap(t -> t.getWorkItems().stream()).flatMap(t -> t.getWorkLogs().stream()).mapToLong(WorkItemWorkLog::getTimeSpent).sum();
         epic.setHours(timeSpent.intValue());
         
         Long storyPoints = epic.getUserStories().stream().mapToLong(UserStory::getEstimate).sum();            
         epic.setStoryPoints(closestValue (ConstantsHelper.STORYPOINTS, storyPoints ==  0? 0:(int) Math.ceil((double)storyPoints.intValue() / epic.getUserStories().size())));
         
         for(UserStory userstory : epic.getUserStories()){
            Long hoursSpentUserStory = userstory.getWorkItems().stream().flatMap(t -> t.getWorkLogs().stream()).mapToLong(WorkItemWorkLog::getTimeSpent).sum();
            userstory.setHours(hoursSpentUserStory.intValue());
         }
      }      
      
      return epic;
   }
   
   public int closestValue (List<Integer> list, int value) {
      int index = Collections.binarySearch(list,value);
      if(index < 0) {
          index = ~index;
      }
      return list.get(index);
  }

   @Override
   public Epic save(Epic epic) {
      if(epic.getId() == 0){
         epic.setType(typeRepository.findByName("Epic"));
         epic.setNumber(formatEpicNumber(epic));
         epic.setStatus(statusRepository.findByTypeOfWorkItem(epic.getType().getId()));
      }
      return repository.save(epic);

   }
   
   @Override
   public Epic saveTeam(Epic epic , Team team) {
      return save(epic);
   }
   
   @Override
   public void updateTeam(Epic epicsaved){
      for(UserStory userstory : epicsaved.getUserStories()){
         userstory.setTeam(epicsaved.getTeam());
         userstoriesService.save(userstory);
         if(!userstory.getWorkItems().isEmpty()){
            for(WorkItem workitem : userstory.getWorkItems()){
               workitem.setTeam(epicsaved.getTeam());
               workitemsService.save(workitem);
            }
         }
      }
   }

   @Override
   public List<Epic> getAll() {
      return repository.findAll();
   }
   
   
   @Override
   public WorkItemStatus getOneStatus(Integer id) {
      return statusRepository.getOne(id);
   }
   
   @Override
   public List<Epic> getAllExceptPresent(Epic epic) {
      return repository.findByIdNotIn(new ArrayList<>(Arrays.asList(epic.getId())));
   }
   
   @Override
   public String formatEpicNumber(Epic epic) {
      String format = "%06d";
      return epic.getType().getAbbreviation().substring(0, 3) +
             DatesHelper.year().toString().substring(2, 4) + 
             String.format(format, repository.count() + 1);
   }
   
   @Override
   public List<Epic> getAllByResource(Resource resource) {
      return repository.findByTeamMembersPkResource(resource);
   }
   
   @Override
   public  List<Epic> getToReport(ReportView reportView){
      return repository.findbyTeamAndAreaAndYear(reportView.getTeam(), reportView.getYear(), reportView.getArea());
   }
   
   @Override
   public List<Epic> getByTeam(Team team){
      return repository.findByTeamOrderByName(team);
   }
   
   @Override
   public List<Epic> getByTeams(List<Team> teams){
      return repository.findByTeams(teams);
   }
   
   @Override
   public List<Epic> getByStatusesAndResource(Resource resource, List<Integer> statuses){
      return repository.findByStatusAndResource(resource, statuses);
   }

   @Override
   public List<Epic> getByTeamId(Integer team) {
      return repository.findByTeamId(team);
   }

   @Override
   public List<Epic> getByTeamAndNotFinalStatus(Team team) {
      return repository.getByTeamAndNotFinalStatus(team);
   }

   @Override
   public List<Epic> findEpicsWithoutUserStories(Team team) {
      return repository.getWithoutWorkItems(team);
   }
}