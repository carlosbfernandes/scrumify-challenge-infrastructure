package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.UserStoryHistory;

public interface UserStoryHistoryService {
	
   UserStoryHistory save(UserStoryHistory userStoryHistory);
   List<UserStoryHistory> getUserStoryHistorybyUserStory(Integer iduserstory);
  
}