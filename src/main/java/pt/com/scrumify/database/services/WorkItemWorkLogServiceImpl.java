package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.WorkItemWorkLogRepository;

@Service
public class WorkItemWorkLogServiceImpl implements WorkItemWorkLogService {
	@Autowired
	private WorkItemWorkLogRepository workItemsWorkLogRepository;

	@Override
	public WorkItemWorkLog getOne(Integer id) {
		return workItemsWorkLogRepository.getOne(id);
	}
	
   @Override
   public WorkItemWorkLog getByIdAndResource(Integer id, Resource resource) {
      return workItemsWorkLogRepository.findByIdAndCreatedBy(id, resource);
   }

	@Override
	public WorkItemWorkLog save(WorkItemWorkLog entity) {
	   /*
       * TODO: clean code
       */
//	   WorkItemWorkLog worklog = workItemWorkLogView.translate();
//	   Resource resource = ResourcesHelper.getResource();
//	   worklog.setDay(timesService.getOne(timesHelper.getId(worklog.getDay().getDate())));
//	   Integer timespentOld = 0;
//	   TimesheetDay oldtimesheetDay =null;
//	   if(worklog.getId() != 0){
//	      WorkItemWorkLog oldworklog =  workItemsWorkLogRepository.getOne(worklog.getId());
//	       timespentOld = oldworklog.getTimeSpent();
//	       Time oldDay = oldworklog.getDay();
//	       oldtimesheetDay = timesheetDaysRepository.findByDate(oldDay, resource);
//	   }
//	   
//	   TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(worklog.getDay(), resource);
//
//	   if(timesheetDay != null){
//	      timesheetDay.setTimespent(oldtimesheetDay != timesheetDay ? timesheetDay.getTimespent() + worklog.getTimeSpent() : timesheetDay.getTimespent() + worklog.getTimeSpent() - timespentOld );
//	      timesheetDaysRepository.save(timesheetDay);
//	      if(oldtimesheetDay != null && timesheetDay != oldtimesheetDay){
//	         oldtimesheetDay.setTimespent(oldtimesheetDay.getTimespent() - timespentOld);
//	         timesheetDaysRepository.save(oldtimesheetDay);
//	         
//	         
//	      }
//	      worklog.setTimesheet(timesheetDay.getTimesheet());
//	   }
//	   else {
//	      
//	      timesheetHelper.createNewTimesheets1(resource, worklog.getDay().getYear(), worklog.getDay().getMonth());
//	      worklog.setTimesheet(timesheetRepository.findByFortnight(resource, worklog.getDay().getYear().getId(), worklog.getDay().getMonth(), worklog.getDay().getFortnight()));
//	      TimesheetDay day = timesheetDaysRepository.findByDate(worklog.getDay(), resource);
//	      day.setTimespent(worklog.getTimeSpent());
//	      timesheetDaysRepository.save(day);
//	   }
		return workItemsWorkLogRepository.save(entity);
	}
	
   @Override
   public void saveAll(List<WorkItemWorkLog> workLogs) {
      workItemsWorkLogRepository.saveAll(workLogs);
   }

	@Override
	public List<WorkItemWorkLog> getAll() {
		return workItemsWorkLogRepository.findAll();
	}
	
	@Override
   public List<WorkItemWorkLog> getByContract(Contract contract) {
      return workItemsWorkLogRepository.findByWorkItemContract(contract);
   }
   
   @Override
   public List<WorkItemWorkLog> getByResource(Resource resource) {
      return workItemsWorkLogRepository.findByCreatedBy(resource);
   }
	
	@Override
   public List<WorkItemWorkLog> getByResourceAndContract(Resource resource, Contract contract) {
      return workItemsWorkLogRepository.findByCreatedByAndWorkItemContract(resource, contract);
   }
	
   @Override
   public void delete(WorkItemWorkLog workItemWorkLog) {
      /*
       * TODO: clean code
       */
//      TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(workItemWorkLog.getDay(), workItemWorkLog.getCreatedBy());
//
//      if(timesheetDay != null){
//         timesheetDay.setTimespent( timesheetDay.getTimespent() - workItemWorkLog.getTimeSpent() );
//         timesheetDaysRepository.save(timesheetDay);         
//      }
//      workItemsWorkLogRepository.deleteById(workItemWorkLog.getId());
      workItemsWorkLogRepository.delete(workItemWorkLog);
   }
   
   @Override
   public List<WorkItemWorkLog> getByYearAndMonthAndResourceAndFortnighOrderByDate(Year year, Integer month, Resource resource, Integer fortnight) {
      return workItemsWorkLogRepository.getByYearAndMonthAndResourceAndFortnightOrderByDate(year, month, resource, fortnight);
   }
   
   @Override
   public List<WorkItemWorkLog> getByYearAndMonthAndResourcesAndFortnighOrderByDate(Year year, Integer month, List<Resource> resources, Integer fortnight) {
      return workItemsWorkLogRepository.getByYearAndMonthAndResourcesAndFortnightOrderByDate(year, month, resources, fortnight);
   }
   
   @Override
   public List<WorkItemWorkLog> getByYearAndMonthAndResourcesAndFortnighAndContractOrderByDate(Year year, Integer month, Resource resource, Integer fortnight, Contract contract) {
      return workItemsWorkLogRepository.getByYearAndMonthAndResourcesAndFortnightAndContractOrderByDate(year, month, resource, fortnight, contract);
   }
   
   @Override
   public List<WorkItemWorkLog> getByYearAndMonthAndResourceOrderByDate(Year year, Integer month, Resource resource) {
      return workItemsWorkLogRepository.getByYearAndMonthAndResourceOrderByDate(year, month, resource);
   }

   @Override
   public int getTimeSpentDuringInterval(Contract contract, Date start, Date end) {
      Integer result = workItemsWorkLogRepository.getHoursSpentDuringInterval(contract, start, end); 
      return result != null ? result : 0;
   }
   
   @Override
   public int getTimespentOnWorkitemAndNotWorklog(WorkItem workItem, Integer worklog) {
      Integer result = workItemsWorkLogRepository.getHoursSpentOnWorkitemAndNotWorklog(workItem, worklog); 
      return result != null ? result : 0;
   }
}