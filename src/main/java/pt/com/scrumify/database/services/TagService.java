package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Tag;

public interface TagService {
   Tag getOne(Integer id);
   Tag save(Tag tag);
   List<Tag> getAll(List<SubArea> subAreas);
   void delete(Integer id);   
}