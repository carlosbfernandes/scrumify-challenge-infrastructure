package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SprintMember;
import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.SprintMemberPK;
import pt.com.scrumify.database.repositories.SprintMemberRepository;


@Service
public class SprintMemberServiceImpl implements SprintMemberService {
   @Autowired
   private SprintMemberRepository repository;

   @Override
   public void delete(SprintMember member) {
      this.repository.delete(member);
   }

   @Override
   public SprintMember getOne(SprintMemberPK pk) {
      return this.repository.getOne(pk);
   }

   @Override
   public List<SprintMember> getBySprint(Sprint sprint) {
      return this.repository.getBySprint(sprint);
   }
   
   @Override
   public SprintMember save(SprintMember sprintMember) {
      return this.repository.save(sprintMember);
   }
   
   @Override
   public List<SprintMember> getByResource(Resource resource) {
      return repository.findByPkResource(resource);
   }

}