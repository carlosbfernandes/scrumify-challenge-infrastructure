package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.DateUtils;

import pt.com.scrumify.database.entities.EpicNote;
import pt.com.scrumify.database.repositories.EpicNoteRepository;

@Service
public class EpicNoteServiceImpl implements EpicNoteService {
   @Autowired
   private EpicNoteRepository repository;

   @Override
   public EpicNote getById(Integer id) {
      return this.repository.getOne(id);
   }
   
   @Override
   public EpicNote save(EpicNote epicNote) {
      epicNote.setCreated(DateUtils.createNow().getTime());
      return this.repository.save(epicNote);
   }
}