package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.repositories.TypeOfNoteRepository;

@Service
public class TypeOfWorkItemNoteServiceImpl implements TypeOfWorkItemNoteService {	
   @Autowired
   private TypeOfNoteRepository repository;
           
   @Override
   public TypeOfNote getById(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public TypeOfNote save(TypeOfNote typeOfWorkItemNote) {
      return repository.save(typeOfWorkItemNote);
   }

   @Override
   public List<TypeOfNote> getAll() {
      return repository.findAll();
   }
   
   @Override
   public List<TypeOfNote> getByTypeOfWorkItem(TypeOfWorkItem typeOfWorkItem) {
      return repository.getByTypeOfWorkItem(typeOfWorkItem);
   }
   
}