package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.ContractStatus;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.ContractRepository;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class ContractServiceImpl implements ContractService {
   @Autowired
   private ContractRepository contractRepository;
   @Autowired
   private AreaService areasService;
   
   @Override
   public Optional<Contract> findById(Integer id) {
      return this.contractRepository.findById(id);
   }
   
   @Override
   public Contract getOne(Integer id) {
      return this.contractRepository.getOne(id);
   }
   
   @Override
   public Contract getByCode(String code) {
      return this.contractRepository.findDistinctByCode(code);
   }
   
   @Override
   public Contract getByPropertyNameAndValue(String propertyName, String value) {
      return this.contractRepository.findByPropertyNameAndValue(propertyName, value);
   }
   
   @Override
   public Contract getByTwoPropertyNameAndValue(String propertyName1, String value1, String propertyName2, String value2) {
      return this.contractRepository.findByTwoPropertyNameAndValue(propertyName1, value1, propertyName2, value2);
   }

   @Override
   public Contract save(Contract contract) {
      return this.contractRepository.save(contract);
   }
   
   @Override
   public List<Contract> findByContractStatus(Integer status) {
      return contractRepository.getByResourceAndStatus(ResourcesHelper.getResource(), status);
   }
   
   @Override
   public List<Contract> getAll() {
      return this.contractRepository.findAll();
   }
   
   @Override
   public List<Contract> getByResource(Resource resource){
      List<Contract> contracts = contractRepository.getByResourceWithoutOpportunities(resource);
      return contracts;
   }
   
   @Override
   public List<Contract> getContractsByResource(Resource resource){
      List<Contract> contracts = contractRepository.getByRes(resource);
      getTimeSpent(contracts);
      return contracts;
   }

   @Override
   public List<Contract> getOpenContractsByResource(Resource resource){
      List<Contract>  contracts = contractRepository.getByResAndStatus(resource, ContractStatus.OPEN);
      getTimeSpent(contracts);
      return contracts;
   }
   
   @Override
   public List<Contract> getContractsInStatusIdsResource(Resource resource, List<Integer> status){
      List<Contract>  contracts = contractRepository.getByResInStatus(resource, status);
      getTimeSpent(contracts);
      return contracts;
   }
   
   @Override
   public List<Contract> getContractsInStatusResource(Resource resource, List<ContractStatus> statuses){
      List<Contract>  contracts = contractRepository.getByResInAndStatuses(resource, statuses);
      getTimeSpent(contracts);
      return contracts;
   }
   
   @Override
   public List<Contract> getByArea(Area area) {
      return this.contractRepository.getByArea(area);
   }
   
   @Override
   public List<Contract> getByTeam(Team team) {
      return this.contractRepository.getByTeam(team);
   }
   
   @Override
   public List<Contract> getByTeamAndStatus(Team team, Integer status) {
      return this.contractRepository.findByTeamContractsPkTeamAndStatusId(team, status);
   }
   
   @Override
   public List<Contract> getBySubarea(SubArea subArea) {
      return this.contractRepository.getBySubarea(subArea);
   }
   
   @Override
   public List<Contract> getBySubareaAndResource(SubArea subArea, Resource resource) {
      return this.contractRepository.getBySubareaAndResource(subArea, resource);
   }
   

   @Override
   public List<Contract> getBySubareaAndStatus(SubArea subArea, Integer status) {
      return this.contractRepository.getBySubareaAndStatus(subArea, status);
   }
   
   @Override
   public List<Contract> getContractsbyYearAndAreaAndMonth(Year year, Area area, Integer month){
      if(area == null){
         return contractRepository.findDistinctBySubAreaAreaInAndWorkItemsWorkLogsDayYear(areasService.getByResource(ResourcesHelper.getResource()), year);
      }else{
         return contractRepository.findDistinctBySubAreaAreaAndWorkItemsWorkLogsDayYear(area, year);
      }
   }

   @Override
   public List<Contract> getContractsWorkedOnThisFortnight(Resource resource, Date start, Date end) {
      return contractRepository.findDistinctByWorkItemsWorkLogsCreatedByAndWorkItemsWorkLogsDayDateBetween(resource, start, end);
   }
   
   @Override
   public List<Contract> getContractsMyTeamsWorkedOnYear(List<Team> teams, Year year) {
      return contractRepository.findDistinctByTeamsAndWorkItemsWorkLogsDayYearId(teams, year);
   }
   
   private void getTimeSpent(List<Contract> contracts) {
      for(Contract contract : contracts){
         if(!contract.getWorkItems().isEmpty()){
            
            Long hoursSpent = contract.getWorkItems().stream().flatMap(t -> t.getWorkLogs().stream()).mapToLong(WorkItemWorkLog::getTimeSpent).sum();
            contract.setTimeSpent(hoursSpent != null ? hoursSpent.intValue() : 0);
            Integer percentage = Math.round(((float)contract.getTimeSpent()/(float)contract.getHours() )*100);
            contract.setPercentageCompleted(percentage <= 100 ? percentage: 100);
         }else{
            contract.setTimeSpent(0);
            contract.setPercentageCompleted(0);
         }
      }
   }
}