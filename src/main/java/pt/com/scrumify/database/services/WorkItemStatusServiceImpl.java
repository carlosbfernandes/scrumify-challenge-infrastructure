package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Workflow;
import pt.com.scrumify.database.repositories.WorkItemStatusRepository;

@Service
public class WorkItemStatusServiceImpl implements WorkItemStatusService {

   @Autowired
   private WorkItemStatusRepository repository;

   @Override
   public WorkItemStatus getOne(Integer id) {
      return repository.getOne(id);
   }
   
   @Override
   public WorkItemStatus getOneByTypeOfWorkitem(Integer id) {
      return repository.findByTypeOfWorkItem(id);
   }
   
   @Override
   public List<WorkItemStatus> getOneByTypeOfWorkitemById(Integer id) {
      return repository.findByTypeOfWorkItemById(id);
   }
   
   @Override
   public List<WorkItemStatus> getByTypeId(Integer id) {
      return repository.findByTypeIdAndIsWorkItem(id, false);
   }

   @Override
   public List<WorkItemStatus> getAll() {
      return repository.findAll();
   }

   public List<WorkItemStatus> getByWorkflow(Workflow workFlow, WorkItemStatus itemStatus) {
      return repository.getByWorkflow(workFlow, itemStatus);
   }

   @Override
   public List<WorkItemStatus> getByIds(List<Integer> status) {
      return repository.getByIds(status);
   }
}