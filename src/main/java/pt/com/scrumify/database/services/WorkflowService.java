package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Workflow;
import pt.com.scrumify.database.entities.WorkflowTransition;

public interface WorkflowService {
   Workflow getOne(Integer id);
   Workflow save(Workflow workFlow);
   List<Workflow> getAll();
   
   WorkflowTransition saveTransition(WorkflowTransition transition);
}