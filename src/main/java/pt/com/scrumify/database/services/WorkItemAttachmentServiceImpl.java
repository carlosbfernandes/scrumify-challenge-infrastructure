package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.WorkItemAttachment;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.repositories.WorkItemAttachmentRepository;

@Service
public class WorkItemAttachmentServiceImpl implements WorkItemAttachmentService {
   
   @Autowired
   private WorkItemAttachmentRepository repository;

   @Override
   public WorkItemAttachment getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public List<WorkItemAttachment> getAll() {
      return repository.findAll();
   }

   @Override
   public List<WorkItemAttachment> getByWorkItem(WorkItem workitem) {
      return repository.findByWorkItem(workitem);
   }
   
   @Override
   public List<WorkItemAttachment> getByWorkItemId(Integer workItemId) {
      return repository.findByWorkItemId(workItemId);
   }  

   @Override
   public WorkItemAttachment save(WorkItemAttachment attachment) {
      return repository.save(attachment);
   }

   @Override
   public void delete(WorkItemAttachment attachment) {
      repository.delete(attachment);      
   }

 
}