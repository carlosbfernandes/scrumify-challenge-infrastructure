package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ProjectPhase;
import pt.com.scrumify.database.repositories.ProjectPhaseRepository;

@Service
public class ProjectPhaseServiceImpl implements ProjectPhaseService {
   @Autowired
   private ProjectPhaseRepository repository;

   @Override
   public ProjectPhase getOne(Integer id) {
      return this.repository.getOne(id);
   }

   @Override
   public List<ProjectPhase> getAll() {
      return this.repository.getAll();
   }

   @Override
   public List<ProjectPhase> getByTechnicalAssistance(boolean technicalAssistance) {
      return this.repository.findByTechnicalAssistance(technicalAssistance);
   }
}