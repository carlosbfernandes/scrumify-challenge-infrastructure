package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.DateUtils;

import pt.com.scrumify.database.entities.UserStoryNote;
import pt.com.scrumify.database.repositories.UserStoryNoteRepository;

@Service
public class UserStoryNoteServiceImpl implements UserStoryNoteService {
   @Autowired
   private UserStoryNoteRepository repository;

   @Override
   public UserStoryNote getById(Integer id) {
      return this.repository.getOne(id);
   }
   
   @Override
   public UserStoryNote save(UserStoryNote userstoryNote) {
      userstoryNote.setCreated(DateUtils.createNow().getTime());
      return this.repository.save(userstoryNote);
   }
}