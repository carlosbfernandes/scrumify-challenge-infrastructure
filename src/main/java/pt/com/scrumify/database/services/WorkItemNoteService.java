package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.entities.ReportView;

public interface WorkItemNoteService {
   WorkItemNote getById(Integer id);
   WorkItemNote save(WorkItemNote workItemNote);
   List<WorkItemNote> getStatusReports(Contract contract, Integer noteid, ReportView report);
   List<WorkItemNote> getStatusReportsByResource(Contract contract, Integer noteid, ReportView resource);
}