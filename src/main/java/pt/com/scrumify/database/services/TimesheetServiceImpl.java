package pt.com.scrumify.database.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.TimesheetRepository;
import pt.com.scrumify.entities.TimeReportView;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class TimesheetServiceImpl implements TimesheetService {   
   @Autowired
   private TimesheetRepository repository;

   @Override
   public Timesheet getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public void delete(Integer id) {
      repository.deleteById(id);      
   }

   @Override
   public Timesheet save(Timesheet timesheet) {
      return repository.save(timesheet);
   }

   @Override
   public List<Timesheet> getAll() {
      return repository.findAll();
   }

   @Override
   public List<Timesheet> getByResourcesYearMonthAndFortnight(List<Resource> resources, Integer year, Integer month, Integer fortnight) {
      return repository.getByResourcesYearMonthAndFortnight(resources, year, month, fortnight);
   }
   
   @Override
   public List<Timesheet> getSheets(Resource resource, Year year, Integer month) {
      return this.repository.findByYearAndMonth(resource, year, month);
   }
   
   @Override
   public List<Timesheet> getSheetsWithComments(Resource resource, Year year, Integer month) {
      return this.repository.findByYearMonthAndComment(resource, year, month);
   }

   @Override
   public Timesheet getSheet(Resource resource, Integer year, Integer month, Integer day) {
      return this.repository.findByToday(resource, year, month, day);
   }

   @Override
   public Timesheet getSheetByFortnight(Resource resource, Integer year, Integer month, Integer fortnight) {
      return this.repository.findByFortnight(resource, year, month, fortnight);
   }

   @Override
   public List<TimeReportView> getReportOfTimeSheet(Resource resource, Time startDate, Time endDate) {
      return this.repository.findWorkLogsOfTimeSheet(resource, startDate, endDate);
   }

   @Override
   public List<Timesheet> getSheetsByYear(Year year) {
      return repository.findByYear(year);
   }

   @Override
   public List<Timesheet> getSheetsofTeams(List<Team> teams, Integer month, Integer year, Integer fortnight) {
      return repository.findDistinctByResourceTeamMembersPkTeamInAndStartingDayMonthAndStartingDayYearIdAndStartingDayFortnightAndReviewedFalse(teams, month, year, fortnight);
   }

   @Override
   public  Date dateLastTimeSheetApprovedorReviewed(Resource resource) {
      Date approvedDate = repository.findTimeofLateTimeSheetAprrovedOrReviewed(resource); 
      if (approvedDate != null) {
         Calendar cal = Calendar.getInstance();
         cal.setTime(approvedDate);
         cal.add(Calendar.DATE, 1);
         approvedDate = cal.getTime();
      }
      
      return approvedDate;
   }

   @Override
   public Timesheet approve(Integer id) {
      Timesheet timesheet = repository.getOne(id);
      timesheet.setApproved(true);
      timesheet.setApprovalDate(DatesHelper.now());
      timesheet.setApprovedBy(ResourcesHelper.getResource());
      
      return save(timesheet);
   }

   @Override
   public List<Timesheet> getByResources(List<Resource> resources) {
      return repository.findByResourceIn(resources);
   }

   @Override
   public boolean checkTimesheetSubmitted(Date date) {
      Resource resource = ResourcesHelper.getResource();
      
      List<Timesheet> timesheets = repository.findTimesheetSubmittedForDay(resource , date);
      
      return timesheets != null && !timesheets.isEmpty();
   }
}