package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Priority;

public interface PriorityService {
   Priority getOne(Integer id);
   List<Priority> getAll();
}