package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.ContractStatus;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Year;


public interface ContractService {
   Optional<Contract> findById(Integer id);
   Contract getOne(Integer id);
   Contract getByCode(String code);
   Contract getByPropertyNameAndValue(String propertyName, String value);
   Contract getByTwoPropertyNameAndValue(String propertyName1, String value1, String propertyName2, String value2);
   List<Contract> getAll();
   Contract save(Contract contract);
   List<Contract> findByContractStatus(Integer status);
   List<Contract> getByArea(Area area);
   List<Contract> getByTeam(Team team);
   List<Contract> getByTeamAndStatus(Team team, Integer status);
   List<Contract> getBySubarea(SubArea subArea);
   List<Contract> getBySubareaAndStatus(SubArea subArea, Integer status);
   List<Contract> getContractsByResource(Resource resource);
   List<Contract> getByResource(Resource resource);
   List<Contract> getContractsbyYearAndAreaAndMonth(Year year, Area area, Integer month);
   List<Contract> getOpenContractsByResource(Resource resource);
   List<Contract> getContractsInStatusIdsResource(Resource resource, List<Integer> status);
   List<Contract> getContractsInStatusResource(Resource resource, List<ContractStatus> statuses);   
   List<Contract> getBySubareaAndResource(SubArea subArea, Resource resource);   
   List<Contract> getContractsMyTeamsWorkedOnYear(List<Team> teams, Year year);
   List<Contract> getContractsWorkedOnThisFortnight(Resource resource, Date start, Date end);
   
}