package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.ApplicationProperty;

public interface ApplicationPropertyService {
   void delete(ApplicationProperty property);
   List<ApplicationProperty> getByApplicationAndEnvironment(int application, int environment);
   ApplicationProperty getOne(int property, int application, int environment);
   ApplicationProperty save (ApplicationProperty property);
}