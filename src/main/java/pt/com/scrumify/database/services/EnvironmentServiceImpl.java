package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Environment;
import pt.com.scrumify.database.repositories.EnvironmentRepository;

@Service
public class EnvironmentServiceImpl implements EnvironmentService {
   @Autowired
   private EnvironmentRepository environmentsRepository;

   @Override
   public List<Environment> getAll() {
      return this.environmentsRepository.findAll();
   }

   @Override
   public Environment getByName(String environmentName) {
      return this.environmentsRepository.findByNameIgnoreCase(environmentName);
   }

   @Override
   public Environment getOne(int id) {
      return this.environmentsRepository.getOne(id);
   }

   @Override
   public void save(Environment environment) {
      this.environmentsRepository.save(environment);
   }
}