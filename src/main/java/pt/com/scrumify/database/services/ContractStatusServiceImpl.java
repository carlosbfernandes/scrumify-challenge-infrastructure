package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ContractStatus;
import pt.com.scrumify.database.repositories.ContractStatusRepository;



@Service
public class ContractStatusServiceImpl implements ContractStatusService {
   @Autowired
   private ContractStatusRepository repository;

   @Override
   public List<ContractStatus> getAllStatus() {
      return repository.findAll();
   }

   @Override
   public List<ContractStatus> getByIds(int[] ids) {
      return repository.findByIdIn(ids);
   }


}