package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ApplicationDataSource;
import pt.com.scrumify.database.repositories.ApplicationDataSourceRepository;

@Service
public class ApplicationDataSourceServiceImpl implements ApplicationDataSourceService {
   @Autowired
   private ApplicationDataSourceRepository applicationsDataSourcesRepository;
   
   @Override
   public void  delete(ApplicationDataSource datasource){
      applicationsDataSourcesRepository.delete(datasource);
   }
   
   @Override
   public List<ApplicationDataSource> getByApplicationAndEnvironment(int application, int environment){
      return applicationsDataSourcesRepository.findByApplicationIdAndEnvironmentId(application, environment);
   }

   @Override
   public ApplicationDataSource getOne(int datasource, int application, int environment){
      return applicationsDataSourcesRepository.findByIdAndApplicationIdAndEnvironmentId(datasource, application, environment);
   }

   @Override
   public ApplicationDataSource save(ApplicationDataSource datasource) {
      return applicationsDataSourcesRepository.save(datasource);
   }
}