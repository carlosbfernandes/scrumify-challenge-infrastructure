package pt.com.scrumify.database.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Wiki;
import pt.com.scrumify.database.repositories.WikiRepository;

@Service
public class WikiServiceImpl implements WikiService {

   @Autowired
   private WikiRepository repository;

   @Override
   public Wiki getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Wiki save(Wiki wiki) {
      return repository.save(wiki);
   }

   @Override
   public List<Wiki> getAll() {
      return repository.findAll();
   }
   
   @Override
   public List<Wiki> getByResource(Resource resource) {
      List<Wiki> wiki;
      if(resource.getProfile().getRoles().stream().anyMatch(t -> t.getName().equals("WIKIS_UPDATE"))){
         wiki = repository.findDistinctByApplicationSubAreaTeamsMembersPkResourceIn(new ArrayList<>(Arrays.asList(resource)));
         wiki.addAll(repository.findByApplicationNull());
      }else{
         wiki = repository.findDistinctByApplicationSubAreaTeamsMembersPkResourceInAndActiveIsTrue(new ArrayList<>(Arrays.asList(resource)));
         wiki.addAll(repository.findByApplicationNullAndActiveIsTrue());
      }
      return wiki;
   }
   
   @Override
   public void delete(Wiki wiki){
      repository.delete(wiki);
   }
   
}