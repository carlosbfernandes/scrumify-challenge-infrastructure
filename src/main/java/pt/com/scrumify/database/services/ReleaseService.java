package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Release;

public interface ReleaseService {
   Release getOne(int id);
   List<Release> getByApplication(Integer application);
   void delete(Release release);
   void deleteById(Integer id);
   void save(Release release);   
}