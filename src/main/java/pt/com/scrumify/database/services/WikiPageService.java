package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.WikiPage;

public interface WikiPageService {
   WikiPage getOne(Integer id);
   WikiPage save(WikiPage page);
   List<WikiPage> getAll();
   void delete(WikiPage page);
}