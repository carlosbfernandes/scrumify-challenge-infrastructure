package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Wiki;

public interface WikiService {
   Wiki getOne(Integer id);
   Wiki save(Wiki wiki);
   List<Wiki> getAll();
   List<Wiki> getByResource(Resource resource);
   void delete(Wiki wiki);
}