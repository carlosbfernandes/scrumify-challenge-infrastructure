package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Year;

public interface WorkItemService {
   WorkItem getOne(Integer id);
   WorkItem getByNumber(String number);
	WorkItem save(WorkItem workitem);
   
	Integer countAll();
	Integer countByTypeOfWorkitem(TypeOfWorkItem type);
	
   List<WorkItem> getAll();
   List<WorkItem> getByContractAndResource(Contract contract, Resource resource, Date startDay, Date endDay);
   List<WorkItem> getBySubAreasAndTeams(Team team, List<SubArea> subAreas, List<WorkItemStatus> status);
   List<WorkItem> getWorkItemsByUser(Resource resource);
   List<WorkItem> getWorkItemsByTeamId(Integer team);
   List<WorkItem> getWorkItemsByTeam(Team team);
   List<WorkItem> getWorkItemsWithoutUserStoryByTeam(Team team);
   List<WorkItem> getWorkItemsByTeams(List<Team> teams);
   List<WorkItem> getByNotAssigned();
   List<WorkItem> getCreatedByUser(Resource resource);
   List<WorkItem> getWorkItemsCreatedByMeAndAssigendToMyTeams(Resource resource, List<Team> teams);
   List<WorkItem> getWorkItemsNotAssigendByTeam(List<Team> team);
   List<WorkItem> getWorkItemsByUserStory(Integer userStory);
   List<WorkItem> getWorkItemsByTeams(List<Team> teams, Year year);
   List<WorkItem> getWorkItemsByTeamsAndArea(List<Team> teams, Year year, Area area);
   List<WorkItem> getWorkItemsByTeamAndArea(Team team, Year year, Area area);
   List<WorkItem> getWorkItemsByTeamAndAreaAndResource(Team team, Year year, Area area, Resource resource);

   List<WorkItem> getBySameTeamAndStatus(Resource resource, int[] status); 
   
   boolean isCreatedByMe(Resource resource, Integer workitem);
   boolean isAssignedToMe(Resource resource, Integer workitem);
   List<WorkItem> getByStatusInOrTypeIn(List<TypeOfWorkItem> types, Resource resource);
  
}