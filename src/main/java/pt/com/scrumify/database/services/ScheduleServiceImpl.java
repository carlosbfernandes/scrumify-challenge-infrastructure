package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.repositories.ScheduleRepository;

@Service
public class ScheduleServiceImpl implements ScheduleService {
   
   @Autowired
   private ScheduleRepository repository;

   @Override
   public Schedule getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Schedule save(Schedule schedule) {
      return repository.save(schedule);
   }

   @Override
   public List<Schedule> getAll() {
      return repository.findAll();
   }
   
   @Override
   public Schedule getActiveSchedule(Resource resource, Date date) {
      return repository.findScheduleByUserAndDate(resource.getId(), date);
   }

   
}