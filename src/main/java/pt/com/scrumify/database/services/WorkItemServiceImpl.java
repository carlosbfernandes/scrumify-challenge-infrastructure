package pt.com.scrumify.database.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.WorkItemRepository;

@Service
public class WorkItemServiceImpl implements WorkItemService {
   @Autowired
   private WorkItemRepository repository;
   
   @Override
   public WorkItem getOne(Integer id) {
      return repository.getOne(id);
   }
   
   @Override
   public WorkItem getByNumber(String number) {
      return repository.getByNumber(number);
   }

   @Override
   public WorkItem save(WorkItem workitem) {
      return repository.save(workitem);
   }
   
   @Override
   public Integer countAll() {
      return repository.countAll();
   }
   
   @Override
   public Integer countByTypeOfWorkitem(TypeOfWorkItem type) {
      return repository.countByTypeOfWorkItem(type);
   }

   @Override
   public List<WorkItem> getAll() {
      return repository.findAll();
   }
   
   @Override
   public List<WorkItem> getByContractAndResource(Contract contract, Resource resource, Date startDay, Date endDay) {
      if(startDay!=null && endDay!=null)
         return repository.findDistinctByCreatedByAndContractBetweenDates(resource, contract, startDay, endDay);
      else
         return repository.findDistinctByCreatedByAndContract(resource, contract);
   }
   
   @Override
   public List<WorkItem> getWorkItemsByUser(Resource resource) {
      return repository.findByAssignedToAndNotClosed(resource);
   }
   
   @Override
   public List<WorkItem> getWorkItemsByTeamId(Integer team) {
      return repository.findByTeamId(team);
   }
   
   @Override
   public List<WorkItem> getWorkItemsByTeam(Team team) {
      return repository.findByTeam(team);
   }
   
   @Override
   public List<WorkItem> getWorkItemsWithoutUserStoryByTeam(Team team) {
      return repository.getWorkItemsWithoutUserStoryByTeam(team);
   }
   
   @Override
   public List<WorkItem> getWorkItemsByTeams(List<Team> teams) {
      return repository.findByTeams(teams);
   }

   @Override
   public List<WorkItem> getByNotAssigned() {
      return repository.findByAssignedToIsNullAndStatusIdNotIn(new ArrayList<>(Arrays.asList(WorkItemStatus.CLOSED)));
   }

   @Override
   public List<WorkItem> getBySubAreasAndTeams(Team team, List<SubArea> subAreas, List<WorkItemStatus> status) {      
      return repository.getBySubAreasAndTeams(team, subAreas, status);
   }  

   @Override
   public List<WorkItem> getCreatedByUser(Resource resource) {      
      return repository.findByCreatedByAndStatusFinalStatusIsFalse(resource);
   }
   
   @Override
   public List<WorkItem> getWorkItemsCreatedByMeAndAssigendToMyTeams(Resource resource, List<Team> teams) {
      if(teams.isEmpty()) {
         return new ArrayList<>();
      }
      else {
         return repository.getWorkItemsCreatedByMeAndAssigendToMyTeams(resource, teams);
      }
   }
   
   @Override
   public List<WorkItem> getWorkItemsNotAssigendByTeam(List<Team> teams) {
      if(teams.isEmpty()) {
         return new ArrayList<>();
      }
      else {
         return repository.getWorkItemsNotAssigendByTeam(teams);
      }
   }
   
   @Override
   public List<WorkItem> getWorkItemsByUserStory(Integer userStory){
      return repository.findByUserStoryId(userStory);
   }
   
   @Override
   public boolean isCreatedByMe(Resource resource, Integer workitem) {
      return this.repository.isCreatedByMe(resource, workitem);
   }
   
   @Override
   public boolean isAssignedToMe(Resource resource, Integer workitem){
      return this.repository.isAssignedToMe(resource, workitem);
   }
   
   @Override
   public List<WorkItem> getWorkItemsByTeams(List<Team> teams, Year year){
      return repository.findDistinctByTeamInAndWorkLogsDayYear(teams , year);
   }
   
   
   @Override
   public List<WorkItem> getWorkItemsByTeamsAndArea(List<Team> teams, Year year, Area area){
      return repository.findDistinctByTeamInAndWorkLogsDayYearAndTeamSubAreaArea(teams, year, area);
   }
   
   @Override
   public List<WorkItem> getWorkItemsByTeamAndArea(Team team, Year year, Area area){
      return repository.findDistinctByTeamAndWorkLogsDayYearAndTeamSubAreaArea(team, year, area);
   }
   
   @Override
   public List<WorkItem> getWorkItemsByTeamAndAreaAndResource(Team team, Year year, Area area, Resource resource){
      return repository.findDistinctByTeamAndWorkLogsDayYearAndTeamSubAreaAreaAndWorkLogsCreatedBy(team, year, area, resource);
   }

   @Override
   public List<WorkItem> getBySameTeamAndStatus(Resource resource, int[] status) {
      return repository.findBySameTeamAndStatus(resource, status);
   }
   
   @Override
   public List<WorkItem> getByStatusInOrTypeIn(List<TypeOfWorkItem> types, Resource resource) {
      return new ArrayList<>();
   } 
}