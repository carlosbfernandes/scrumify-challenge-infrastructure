package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Profile;

public interface ProfileService {
   Profile getOne(Integer id);
   Profile save(Profile profile);
   List<Profile> getAll();
   List<Profile> getNonSystem();
   List<Profile> getBySystem(boolean system);
}