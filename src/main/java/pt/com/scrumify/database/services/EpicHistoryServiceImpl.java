package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.EpicHistory;
import pt.com.scrumify.database.repositories.EpicHistoryRepository;

@Service
public class EpicHistoryServiceImpl implements EpicHistoryService {

   @Autowired
   private EpicHistoryRepository repository;
   

   @Override
   public EpicHistory save(EpicHistory epicHistory) {
      return repository.saveAndFlush(epicHistory);
   }
   
   @Override
   public List<EpicHistory> getEpicHistorybyEpic(Integer idepic) {
      return repository.findByEpicIdOrderByLastUpdateDesc(idepic);
   }
   

}