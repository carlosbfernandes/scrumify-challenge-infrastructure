package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.TimesheetDayRepository;

@Service
public class TimesheetDayServiceImpl implements TimesheetDayService {
   @Autowired
   private TimesheetDayRepository repository;

   @Override
   public TimesheetDay getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public TimesheetDay getOne(Resource resource, Date date) {
      return repository.getByResourceAndDate(resource, date);
   }

   @Override
   public TimesheetDay save(TimesheetDay timesheetDay) {
      return repository.save(timesheetDay);
   }

   @Override
   public List<TimesheetDay> getAll() {
      return repository.findAll();
   }
   
   @Override
   public List<TimesheetDay> findByYearAndMonthOrderByDate(Year year, Integer month, Resource resource) {
      return repository.findByYearAndMonthOrderByDate(year, month, resource);
   }

   @Override
   public List<TimesheetDay> findByCompensationDay(Year year, Integer month, Integer fortnight, Resource resource) {
      return repository.findByCompensationDay(year, month, fortnight, resource);
   }
}