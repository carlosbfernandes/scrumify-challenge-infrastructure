package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Activity;
import pt.com.scrumify.database.repositories.ActivityRepository;

@Service
public class ActivityServiceImpl implements ActivityService {
   @Autowired
   private ActivityRepository repository;
   
   @Override
   public Activity getOne(Integer id) {
      return this.repository.getOne(id);
   }

   @Override
   public List<Activity> getAll() {
      return this.repository.getAll();
   }
   
   @Override
   @Cacheable(value = "activities", key = "#technicalAssistance")
   public List<Activity> getByTechnicalAssistance(boolean technicalAssistance) {
      return this.repository.findByTechnicalAssistance(technicalAssistance);
   }

   @Override
   @CacheEvict(value = "activities", key = "#activity.projectPhase.technicalAssistance")
   public Activity save(Activity activity) {
      return this.repository.save(activity);
   }
}