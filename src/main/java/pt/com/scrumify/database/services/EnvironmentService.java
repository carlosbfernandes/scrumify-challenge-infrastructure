package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Environment;

public interface EnvironmentService {
   List<Environment> getAll();
   Environment getOne(int id);
   Environment getByName(String environmentName);
   void save(Environment environment);
}