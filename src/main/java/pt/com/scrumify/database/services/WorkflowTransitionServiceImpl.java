package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.WorkflowTransition;
import pt.com.scrumify.database.repositories.WorkflowTransitionRepository;

@Service
public class WorkflowTransitionServiceImpl implements WorkflowTransitionService {

   @Autowired
   
   private WorkflowTransitionRepository repository;
   @Override
   public WorkflowTransition getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public WorkflowTransition save(WorkflowTransition transition) {
      return repository.save(transition);
   }

   @Override
   public List<WorkflowTransition> getAll() {
      return repository.findAll();
   }

   @Override
   public void delete(WorkflowTransition transition) {
      repository.delete(transition);
   }

}