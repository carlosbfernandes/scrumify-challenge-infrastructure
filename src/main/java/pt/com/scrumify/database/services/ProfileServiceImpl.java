package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Profile;
import pt.com.scrumify.database.repositories.ProfileRepository;

@Service
public class ProfileServiceImpl implements ProfileService {
   @Autowired
   private ProfileRepository profilesRepository;

   @Override
   public Profile getOne(Integer id) {
      return this.profilesRepository.getOne(id);
   }

   @Override
   public Profile save(Profile profile) {
      return this.profilesRepository.save(profile);
   }

   @Override
   public List<Profile> getAll() {
      return this.profilesRepository.findAll();
   }

   @Override
   public List<Profile> getBySystem(boolean system) {
      return this.profilesRepository.getBySystem(system);
   }

   @Override
   public List<Profile> getNonSystem() {
      return profilesRepository.findBySystemFalse();
   }
}