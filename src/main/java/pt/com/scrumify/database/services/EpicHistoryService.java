package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.EpicHistory;

public interface EpicHistoryService {
	
   EpicHistory  save(EpicHistory epicHistory);
   List<EpicHistory> getEpicHistorybyEpic(Integer idepic);
  
}