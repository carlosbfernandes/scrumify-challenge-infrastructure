package pt.com.scrumify.database.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.TypeOfAbsence;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;

public interface AbsenceService {

   List<Absence> listByResource(Resource resource);
   List<Absence> getAllBetween(Date start, Date end, List<Resource> resources);
   List<Absence> listByResourceYearAndMonth(Resource resource, Year year, Integer month);
   List<Absence> listByResourceAndTimesheet(Resource resource, Time startDate, Time endDate);
   List<Absence> getByTimesheet(Resource resource, Year year, Integer month, Integer fortnight);
   
   Absence getById(int id);
   Absence getByIdAndResource(int id, Resource resource);
   
   Absence save(Absence absence);
   void saveAll(List<Absence> absences);
   void delete(Absence absence);
   
   Map<Date, List<Absence>> absencesgroupByMonth(List<Absence> absences) throws ParseException;
   List<TypeOfAbsence> getAllTypes();
   
   @Cacheable("hours")
   Long hoursbyResourceCurrentFortnight(Integer year, Integer month, Integer fortnight, Resource resource);
}