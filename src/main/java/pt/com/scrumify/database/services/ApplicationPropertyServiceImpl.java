package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ApplicationProperty;
import pt.com.scrumify.database.repositories.ApplicationPropertyRepository;

@Service
public class ApplicationPropertyServiceImpl implements ApplicationPropertyService {
   @Autowired
   private ApplicationPropertyRepository applicationsPropertiesRepository;
   
   @Override
   public void  delete(ApplicationProperty property){
      applicationsPropertiesRepository.delete(property);
   }
   
   @Override
   public List<ApplicationProperty> getByApplicationAndEnvironment(int application, int environment){
      return applicationsPropertiesRepository.findByPkApplicationIdAndPkEnvironmentId(application, environment);
   }

   @Override
   public ApplicationProperty getOne(int property, int application, int environment){
      return applicationsPropertiesRepository.findByPkPropertyIdAndPkApplicationIdAndPkEnvironmentId(property, application, environment);
   }

   @Override
   public ApplicationProperty save(ApplicationProperty property) {
      return applicationsPropertiesRepository.save(property);
   }
}