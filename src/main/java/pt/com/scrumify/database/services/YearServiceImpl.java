package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.YearRepository;

@Service
public class YearServiceImpl implements YearService {
   
   @Autowired
   private YearRepository repository;

   @Override
   public Year getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Year save(Year year) {
      return repository.save(year);
   }

   @Override
   public List<Year> getAll() {
      return repository.findAllByOrderByIdDesc();
   }
   
   @Override
   public List<Year> getAllActive() {
      return repository.findByActiveIsTrue();
   }

   @Override
   public List<Year> getByHoldiays() {
      return repository.findByHolidays();
   }

   @Override
   public List<Year> getByCompensationDays() {
      return repository.findByCompensationDays();
   }
   
   @Override
   public boolean yearsExist(List<Integer> years) {
      return years.size() == repository.findByIdIn(years).size();
   }

   @Override
   public Year getFirstYear() {
      return repository.findTop1ByOrderByIdAsc();
   }

   @Override
   public Year getFirstActiveYear() {
      return repository.findTop1ByActiveIsTrueOrderByIdAsc();
   }
   
   @Override
   public Year getLastYear() {
      return repository.findTop1ByOrderByIdDesc();
   }
   
   @Override
   public Year getLastActiveYear() {
      return repository.findTop1ByActiveIsTrueOrderByIdDesc();
   }
}