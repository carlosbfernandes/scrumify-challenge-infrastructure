package pt.com.scrumify.database.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.entities.Year;

public interface VacationService {
   Vacation getById(Integer id);
   Vacation getByIdAndResource(Integer id, Resource resource);
   
   List<Vacation> getByTimesheet(Resource resource, Year year, Integer month, Integer fortnight);
   
   List<Vacation> listAll();
   List<Vacation> listByResource(Resource resource);
   List<Vacation> save(List<Vacation> vacations);
   List<Vacation> listDays(Vacation vacation);
   List<Vacation> listByResourceYearAndMonth(Resource resource, Year year, Integer month);
   
   List<Vacation> listByResourceAndTimesheet(Resource resource, Time startDate, Time endDate);
   
   Map<Date, List<Vacation>> vacationsgroupByMonth(List<Vacation> vacations) throws ParseException;
   
   Vacation save(Vacation vacation);
   void delete(Vacation vacation);
   
   Long getResourceHoursByTimesheet(Integer year, Integer month, Integer fortnight, Resource resource);
}