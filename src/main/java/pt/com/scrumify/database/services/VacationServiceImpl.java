package pt.com.scrumify.database.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.TimesheetDayRepository;
import pt.com.scrumify.database.repositories.VacationsRepository;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class VacationServiceImpl implements VacationService {
   @Autowired
   private VacationsRepository vacationRepository;
   @Autowired
   private TimeService timesService;
   @Autowired
   private TimesheetDayRepository timesheetDaysRepository;

   @Override
   public Vacation getById(Integer id) {
      return vacationRepository.getOne(id);
   }
   
   @Override
   public Vacation getByIdAndResource(Integer id, Resource resource) {
      return vacationRepository.findByIdAndResource(id, resource);
   }

   @Override
   public Vacation save(Vacation vacation) {      
      return vacationRepository.save(vacation);
   }
   
   @Override
   public List<Vacation> save(List<Vacation> vacations) {
      for(Vacation vacation : vacations){
         TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(vacation.getDay(), ResourcesHelper.getResource());
         if(timesheetDay != null){
            timesheetDay.setHoursVacation(vacation.getDay().getDayOfWeek() <= 4 ? timesheetDay.getHours() : 7);
            timesheetDaysRepository.save(timesheetDay);
            vacation.setTimesheet(timesheetDay.getTimesheet());
         }
         if(vacationRepository.findByResourceDay(ResourcesHelper.getResource(), vacation.getDay()) == null){
            vacationRepository.save(vacation);
         }
      }
      return vacations; 
   }

   @Override
   public List<Vacation> listAll() {
      return vacationRepository.findAll();
   }

   @Override
   public List<Vacation> listByResource(Resource resource) {
      return vacationRepository.listByResource(resource.getId());
   }

   @Override
   public void delete(Vacation vacation) {
      TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(vacation.getDay(), ResourcesHelper.getResource());
      if(timesheetDay != null){
         timesheetDay.setHoursVacation(0);
      }
      vacationRepository.delete(vacation);
   }
   
   @Override
   public List<Vacation> listDays(Vacation vacation){
      List<Vacation> vacations = new ArrayList<>();
      List<Time> days = timesService.getBetweenDates(vacation.getStartDay(), vacation.getEndDay());
      Resource resource = ResourcesHelper.getResource();
      for (Time day : days) {
         if(!day.isCompensationDay() && !day.isHoliday() && day.getDayOfWeek()<=5 ){
            Vacation temp = new Vacation();
            temp.setDay(day);
            temp.setResource(resource);
            vacations.add(temp);       
         }
      }      
      return vacations;
   }
   
   @Override
   public Map<Date, List<Vacation>> vacationsgroupByMonth (List<Vacation> vacations) throws ParseException{
      Map<Date, List<Vacation>> hashvacations = new HashMap<>();
      LocalDate today = LocalDate.now(); 
      for(Vacation vacation : vacations){
         DateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
         Date date = format.parse(Integer.toString(today.getYear()) +"-"+ (vacation.getDay().getMonth().toString().length() >1 ?vacation.getDay().getMonth().toString() :"0"+vacation.getDay().getMonth().toString())
               +"-"+  "01 00:00:00");
            if(hashvacations.containsKey(date)){
               hashvacations.get(date).add(vacation);
            }else{ 
               List<Vacation> lista = new ArrayList<>();
               lista.add(vacation);
               hashvacations.put(date, lista);
            }
         }
      TreeMap<Date, List<Vacation>> sorted = new TreeMap<>(); 
      sorted.putAll(hashvacations); 
     return sorted; 
   }
   
   @Override
   public List<Vacation> listByResourceYearAndMonth(Resource resource, Year year ,Integer month){
          
      return vacationRepository.listByResourceYearAndMonthOrderByDate(resource, year, month);     
   }

   @Override
   public List<Vacation> listByResourceAndTimesheet(Resource resource,Time startDate, Time endDate){
          
      return vacationRepository.listByResourceAndTimesheetOrderByDate(resource, startDate, endDate);     
   }

   @Override
   public Long getResourceHoursByTimesheet(Integer year, Integer month, Integer fortnight, Resource resource) {
      Long result = vacationRepository.getHours(resource, year, month, fortnight);
      return result != null ? result : 0;
   }

   @Override
   public List<Vacation> getByTimesheet(Resource resource, Year year, Integer month, Integer fortnight) {
      return vacationRepository.getByTimesheet(resource, year, month, fortnight);
   }
}