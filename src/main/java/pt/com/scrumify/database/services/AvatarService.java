package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.entities.Gender;

public interface AvatarService {
   Avatar getOne(Integer id);
   List<Avatar> getByGender(Gender gender);
   Avatar getFirst(Gender gender);
   Avatar getRandomByGender(Gender gender);
}