package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.Year;

public interface TimesheetDayService {
   TimesheetDay getOne(Integer id);
   TimesheetDay getOne(Resource resource, Date day);
   TimesheetDay save(TimesheetDay timesheetDay);
   List<TimesheetDay> getAll();
   List<TimesheetDay> findByYearAndMonthOrderByDate(Year year, Integer month, Resource resource);
   List<TimesheetDay> findByCompensationDay(Year year, Integer month, Integer fortnight, Resource resource);
}