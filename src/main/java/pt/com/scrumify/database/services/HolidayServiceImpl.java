package pt.com.scrumify.database.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Holiday;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.HolidayRepository;
import pt.com.scrumify.database.repositories.TimesheetDayRepository;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TimesHelper;

@Service
public class HolidayServiceImpl implements HolidayService {
   
   @Autowired
   private HolidayRepository repository;
   @Autowired
   private TimeService timesService;   
   @Autowired
   private TimesHelper timesHelper;
   @Autowired
   private TimesheetDayRepository timesheetDaysRepository;

   @Override
   public Holiday getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Holiday save(Holiday holiday) {
      holiday.setDay(holiday.getDay().getId() == null ? timesService.getOne(timesHelper.getId(holiday.getDay().getDate())): holiday.getDay());
      
      if(holiday.getId() != 0){
         Time timetOld = repository.getOne(holiday.getId()).getDay();
         if(timetOld != null){
            TimesheetDay timesheetDayold = timesheetDaysRepository.findByDate(timetOld, ResourcesHelper.getResource());
            if(timesheetDayold != null){
               timesheetDayold.setHoursHoliday(0);
               timesheetDaysRepository.save(timesheetDayold);
            }
         }
      }
      TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(holiday.getDay(), ResourcesHelper.getResource());
      if(timesheetDay != null){
         timesheetDay.setHoursHoliday(0);
         timesheetDaysRepository.save(timesheetDay);
      }  
      return repository.save(holiday);
   }

   @Override
   public List<Holiday> getAll() {
      return repository.findAll();
   }
   
   @Override
   public Map<Integer, List<Holiday>> getAllInMap() {
      List<Holiday> holidays= repository.findByOrderByDayIdAsc();
      Map<Integer, List<Holiday>> hashholidays = new HashMap<>();
      for(Holiday holiday : holidays){
            if(hashholidays.containsKey(holiday.getDay().getYear().getId())){
               hashholidays.get(holiday.getDay().getYear().getId()).add(holiday);
            }else{ 
               List<Holiday> lista = new ArrayList<>();
               lista.add(holiday);
               hashholidays.put(holiday.getDay().getYear().getId(), lista);
            }
         }
      TreeMap<Integer, List<Holiday>> sorted = new TreeMap<>(); 
      sorted.putAll(hashholidays); 
      
      return sorted;
   }
   
   @Override
   public List<Holiday> getByYearAndMonth(Year year, Integer month) {
      return repository.findByDayYearAndDayMonth(year, month);
   }
   
   @Override
   public void delete(Holiday holiday){
      TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(holiday.getDay(), ResourcesHelper.getResource());
      if(timesheetDay != null){
         timesheetDay.setHoursHoliday(0);
         timesheetDaysRepository.save(timesheetDay);
      }
      repository.delete(holiday);
   }

   @Override
   public Long getResourceHoursByTimesheet(Integer year, Integer month, Integer fortnight, Schedule schedule) {
      Long hours = repository.getHours(year, month, fortnight, schedule);
      return hours != null ? hours : 0 ;
   }

   
}