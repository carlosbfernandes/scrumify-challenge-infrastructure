package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ApplicationCredential;
import pt.com.scrumify.database.repositories.ApplicationCredentialRepository;

@Service
public class ApplicationCredentialServiceImpl implements ApplicationCredentialService {
   @Autowired
   private ApplicationCredentialRepository credentialsRepository;

   @Override
   public void delete(ApplicationCredential credencial) {
      credentialsRepository.delete(credencial);
   }

   @Override
   public List<ApplicationCredential> getByApplicationAndEnvironment(int application, int environment) {
      return credentialsRepository.findByApplicationIdAndEnvironmentId(application, environment);
   }

   @Override
   public ApplicationCredential getOne(int credencial, int application, int environment) {
      return credentialsRepository.findByIdAndApplicationIdAndEnvironmentId(credencial, application, environment);
   }

   @Override
   public ApplicationCredential save(ApplicationCredential credential) {
      return this.credentialsRepository.save(credential);
   }
}