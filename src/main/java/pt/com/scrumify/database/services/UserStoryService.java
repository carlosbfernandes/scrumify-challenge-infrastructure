package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.UserStory;

public interface UserStoryService {
   UserStory getOne(Integer id);
   UserStory save(UserStory userStory);
   List<UserStory> getAll();
   long countAll();
   List<UserStory> getByResource();
   List<UserStory> getByTeam(Team team);
   List<UserStory> getByTeams(List<Team> teams);
   List<UserStory> getByTeamId(Integer team);
   List<UserStory> getByTeamAndNotFinalStatus(Team team);   
   List<UserStory> getWithoutEpic(Team team);
   List<UserStory> getWithoutWorkItems(Team team);
   List<UserStory> getByStatusesAndResource(Resource resource, List<Integer> ids);
}