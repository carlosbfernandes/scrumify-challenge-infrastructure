package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Tag;
import pt.com.scrumify.database.repositories.TagRepository;

@Service
public class TagServiceImpl implements TagService {
   
   @Autowired
   private TagRepository repository;

   @Override
   public Tag getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Tag save(Tag tag) {
      return repository.save(tag);
   }

   @Override
   public List<Tag> getAll(List<SubArea> subAreas) {
      return repository.findBySubAreas(subAreas);
   }
   
   @Override
   public void delete(Integer id) {
      repository.deleteById(id);
   }
}