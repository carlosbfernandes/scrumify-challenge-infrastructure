package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.entities.TimeReportView;

public interface TimesheetService {
   void delete(Integer id);
   Timesheet getOne(Integer id);
   Timesheet save(Timesheet timesheet);
   Timesheet getSheet(Resource resource, Integer year, Integer month, Integer day);
   Timesheet getSheetByFortnight(Resource resource, Integer year, Integer month, Integer fortnight);
   
   List<Timesheet> getAll();
   List<Timesheet> getByResourcesYearMonthAndFortnight(List<Resource> resources, Integer year, Integer month, Integer fortnight);
   List<Timesheet> getSheets(Resource resource, Year year, Integer month);
   List<Timesheet> getSheetsWithComments(Resource resource, Year year, Integer month);
   List<TimeReportView> getReportOfTimeSheet(Resource resource, Time startDate, Time endDate);
   List<Timesheet> getSheetsByYear(Year year);
   
   Date dateLastTimeSheetApprovedorReviewed(Resource resource);
   List<Timesheet> getSheetsofTeams(List<Team> teams, Integer month, Integer year, Integer fortnight);
 
   Timesheet approve(Integer id);
   
   List<Timesheet> getByResources(List<Resource> resources);
   
   boolean checkTimesheetSubmitted(Date date);
}