package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.TypeOfWork;
import pt.com.scrumify.database.repositories.TypeOfWorkRepository;

@Service
public class TypeOfWorkServiceImpl implements TypeOfWorkService {
   @Autowired
   private TypeOfWorkRepository repository;

   @Override
   public TypeOfWork getById(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public List<TypeOfWork> getAll() {
      return repository.findAll();
   }

   @Override
   public TypeOfWork save(TypeOfWork typeOfWork) {
      return repository.save(typeOfWork);
   }
}