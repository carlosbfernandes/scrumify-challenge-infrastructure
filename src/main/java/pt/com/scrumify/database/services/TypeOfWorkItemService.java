package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.TypeOfWorkItem;

public interface TypeOfWorkItemService {
   List<TypeOfWorkItem> getAll();
   List<TypeOfWorkItem> getAvailable();
   TypeOfWorkItem getOne(Integer id);

}
