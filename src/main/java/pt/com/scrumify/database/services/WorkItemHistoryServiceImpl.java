package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.WorkItemHistory;
import pt.com.scrumify.database.repositories.WorkItemHistoryRepository;

@Service
public class WorkItemHistoryServiceImpl implements WorkItemHistoryService {

   @Autowired
   private WorkItemHistoryRepository repository;
   

   @Override
   public WorkItemHistory save(WorkItemHistory workItemHistory) {
      return repository.saveAndFlush(workItemHistory);
   }
   
   @Override
   public List<WorkItemHistory> getWorkItemHistoryByWorkItem(Integer idworkitem) {
      return repository.findByWorkItemIdOrderByLastUpdateDesc(idworkitem);
   }
   

}