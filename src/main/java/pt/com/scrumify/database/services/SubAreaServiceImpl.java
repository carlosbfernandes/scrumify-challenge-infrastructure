package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.repositories.SubAreaRepository;

@Service
public class SubAreaServiceImpl implements SubAreaService {
   @Autowired
   private SubAreaRepository subAreaRepository;

   @Override
   public SubArea getOne(Integer id) {
      return this.subAreaRepository.getOne(id);
   }

   @Override
   public List<SubArea> getAll() {
      return this.subAreaRepository.findAll();
   }

   @Override
   public SubArea save(SubArea subArea) {
      return this.subAreaRepository.save(subArea);
   }

   @Override
   public List<SubArea> byTeams(List<Team> teams) {
      return this.subAreaRepository.byTeams(teams);
   }
  
   @Override
   public List<SubArea> listSubarea(SubArea subArea) {
      return this.subAreaRepository.listSubarea(subArea);
   }
   
   @Override
   public List<SubArea> listSubareasByResource(Resource resource) {
      return this.subAreaRepository.listSubareasByResource(resource);
   }

   @Override
   public List<SubArea> getByAreaAndResource(Area area, Resource resource) {
      return subAreaRepository.getByAreaAndResource(area, resource);
   }
}