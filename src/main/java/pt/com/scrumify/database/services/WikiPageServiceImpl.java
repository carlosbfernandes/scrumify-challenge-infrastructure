package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.WikiPage;
import pt.com.scrumify.database.repositories.WikiPageRepository;

@Service
public class WikiPageServiceImpl implements WikiPageService {

   @Autowired
   private WikiPageRepository repository;

   @Override
   public WikiPage getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public WikiPage save(WikiPage page) {
      return repository.save(page);
   }

   @Override
   public List<WikiPage> getAll() {
      return repository.findAll();
   }
   
   @Override
   public void delete(WikiPage page){
      repository.delete(page);
   }
}