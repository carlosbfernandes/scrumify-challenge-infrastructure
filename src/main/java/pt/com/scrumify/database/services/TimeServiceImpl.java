package pt.com.scrumify.database.services;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.TimeRepository;
import pt.com.scrumify.helpers.ConstantsHelper;

@Service
public class TimeServiceImpl implements TimeService {
   
   @Autowired
   private TimeRepository repository;

   @Override
   public Time getOne(String id) {
      return repository.findById(id);
   }
   
   @Override
   public Time getToday() {
      Calendar cal = Calendar.getInstance();
      String id = String.format(ConstantsHelper.DEFAULT_DATETIME, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), 0);
      
      return repository.findById(id);
   }

   @Override
   public Time save(Time time) {
      return repository.save(time);
   }
   
   @Override
   public List<Time> getBetween(Time di, Time df) {
      return repository.getBetween(di.getDate(), df.getDate());
   }

   @Override
   public List<Time> getAll() {
      return repository.findAll();
   }

   @Override
   public List<Time> getByYear(Integer year) {
      return repository.findByYearId(year);
   }

   @Override
   public List<Time> getMonths() {
      LocalDate today = LocalDate.now();
      return repository.findByYearAndDayOrderByDate(new Year(today.getYear()), 1);
   }
   
   @Override
   public List<Time> getYears() {
      return repository.findByMonthAndDayOrderByDate(1, 1);
   }
   
   @Override
   public List<Time> findByYearAndMonth(Year year, Integer month) {
      return repository.findByYearAndMonthOrderByDate(year, month);
   }
   
   @Override
   public List<Time> getBetweenDates(Date startDay,Date endDay){
      return repository.findByDateBetween(startDay, endDay != null ? endDay : startDay);
   }
   
   @Override
   public Time getbyDate(Date date){
      return repository.findByDate(date);
   }
   
}