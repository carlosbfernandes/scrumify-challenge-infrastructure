package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.repositories.UserStoryRepository;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.UserStoriesHelper;

@Service
public class UserStoryServiceImpl implements UserStoryService {
   @Autowired
   private UserStoriesHelper userstoriesHelper;
   @Autowired
   private UserStoryRepository repository;

   @Override
   public UserStory getOne(Integer id) {
      UserStory story = repository.getOne(id);

      if (!story.getWorkItems().isEmpty()) {
         Integer sum = 0;
         for (WorkItem workitem : story.getWorkItems()) {
            if (workitem.getWorkLogs() != null) {
               sum += workitem.getWorkLogs().stream().mapToInt(WorkItemWorkLog::getTimeSpent).sum();
            }
         }
         
         story.setHours(sum);
      }

      return story;
   }

   @Override
   public UserStory save(UserStory userStory) {
      return repository.saveAndFlush(userStory);
   }

   @Override
   public List<UserStory> getAll() {
      return repository.findAll();
   }

   @Override
   public List<UserStory> getByResource() {
      List<UserStory> userstories = repository.getByMember(ResourcesHelper.getResource());
      for (UserStory userstory : userstories)
         userstoriesHelper.setHours(userstory);
      return userstories;
   }

   @Override
   public long countAll() {
      return repository.count();
   }

   @Override
   public List<UserStory> getByTeam(Team team) {
      return repository.findByTeam(team);
   }

   @Override
   public List<UserStory> getByTeams(List<Team> teams) {
      return repository.findByTeams(teams);
   }

   @Override
   public List<UserStory> getByTeamId(Integer team) {
      return repository.findByTeamId(team);
   }

   @Override
   public List<UserStory> getByTeamAndNotFinalStatus(Team team) {
      return repository.findByTeamAndNotFinalStatus(team);
   }

   @Override
   public List<UserStory> getWithoutEpic(Team team) {
      return repository.getWithoutEpic(team);
   }

   @Override
   public List<UserStory> getWithoutWorkItems(Team team) {
      return repository.getWithoutWorkItems(team);
   }

   @Override
   public List<UserStory> getByStatusesAndResource(Resource resource, List<Integer> ids) {
      List<UserStory> userstories = repository.findByStatusAndResource(resource, ids);
      for (UserStory userstory : userstories)
         userstoriesHelper.setHours(userstory);
      
      return userstories;
   }
}