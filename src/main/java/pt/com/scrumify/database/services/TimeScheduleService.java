package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimeSchedule;
import pt.com.scrumify.database.entities.Year;

public interface TimeScheduleService {
   List<TimeSchedule> getByYearMonthAndFortnight(Year year, Integer month, Integer fortnight, Schedule schedule);
   TimeSchedule getByDay(Time day, Schedule schedule);
}