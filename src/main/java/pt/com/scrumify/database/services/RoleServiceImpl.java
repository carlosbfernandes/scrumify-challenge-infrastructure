package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Role;
import pt.com.scrumify.database.repositories.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService {
   @Autowired
   private RoleRepository rolesRepository;

   @Override
   public Role getOne(Integer id) {
      return this.rolesRepository.getOne(id);
   }

   @Override
   public Role save(Role role) {
      return this.rolesRepository.save(role);
   }

   @Override
   public List<Role> getAll() {
      return this.rolesRepository.findAll();
   }
   
   @Override
   public List<Role> getAllOrderedByName() {
      return rolesRepository.findAllByOrderByNameAsc();
   }
}