package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Schedule;

public interface ScheduleService {
   Schedule getOne(Integer id);
   Schedule save(Schedule schedule);
	
   List<Schedule> getAll();
   Schedule getActiveSchedule(Resource resource, Date date);
}