package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;

public interface SubAreaService {
   SubArea getOne(Integer id);
   SubArea save(SubArea subArea);
   List<SubArea> getAll();
   List<SubArea> byTeams(List<Team> teams);
   List<SubArea> getByAreaAndResource(Area area, Resource resource);
   List<SubArea> listSubarea(SubArea subArea);
   List<SubArea> listSubareasByResource(Resource resource);
}
   