package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.WorkItemAttachment;
import pt.com.scrumify.database.entities.WorkItem;

public interface WorkItemAttachmentService {
   
   WorkItemAttachment getOne(Integer id);
   
   List<WorkItemAttachment> getAll();
   List<WorkItemAttachment> getByWorkItem(WorkItem workitem);
   List<WorkItemAttachment> getByWorkItemId(Integer workitemId);
   
   WorkItemAttachment save(WorkItemAttachment attachment);
   void delete(WorkItemAttachment attachment);
   
}