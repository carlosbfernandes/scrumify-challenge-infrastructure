package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Property;
import pt.com.scrumify.database.repositories.PropertyRepository;

@Service
public class PropertyServiceImpl implements PropertyService {

   @Autowired
   private PropertyRepository repository;
   
   @Override
   public Property getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Property save(Property property) {
      return repository.save(property);
   }

   @Override
   public List<Property> getAll() {
      return repository.findAll();
   }

   @Override
   public List<Property> getByGroup(String group) {
      return repository.findByGroup(group);
   }
   
   @Override
   public List<Property> getPropertyGroupNotIn(List<Integer> listIds, String group) {
      
      if(listIds.isEmpty())
         return repository.findByGroup(group);
      else
         return repository.findByGroupNotIn(listIds,group);
   }
}