package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.DateUtils;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.database.repositories.WorkItemNoteRepository;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.helpers.DatesHelper;

@Service
public class WorkItemNoteServiceImpl implements WorkItemNoteService {
   @Autowired
   private WorkItemNoteRepository repository;

   @Override
   public WorkItemNote getById(Integer id) {
      return this.repository.getOne(id);
   }
   
   @Override
   public WorkItemNote save(WorkItemNote workItemNote) {
      workItemNote.setCreated(DateUtils.createNow().getTime());
      return this.repository.save(workItemNote);
   }
   
   @Override
   public List<WorkItemNote> getStatusReports(Contract contract, Integer noteid, ReportView report){
      return repository.getStatusReports(contract, noteid, report.getStartDay(), DatesHelper.addDays(report.getEndDay(), 1));
   }

   @Override
   public List<WorkItemNote> getStatusReportsByResource(Contract contract, Integer noteid, ReportView report) {
      return repository.getStatusReportsByResource(contract, noteid, report.getResource(), report.getStartDay(), DatesHelper.addDays(report.getEndDay(), 1));
   }
}