package pt.com.scrumify.database.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TypeOfAbsence;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.AbsenceRepository;
import pt.com.scrumify.database.repositories.TimesheetDayRepository;
import pt.com.scrumify.database.repositories.TypeOfAbsenceRepository;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.TimesHelper;

@Service
public class AbsenceServiceImpl implements AbsenceService {
   @Autowired
   private AbsenceRepository absencesRepository;
   @Autowired
   private TimesheetDayRepository timesheetDaysRepository;
   @Autowired
   private TimeService timesService;
   @Autowired
   private TypeOfAbsenceRepository absencesTypeRepository;
   @Autowired
   private TimesHelper timesHelper;

   @Override
   public List<Absence> listByResource(Resource resource) {
      return absencesRepository.findByResourceIdOrderByDayDateAsc(resource.getId());
   }

   @Override
   public List<Absence> getAllBetween(Date start, Date end, List<Resource> resources) {
      return absencesRepository.findByDayDateBetweenAndResourceIn(start, DatesHelper.addDays(end, 1), resources);
   }

   @Override
   public List<TypeOfAbsence> getAllTypes() {
      return absencesTypeRepository.findAll();
   }

   @Override
   public Absence getById(int id) {
      return absencesRepository.getOne(id);
   }

   @Override
   public Absence save(Absence absence) {
      /*
       * TODO: clean code
       */
      // Resource resource = ResourcesHelper.getResource();
      // absence.setResource(ResourcesHelper.getResource());
      // absence.setDay(timesService.getOne(timesHelper.getId(absence.getDay().getDate())));
      // Integer timespentOld = 0;
      // TimesheetDay previousTimesheetDay =null;
      // if(absence.getId() != 0){
      // Absence previousAbsence = absencesRepository.getOne(absence.getId());
      // timespentOld = previousAbsence.getHours();
      // previousTimesheetDay = timesheetDaysRepository.findByDate(previousAbsence.getDay(), resource);
      // }
      //
      // TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(absence.getDay(),
      // ResourcesHelper.getResource());
      // if(timesheetDay != null){
      // timesheetDay.setAbsence(previousTimesheetDay != timesheetDay ? timesheetDay.getAbsence() + absence.getHours() :
      // timesheetDay.getAbsence() + absence.getHours() - timespentOld);
      // timesheetDaysRepository.save(timesheetDay);
      // if(previousTimesheetDay != null && timesheetDay != previousTimesheetDay) {
      // previousTimesheetDay.setAbsence(previousTimesheetDay.getAbsence() - timespentOld);
      // timesheetDaysRepository.save(previousTimesheetDay);
      // }
      // absence.setTimesheet(timesheetDay.getTimesheet());
      // }

      return absencesRepository.save(absence);
   }

   @Override
   public void saveAll(List<Absence> absences) {
      absencesRepository.saveAll(absences);
   }

   @Override
   public void delete(Absence absence) {
      /*
       * TODO: clean code
       */
      // TimesheetDay timesheetDay = timesheetDaysRepository.findByDate(absence.getDay(),
      // ResourcesHelper.getResource());
      // if(timesheetDay != null){
      // timesheetDay.setAbsence(timesheetDay.getAbsence() - absence.getHours());
      // }
      absencesRepository.delete(absence);
   }

   @Override
   public List<Absence> listByResourceYearAndMonth(Resource resource, Year year, Integer month) {
      return absencesRepository.findByResourceAndDayYearAndDayMonthOrderByDayDateAsc(resource, year, month);
   }

   @Override
   public List<Absence> listByResourceAndTimesheet(Resource resource, Time startDate, Time endDate) {
      return absencesRepository.listByResourceAndTimesheetOrderByDate(resource, startDate, endDate);
   }

   @Override
   public Map<Date, List<Absence>> absencesgroupByMonth(List<Absence> absences) throws ParseException {
      Map<Date, List<Absence>> hashabsence = new HashMap<>();
      LocalDate today = LocalDate.now();
      /*
       * TODO: code review
       */
      for (Absence absence : absences) {
         DateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
         Date date = format.parse(Integer.toString(today.getYear()) + "-"
               + (absence.getDay().getMonth().toString().length() > 1 ? absence.getDay().getMonth().toString() : "0" + absence.getDay().getMonth().toString()) + "-"
               + "01 00:00:00");
         if (hashabsence.containsKey(date)) {
            hashabsence.get(date).add(absence);
         } else {
            List<Absence> lista = new ArrayList<>();
            lista.add(absence);
            hashabsence.put(date, lista);
         }
      }
      TreeMap<Date, List<Absence>> sorted = new TreeMap<>();
      sorted.putAll(hashabsence);
      return sorted;
   }

   @Override
   public Long hoursbyResourceCurrentFortnight(Integer year, Integer month, Integer fortnight, Resource resource) {
      Long result = absencesRepository.hoursByResourceCurrentFortnight(resource, year, month, fortnight);
      return result != null ? result : 0;
   }

   @Override
   public List<Absence> getByTimesheet(Resource resource, Year year, Integer month, Integer fortnight) {
      return absencesRepository.getByTimesheet(resource, year, month, fortnight);
   }

   @Override
   public Absence getByIdAndResource(int id, Resource resource) {
      return absencesRepository.findByIdAndResource(id, resource);
   }
}