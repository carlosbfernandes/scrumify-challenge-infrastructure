package pt.com.scrumify.database.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.TypeOfAbsence;
import pt.com.scrumify.database.repositories.TypeOfAbsenceRepository;

@Service
public class TypeOfAbsencServiceImpl implements TypeOfAbsenceService {
   @Autowired
   private TypeOfAbsenceRepository absencesTypeRepository; 

   @Override
   public List<TypeOfAbsence> getAllTypes(){
      return absencesTypeRepository.findAllByOrderByNameAsc();     
   }

   @Override
   public TypeOfAbsence findByCodeAndName(String code, String name) {
      return absencesTypeRepository.findByCodeAndName(code, name);
   }
  
}