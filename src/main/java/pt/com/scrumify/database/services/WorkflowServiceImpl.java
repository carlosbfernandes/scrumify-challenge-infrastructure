package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Workflow;
import pt.com.scrumify.database.entities.WorkflowTransition;
import pt.com.scrumify.database.repositories.WorkflowRepository;
import pt.com.scrumify.database.repositories.WorkflowTransitionRepository;

@Service
public class WorkflowServiceImpl implements WorkflowService {

   @Autowired
   private WorkflowRepository repository;
   @Autowired
   private WorkflowTransitionRepository transitionRepository;
   
   @Override
   public Workflow getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Workflow save(Workflow workFlow) {
      return repository.save(workFlow);
   }

   @Override
   public List<Workflow> getAll() {
      return repository.findAll();
   }

   @Override
   public WorkflowTransition saveTransition(WorkflowTransition transition) {
      return transitionRepository.save(transition);
   }   
}