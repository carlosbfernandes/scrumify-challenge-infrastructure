package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.WorkItemHistory;

public interface WorkItemHistoryService {
	
   WorkItemHistory save(WorkItemHistory workItemHistory);
   List<WorkItemHistory> getWorkItemHistoryByWorkItem(Integer idworkitem);
  
}