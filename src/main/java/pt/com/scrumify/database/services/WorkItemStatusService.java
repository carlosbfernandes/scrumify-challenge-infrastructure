package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Workflow;

public interface WorkItemStatusService {
   WorkItemStatus getOne(Integer id);
   WorkItemStatus getOneByTypeOfWorkitem(Integer id);
   List<WorkItemStatus> getAll();
   List<WorkItemStatus> getByIds(List<Integer> status);
   List<WorkItemStatus> getByWorkflow(Workflow workFlow, WorkItemStatus itemStatus);
   List<WorkItemStatus> getOneByTypeOfWorkitemById(Integer id);
   List<WorkItemStatus> getByTypeId(Integer id);
}