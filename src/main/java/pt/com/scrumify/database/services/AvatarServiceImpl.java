package pt.com.scrumify.database.services;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.entities.Gender;
import pt.com.scrumify.database.repositories.AvatarRepository;

@Service
public class AvatarServiceImpl implements AvatarService {
   @Autowired
   private AvatarRepository avatarsRepository;

   @Override
   public Avatar getOne(Integer id) {
      return this.avatarsRepository.getOne(id);
   }

   @Override
   @Cacheable(value = "Avatars.byGender", key = "#gender.id", unless = "#result==null")
   public List<Avatar> getByGender(Gender gender) {
      return this.avatarsRepository.getByGender(gender);
   }
   
   @Override
   public Avatar getFirst(Gender gender) {
      return avatarsRepository.findTop1ByGender(gender);
   }
   
   @Override
   public Avatar getRandomByGender(Gender gender) {
      int result = new Random().nextInt(19) + 1;
      return avatarsRepository.findByIdAndGender(result, gender);
   }
}