package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Activity;

public interface ActivityService {
   Activity getOne(Integer id);
   List<Activity> getAll();
   List<Activity> getByTechnicalAssistance(boolean technicalAssistance);
   Activity save (Activity activity);
}