package pt.com.scrumify.database.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TimesheetApproval;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.TimesheetApprovalRepository;
import pt.com.scrumify.entities.TimesheetApprovalView;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class TimesheetApprovalServiceImpl implements TimesheetApprovalService {
   @Autowired
   private TimesheetApprovalRepository repository;
   @Autowired
   private AreaService areasService;

   @Override
   public TimesheetApproval getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public TimesheetApproval getOne(Resource resource, Year year, Integer month, Integer fortnight, String code, String description) {
      return repository.getOne(resource, year, month, fortnight, code, description);
   }

   @Override
   public TimesheetApproval save(TimesheetApproval timesheet) {
      return repository.save(timesheet);
   }

   @Override
   public List<TimesheetApproval> saveAll(List<TimesheetApproval> approvals) {
      return repository.saveAll(approvals);
   }

   @Override
   public List<TimesheetApproval> getAll() {
      return repository.findAll();
   }

   @Override
   public List<TimesheetApproval> getbyYearAndArea(Year year, Area area) {
      if (area == null) {
         return repository.findDistinctByYearIdAndContractSubAreaAreaIn(year.getId(), areasService.getByResource(ResourcesHelper.getResource()));
      } else {
         return repository.findDistinctByYearIdAndContractSubAreaArea(year.getId(), area);
      }
   }

   @Override
   public TimesheetApproval getbyContract(Contract contract) {
      return repository.findByContract(contract);
   }

   @Override
   public TimesheetApproval getbyYearAndMonthAndResourceAndFortnightAndContract(Year year, Integer month, Resource resource, Integer fortnight, Contract contract) {
      return repository.findByYearAndMonthAndResourceAndFortnightAndContract(year, month, resource, fortnight, contract);
   }

   @Override
   public List<TimesheetApproval> getbyYearAndMonthAndFortnightAndContract(Year year, Integer month, Integer fortnight, Integer contract) {
      return repository.findByYearAndMonthAndFortnightAndContractId(year, month, fortnight, contract);
   }
   
   @Override
   public List<TimesheetApproval> getbyYearAndMonthAndContract(Year year, Integer month, Integer contract) {
      return repository.findByYearAndMonthAndContractId(year, month, contract);
   }

   @Override
   public List<TimesheetApproval> getbyTimesheetAndResource(Integer year, Integer month, Integer fortnight, Integer resource) {
      List<TimesheetApproval> timesheetBillableList = repository.findByYearIdAndMonthAndFortnightAndResourceId(year, month, fortnight, resource);
      List<TimesheetApproval> timesheetBillableListdelete = new ArrayList<>();
      for (TimesheetApproval timesheetBillable : timesheetBillableList) {
         if (timesheetBillable.getDescription().equals("")) {
            timesheetBillableListdelete.add(timesheetBillable);
         }
      }
      repository.deleteAll(timesheetBillableListdelete);
      timesheetBillableList.removeAll(timesheetBillableListdelete);
      return timesheetBillableList;
   }

   @Override
   public List<TimesheetApproval> getbyYearAndMonthAndFortnightAndTeams(Integer year, Integer month, Integer fortnight, List<Team> teams) {
      return repository.findByYearIdAndMonthAndFortnightAndContractTeamContractsPkTeamIn(year, month, fortnight, teams);
   }

   @Override
   public List<TimesheetApproval> getby(Integer year, Integer month, Integer fortnight, Integer resource) {
      return repository.findByYearIdAndMonthAndFortnightAndResourceIdAndContractIsNotNull(year, month, fortnight, resource);
   }

   @Override
   public long getHoursByYearAndMonthAndAreaAndFortnight(Year year, Integer month, Contract contract, Integer fortnight) {
      return repository.findHoursByYearAndMonthAndContractAndFortnight(contract, year, month, fortnight) != null
            ? repository.findHoursByYearAndMonthAndContractAndFortnight(contract, year, month, fortnight)
            : 0;
   }

   @Override
   public Integer getHoursForStatusReport(Contract contract, Year year, Integer month) {
      Integer result = repository.getHoursForStatusReport(contract, year, month);
      return result != null ? result : 0;
   }

   @Override
   public Integer getHoursForStatusReportByContract(Contract contract) {
      Integer result = repository.getHoursForStatusReportByContract(contract);
      return result != null ? result : 0;
   }

   @Override
   public List<TimesheetApproval> getbyYearAndMonthAndArea(Year year, Integer month, Area area) {
      if (month == null) {
         if (area == null) {
            return repository.findDistinctByYearIdAndMonthAndContractSubAreaAreaIn(year.getId(), month, areasService.getByResource(ResourcesHelper.getResource()));
         } else {
            return repository.findDistinctByYearIdAndMonthAndContractSubAreaArea(year.getId(), month, area);
         }
      } else {
         if (area == null) {
            return repository.findDistinctByYearIdAndContractSubAreaAreaIn(year.getId(), areasService.getByResource(ResourcesHelper.getResource()));
         } else {
            return repository.findDistinctByYearIdAndContractSubAreaArea(year.getId(), area);
         }
      }
   }

   @Override
   public List<TimesheetApprovalView> getbyYearAndAreaAndMonth(Year year, Integer month, Area area) {
      if (area == null) {
         if (month == null) {
            return repository.findByYearAndArea(areasService.getByResource(ResourcesHelper.getResource()), year);
         } else {
            return repository.findByYearAndMonthAndArea(areasService.getByResource(ResourcesHelper.getResource()), year, month);
         }
      } else {
         List<Area> areas = new ArrayList<>();
         areas.add(area);
         return repository.findByYearAndMonthAndArea(areas, year, month);
      }
   }

   @Override
   public List<TimesheetApprovalView> getbyYearAndMonth(Year year, Integer month) {
      return repository.findByYearAndMonthAndArea(areasService.getByResource(ResourcesHelper.getResource()), year, month);
   }

   @Override
   public List<TimesheetApproval> getContractsInfo(Year year) {
      return repository.getContractsInfo(year);
   }
   
   @Override
   public void delete(List<TimesheetApproval> approvals) {
      repository.deleteAll(approvals);      
   }
   
   @Override
   public void delete(Integer id) {
      repository.deleteById(id);      
   }
}