package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.WorkflowTransition;

public interface WorkflowTransitionService {
   WorkflowTransition getOne(Integer id);
   WorkflowTransition save(WorkflowTransition transition);
   void delete(WorkflowTransition transition);
   List<WorkflowTransition> getAll();
   
}