package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_RESOURCES)
public class Resource implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "abbreviation", length = 3, nullable = true)
   private String abbreviation;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "gender", nullable = false)
   private Gender gender = new Gender(Gender.MALE);

   @Getter
   @Setter
   @Column(name = "email", length = 80, nullable = false)
   private String email;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "avatar", nullable = false)
   private Avatar avatar = new Avatar();

   @Getter
   @Setter
   @Column(name = "startingday", nullable = false)
   private Date startingDay;

   @Getter
   @Setter
   @Column(name = "endingday", nullable = true)
   private Date endingDay;

   @Getter
   @Setter
   @Column(name = "username", length = 15, nullable = false, unique = true)
   private String username;

   @Getter
   @Setter
   @Column(name = "password", length = 60, nullable = false)
   private String password;

   @Getter
   @Setter
   @Transient
   private String passwordConfirmation;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "profile", nullable = false)
   private Profile profile = new Profile();

   @Getter
   @Setter
   @Column(name = "lastlogin", nullable = true)
   private Date lastLogin;

   @Getter
   @Setter
   @Column(name = "lastpasswordchange", nullable = true)
   private Date lastPasswordChange;

   @Getter
   @Setter
   @Column(name = "externalcode", length = 7, nullable = true)
   private String code;

   @Getter
   @Setter
   @Column(name = "phone", length = 20, nullable = true)
   private String phone;

   @Getter
   @Setter
   @Column(name = "mobile", length = 20, nullable = true)
   private String mobile;

   @Getter
   @Setter
   @Column(name = "extension", length = 5, nullable = true)
   private String extension;

   @Getter
   @Setter
   @Column(name = "approved", nullable = false)
   private boolean approved = false;

   @Getter
   @Setter
   @Column(name = "active", nullable = false)
   private boolean active = false;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY, targetEntity = Schedule.class)
   @JoinColumn(name = "schedule", nullable = true)
   private Schedule schedule;

   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.resource")
   private List<TeamMember> teamMembers = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.resource")
   private List<SprintMember> sprintMembers = new ArrayList<>(0);

   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
   private List<Timesheet> timesheets;

   public Resource(int id) {
      super();

      this.id = id;
   }
}