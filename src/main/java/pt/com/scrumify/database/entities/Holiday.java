package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_HOLIDAYS)
public class Holiday implements Serializable {
   private static final long serialVersionUID = -8576439526334223026L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "day", nullable = false)
   private Time day = new Time();

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
}