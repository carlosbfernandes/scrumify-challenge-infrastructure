package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_CONTRACT_PROPERTIES)
@AssociationOverrides({
   @AssociationOverride(name = "pk.contract", joinColumns = @JoinColumn(name = "contract")),
   @AssociationOverride(name = "pk.property", joinColumns = @JoinColumn(name = "property"))
})
public class ContractProperty implements Serializable {
   private static final long serialVersionUID = 7185035791305502797L;
   
   public static final String CONTRACT_NUMBER = "Contract Number (AT)";
   public static final String PROJECT_NUMBER = "Project Number (AT)";

   @Getter
   @Setter
   @EmbeddedId
   private ContractPropertyPK pk = new ContractPropertyPK();

   @Getter
   @Setter
   @Column(name = "value", nullable = false)
   private String value;

   @Transient
   public Contract getContract() {
      return this.pk.getContract();
   }
   
   public void setContract(Contract contract) {
      this.pk.setContract(contract);
   }

   @Transient
   public Property getProperty() {
      return this.pk.getProperty();
   }
}