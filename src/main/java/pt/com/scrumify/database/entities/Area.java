package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_AREAS)
public class Area implements Serializable {
   private static final long serialVersionUID = 13109788275655282L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "abbreviation", length = 4, nullable = false)
   private String abbreviation;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "area")
   private List<SubArea> subArea = new ArrayList<>(0);
   
   public Area(Integer id) {
      super();
      this.id = id;
   }
}