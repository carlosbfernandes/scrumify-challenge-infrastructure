package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.SerializationUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMES)
public class Time implements Serializable {
   private static final long serialVersionUID = 13109788275655282L;

   @Id
   @Getter
   @Setter
   @Column(name = "id", length = 14, nullable = false)
   private String id;

   @Getter
   @Setter
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.time")
   private List<TimeSchedule> timesSchedules = new ArrayList<>(0);

   @Getter
   @Setter
   @Column(name = "dateofday", nullable = false)
   private Date date;

   @Getter
   @Setter
   @Column(name = "day", nullable = false)
   private Integer day;

   @Getter
   @Setter
   @Column(name = "dayofweek", nullable = false)
   private Integer dayOfWeek;

   @Getter
   @Setter
   @Column(name = "week", nullable = false)
   private Integer week;

   @Getter
   @Setter
   @Column(name = "fortnight", nullable = false)
   private Integer fortnight;

   @Getter
   @Setter
   @Column(name = "month", nullable = false)
   private Integer month;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "year", nullable = false)
   private Year year;

   @Getter
   @Setter
   @Column(name = "quarter", nullable = false)
   private Integer quarter;

   @Getter
   @Setter
   @Column(name = "semester", nullable = false)
   private Integer semester;

   @Getter
   @Setter
   @Column(name = "holiday", nullable = false)
   private boolean holiday = false;

   @Getter
   @Setter
   @Column(name = "compensationday", nullable = false)
   private boolean compensationDay = false;
   
   public Time(String id) {
      super();
      
      this.id = id;
   }

   @Transient
   public boolean isWeekend() {
      return this.dayOfWeek >= 6;
   }

   public void addTimeSchedule(TimeSchedule timeSchedule) {
      this.timesSchedules.add(timeSchedule);
   }

   public Time clone() {
      return (Time) SerializationUtils.clone(this);
   }
}