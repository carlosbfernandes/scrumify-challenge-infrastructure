package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_VACATIONS)
public class Vacation implements Serializable {
   private static final long serialVersionUID = 2644686174353497937L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "day", nullable = false)
   private Time day;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "resource", nullable = false)
   private Resource resource;

   @Getter
   @Setter
   @Column(name = "approved")
   private Date approved;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "approvedby")
   private Resource approvedBy;

   @Getter
   @Setter
   @Column(name = "declined")
   private Date declined;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "declinedby")
   private Resource declinedBy;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "timesheet", nullable = false)
   private Timesheet timesheet;

   @Getter
   @Setter
   @Transient
   private Date startDay;

   @Getter
   @Setter
   @Transient
   private Date endDay;

   @Getter
   @Setter
   @Transient
   private Boolean approvedBox;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
}