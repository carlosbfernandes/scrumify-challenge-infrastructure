package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_WORKFLOWS)
public class Workflow implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;
   
   public static final int WF_EPIC = 1;
   public static final int WF_STORY = 2;
   public static final int WF_TASK = 3;
   public static final int WF_BUG = 4;
   public static final int WF_INCIDENT = 5;
   public static final int WF_REQUEST = 5;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @OneToMany(mappedBy="workflow")
   private List<WorkflowTransition> transitions;

   @Getter
   @Setter
   @Column(name = "active", nullable = false)
   private Boolean active = true;

   public Workflow(int id) {
      super();

      this.id = id;
   }
}