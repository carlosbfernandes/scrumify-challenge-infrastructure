package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMESHEETDAYS)
public class TimesheetDay implements Serializable {
   private static final long serialVersionUID = 13109788275655282L;
   
   private static String empty = "";
   private static String ok = "ok";
   private static String viewAbsence = "absence";
   private static String viewCompensation = "compensationday";
   private static String viewHoliday = "holiday";
   private static String viewOvertime = "overtime";
   private static String viewVacation = "vacation";
   private static String viewWarning = "warning";
   private static String viewWeekend = "weekend";

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "timesheet", nullable = false)
   private Timesheet timesheet;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "day", nullable = false)
   private Time day;

   @Getter
   @Setter
   @Column(name = "hours", nullable = false)
   private Integer hours;

   @Getter
   @Setter
   @Column(name = "timespent", nullable = false)
   private Integer timeSpent = 0;

   @Getter
   @Setter
   @Column(name = "absence", nullable = false)
   private Integer hoursAbsence = 0;

   @Getter
   @Setter
   @Column(name = "compensationday", nullable = false)
   private Integer hoursCompensationDay = 0;

   @Getter
   @Setter
   @Column(name = "holiday", nullable = false)
   private Integer hoursHoliday = 0;

   @Getter
   @Setter
   @Column(name = "vacation", nullable = false)
   private Integer hoursVacation = 0;

   @Getter
   @Setter
   @Column(name = "cd", nullable = false)
   private Boolean compensationDay;

   @Getter
   @Setter
   @Column(name = "h", nullable = false)
   private Boolean holiday;

   @Getter
   @Setter
   @Column(name = "weekend", nullable = false)
   private Boolean weekend;
   
   public void incrementAbsence(Integer hours) {
      this.hoursAbsence += hours;
   }
   
   public void decrementAbsence(Integer hours) {
      this.hoursAbsence -= hours;
   }
   
   public void incrementTimeSpent(Integer hours) {
      this.timeSpent += hours;
   }
   
   public void decrementTimeSpent(Integer hours) {
      this.timeSpent -= hours;
   }
   
   public void incrementVacation(Integer hours) {
      this.hoursVacation += hours;
   }
   
   public void decrementVacation(Integer hours) {
      this.hoursVacation -= hours;
   }
   
   public String getWorkingTimeColor() {
      if (this.hoursCompensationDay > 0)
         return viewCompensation;
      if (this.hoursHoliday > 0)
         return viewHoliday;
      if (this.weekend)
         return viewWeekend;
      if ((this.hours < this.timeSpent + this.hoursVacation + this.hoursAbsence) && this.hoursCompensationDay == 0 && this.hoursHoliday == 0)
         return viewOvertime;
      if ((this.hours == this.timeSpent + this.hoursVacation + this.hoursAbsence) && this.hoursCompensationDay == 0 && this.hoursHoliday == 0)
         return ok; 
      
      return empty;
   }
   
   public String getNonWorkingTimeColor() {
      if (this.hoursAbsence > 0)
         return viewAbsence;
      if (this.hoursCompensationDay > 0)
         return viewCompensation;
      if (this.hoursHoliday > 0)
         return viewHoliday;
      if (this.hoursVacation > 0)
         return viewVacation;
      if (this.weekend)
         return viewWeekend;
      
      return empty;
   }
   
   public String getMissingHoursColor() {
      if (this.hoursCompensationDay > 0)
         return viewCompensation;
      if (this.hoursHoliday > 0)
         return viewHoliday;
      if (this.weekend)
         return viewWeekend;      
      if ((this.hours > this.timeSpent + this.hoursVacation + this.hoursAbsence) && this.hoursCompensationDay == 0 && this.hoursHoliday == 0)
         return viewWarning;
      if ((this.hours < this.timeSpent + this.hoursVacation + this.hoursAbsence) && this.hoursCompensationDay == 0 && this.hoursHoliday == 0)
         return viewOvertime;
      
      return empty;
   }
}