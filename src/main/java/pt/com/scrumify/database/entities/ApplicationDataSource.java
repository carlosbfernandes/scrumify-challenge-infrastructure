package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_APPLICATION_DATASOURCES)
public class ApplicationDataSource implements Serializable {
   private static final long serialVersionUID = -5204362865184421535L;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", nullable = false)
   private String name;
   
   @Getter
   @Setter
   @Column(name = "type", nullable = true)
   private String type;
   
   @Getter
   @Setter
   @Column(name = "databasetype", nullable = true)
   private String databaseType;
   
   @Getter
   @Setter
   @Column(name = "databasename", nullable = false)
   private String databaseName;
   
   @Getter
   @Setter
   @Column(name = "hostname", nullable = false)
   private String hostname;
   
   @Getter
   @Setter
   @Column(name = "port", nullable = false)
   private String port;
   
   @Getter
   @Setter
   @Column(name = "username", nullable = false)
   private String username;
   
   @Getter
   @Setter
   @Column(name = "password", nullable = true)
   private String password;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "application", nullable = false)
   private Application application;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "environment", nullable = false)
   private Environment environment;

   public ApplicationDataSource(Integer id) {
      this.id = id;
   }
   
   public String getConnection() {
      String jdbc = "jdbc:";
      
      switch (this.getDatabaseType()) {
         case "1":
            jdbc += "db2://";
            break;
         case "2":
            jdbc += "oracle:thin:@";
            break;
         default:
            break;
      }
      
      return jdbc + this.getHostname() + ":" + this.getPort() + (this.getDatabaseName() != "" ? ":" + this.getDatabaseName() : "");
   }
}