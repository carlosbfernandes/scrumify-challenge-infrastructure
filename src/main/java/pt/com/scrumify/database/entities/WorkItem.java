package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@Indexed
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_WORKITEMS)
public class WorkItem implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Field
   @Getter
   @Setter
   @Column(name = "number", length = 11, nullable = false)
   private String number;

   @Field
   @Getter
   @Setter
   @Column(name = "name", length = 80, nullable = false)
   private String name;

   @Field
   @Getter
   @Setter
   @Column(name = "description", columnDefinition = "CLOB", nullable = true)
   private String description;

   @Getter
   @Setter
   @Column(name = "estimate", nullable = false)
   private Integer estimate;

   @Getter
   @Setter
   @Column(name = "hours", nullable = false)
   private Integer hours = 0;

   @Getter
   @Setter
   @Column(name = "etc", nullable = false)
   private Integer etc;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "type", nullable = false)
   private TypeOfWorkItem type;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "contract", nullable = false)
   private Contract contract;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "status", nullable = false)
   private WorkItemStatus status;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "userstory", nullable = true)
   private UserStory userStory;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "team", nullable = true)
   private Team team;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "application", nullable = true)
   private Application application;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "release", nullable = true)
   private Release release;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "priority", nullable = true)
   private Priority priority;

   @Getter
   @Setter
   @Column(name = "assigned", nullable = true)
   private Date assigned;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "assignedto", nullable = true)
   private Resource assignedTo;

   @Getter
   @Setter
   @Column(name = "closed", nullable = true)
   private Date closed;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "closedby", nullable = true)
   private Resource closedBy;

   @Getter
   @Setter
   @OneToMany(mappedBy = "workItem")
   private List<WorkItemNote> notes;

   @Getter
   @Setter
   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "workItem")
   private List<WorkItemAttachment> attachments;

   @Getter
   @Setter
   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "workItem")
   private List<WorkItemWorkLog> workLogs = new ArrayList<>(0);

   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "workItem")
   private List<WorkItemHistory> history = new ArrayList<>(0);

   @Getter
   @Setter
   @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
   @JoinTable(name = ConstantsHelper.DATABASE_TABLE_WORKITEM_TAGS, 
              joinColumns = @JoinColumn(name = "workitem"), 
              inverseJoinColumns = @JoinColumn(name = "tag"))
   private List<Tag> tags = new ArrayList<>();

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   public WorkItem(int id) {
      super();

      this.id = id;
   }
   
   public WorkItem(WorkItemView view) {
      super();
      
      this.setId(view.getId());
      this.setNumber(view.getNumber());
      this.setName(view.getName());
      this.setDescription(view.getDescription());
      this.setEstimate(view.getEstimate());
      this.setHours(view.getHours());
      this.setEtc(view.getEtc());
      this.setType(view.getType());
      this.setContract(view.getContract());
      this.setStatus(view.getStatus());
      this.setUserStory(view.getUserStory());
      this.setTeam(view.getTeam());
      this.setApplication(view.getApplication());
      this.setRelease(view.getRelease());
      this.setPriority(view.getPriority());
      this.setAssigned(view.getAssigned());
      this.setAssignedTo(view.getAssignedTo());
      this.setClosed(view.getClosed());
      this.setClosedBy(view.getClosedBy());
      this.setNotes(view.getNotes());
      this.setAttachments(view.getAttachments());
      this.setHistory(view.getHistory());
      this.setWorkLogs(view.getWorkLogs());
      this.setTags(view.getTags());
      this.setCreated(view.getCreated());
      this.setCreatedBy(view.getCreatedBy());
      this.setLastUpdate(view.getLastUpdate());
      this.setLastUpdateBy(view.getLastUpdateBy());
   }

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
   
   public void decrementHours(Integer hours) {
      this.hours -= hours;
   }
   
   public void incrementHours(Integer hours) {
      this.hours += hours;
   }

   public String getFeatureName() {
      return this.type.getName().toLowerCase() + "s/20" + this.number.substring(3, 5) + "/" + this.number.substring(5);
   }
   
   public String getProgress() {
      Integer green, yellow, orange, red;

      /*
       * Green bar (60% of estimate)
       */
      green = Math.round(Math.min(this.estimate - this.estimate * 0.3f - this.estimate * 0.1f, (float) this.hours));
      
      /*
       * Yellow bar (30% of estimate)
       */
      yellow = Math.round(Math.min(this.estimate * 0.3f, (float) this.hours - green));
      
      /*
       * Orange bar (10% of estimate)
       */
      orange = Math.round(Math.min(this.estimate * 0.1f, (float) this.hours - green - yellow));
      
      /*
       * Red bar (below 100% of estimate)
       */
      red = this.hours > green + yellow + orange ? this.hours - green - yellow - orange : 0;
      
      if (red > 0)
         return "0,0,0," + this.hours;
      else if (orange > 0)
         return "0,0," + this.hours + ",0";
      else if (yellow > 0)
         return "0," + this.hours + ",00,";
      else
         return this.hours + ",0,0,0";
   }
   
   public String getMultipleProgress() {
      Integer green, yellow, orange, red;

      /*
       * Green bar (60% of estimate)
       */
      green = Math.round(Math.min(this.estimate - this.estimate * 0.3f - this.estimate * 0.1f, (float) this.hours));
      
      /*
       * Yellow bar (30% of estimate)
       */
      yellow = Math.round(Math.min(this.estimate * 0.3f, (float) this.hours - green));
      
      /*
       * Orange bar (10% of estimate)
       */
      orange = Math.round(Math.min(this.estimate * 0.1f, (float) this.hours - green - yellow));
      
      /*
       * Red bar (below 100% of estimate)
       */
      red = this.hours > green + yellow + orange ? this.hours - green - yellow - orange : 0;
      
      return green.toString() + "," + yellow.toString() + "," + orange.toString() + "," + red.toString();
   }
   
   public int getProgressTotal() {
      return this.hours > 0 ? this.hours + this.etc : this.estimate;
   }

   public WorkItem clone() {
      return (WorkItem) SerializationUtils.clone(this);
   }
}