package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_EPICS_HISTORY)
public class EpicHistory implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "epic", nullable = false)
   private Epic epic;

   @Getter
   @Setter
   @Size(min = 0, max = 120)
   @Column(name = "name", length = 120, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @Size(min = 11, max = 11)
   @Column(name = "number", length = 11, nullable = false)
   private String number;
   
   @Getter
   @Setter
   @Column(name = "description", columnDefinition="CLOB", nullable = true)
   private String description;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "type", nullable = true)
   private TypeOfWorkItem type;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "status", nullable = false)
   private WorkItemStatus status;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "epic")   
   private List<UserStory> userStories;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "team", nullable = true)
   private Team team;
   
   @Getter
   @Setter
   @Transient
   private Integer storyPoints = 0;
   
   @Getter
   @Setter
   @Transient
   private Integer hours = 0;
   
   @Getter
   @Setter
   @Column(name = "closed", nullable = true)
   private Date closed;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "closedby", nullable = true)
   private Resource closedBy;
   
   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;
   
   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   public EpicHistory(int id) {
      super();

      this.id = id;
   }

   public EpicHistory(Epic epic) {
      super();
      
      this.epic = epic;
      this.number = epic.getNumber();
      this.name = epic.getName();
      this.description =  epic.getDescription();
      this.type = epic.getType();
      this.status = epic.getStatus();
      this.userStories = epic.getUserStories();
      this.team = epic.getTeam();
      this.hours = epic.getHours();
      this.storyPoints = epic.getStoryPoints();
      this.closed = epic.getClosed();
      this.closedBy = epic.getCreatedBy();
      this.created = epic.getCreated();
      this.createdBy = epic.getCreatedBy();
      this.lastUpdate = epic.getLastUpdate();
      this.lastUpdateBy = epic.getLastUpdateBy();
   }  
}