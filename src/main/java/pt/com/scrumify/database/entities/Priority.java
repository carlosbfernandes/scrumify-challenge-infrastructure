package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.SerializationUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_PRIORITIES)
public class Priority implements Serializable {
   private static final long serialVersionUID = 8139875669967902895L;

   @Id
   @Getter
   @Setter
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "number", nullable = false)
   private Integer number;
   
   @Getter
   @Setter
   @Column(name = "name", length = 20, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "sortorder", nullable = false)
   private Integer sortOrder;

   public Priority(Integer id) {
      super();
      this.id = id;
   }

   public Priority clone() {
      return (Priority) SerializationUtils.clone(this);
   }
}