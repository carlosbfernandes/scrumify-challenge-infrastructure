package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.apache.commons.lang3.SerializationUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_WORKITEM_WORKLOGS)
public class WorkItemWorkLog implements Serializable {
   private static final long serialVersionUID = 4644510997697650440L;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "workitem", nullable = false)
   private WorkItem workItem;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "day", nullable = false)
   private Time day = new Time();

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "typeofwork", nullable = false)
   private TypeOfWork typeOfWork;

   @Getter
   @Setter
   @Column(name = "timespent", nullable = false)
   private Integer timeSpent;

   @Getter
   @Setter
   @Column(name = "etc", nullable = false)
   private Integer etc;

   @Getter
   @Setter
   @Column(name = "description", columnDefinition="CLOB", nullable = true)
   private String description;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "timesheet", nullable = false)
   private Timesheet timesheet;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "activity", nullable = true)
   private Activity activity;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   public WorkItemWorkLog(Integer id) {
      super();
      
      this.id = id;
   }

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
   }

   public WorkItemWorkLog clone() {
      return (WorkItemWorkLog) SerializationUtils.clone(this);
   }
}