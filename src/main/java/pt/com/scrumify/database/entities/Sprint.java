package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_SPRINTS)
public class Sprint implements Serializable {
   private static final long serialVersionUID = 13109788275655282L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "name", length = 80, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @Column(name = "goal", length = 400, nullable = true)
   private String goal;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "team", nullable = false)
   private Team team;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.sprint")
   private List<SprintMember> members = new ArrayList<>(0);
   
   @Getter
   @Setter
   @ManyToMany
   @JoinTable(
     name = "sprinttasks", 
     joinColumns = @JoinColumn(name = "sprint"), 
     inverseJoinColumns = @JoinColumn(name = "workitem"))
   private List<WorkItem> tasks;
   
   @Getter
   @Setter
   @Column(name = "estimatedstart", nullable = false)
   private Date estimatedStart;
   
   @Getter
   @Setter
   @Column(name = "estimatedend", nullable = false)
   private Date estimatedEnd;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "startingday", nullable = true)
   private Time startingDay;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "endingday", nullable = true)
   private Time endingDay;
   
   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;   
      
   public Sprint(Integer id) {
      super();
      this.id = id;
   }
   
   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
   
   public List<Resource> getSprintMembers(){
      List<Resource> sprintMembers = new ArrayList<>();
      
      for (SprintMember member : this.getMembers()) {
         sprintMembers.add(member.getResource());
      }
      
      return sprintMembers;
   }
}