package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TYPES_OF_WORK)
public class TypeOfWork implements Serializable {
   private static final long serialVersionUID = 4491381646204501639L;
   
   public static final int ANALYSIS = 1;
   public static final int DESIGN = 2;
   public static final int DEVELOPMENT = 3;
   public static final int TEST = 4;
   public static final int OTHER = 5;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "abbreviation", length = 3, nullable = false)
   private String abbreviation;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "externalcode", length = 4, nullable = false)
   private String code;
}