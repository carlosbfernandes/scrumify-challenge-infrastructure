package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_WORKITEM_STATUS)
public class WorkItemStatus implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;
   
   public static final int NEW = 1;
   public static final int IN_PROGRESS = 3;
   public static final int CLOSED = 13;
   public static final int CANCELED = 10;
   public static final int DUPLICATED = 9;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;
   
   @Getter
   @Setter
   @Column(name = "abbreviation", length = 3, nullable = false)
   private String abbreviation;

   @Getter
   @Setter
   @Size(min = 0, max = 80)
   @Column(name = "name", length = 50, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @Column(name = "initialstatus", nullable = false)
   private boolean initialStatus = false;

   @Getter
   @Setter
   @Column(name = "finalstatus", nullable = false)
   private boolean finalStatus = false;

   @Getter
   @Setter
   @Column(name = "notify", nullable = false)
   private Boolean notify = false;

   @Getter
   @Setter
   @Column(name = "unassign", nullable = false)
   private Boolean unassign = false;
   
   @Getter
   @Setter
   @Column(name = "color", nullable = false)
   private String color;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
   private List<WorkItem> workItems = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
   private List<WorkflowTransition> transitions;

   @Getter
   @Setter
   @OneToMany(fetch = FetchType.EAGER, mappedBy = "nextStatus")
   private List<WorkflowTransition> nextTransitions;
   
   public WorkItemStatus(int id) {
      super();

      this.id = id;
   }
}