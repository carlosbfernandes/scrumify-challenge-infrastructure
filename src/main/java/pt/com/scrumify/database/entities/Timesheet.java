package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMESHEETS)
public class Timesheet implements Serializable {
   private static final long serialVersionUID = 13109788275655282L;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "resource", nullable = false)
   private Resource resource;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "startingday", nullable = false)
   private Time startingDay;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "endingday", nullable = false)
   private Time endingDay;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "status", nullable = true)
   private TimesheetStatus status;

   @Getter
   @Setter
   @Column(name = "submitted", nullable = false)
   private Boolean submitted;
   
   @Getter
   @Setter
   @Column(name = "reviewed", nullable = false)
   private Boolean reviewed;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "reviewedby", nullable = true)
   private Resource reviewedBy;
   
   @Getter
   @Setter
   @Column(name = "reviewdate", nullable = true)
   private Date reviewDate;
   
   @Getter
   @Setter
   @Column(name = "approved", nullable = false)
   private Boolean approved;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "approvedby", nullable = true)
   private Resource approvedBy;
   
   @Getter
   @Setter
   @Column(name = "approvaldate", nullable = true)
   private Date approvalDate;
   
   @Getter
   @Setter
   @Column(name = "comment", columnDefinition="CLOB", nullable = true)
   private String comment;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "commentedby", nullable = true)
   private Resource commentedBy;
   
   @Getter
   @Setter
   @Transient
   private Integer month;
   
   @Getter
   @Setter
   @Transient
   private Integer year;

   @Transient
   public Integer getOccupation() {
      int total = this.getDays().stream().mapToInt(TimesheetDay::getHours).sum();
      int sum = this.getDays().stream().mapToInt(TimesheetDay::getHoursAbsence).sum() + 
                this.getDays().stream().mapToInt(TimesheetDay::getHoursCompensationDay).sum() + 
                this.getDays().stream().mapToInt(TimesheetDay::getHoursHoliday).sum() +
                this.getDays().stream().mapToInt(TimesheetDay::getHoursVacation).sum() + 
                this.getDays().stream().mapToInt(TimesheetDay::getTimeSpent).sum();
      
      return (int) Math.round(sum * 100.0/ total);
   }

   @Transient
   public Integer getApprovalOccupation() {
      int total = this.getDays().stream().mapToInt(TimesheetDay::getHours).sum();
      int sum = this.getApprovals().stream().mapToInt(TimesheetApproval::getTotal).sum();
      
      return (int) Math.round(sum * 100.0 / total);
   }
   
   @Getter
   @Setter
   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "timesheet")
   private List<TimesheetDay> days = new ArrayList<>();
   
   @Getter
   @Setter
   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "timesheet")
   private List<TimesheetApproval> approvals = new ArrayList<>();
   
   @Getter
   @Setter
   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "timesheet")
   private List<TimesheetReview> reviews = new ArrayList<>();
   
   @Getter
   @Setter
   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "timesheet")
   private List<WorkItemWorkLog> workLogs = new ArrayList<>();
}