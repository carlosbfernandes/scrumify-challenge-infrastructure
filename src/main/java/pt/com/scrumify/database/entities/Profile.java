package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_PROFILES)
public class Profile implements Serializable {
   private static final long serialVersionUID = 1504309949387115127L;

   public static final Integer ANONYMOUS = 1;
   public static final Integer ADMINISTRATORS = 2;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @OneToMany(mappedBy = "profile")
   private List<Resource> resources = new ArrayList<>(0);

   @Getter
   @Setter
   @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
   @JoinTable(name = ConstantsHelper.DATABASE_TABLE_PROFILES_ROLES, 
              joinColumns = @JoinColumn(name = "profile", referencedColumnName = "id", nullable = false, updatable = false), 
              inverseJoinColumns = @JoinColumn(name = "role", referencedColumnName = "id", nullable = false, updatable = false))
   private List<Role> roles = new ArrayList<>(0);

   @Getter
   @Setter
   @Column(name = "impersonable", nullable = false)
   private boolean impersonable = false;

   @Getter
   @Setter
   @Column(name = "system", nullable = false)
   private boolean system = false;

   public Profile(int id) {
      super();

      this.id = id;
   }
}