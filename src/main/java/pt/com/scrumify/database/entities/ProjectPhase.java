package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.SerializationUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_PROJECTPHASES)
public class ProjectPhase implements Serializable {
   private static final long serialVersionUID = -7099989938505035076L;

   @Id
   @Getter
   @Setter
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "number", length = 10, nullable = false)
   private String number;
   
   @Getter
   @Setter
   @Column(name = "name", length = 30, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "technicalassistance", nullable = false)
   private boolean technicalAssistance;
   
   @Getter
   @Setter
   @Column(name = "sortorder", nullable = false)
   private Integer sortOrder;

   public ProjectPhase clone() {
      return (ProjectPhase) SerializationUtils.clone(this);
   }
}