package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@NoArgsConstructor
public class ApplicationPropertyPK implements Serializable {
   private static final long serialVersionUID = -7225587025694040355L;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Application application = new Application();
   
   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Property property = new Property();
   
   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Environment environment = new Environment();

   public ApplicationPropertyPK(Application application, Environment environment, Property property) {
      super();
    
      this.application = application;
      this.environment  = environment;
      this.property = property;
   }
}