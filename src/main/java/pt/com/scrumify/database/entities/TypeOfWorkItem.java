package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TYPES_OF_WORKITEMS)
public class TypeOfWorkItem implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   public static final int EPIC = 1;
   public static final int USERSTORY = 2;
   public static final int TASK = 3;
   public static final int BUG = 4;
   public static final int INCIDENT = 5;
   public static final int REQUEST = 6;
   
   @Id
   @Getter
   @Setter
   @Column(name = "id", nullable = false)
   private Integer id;
   
   @Getter
   @Setter
   @Column(name = "abbreviation", length = 5, nullable = false)
   private String abbreviation;

   @Getter
   @Setter
   @Size(min = 0, max = 80)
   @Column(name = "name", length = 50, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "role", nullable = false)
   private Role role;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "workflow", nullable = false)
   private Workflow workflow;
   
   @Getter
   @Setter
   @Column(name = "workitem", nullable = false)
   private Boolean workItem;
   
   @Getter
   @Setter
   @Column(name = "estimate", nullable = false)
   private boolean estimate;
   
   @Getter
   @Setter
   @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
   @JoinTable(name = ConstantsHelper.DATABASE_TABLE_TYPES_OF_WORKITEMS_TYPES_OF_NOTES, 
              joinColumns = @JoinColumn(name = "typeofworkitem", referencedColumnName = "id", nullable = false, updatable = false), 
              inverseJoinColumns = @JoinColumn(name = "typeofnote", referencedColumnName = "id", nullable = false, updatable = false))
   private List<TypeOfNote> typesOfNotes;
   
   @Getter
   @Setter
   @Transient
   private List<WorkItemStatus> statuses;

   public TypeOfWorkItem(int id) {
      super();

      this.id = id;
   }
}