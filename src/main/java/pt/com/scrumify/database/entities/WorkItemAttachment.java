package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_WORKITEM_ATTACHMENTS)
public class WorkItemAttachment implements Serializable {
	private static final long serialVersionUID = -5922365204452151662L;

	@Id
   @Getter
   @Setter
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

   @Getter
   @Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workitem", nullable = false)
	private WorkItem workItem;

   @Getter
   @Setter
	@Column(name = "attachment", length = 400, nullable = false)
	private String attachment;

   @Getter
   @Setter
	@Column(name = "name", length = 100, nullable = false)
	private String name;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;
   @Getter
   @Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdby", nullable = false)
	private Resource createdBy;

	@PrePersist
	@PreUpdate
	public void onInsert() {
		this.created = DatesHelper.now();
		this.createdBy = ResourcesHelper.getResource();
	}
}