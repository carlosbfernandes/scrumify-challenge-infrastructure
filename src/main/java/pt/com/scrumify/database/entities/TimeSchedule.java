package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMES_SCHEDULES)
@AssociationOverrides({
   @AssociationOverride(name = "pk.time", joinColumns = @JoinColumn(name = "time")),
   @AssociationOverride(name = "pk.schedule", joinColumns = @JoinColumn(name = "schedule"))
})
public class TimeSchedule implements Serializable {
   private static final long serialVersionUID = 2927494291672664391L;

   @Getter
   @Setter
   @EmbeddedId
   private TimeSchedulePK pk = new TimeSchedulePK();

   @Getter
   @Setter
   @Column(name = "hours", nullable = false)
   private Integer hours;

   @Transient
   public Schedule getSchedule() {
      return this.pk.getSchedule();
   }
   
   public void setSchedule(Schedule schedule) {
      this.pk.setSchedule(schedule);
   }

   @Transient
   public Time getTime() {
      return this.pk.getTime();
   }
   
   public void setTime(Time time) {
      this.pk.setTime(time);
   }
}