package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TYPES_OF_NOTES)
public class TypeOfNote implements Serializable {
   private static final long serialVersionUID = 6240998602432905911L;
   
   public static final int ACCEPTANCE_CRITERIA = 1;
   public static final int ASSUMPTION = 2;
   public static final int COMMENT = 3;
   public static final int DESIGN_CONSIDERATION = 4;
   public static final int IMPEDIMENT = 5;
   public static final int REPRODUCTION_STEPS = 6;
   public static final int RESOLUTION = 7;
   public static final int TECHNICAL_RISK = 8;
   public static final int EMAIL = 9;
   public static final int STATUS_REPORT = 10;

   @Id
   @Getter
   @Setter
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @ManyToMany(mappedBy="typesOfNotes")
   private List<TypeOfWorkItem> typesOfWorkItem;
   
   public TypeOfNote(int id) {
      super();
      
      this.id = id;
   }
}