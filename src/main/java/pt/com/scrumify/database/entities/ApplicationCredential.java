package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_APPLICATION_CREDENTIALS)
public class ApplicationCredential implements Serializable {
   private static final long serialVersionUID = -5204362865184421535L;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "type", nullable = false)
   private Integer type;
   
   @Getter
   @Setter
   @Column(name = "hostname", nullable = false)
   private String hostname;
   
   @Getter
   @Setter
   @Column(name = "port", nullable = false)
   private String port;
   
   @Getter
   @Setter
   @Column(name = "username", nullable = false)
   private String username;
   
   @Getter
   @Setter
   @Column(name = "password", nullable = false)
   private String password;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "application", nullable = false)
   private Application application;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "environment", nullable = false)
   private Environment environment;

   public ApplicationCredential(Integer id) {
      this.id = id;
   }
   
   public String getConnection() {
      String result = "";
      
      switch (this.getType()) {
         case 1:
            result = "ftp://" + this.getUsername() + "@" + this.getHostname() + (this.getPort() != "" ? ":" + this.getPort() : "");
            break;
         case 2:
            result = this.getUsername() + "@" + this.getHostname() + (this.getPort() != "" ? " -p " + this.getPort() : "");
            break;
         default:
            break;
      }
      return result;
   }
}