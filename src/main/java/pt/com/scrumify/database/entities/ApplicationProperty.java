package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_APPLICATION_PROPERTIES)
@AssociationOverrides({
   @AssociationOverride(name = "pk.application", joinColumns = @JoinColumn(name = "application")),
   @AssociationOverride(name = "pk.environment", joinColumns = @JoinColumn(name = "environment")),
   @AssociationOverride(name = "pk.property", joinColumns = @JoinColumn(name = "property"))
})
public class ApplicationProperty implements Serializable {
   private static final long serialVersionUID = -5204362865184421535L;

   @EmbeddedId
   @Getter
   @Setter
   private ApplicationPropertyPK pk = new ApplicationPropertyPK();

   @Getter
   @Setter
   @Column(name = "value", nullable = false)
   private String value;

   @Transient
   public Application getApplication() {
      return this.pk.getApplication();
   }
   
   public void setApplication(Application application) {
      this.pk.setApplication(application);
   }
   
   @Transient
   public Environment getEnvironment() {
      return this.pk.getEnvironment();
   }
   
   public void setEnvironment(Environment environment) {
      this.pk.setEnvironment(environment);
   }

   @Transient
   public Property getProperty() {
      return this.pk.getProperty();
   }
   
   public void setProperty(Property property) {
      this.pk.setProperty(property);
   }
}