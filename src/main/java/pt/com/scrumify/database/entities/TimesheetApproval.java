package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMESHEETS_APPROVAL)
public class TimesheetApproval implements Serializable {
   private static final long serialVersionUID = 13109788275655282L;

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Getter
   @Setter
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "type", nullable = true)
   private TypeOfTimesheetApproval type;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "resource", nullable = false)
   private Resource resource;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "contract")
   private Contract contract;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "year", nullable = false)
   private Year year;

   @Getter
   @Setter
   @Column(name = "month", nullable = false)
   private Integer month;

   @Getter
   @Setter
   @Column(name = "fortnight", nullable = false)
   private Integer fortnight;

   @Getter
   @Setter
   @Column(name = "code", nullable = false)
   private String code;

   @Getter
   @Setter
   @Column(name = "description", length = 120, nullable = false)
   private String description;

   @Getter
   @Setter
   @Column(name = "analysis", nullable = false)
   private Integer analysis = 0;

   @Getter
   @Setter
   @Column(name = "design", nullable = false)
   private Integer design = 0;

   @Getter
   @Setter
   @Column(name = "development", nullable = false)
   private Integer development = 0;

   @Getter
   @Setter
   @Column(name = "test", nullable = false)
   private Integer test = 0;

   @Getter
   @Setter
   @Column(name = "other", nullable = false)
   private Integer other = 0;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "timesheet", nullable = false)
   private Timesheet timesheet;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
   
   public TimesheetApproval(Resource resource, Year year, Integer month, Integer fortnight) {
      super();
      this.resource = resource;
      this.year = year;
      this.month = month;
      this.fortnight = fortnight;
      this.code = ""; 
      this.description = ""; 
   }
   
   public TimesheetApproval(Contract contract, Year year, int month, long analysis, long design, long development, long test, long other) {
      super();
      
      this.contract = contract;
      this.year = year;
      this.month = month;
      this.analysis = (int) analysis;
      this.design = (int) design;
      this.development = (int) development;
      this.test = (int) test;
      this.other = (int) other;
   }
   
   public TimesheetApproval(TimesheetReview review) {
      super();
      
      this.analysis = review.getAnalysis();
      this.code = review.getCode();
      this.contract = review.getContract();
      this.description = review.getDescription();
      this.design = review.getDesign();
      this.development = review.getDevelopment();
      this.fortnight = review.getFortnight();
      this.month = review.getMonth();
      this.other = review.getOther();
      this.resource = review.getResource();
      this.test = review.getTest();
      this.timesheet = review.getTimesheet();
      this.type = new TypeOfTimesheetApproval(TypeOfTimesheetApproval.HOURS);
      this.year = review.getYear();
   }

   public TimesheetApproval(Resource resource, Timesheet timesheet, TypeOfTimesheetApproval type, Year year, Integer month, Integer fortnight) {
      this(null, resource, timesheet, type, year, month, fortnight, "", "", 0, 0, 0, 0, 0);
   }
   
   public TimesheetApproval(Contract contract, Resource resource, Timesheet timesheet, TypeOfTimesheetApproval type, Year year, Integer month, Integer fortnight, String code, String description, Integer analysis, Integer design, Integer development, Integer test, Integer other) {
      super();
      
      this.analysis = analysis;
      this.code = code;
      this.contract = contract;
      this.description = description;
      this.design = design;
      this.development = development;
      this.fortnight = fortnight;
      this.month = month;
      this.other = other;
      this.resource = resource;
      this.test = test;
      this.timesheet = timesheet;
      this.type = type;
      this.year = year;
   }
   
   @Transient
   public int getAbsences() {
      if (this.code.startsWith("99-") && !this.description.equals("Compensation Days") && !this.description.equals("Holidays") && !this.description.equals("Vacations")) {
         return this.other;
      }
      
      return 0;
   }
   
   @Transient
   public int getCompensationDays() {
      if (this.description.equals("Compensation Days")) {
         return this.other;
      }
      
      return 0;
   }
   
   @Transient
   public int getHolidays() {
      if (this.description.equals("Holidays")) {
         return this.other;
      }
      
      return 0;
   }
   
   @Transient
   public int getVacations() {
      if (this.description.equals("Vacations")) {
         return this.other;
      }
      
      return 0;
   }
   
   @Transient
   public int getNonWorkingHours() {
      if (this.code != null && this.code.startsWith("99-")) {
         return this.analysis + this.design + this.development + this.test + this.other;
      }
      
      return 0;
   }
   
   @Transient
   public int getWorkingHours() {
      if (this.code != null && !this.code.startsWith("99-")) {
         return this.analysis + this.design + this.development + this.test + this.other;
      }
      
      return 0;
   }
   
   @Transient
   public int getTotal() {
      return this.analysis + this.design + this.development + this.test + this.other;
   }
}