package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_YEARS)
public class Year implements Serializable {
   private static final long serialVersionUID = 13109788275655282L;

   @Id
   @Getter
   @Setter   
   @Column(name = "id", nullable = false)
   private Integer id;
   
   @Getter
   @Setter   
   @Column(name = "active", nullable = false)
   private boolean active;
   
   public Year(Integer id) {
      super();

      this.id = id;
   }
}