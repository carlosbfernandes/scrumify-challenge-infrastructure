package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@Indexed
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_EPICS)
public class Epic implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;
   
   @Getter
   @Setter
   @Field
   @Column(name = "name", length = 120, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @Field
   @Column(name = "number", length = 11, nullable = false)
   private String number;
   
   @Getter
   @Setter
   @Field
   @Column(name = "description", columnDefinition="CLOB", nullable = true)
   private String description;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "type", nullable = true)
   private TypeOfWorkItem type;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "status", nullable = false)
   private WorkItemStatus status;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "epic")   
   private List<UserStory> userStories;
   
   @Getter
   @Setter
   @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
   @JoinTable(name = "epicstags", 
              joinColumns = @JoinColumn(name = "epic"), 
              inverseJoinColumns = @JoinColumn(name = "tag"))
   private List<Tag> tags = new ArrayList<>();
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "team", nullable = true)
   private Team team;
   
   @Getter
   @Setter
   @OneToMany(mappedBy = "epic")
   private List<EpicNote> notes;
   
   @Getter
   @Setter
   @Transient
   private Integer storyPoints = 0;
   
   @Getter
   @Setter
   @Transient
   private Integer hours = 0;
   
   @Getter
   @Setter
   @Column(name = "closed", nullable = true)
   private Date closed;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "closedby", nullable = true)
   private Resource closedBy;
   
   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;
   
   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;
   
   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
   
   public void addTag(Tag tag) {
       tags.add(tag);
//       tag.getEpics().add(this);
   }

   public void removeTag(Tag tag) {
       tags.remove(tag);
//       tag.getEpics().remove(this);
   }
}