package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_WORKITEMS_HISTORY)
public class WorkItemHistory implements Serializable {
   private static final long serialVersionUID = -6526732806482173774L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
   @JoinColumn(name = "workitem", nullable = false)
   private WorkItem workItem;

   @Getter
   @Setter
   @Column(name = "number", length = 15, nullable = false)
   private String number;

   @Getter
   @Setter
   @Column(name = "name", length = 80, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "description", columnDefinition="CLOB", nullable = false)
   private String description;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "contract", nullable = false)
   private Contract contract;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "type", nullable = false)
   private TypeOfWorkItem type;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "status", nullable = false)
   private WorkItemStatus status;
   
   @Getter
   @Setter
   @Column(name = "estimate", nullable = false)
   private Integer estimate;
   
   @Getter
   @Setter   
   @Column(name = "hours", nullable = false)
   private Integer hours;
   
   @Getter
   @Setter
   @Column(name = "etc", nullable = false)
   private Integer etc;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "application", nullable = true)
   private Application application;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "userstory", nullable = true)
   private UserStory userStory;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "team", nullable = true)
   private Team team;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "release", nullable = true)
   private Release release;

   @Getter
   @Setter
   @Column(name = "assigned", nullable = true)
   private Date assigned;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "assignedto", nullable = true)
   private Resource assignedTo;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "priority", nullable = true)
   private Priority priority;

   @Getter
   @Setter
   @Column(name = "closed", nullable = true)
   private Date closed;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "closedby", nullable = true)
   private Resource closedBy;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   public WorkItemHistory(WorkItem workItem) {
   	super();
   	
   	this.id = 0;
   	this.workItem = workItem;
   	this.number = workItem.getNumber();
   	this.name = workItem.getName();
   	this.description =  workItem.getDescription();
      this.contract = workItem.getContract();
   	this.type =  workItem.getType();
   	this.status = workItem.getStatus();
   	this.estimate = workItem.getEstimate();
   	this.hours = workItem.getHours();
   	this.etc = workItem.getEtc();
   	this.application = workItem.getApplication();
   	this.team = workItem.getTeam();
   	this.userStory = workItem.getUserStory();
      this.assigned = workItem.getAssigned();
   	this.assignedTo = workItem.getAssignedTo();
   	this.priority = workItem.getPriority();
      this.closed = workItem.getClosed();
   	this.closedBy = workItem.getClosedBy();
      this.created = workItem.getCreated();
      this.createdBy = workItem.getCreatedBy();
      this.lastUpdate = workItem.getLastUpdate();
      this.lastUpdateBy = workItem.getLastUpdateBy();
   }
}