package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_CONTRACTS)
public class Contract implements Serializable {
   private static final long serialVersionUID = 2336412266077928729L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "year", nullable = false)
   private Year year;

   @Getter
   @Setter
   @Column(name = "name", length = 120, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @Column(name = "description", length = 400, nullable = true)
   private String description;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "startingday", nullable = false)
   private Time startingDay = new Time();

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "endingday", nullable = true)
   private Time endingDay = new Time();

   @Getter
   @Setter
   @Column(name = "hours", nullable = false)
   private Integer hours = 0;

   @Getter
   @Setter
   @Column(name = "hours1styear", nullable = false)
   private Integer hours1stYear = 0;

   @Getter
   @Setter
   @Column(name = "hours2ndyear", nullable = false)
   private Integer hours2ndYear = 0;
   
   @Getter
   @Setter
   @Column(name = "externalcode", length = 6, nullable = true)
   private String code;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "status", nullable = false)
   private ContractStatus status;

   @Getter
   @Setter
   @Column(name = "technicalassistance", nullable = false)
   private Boolean technicalAssistance;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "subarea", nullable = false)
   private SubArea subArea;

   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.contract")
   private List<ContractProperty> properties = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.contract")
   private List<TeamContract> teamContracts = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "contract")
   private List<WorkItem> workItems = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "contract")
   private List<TimesheetApproval> billable = new ArrayList<>(0);
   
   @Getter
   @Setter
   @Transient
   private Integer timeSpent;

   @Transient
   public int getBilledHours() {
      return this.billable.stream().mapToInt(TimesheetApproval::getWorkingHours).sum();
   }
   
   @Getter
   @Setter
   @Transient
   private Integer percentageCompleted;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   public Contract(Integer id) {
      super();
      this.id = id;
   }

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

      
   public void addProperty(ContractProperty contractProperty) {
      this.properties.add(contractProperty);
   }
   
   public String getProgress() {
      Integer green, yellow, orange, red;

      /*
       * Green bar (60% of estimate)
       */
      green = Math.round(Math.min(this.hours - this.hours * 0.3f - this.hours * 0.1f, (float) this.timeSpent));
      
      /*
       * Yellow bar (30% of estimate)
       */
      yellow = Math.round(Math.min(this.hours * 0.3f, (float) this.timeSpent - green));
      
      /*
       * Orange bar (10% of estimate)
       */
      orange = Math.round(Math.min(this.hours * 0.1f, (float) this.timeSpent - green - yellow));
      
      /*
       * Red bar (below 100% of estimate)
       */
      red = this.timeSpent > green + yellow + orange ? this.timeSpent - green - yellow - orange : 0;
      
      if (red > 0)
         return "0,0,0," + this.timeSpent;
      else if (orange > 0)
         return "0,0," + this.timeSpent + ",0";
      else if (yellow > 0)
         return "0," + this.timeSpent + ",00,";
      else
         return this.timeSpent + ",0,0,0";
   }
   
   public int getProgressTotal() {
      return this.hours;
   }
   
   public List<Team> getTeams(){
      List<Team> teams = new ArrayList<>();
      for(TeamContract teamContract : this.teamContracts)
         teams.add(teamContract.getTeam());     
      return teams;
   }
}