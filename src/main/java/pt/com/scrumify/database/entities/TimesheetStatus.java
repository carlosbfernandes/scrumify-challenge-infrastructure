package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMESHEET_STATUS)
public class TimesheetStatus implements Serializable {
   private static final long serialVersionUID = -7898327388413741201L;
   
   public static final int NOT_SUBMITTED = 1;
   public static final int SUBMITTED = 2;
   public static final int REVIEWED = 3;
   public static final int APPROVED = 4;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Size(min = 0, max = 30)
   @Column(name = "name", length = 30, nullable = false)
   private String name;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
   private List<Timesheet> timesheets = new ArrayList<>(0);
   
   public TimesheetStatus(int id) {
      super();

      this.id = id;
   }
}