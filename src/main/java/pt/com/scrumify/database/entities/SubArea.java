package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_SUBAREAS)
public class SubArea implements Serializable {
   private static final long serialVersionUID = -7437028217521611228L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "abbreviation", length = 5, nullable = false)
   private String abbreviation;

   @Getter
   @Setter
   @Column(name = "name", length = 80, nullable = false)
   private String name;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "area", nullable = false)
   private Area area;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "subArea")
   private List<Contract> contracts = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "subArea")
   private List<Team> teams = new ArrayList<>(0);

   public SubArea(int id) {
      super();
      
      this.id = id;
   }
}