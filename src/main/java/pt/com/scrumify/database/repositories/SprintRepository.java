package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.Sprint;

public interface SprintRepository extends JpaRepository<Sprint, Integer> {

}