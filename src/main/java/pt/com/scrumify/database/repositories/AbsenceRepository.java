package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;


public interface AbsenceRepository extends JpaRepository<Absence, Integer> {
	
   List<Absence> findByResourceAndDayYearAndDayMonthOrderByDayDateAsc(Resource resource, Year year, Integer month);
   
   List<Absence> findByResourceIdOrderByDayDateAsc(Integer id);
   
   List<Absence> findByDayDateBetweenAndResourceIn(Date start, Date end, List<Resource> resources);
   
   List<Absence> findByResourceAndDay(Resource resource, Time day);
   
   Absence findByIdAndResource(Integer id, Resource resource);
   
   @Query(nativeQuery = false, 
           value = "SELECT new Absence(ab.type, sum(ab.hours)) " + 
                   "FROM Absence ab " + 
                   "WHERE ab.resource = :resource " + 
                   "AND ab.day >= :startDate " + 
                   "AND ab.day <= :endDate " + 
                   "GROUP BY ab.type ")
   List<Absence> listByResourceAndTimesheetOrderByDate(@Param("resource") Resource resource, @Param("startDate") Time startDate, @Param("endDate") Time endDate);
   
   @Query(nativeQuery = false, 
         value = "SELECT SUM(ab.hours) " + 
                 "FROM Absence ab " + 
                 "WHERE ab.resource = :resource " + 
                 "AND ab.day.year.id = :year " + 
                 "AND ab.day.month = :month " + 
                 "AND ab.day.fortnight = :fortnight " )
   Long hoursByResourceCurrentFortnight(@Param("resource") Resource resource, @Param("year") Integer year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);
   
   @Query(value = "SELECT a " + 
                  "FROM Absence a " + 
                  "WHERE a.resource = :resource " + 
                  "AND a.day.year = :year " + 
                  "AND a.day.month = :month " + 
                  "AND a.day.fortnight = :fortnight ")
   List<Absence> getByTimesheet(@Param("resource") Resource resource, @Param("year") Year year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);
   
}