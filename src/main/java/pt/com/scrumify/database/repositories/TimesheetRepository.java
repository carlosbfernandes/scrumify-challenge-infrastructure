package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.entities.TimeReportView;

public interface TimesheetRepository extends JpaRepository<Timesheet, Integer> {
   
   @Query("SELECT sheet FROM Timesheet sheet  " +
         "WHERE sheet.resource = :resource " +
         "AND  sheet.startingDay.year = :year " +
         "AND  sheet.startingDay.month = :month " +
         "GROUP BY sheet.id, sheet.startingDay.fortnight " + 
         "ORDER BY  sheet.startingDay.fortnight ")
   List<Timesheet> findByYearAndMonth(@Param("resource") Resource resource, @Param("year") Year year, @Param("month") Integer month);
   
   @Query("SELECT sheet FROM Timesheet sheet  " +
         "WHERE sheet.resource = :resource " +
         "AND  sheet.commentedBy != NULL " +
         "AND  sheet.startingDay.year = :year " +
         "AND  sheet.startingDay.month = :month " +
         "GROUP BY sheet.id, sheet.startingDay.fortnight " + 
         "ORDER BY  sheet.startingDay.fortnight ")
   List<Timesheet> findByYearMonthAndComment(@Param("resource") Resource resource, @Param("year") Year year, @Param("month") Integer month);
   
   @Query("SELECT sheet FROM Timesheet sheet " +
         "WHERE sheet.resource = :resource " +
         "AND  sheet.startingDay.year.id = :year " +
         "AND  sheet.startingDay.month = :month " +
         "AND  sheet.startingDay.day <= :day " +
         "AND  sheet.endingDay.day >= :day ")
   Timesheet findByToday(@Param("resource") Resource resource, @Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);
   
   @Query("SELECT ts " +
          "FROM Timesheet ts " + 
          "JOIN ts.resource r " +
          "JOIN ts.startingDay t " +
          "WHERE t.year.id = :year " + 
          "AND t.month = :month " +
          "AND (t.fortnight = :fortnight OR :fortnight = NULL) " +
          "AND ts.resource IN (:resources) " + 
          "ORDER BY r.name, t.fortnight")
   List<Timesheet> getByResourcesYearMonthAndFortnight(@Param("resources") List<Resource> resources, @Param("year") Integer year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);
   
   @Query(value = "SELECT new " + 
           "pt.com.scrumify.entities.TimeReportView(wlog.day, s.abbreviation, c.name, w.name, SUM(wlog.timeSpent)) " +
           "FROM WorkItemWorkLog as wlog " +
           "INNER JOIN wlog.workItem as w " +
           "INNER JOIN w.contract as c " +
           "INNER JOIN c.subArea as s " +
           "WHERE w.assignedTo = :resource " +
           "AND wlog.day >= :startDate " +
           "AND wlog.day <= :endDate " +
           "GROUP BY wlog.day, s.abbreviation, c.name, w.id " +
           "ORDER BY wlog.day ASC")
     List<TimeReportView> findWorkLogsOfTimeSheet(@Param("resource") Resource resource, @Param("startDate") Time startDate, @Param("endDate") Time endDate);
   
   @Query("SELECT sheet FROM Timesheet sheet  " +
         "WHERE sheet.startingDay.year = :year " +
         "GROUP BY sheet.id, sheet.resource ")
   List<Timesheet> findByYear(@Param("year") Year year);
   
   @Query("SELECT MAX(sheet.endingDay.date) FROM Timesheet sheet  " +
         "WHERE sheet.resource = :resource " +
         "AND (sheet.approved = true OR sheet.reviewed = true) ")
   Date findTimeofLateTimeSheetAprrovedOrReviewed(@Param("resource") Resource resource); 
   
   List<Timesheet> findDistinctByResourceTeamMembersPkTeamInAndStartingDayMonthAndStartingDayYearIdAndStartingDayFortnightAndReviewedFalse(List<Team> teams,Integer month,Integer year,Integer fortnight);
   
   @Query("SELECT ts " +
          "FROM Timesheet ts " +
          "WHERE ts.resource = :resource " +
          "AND ts.startingDay.year.id = :year " +
          "AND ts.startingDay.month = :month " +
          "AND ts.startingDay.fortnight = :fortnight")
   Timesheet findByFortnight(@Param("resource") Resource resource, @Param("year") Integer year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);

   List<Timesheet> findByResourceIn(List<Resource> resources);
   
   @Query(value="SELECT t FROM Timesheet t "
               + "JOIN t.days d "
               + "WHERE d.day.date = :date "
               + "AND t.resource = :resource "
               + "AND t.submitted is true ")
   List<Timesheet> findTimesheetSubmittedForDay(@Param("resource") Resource resource, @Param("date") Date date);
}
