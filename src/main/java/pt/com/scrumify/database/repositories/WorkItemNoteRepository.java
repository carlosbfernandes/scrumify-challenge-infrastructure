package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.WorkItemNote;

public interface WorkItemNoteRepository extends JpaRepository<WorkItemNote, Integer> {
   
   @Query(value=
           "SELECT DISTINCT note FROM WorkItemNote note "
         + " JOIN note.workItem w "
         + " JOIN w.contract c "
         + " WHERE c = :contract "
         + " AND note.typeOfNote.id = :noteid "
         + " AND note.created between :startday and :endday ")
   public List<WorkItemNote> getStatusReports(@Param("contract") Contract contract, @Param("noteid") Integer noteid, @Param("startday") Date startday, @Param("endday") Date endday);
   
   @Query(value=
           "SELECT DISTINCT note FROM WorkItemNote note "
         + " JOIN note.workItem w "
         + " JOIN w.contract c "
         + " WHERE c = :contract "
         + " AND note.typeOfNote.id = :noteid "
         + " AND note.createdBy = :resource "
         + " AND note.created between :startday and :endday ")
   public List<WorkItemNote> getStatusReportsByResource(@Param("contract") Contract contract, @Param("noteid") Integer noteid, @Param("resource") Resource resource, @Param("startday") Date startday, @Param("endday") Date endday);

}