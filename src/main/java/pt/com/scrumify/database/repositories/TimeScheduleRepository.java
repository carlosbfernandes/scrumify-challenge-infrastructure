package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimeSchedule;
import pt.com.scrumify.database.entities.Year;

public interface TimeScheduleRepository extends JpaRepository<TimeSchedule, String> {

   @Query("SELECT s " + 
          "FROM Time t " +
          "JOIN t.timesSchedules s " +
          "WHERE t.year = :year " +
          "AND t.month = :month " +
          "AND t.fortnight = :fortnight " +
          "AND s.pk.schedule = :schedule " +
          "ORDER BY t.date")
   List<TimeSchedule> getByYearMonthAndFortnight(@Param("year") Year year, @Param("month") Integer month, @Param("fortnight") Integer fortnight, @Param("schedule") Schedule schedule);

   TimeSchedule findByPkTimeAndPkSchedule(Time day, Schedule schedule);
}