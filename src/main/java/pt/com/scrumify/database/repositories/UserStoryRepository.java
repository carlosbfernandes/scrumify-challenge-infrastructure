package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.UserStory;

public interface UserStoryRepository extends JpaRepository<UserStory, Integer> {
   
   @Query(nativeQuery = false,
         value = "SELECT ust " +
                 "FROM UserStory ust " +
                 "INNER JOIN ust.team t " +
                 "INNER JOIN t.members tm " +
                 "WHERE tm.pk.resource = :resource ")
   List<UserStory> getByMember(@Param("resource") Resource resource);
   
   @Query(value="SELECT us " +
                "FROM UserStory us " +
                "JOIN FETCH us.workItems " +
                "JOIN us.team t " +
                "WHERE t = :team")
   List<UserStory> findByTeam(@Param("team") Team team);

   @Query(value="SELECT us " +
                "FROM UserStory us " +
                "JOIN us.team t " +
                "WHERE t IN (:teams)")
   List<UserStory> findByTeams(List<Team> teams);
   List<UserStory> findByTeamId(Integer team);
   
   @Query(value="SELECT u FROM UserStory u "
         + "JOIN u.team t "
         + "WHERE t = :team "
         + "AND u.status.finalStatus is false ")
   List<UserStory> findByTeamAndNotFinalStatus(Team team);
   
   @Query(value= "SELECT DISTINCT u " +
                 "FROM UserStory u " +
                 "JOIN u.team t " + 
                 "JOIN t.members tm " +
                 "WHERE tm.pk.resource = :resource " +
                 "AND u.status.id in (:statuses)")
   List<UserStory> findByStatusAndResource(@Param("resource") Resource resource, @Param("statuses") List<Integer> statuses);
   
   @Query(value="SELECT us " +
                "FROM UserStory us " +
                "WHERE us.team = :team " +
                "AND us.epic IS NULL " +
                "ORDER BY us.name")
   List<UserStory> getWithoutEpic(@Param("team") Team team);
   
   @Query(value="SELECT us " +
                "FROM UserStory us " +
                "LEFT JOIN us.workItems wi " +
                "WHERE wi.userStory IS NULL " +
                "AND us.team = :team")
   List<UserStory> getWithoutWorkItems(@Param("team") Team team);
   
}