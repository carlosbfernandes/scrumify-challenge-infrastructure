package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Release;

public interface ReleaseRepository extends JpaRepository<Release, Integer> {
   List<Release> getByApplication(Application application);
}