package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.entities.Year;


public interface VacationsRepository extends JpaRepository<Vacation, Integer> {
   
   Vacation findByIdAndResource(Integer id, Resource resource);
   
	@Query(nativeQuery = false,
		   value="SELECT v " + 
               "FROM Vacation v "	+ 
			      "INNER JOIN v.day t " +
               "WHERE t.year = :year " +
			      "AND t.month = :month " +
               "ORDER by t.date")
	List<Vacation> findByYearAndMonthOrderByDate(@Param("year") Year year, @Param("month") Integer month);
	
   @Query(nativeQuery = false, 
          value = "SELECT v " + 
                  "FROM Vacation v " + 
                  "INNER JOIN v.day t " + 
                  "INNER JOIN v.resource vr " + 
                  "WHERE t.year = :year " + 
                  "AND t.month = :month " + 
                  "AND vr = :resource " + 
                  "ORDER by t.date")
   List<Vacation> listByResourceYearAndMonthOrderByDate(@Param("resource") Resource resource, @Param("year") Year year, @Param("month") Integer month);
   

   @Query(nativeQuery = false, 
          value = "SELECT v " + 
                  "FROM Vacation v " + 
                  "INNER JOIN v.resource vr " + 
                  "INNER JOIN v.day t " + 
                  "WHERE vr.id = :id " + 
                  "ORDER by t.date")
   List<Vacation> listByResource(@Param("id") Integer id);
   
   @Query(nativeQuery = false, 
         value = "SELECT v " + 
                 "FROM Vacation v " + 
                 "INNER JOIN v.day t " + 
                 "INNER JOIN v.resource vr " + 
                 "WHERE t = :day " + 
                 "AND vr = :resource ")
  Vacation findByResourceDay(@Param("resource") Resource resource, @Param("day") Time day);

   @Query(nativeQuery = false, 
           value = "SELECT v " + 
                   "FROM Vacation v " + 
                   "WHERE v.resource = :resource " + 
                   "AND v.day >= :startDate " + 
                   "AND v.day <= :endDate " + 
                   "ORDER by v.day")
    List<Vacation> listByResourceAndTimesheetOrderByDate(@Param("resource") Resource resource, @Param("startDate") Time startDate, @Param("endDate") Time endDate);
   
   @Query( "SELECT sum(ts.hours) "
         + "FROM Vacation v "
         + "JOIN v.day.timesSchedules ts "
         + "WHERE v.resource = :resource "
         + "AND v.day.fortnight = :fortnight "
         + "AND v.day.month = :month "
         + "AND v.day.year.id = :year ")
   Long getHours(@Param("resource") Resource resource, @Param("year") Integer year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);
    
   @Query( "SELECT v "
         + "FROM Vacation v "
         + "WHERE v.resource = :resource "
         + "AND v.day.fortnight = :fortnight "
         + "AND v.day.month = :month "
         + "AND v.day.year = :year ")
   List<Vacation> getByTimesheet(@Param("resource") Resource resource, @Param("year") Year year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);
}