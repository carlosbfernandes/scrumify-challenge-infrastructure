package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.ScheduleSetting;

public interface ScheduleSettingRepository extends JpaRepository<ScheduleSetting, Integer> {
   
}