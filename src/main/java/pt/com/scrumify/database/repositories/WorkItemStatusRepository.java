package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Workflow;

public interface WorkItemStatusRepository extends JpaRepository<WorkItemStatus, Integer> {
   @Query(value= " SELECT status " +
                 " FROM WorkItemStatus status " + 
                 " INNER JOIN status.nextTransitions wft " + 
                 " WHERE wft.workflow = :workflow " + 
                 " AND wft.status = :status " + 
                 " ORDER BY wft.order ")
   List<WorkItemStatus> getByWorkflow(@Param("workflow") Workflow workflow, @Param("status") WorkItemStatus status);
   
   @Query(value= 
         " SELECT st " +
         " FROM TypeOfWorkItem type " + 
         " JOIN type.workflow.transitions t " +
         " JOIN t.status st " +
         " WHERE st.initialStatus is TRUE " + 
         " AND type.id = :idType ")
   WorkItemStatus findByTypeOfWorkItem(@Param("idType") Integer idType);
   
   @Query(value= 
         " SELECT distinct st " +
         " FROM TypeOfWorkItem type " + 
         " JOIN type.workflow.transitions t " +
         " INNER JOIN WorkItemStatus st " +
         " ON t.status = st.id or t.nextStatus = st.id " +
         " WHERE type.id = :idType " +
         " AND type.workItem is true " +
         " ORDER BY st.name ")
   List<WorkItemStatus> findByTypeOfWorkItemById(@Param("idType") Integer idType);
   
   @Query(value= 
         " SELECT DISTINCT st " +
         " FROM TypeOfWorkItem type " + 
         " JOIN type.workflow.transitions t " +
         " INNER JOIN WorkItemStatus st ON ((t.status = st.id AND type.id = :type) OR (t.nextStatus = st.id AND type.id = :type)) " +
         " AND type.workItem is :isWorkItem " +
         " ORDER BY st.name ")
   List<WorkItemStatus> findByTypeIdAndIsWorkItem(@Param("type") Integer type, @Param("isWorkItem") boolean isWorkItem);

   @Query(nativeQuery = false,
          value = "SELECT wis " +
                  "FROM WorkItemStatus wis " +
                  "WHERE wis.id IN :status")
   List<WorkItemStatus> getByIds(@Param("status") List<Integer> status);
}