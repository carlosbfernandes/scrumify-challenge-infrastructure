package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;

public interface AreaRepository extends JpaRepository<Area, Integer> {
   
   @Query(nativeQuery = false,
         value = "SELECT distinct(area) " +
                 "FROM Area area " + 
                 "INNER JOIN area.subArea suba " +
                 "INNER JOIN suba.teams team " +
                 "INNER JOIN team.members mb " +
                 "INNER JOIN mb.pk mbpk " +
                 "WHERE mbpk.resource = :resource ")
  List<Area> getByResource(@Param("resource")Resource resource);
}