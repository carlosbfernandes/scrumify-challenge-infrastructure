package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.WorkItemAttachment;
import pt.com.scrumify.database.entities.WorkItem;

public interface WorkItemAttachmentRepository extends JpaRepository<WorkItemAttachment, Integer> {
   
   List<WorkItemAttachment> findByWorkItem(WorkItem workItem);
   List<WorkItemAttachment> findByWorkItemId(Integer workItemId);
}