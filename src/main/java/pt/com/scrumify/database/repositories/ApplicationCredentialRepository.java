package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.ApplicationCredential;


public interface ApplicationCredentialRepository extends JpaRepository<ApplicationCredential, Integer> {
   ApplicationCredential findByIdAndApplicationIdAndEnvironmentId(int credential, int application, int environment);
   List<ApplicationCredential> findByApplicationIdAndEnvironmentId(int application, int environment);   
}