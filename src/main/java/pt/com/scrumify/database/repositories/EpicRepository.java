package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Year;

public interface EpicRepository extends JpaRepository<Epic, Integer> {

   List<Epic> findByIdNotIn(List<Integer> epic);
   List<Epic> findByTeamMembersPkResource(Resource resource);

   @Query(nativeQuery = false,
         value = "SELECT DISTINCT(epic) " +
                 "FROM Epic epic " +
                 "INNER JOIN epic.userStories us " +
                 "INNER JOIN epic.team tm " +
                 "INNER JOIN tm.subArea sa " +
                 "INNER JOIN us.workItems wi " +
                 "INNER JOIN wi.workLogs wl " +
                 "INNER JOIN wl.day day " + 
                 "WHERE (:team is null OR epic.team = :team) " +
                 "AND (:year is null OR day.year = :year) " +
                 "AND (:area is null OR sa.area = :area)")
   List<Epic> findbyTeamAndAreaAndYear(@Param("team") Team team, @Param("year") Year year, @Param("area") Area area);
   
   List<Epic> findByTeamOrderByName(Team team);

   @Query(value="SELECT e FROM Epic e "
              + "JOIN e.team t "
              + "WHERE t IN (:teams)")
   List<Epic> findByTeams(List<Team> teams);
   List<Epic> findByTeamId(Integer team);
   
   @Query(value = "SELECT e " +
                  "FROM Epic e " + 
                  "LEFT JOIN e.userStories us " +
                  "WHERE us.epic IS NULL " +
                  "AND e.team = :team")
   List<Epic> getWithoutWorkItems(@Param("team") Team team);

   @Query(value= " SELECT DISTINCT e FROM Epic e "
               + " JOIN e.team t "
               + " JOIN t.members tm "
               + " WHERE tm.pk.resource = :resource "
               + " AND e.status.id in (:statuses) ")
   List<Epic> findByStatusAndResource(@Param("resource") Resource resource, @Param("statuses") List<Integer> statuses);
   
   @Query(value="SELECT e FROM Epic e "
              + "JOIN e.team t "
              + "WHERE t = :team "
              + "AND e.status.finalStatus is false ")
   List<Epic> getByTeamAndNotFinalStatus(Team team);
}