package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.ContractStatus;


public interface ContractStatusRepository extends JpaRepository<ContractStatus, Integer> {
   List<ContractStatus> findByIdIn(int[] ids);

}