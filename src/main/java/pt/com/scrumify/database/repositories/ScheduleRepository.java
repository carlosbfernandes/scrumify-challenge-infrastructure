package pt.com.scrumify.database.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Schedule;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
   
   @Query(nativeQuery = false, 
         value = "SELECT s " + 
                 "FROM Schedule s " + 
                 "INNER JOIN s.settings ss " + 
                 "INNER JOIN s.resources r " + 
                 "WHERE r.id = :resource " + 
                 "AND :date between ss.startingDay.date and ss.endingDay.date ")
  Schedule findScheduleByUserAndDate(@Param("resource") Integer resource, @Param("date") Date date);
}