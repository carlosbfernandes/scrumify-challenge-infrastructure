package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.Year;

public interface TimesheetDayRepository extends JpaRepository<TimesheetDay, Integer> {
   @Query(value = "SELECT tsd " +
                  "FROM TimesheetDay tsd " +
                  "JOIN tsd.day t " +
                  "WHERE tsd.timesheet.resource = :resource " +
                  "AND t.date = :date")
   TimesheetDay getByResourceAndDate(@Param("resource") Resource resource, @Param("date") Date date);
   
   @Query(value = "SELECT td " +
                  "FROM TimesheetDay td " +
                  "JOIN td.day t " +
                  "WHERE t.year = :year " +
                  "AND t.month = :month " +
                  "AND td.timesheet.resource = :resource " +   
                  "ORDER BY t.date")
   List<TimesheetDay> findByYearAndMonthOrderByDate(@Param("year") Year year, @Param("month") Integer month, @Param("resource") Resource resource);
   
   @Query(value = "SELECT td " +
                  "FROM TimesheetDay td " +
                  "WHERE td.day = :date " +
                  "AND td.timesheet.resource = :resource ")
   TimesheetDay findByDate( @Param("date") Time date, @Param("resource") Resource resource);
   
   @Query(value = "SELECT td " +
                  "FROM TimesheetDay td " +
                  "JOIN td.day t " +
                  "WHERE t.year = :year " +
                  "AND t.month = :month " +
                  "AND t.fortnight = :fortnight " +
                  "AND td.timesheet.resource = :resource " +   
                  "AND (td.compensationDay > 0 or td.holiday > 0 )" +
                  "ORDER BY t.date")
   List<TimesheetDay> findByCompensationDay( @Param("year") Year year, @Param("month") Integer month, @Param("fortnight") Integer fortnight, @Param("resource") Resource resource);  
}