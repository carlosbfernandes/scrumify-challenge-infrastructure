package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.ApplicationProperty;


public interface ApplicationPropertyRepository extends JpaRepository<ApplicationProperty, Integer> {
   ApplicationProperty findByPkPropertyIdAndPkApplicationIdAndPkEnvironmentId(int property, int application, int environment);
   List<ApplicationProperty> findByPkApplicationIdAndPkEnvironmentId(int application, int environment);
}