package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Year;

public interface WorkItemRepository extends JpaRepository<WorkItem, Integer> {
   WorkItem getByNumber(String number);
   
   @Query("SELECT COUNT(w.id) FROM WorkItem w")
   Integer countAll();
   
   @Query("SELECT COUNT(w.id) FROM WorkItem w WHERE w.type = :type")
   Integer countByTypeOfWorkItem(@Param("type") TypeOfWorkItem type );
   
   List<WorkItem> findByAssignedTo(Resource resource);
   List<WorkItem> findByAssignedToIsNullAndStatusIdNotIn(List<Integer> statusIds);
   
   List<WorkItem> findByCreatedByAndStatusFinalStatusIsFalse(Resource resource);
   
   @Query("SELECT DISTINCT w " +
          "FROM WorkItem w " +
          "JOIN w.workLogs wlog " +
          "WHERE w.contract = :contract " + 
          "AND wlog.createdBy = :resource " )
   List<WorkItem> findDistinctByCreatedByAndContract(Resource resource, Contract contract);
   
   /*
    * Join fetch -> returns a Workitem with the specified worklogs already fetched
    */
   @Query("SELECT DISTINCT w " +
         "FROM WorkItem w " +
         "JOIN FETCH w.workLogs wlog " +
         "WHERE w.contract = :contract " + 
         "AND wlog.createdBy = :resource " +
         "AND wlog.created between :start and :end " )
   List<WorkItem> findDistinctByCreatedByAndContractBetweenDates(Resource resource, Contract contract, Date start, Date end);
   
   @Query(nativeQuery = false,
   value = "SELECT w " +
           "FROM WorkItem w " +
           "WHERE w.team = :team " +
           "AND w.team.subArea IN (:subAreas) " +
           "AND w.status IN (:status) " +
           "ORDER BY w.team, w.userStory, w.priority DESC, w.number")
   List<WorkItem> getBySubAreasAndTeams(@Param("team") Team team, @Param("subAreas") List<SubArea> subAreas, @Param("status") List<WorkItemStatus> status);

   @Query("SELECT w " +
            "FROM WorkItem w " +
            "WHERE w.assignedTo = :resource " + 
            "AND w.status.name <> 'Closed' " +
            "AND w.status.finalStatus is false ")
   List<WorkItem> findByAssignedToAndNotClosed(@Param("resource") Resource resource);
   
   @Query(nativeQuery = false,
   value = "SELECT w " +
            "FROM WorkItem w " +
            "WHERE w.team = :team ")
   List<WorkItem> getWorkItemsByTeam(@Param("team") Team team);
   
   List<WorkItem> findByTeamId(Integer team);
   
   @Query(value="SELECT w " +
                "FROM WorkItem w " +
                "JOIN w.team t " +
                "LEFT JOIN w.userStory us " +
                "LEFT JOIN us.epic e " +
                "WHERE t = :team " +
                "AND w.status.finalStatus is false " +
                "ORDER BY e.name, us.name, w.name")
   List<WorkItem> findByTeam(Team team);
   
   @Query(value="SELECT w " +
                "FROM WorkItem w " +
                "JOIN w.team t " +
                "WHERE t = :team " +
                "AND w.userStory IS NULL " +
                "ORDER BY w.name")
   List<WorkItem> getWorkItemsWithoutUserStoryByTeam(Team team);
   
   @Query(value="SELECT w " +
                "FROM WorkItem w " + 
                "JOIN w.team t " +
                "WHERE t IN (:teams)")
   List<WorkItem> findByTeams(List<Team> teams);

   @Query(nativeQuery = false,
   value = "SELECT w " +
           "FROM WorkItem w " +
           "WHERE (w.team IN (:list) "+
           "OR w.createdBy = :resource) ")
   List<WorkItem> getWorkItemsCreatedByMeAndAssigendToMyTeams(@Param("resource")Resource resource, @Param("list") List<Team> teams);
   
   @Query(nativeQuery = false,
   value = "SELECT w " +
           "FROM WorkItem w " +
           "WHERE w.team IN (:list) "+
           "AND w.assignedTo is null "+
           "AND w.status.finalStatus is false ")
   List<WorkItem> getWorkItemsNotAssigendByTeam(@Param("list") List<Team> team);
   
   List<WorkItem> findByUserStoryId(Integer userStory);
  
   List<WorkItem> findDistinctByTeamInAndWorkLogsDayYear(List<Team> teams, Year year);
   List<WorkItem> findDistinctByTeamInAndWorkLogsDayYearAndTeamSubAreaArea(List<Team> teams, Year year, Area area);
   List<WorkItem> findDistinctByTeamAndWorkLogsDayYearAndTeamSubAreaArea(Team team, Year year, Area area);
   List<WorkItem> findDistinctByTeamAndWorkLogsDayYearAndTeamSubAreaAreaAndWorkLogsCreatedBy(Team team, Year year,Area area, Resource resource);
   
   @Query(nativeQuery = false,
         value = "SELECT case when (count(w) > 0)  then true else false end " +
                 "FROM WorkItem w " +
                 "WHERE w.id = :workItem " +
                 "AND w.createdBy = :resource ")
   boolean isCreatedByMe(@Param("resource") Resource resource, @Param("workItem") Integer workItem);
     
   @Query(nativeQuery = false,
         value = "SELECT case when (count(w) > 0)  then true else false end " +
                 "FROM WorkItem w " +
                 "WHERE w.id = :workItem " +
                 "AND w.assignedTo = :resource ")
   boolean isAssignedToMe(@Param("resource") Resource resource, @Param("workItem") Integer workItem);
   
   @Query(
         value = "SELECT DISTINCT w FROM WorkItem w "
               + "JOIN w.team t "
               + "JOIN t.members tm "
               + "WHERE tm.pk.resource = :resource "
               + "AND w.status.id in :listStatus ")
   List<WorkItem> findBySameTeamAndStatus(@Param("resource")Resource resource, @Param("listStatus") int[] status);
   
   @Query(
         value = "SELECT DISTINCT w FROM WorkItem w "
               + "WHERE (w.type in :types "
               + "OR w.status in :statuses) ")
   List<WorkItem> findByStatusInOrTypeIn( @Param("statuses") List<WorkItemStatus> statuses, @Param("types") List<TypeOfWorkItem> types);

}