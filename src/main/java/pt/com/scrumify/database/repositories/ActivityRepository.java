package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Activity;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {
   @Query(nativeQuery = false,
          value = "SELECT a " +
                  "FROM Activity a " +
                  "ORDER BY a.projectPhase.technicalAssistance, a.projectPhase.sortOrder, a.number")
   List<Activity> getAll();
   
   @Query(nativeQuery = false,
          value = "SELECT a " +
                  "FROM Activity a " + 
                  "WHERE a.projectPhase.technicalAssistance = :technicalAssistance " +
                  "ORDER BY a.projectPhase.sortOrder, a.number")
   List<Activity> findByTechnicalAssistance(@Param("technicalAssistance") boolean technicalAssistance);
}