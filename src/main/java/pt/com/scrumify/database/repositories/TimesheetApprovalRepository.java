package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TimesheetApproval;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.entities.TimesheetApprovalView;

/*
 * TODO: code review - all class
 */
public interface TimesheetApprovalRepository extends JpaRepository<TimesheetApproval, Integer> {
   List<TimesheetApproval> findDistinctByYearIdAndContractSubAreaArea( Integer year, Area area);
   List<TimesheetApproval> findDistinctByYearIdAndContractSubAreaAreaIn(Integer year, List<Area> areas);
   List<TimesheetApproval> findDistinctByYearIdAndMonthAndContractSubAreaArea( Integer year, Integer month, Area area);
   List<TimesheetApproval> findDistinctByYearIdAndMonthAndContractSubAreaAreaIn(Integer year, Integer month, List<Area> areas);
   
   @Query("SELECT  NEW pt.com.scrumify.entities.TimesheetApprovalView(sheetBill.contract,sheetBill.year, sheetBill.month, SUM(sheetBill.analysis),SUM(sheetBill.design),SUM(sheetBill.development),SUM(sheetBill.test),SUM(sheetBill.other)) "+
         "FROM TimesheetApproval sheetBill " +
         "WHERE sheetBill.year = :year " +
         "AND  sheetBill.month = sheetBill.month " +
         "AND  sheetBill.contract.subArea.area IN :areas " +
         "GROUP BY sheetBill.contract , sheetBill.year  ,  sheetBill.month  ")
   List<TimesheetApprovalView> findByYearAndArea(@Param("areas") List<Area> areas, @Param("year") Year year);

   @Query("SELECT  NEW pt.com.scrumify.entities.TimesheetApprovalView(sheetBill.contract,sheetBill.year, sheetBill.month, SUM(sheetBill.analysis),SUM(sheetBill.design),SUM(sheetBill.development),SUM(sheetBill.test),SUM(sheetBill.other)) "+
         "FROM TimesheetApproval sheetBill " +
         "WHERE sheetBill.year = :year " +
         "AND  sheetBill.month = COALESCE(:month,sheetBill.month) " +
         "AND  sheetBill.contract.subArea.area IN :areas " +
         "GROUP BY sheetBill.contract , sheetBill.year  ,  sheetBill.month  ")
   List<TimesheetApprovalView> findByYearAndMonthAndArea(@Param("areas") List<Area> areas, @Param("year") Year year, @Param("month") Integer month);
   
   TimesheetApproval findByContract(Contract contract);
   TimesheetApproval findByYearAndMonthAndResourceAndFortnightAndContract(Year year, Integer month , Resource resource, Integer fortnight, Contract contract);
   List<TimesheetApproval> findByYearAndMonthAndContractId(Year year, Integer month, Integer contract);
   List<TimesheetApproval> findByYearAndMonthAndFortnightAndContractId(Year year, Integer month, Integer fortnight, Integer contract);
   List<TimesheetApproval> findByYearIdAndMonthAndFortnightAndResourceId(Integer year, Integer month, Integer fortnight, Integer resource);
   List<TimesheetApproval> findByYearIdAndMonthAndFortnightAndContractTeamContractsPkTeamIn(Integer year, Integer month , Integer fortnight, List<Team> teams);  
   List<TimesheetApproval> findByYearIdAndMonthAndFortnightAndResourceIdAndContractIsNotNull(Integer year, Integer month , Integer fortnight, Integer resource);

   @Query("SELECT  (SUM(sheetBill.analysis) + SUM(sheetBill.design) + SUM(sheetBill.development) + SUM(sheetBill.test) + SUM(sheetBill.other)) "+
         "FROM TimesheetApproval sheetBill " +
         "WHERE sheetBill.year = :year " +
         "AND  sheetBill.month = COALESCE(:month,sheetBill.month) " +
         "AND  sheetBill.contract = :contract " +
         "AND  sheetBill.fortnight = :fortnight " +
         "GROUP BY sheetBill.contract , sheetBill.year  ,  sheetBill.month  ")
   Long findHoursByYearAndMonthAndContractAndFortnight(@Param("contract") Contract contract, @Param("year") Year year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);
   
   @Query(value=
         "SELECT (SUM(tb.analysis) + SUM(tb.design) + SUM(tb.development) + SUM(tb.test) + SUM(tb.other)) "+
         "FROM TimesheetApproval tb " +
         "WHERE tb.contract = :contract ")
   Integer getHoursForStatusReportByContract(@Param("contract") Contract contract);
   
   @Query(value=
         "SELECT (SUM(tb.analysis) + SUM(tb.design) + SUM(tb.development) + SUM(tb.test) + SUM(tb.other)) "+
         "FROM TimesheetApproval tb " +
         "WHERE tb.contract = :contract " + 
         "AND tb.year = :year " + 
         "AND tb.month = :month ")
   Integer getHoursForStatusReport(@Param("contract") Contract contract, @Param("year") Year year, @Param("month") Integer month);
   @Query(nativeQuery = false, 
         value = "SELECT tsa " + 
                 "FROM TimesheetApproval tsa " +
                 "WHERE tsa.year = :year " +
                 "AND tsa.month = :month " +
                 "AND tsa.fortnight = :fortnight " +
                 "AND tsa.resource = :resource " +
                 "AND tsa.code = :code " +
                 "AND tsa.description = :description")
   TimesheetApproval getOne(@Param("resource") Resource resource, @Param("year") Year year, @Param("month") Integer month, @Param("fortnight") Integer fortnight, @Param("code") String code, @Param("description") String description);
   
   @Query(value=
            "SELECT new TimesheetApproval(tb.contract, tb.year, tb.month, SUM(tb.analysis), SUM(tb.design), SUM(tb.development), SUM(tb.test), SUM(tb.other)) " + 
            "FROM TimesheetApproval tb " + 
            "WHERE tb.year = :year " +
            "GROUP BY tb.contract, tb.month, tb.year " +
            "")
   List<TimesheetApproval> getContractsInfo(@Param("year") Year year);
}