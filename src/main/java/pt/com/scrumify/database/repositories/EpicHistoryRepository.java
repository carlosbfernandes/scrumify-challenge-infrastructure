package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.EpicHistory;

public interface EpicHistoryRepository extends JpaRepository<EpicHistory, Integer> {

   List<EpicHistory> findByEpicIdOrderByLastUpdateDesc(Integer idepic);
}