package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.WorkItemHistory;

public interface WorkItemHistoryRepository extends JpaRepository<WorkItemHistory, Integer> {
   
   List<WorkItemHistory> findByWorkItemIdOrderByLastUpdateDesc(Integer idworkitem);
}