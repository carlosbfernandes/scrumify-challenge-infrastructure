package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.WorkflowTransition;

   
public interface WorkflowTransitionRepository extends JpaRepository<WorkflowTransition, Integer> {
}