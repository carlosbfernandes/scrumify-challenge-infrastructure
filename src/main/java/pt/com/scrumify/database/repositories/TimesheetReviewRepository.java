package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetReview;
import pt.com.scrumify.database.entities.Year;

public interface TimesheetReviewRepository extends JpaRepository<TimesheetReview, Integer> {
   @Query(nativeQuery = false, 
          value = "SELECT tsr " + 
                  "FROM TimesheetReview tsr " +
                  "WHERE tsr.year = :year " +
                  "AND tsr.month = :month " +
                  "AND tsr.fortnight = :fortnight " +
                  "AND tsr.resource = :resource " +
                  "AND tsr.code = :code " +
                  "AND tsr.description = :description")
   TimesheetReview getOne(@Param("resource") Resource resource, @Param("year") Year year, @Param("month") Integer month, @Param("fortnight") Integer fortnight, @Param("code") String code, @Param("description") String description);
   List<TimesheetReview> getByTimesheet(Timesheet timesheet);
}
