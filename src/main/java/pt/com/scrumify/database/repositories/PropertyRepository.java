package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Property;

public interface PropertyRepository extends JpaRepository<Property, Integer> {
   
   List<Property> findByGroup( String group);
   
   @Query(nativeQuery = false,
         value = "SELECT p " +
                 "FROM Property p " +
                 "WHERE p.group = :group " +
                 "AND p.id NOT IN (:list) ")
   List<Property> findByGroupNotIn(@Param("list") List<Integer> id, @Param("group") String group);
   
}