package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.UserStoryHistory;

public interface UserStoryHistoryRepository extends JpaRepository<UserStoryHistory, Integer> {
   
   List<UserStoryHistory> findByUserStoryIdOrderByLastUpdateDesc(Integer iduserStory);
}