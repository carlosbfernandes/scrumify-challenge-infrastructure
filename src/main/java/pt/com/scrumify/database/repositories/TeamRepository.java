package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;

public interface TeamRepository extends JpaRepository<Team, Integer> {
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT t " +
                 "FROM Team t " +
                 "INNER JOIN t.members tm " +
                 "WHERE tm.pk.resource = :resource ")
   List<Team> getByMemberAndInactive(@Param("resource") Resource resource);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT t " +
                 "FROM Team t " +
                 "INNER JOIN t.members tm " +
                 "WHERE tm.pk.resource = :resource " + 
                 "AND t.active = TRUE " + 
                 "ORDER BY t.name ASC ")
   List<Team> getByMember(@Param("resource") Resource resource);
   
   List<Team> findByMembersPkResourceIdAndActiveIsTrue(Integer idResource);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT t " +
                 "FROM Team t " +
                 "INNER JOIN t.members tm " +
                 "WHERE tm.pk.resource = :resource " +
                 "AND t != :team " +
                 "AND t.active is true ")
   List<Team> getByMemberExceptActual(@Param("resource") Resource resource, @Param("team") Team team);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT t " +
                 "FROM Team t " +
                 "INNER JOIN t.members tm " +
                 "WHERE tm.pk.resource = :resource " +
                 "AND (tm.reviewer = TRUE OR tm.approver = TRUE) " +
                 "AND t.active = TRUE" )
   List<Team> getByMemberAndReviewerOrApprover(@Param("resource") Resource resource);

   @Query(nativeQuery = false,
           value = "SELECT DISTINCT t " +
                   "FROM Team t " +
                   "INNER JOIN t.members tm " +
                   "WHERE tm.pk.resource = :resource " +
                   "AND tm.reviewer = TRUE " +
                   "AND t.active = TRUE ")
     List<Team> getByMemberAndReviewer(@Param("resource") Resource resource);
   
   @Query(nativeQuery = false,
           value = "SELECT DISTINCT t " +
                   "FROM Team t " +
                   "INNER JOIN t.members tm " +
                   "WHERE tm.pk.resource = :resource " +
                   "AND tm.approver = TRUE " +
                   "AND t.active = TRUE ")
     List<Team> getByMemberAndApprover(@Param("resource") Resource resource);

     List<Team> findBySubAreaAreaIdAndActiveIsTrue(Integer idArea);
 
     @Query(nativeQuery = false,
           value = "SELECT case when (count(c) > 0)  then true else false end " +
                    "FROM Contract c " +
                    "JOIN c.subArea s " +
                    "JOIN s.teams t " + 
                    "JOIN t.members m " + 
                    "WHERE m.pk.resource = :resource " + 
                    "AND c.id = :contract " +
                    "AND t.active = TRUE ")
     boolean isMemberOfContract(@Param("resource") Resource resource, @Param("contract") Integer contract);
     
     @Query(nativeQuery = false,
           value = "SELECT case when (count(t) > 0)  then true else false end " +
                   "FROM Team t " +
                   "INNER JOIN t.members tm " +
                   "WHERE tm.pk.resource = :resource " +
                   "AND t.subArea.id = :subArea " +
                   "AND t.active = TRUE ")
     boolean isMemberOfApplication(@Param("resource") Resource resource, @Param("subArea") Integer subArea);
   
     @Query(nativeQuery = false,
           value = "SELECT case when (count(t) > 0)  then true else false end " +
                   "FROM Team t " +
                   "INNER JOIN t.members tm " +
                   "INNER JOIN t.workItems w " +
                   "WHERE tm.pk.resource = :resource " +
                   "AND w.id = :workItem " +
                   "AND t.active = TRUE ")
     boolean isMemberOfWorkItem(@Param("resource") Resource resource, @Param("workItem") Integer workItem);
     
     @Query(value=
             "SELECT t "
           + "FROM Team t "
           + "JOIN t.members tm "
           + "WHERE tm.pk.team <> :team "
           + "AND tm.pk.resource = :resource "
           + "AND t.active = TRUE ")
     List<Team> findByWorkitem();
     
     @Query(nativeQuery = false,
            value = "SELECT t " +
                    "FROM Team t " +
                    "JOIN t.members tm " +
                    "WHERE t.subArea IN (:subAreas) " +
                    "AND tm.pk.resource = :resource " +
                    "AND t.active = TRUE")
     List<Team> getBySubAreasAndResource(@Param("subAreas") List<SubArea> subAreas, @Param("resource") Resource resource);
     
     @Query(nativeQuery = false,
            value = "SELECT t " +
                    "FROM Team t " +
                    "JOIN t.members tm " +
                    "WHERE t.subArea IN (:subAreas) " +
                    "AND t IN (:teams) " +
                    "AND tm.pk.resource = :resource " +
                    "AND t.active = TRUE")
     List<Team> getBySubAreasTeamsAndResource(@Param("subAreas") List<SubArea> subAreas, @Param("teams") List<Team> teams, @Param("resource") Resource resource);
}