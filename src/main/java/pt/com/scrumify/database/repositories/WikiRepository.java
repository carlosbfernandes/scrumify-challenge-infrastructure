package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Wiki;

public interface WikiRepository extends JpaRepository<Wiki, Integer> {
   
   List<Wiki> findDistinctByApplicationSubAreaTeamsMembersPkResourceInAndActiveIsTrue(List<Resource> resource);
   List<Wiki> findByApplicationNullAndActiveIsTrue();
   List<Wiki> findDistinctByApplicationSubAreaTeamsMembersPkResourceIn(List<Resource> resource);
   List<Wiki> findByApplicationNull();
   
}