package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;

public interface CompensationDayRepository extends JpaRepository<CompensationDay, Integer> {
   List<CompensationDay> findByDayYearAndDayMonth(Year year, Integer month);
   CompensationDay findByDay(Time day);
   List<CompensationDay> findAllByOrderByDayDateAsc();
   
   @Query( "SELECT sum(ts.hours) "
         + "FROM CompensationDay c "
         + "JOIN c.day.timesSchedules ts "
         + "WHERE ts.pk.schedule = :schedule " 
         + "AND c.day.fortnight = :fortnight "
         + "AND c.day.month = :month "
         + "AND c.day.year.id = :year ")
   Long getHours(@Param("year") Integer year, @Param("month") Integer month, @Param("fortnight") Integer fortnight, @Param("schedule") Schedule schedule);    
}