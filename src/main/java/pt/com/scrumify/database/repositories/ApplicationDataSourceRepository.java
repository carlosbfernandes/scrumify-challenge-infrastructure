package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.ApplicationDataSource;


public interface ApplicationDataSourceRepository extends JpaRepository<ApplicationDataSource, Integer> {
   ApplicationDataSource findByIdAndApplicationIdAndEnvironmentId(int datasource, int application, int environment);   
   List<ApplicationDataSource> findByApplicationIdAndEnvironmentId(int application, int environment);   
}