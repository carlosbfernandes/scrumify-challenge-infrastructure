package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.TimesheetApprovalMonthly;
import pt.com.scrumify.database.entities.TimesheetApprovalMonthlyPK;

public interface TimesheetApprovalMonthlyRepository extends JpaRepository<TimesheetApprovalMonthly, TimesheetApprovalMonthlyPK> {
   @Query("SELECT new TimesheetApprovalMonthly(tsam.pk.year, c, SUM(tsam.january), SUM(tsam.february), SUM(tsam.march), SUM(tsam.april), SUM(tsam.may), SUM(tsam.june), SUM(tsam.july), SUM(tsam.august), SUM(tsam.september), SUM(tsam.october), SUM(tsam.november), SUM(tsam.december)) " +
          "FROM TimesheetApprovalMonthly tsam " +
          "JOIN tsam.pk.contract c " +
          "JOIN c.subArea sa " +
          "WHERE tsam.pk.year = :year " +
          "AND c IN (:contracts) " +
          "GROUP BY tsam.pk.year, c, sa.abbreviation, c.startingDay.date " +
          "ORDER BY sa.abbreviation, c.startingDay.date")
  List<TimesheetApprovalMonthly> findByContractsAndYear(@Param("contracts") List<Contract> contracts, @Param("year") int year);
}