package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {
   @Query(nativeQuery = false,
          value = "SELECT t " +
                  "FROM Tag t " +
                  "WHERE t.subArea IN (:subAreas)")
   List<Tag> findBySubAreas(@Param("subAreas") List<SubArea> subAreas);
}