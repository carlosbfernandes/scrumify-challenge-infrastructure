package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;

public interface SubAreaRepository extends JpaRepository<SubArea, Integer> {
   
   @Query(nativeQuery = false,
         value = "SELECT s " +
                 "FROM SubArea s " +
                 "WHERE s != :subArea")
   List<SubArea> listSubarea(@Param("subArea") SubArea subArea);
   
   @Query(nativeQuery = false,
          value = "SELECT DISTINCT sa " +
                  "FROM SubArea sa " +
                  "JOIN sa.teams t " +
                  "WHERE t IN :teams")
   List<SubArea> byTeams(@Param("teams") List<Team> teams);
   
   @Query(value = " SELECT DISTINCT s " +
                  " FROM SubArea s " +
                  " JOIN s.teams t " + 
                  " JOIN t.members m " + 
                  " where m.pk.resource = :resource ")
   List<SubArea> listSubareasByResource(@Param("resource") Resource resource);
   
   @Query(value = " SELECT DISTINCT s " +
                  " FROM SubArea s " +
                  " JOIN s.teams t " + 
                  " JOIN t.members m " + 
                  " where m.pk.resource = :resource " +
                  " AND s.area = :area ")
List<SubArea> getByAreaAndResource(@Param("area") Area area, @Param("resource") Resource resource);
} 