package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pt.com.scrumify.database.entities.Year;

public interface YearRepository extends JpaRepository<Year, Integer> {
   public List<Year> findAllByOrderByIdDesc();
   
   @Query(" SELECT y " +
          " FROM Year y " +
          " WHERE NOT EXISTS ( " +
          " SELECT h from Holiday h WHERE h.day.year = y " +
          " ) ")
   List<Year> findByHolidays();
   
   @Query(" SELECT y " +
         " FROM Year y " +
         " WHERE NOT EXISTS ( " +
         " SELECT c from CompensationDay c WHERE c.day.year = y " +
         " ) ")
   List<Year> findByCompensationDays();
   
   List<Year> findByIdIn(List<Integer> years);
   List<Year> findByActiveIsTrue();

   Year findTop1ByOrderByIdDesc();
   Year findTop1ByActiveIsTrueOrderByIdDesc();
   Year findTop1ByOrderByIdAsc();
   Year findTop1ByActiveIsTrueOrderByIdAsc();
}