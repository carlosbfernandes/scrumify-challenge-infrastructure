package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;

public interface TimeRepository extends JpaRepository<Time, Integer> {
     
   Time findById(String id);
   Time findByDate(Date date);
   List<Time> findByYearId(Integer year);
   List<Time> findByMonthAndDayOrderByDate(Integer month, Integer day);
   List<Time> findByYearAndDayOrderByDate(Year year, Integer day);
   List<Time> findByYearAndMonthOrderByDate(Year year, Integer month);
   List<Time> findByDateBetween(Date startDay, Date endDay);
   
   @Query(nativeQuery = false,
         value = "SELECT t " +
               "FROM Time t " +
               "WHERE t.date BETWEEN :di AND :df")
   List<Time> getBetween(@Param("di") Date di, @Param("df") Date df);
} 