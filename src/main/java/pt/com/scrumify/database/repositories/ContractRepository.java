package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.ContractStatus;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Year;


public interface ContractRepository extends JpaRepository<Contract, Integer> {
   
   List<Contract> findByStatusId(Integer status);
   Contract findDistinctByCode(String code);
   List<Contract> findByTeamContractsPkTeamAndStatusId(Team team, Integer status);
   
   @Query(nativeQuery = false,
         value = "SELECT c " +
                 "FROM Contract c " +
                 "INNER JOIN c.teamContracts tc " +
                 "WHERE tc.pk.team = :team ")
   List<Contract> getByTeam(@Param("team")Team team);
   
   @Query(value ="SELECT c " +
                 "FROM Contract c " +
                 "JOIN c.subArea.area a " +
                 "WHERE a = :area ")
   List<Contract> getByArea(@Param("area") Area area);
   
   @Query(nativeQuery = false,
         value = "SELECT c " +
                 "FROM Contract c " +
                 "WHERE c.subArea = :subArea ")
   List<Contract> getBySubarea(@Param("subArea")SubArea subArea);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT c " +
                 "FROM Contract c " +
                 "JOIN c.subArea.teams t " +
                 "JOIN t.members m " +
                 "WHERE c.subArea = :subArea " +
                 "AND m.pk.resource = :resource " +
                 "AND c.status.id <> 4 ")
   List<Contract> getBySubareaAndResource(@Param("subArea")SubArea subArea, @Param("resource") Resource resource);
   
   @Query(nativeQuery = false,
         value = "SELECT c " +
                 "FROM Contract c " +
                 "WHERE c.subArea = :subArea " + 
                 "AND c.status.id = :status " )
   List<Contract> getBySubareaAndStatus(@Param("subArea") SubArea subArea, @Param("status") Integer status);
   
   @Query(nativeQuery = false,
         value = "SELECT c " +
                 "FROM Contract c " +
                 "INNER JOIN c.teamContracts tc " +
                 "INNER JOIN tc.pk tcpk " +
                 "INNER JOIN tcpk.team team " +
                 "INNER JOIN team.members mb " +
                 "INNER JOIN mb.pk mbpk " +
                 "WHERE mbpk.resource = :resource ")
   List<Contract> getByResource(@Param("resource")Resource resource);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT c " +
                 "FROM Contract c " +
                 "JOIN c.subArea s " +
                 "JOIN s.teams t " + 
                 "JOIN t.members m " + 
                 "WHERE m.pk.resource = :resource " )
   List<Contract> getByRes(@Param("resource")Resource resource);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT c, c.startingDay.date " +
                 "FROM Contract c " +
                 "JOIN c.subArea s " +
                 "JOIN s.teams t " + 
                 "JOIN t.members m " + 
                 "WHERE m.pk.resource = :resource " +
                 "AND c.status IN (2, 3, 4) " +
                 "ORDER BY c.startingDay.date")
   List<Contract> getByResourceWithoutOpportunities(@Param("resource") Resource resource);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT c " +
                 "FROM Contract c " +
                 "JOIN c.subArea s " +
                 "JOIN s.teams t " + 
                 "JOIN t.members m " +
                 "JOIN c.status status " +
                 "WHERE m.pk.resource = :resource " +
                 "AND status.id = :status " )
   List<Contract> getByResAndStatus(@Param("resource")Resource resource, @Param("status") int status);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT c " +
                 "FROM Contract c " +
                 "JOIN c.subArea s " +
                 "JOIN s.teams t " + 
                 "JOIN t.members m " +
                 "JOIN c.status status " +
                 "WHERE m.pk.resource = :resource " +
                 "AND status.id in (:status) "+
                 "ORDER BY c.startingDay ")
   List<Contract> getByResInStatus(@Param("resource")Resource resource, @Param("status") List<Integer> status);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT c " +
                 "FROM Contract c " +
                 "JOIN c.subArea s " +
                 "JOIN s.teams t " + 
                 "JOIN t.members m " +
                 "JOIN c.status status " +
                 "WHERE m.pk.resource = :resource " +
                 "AND status in (:statuses) " )
   List<Contract> getByResInAndStatuses(@Param("resource")Resource resource, @Param("statuses") List<ContractStatus> statuses);
   
   @Query(nativeQuery = false,
         value = "SELECT c " +
                 "FROM Contract c " +
                 "INNER JOIN c.teamContracts tc " +
                 "INNER JOIN tc.pk tcpk " +
                 "INNER JOIN tcpk.team team " +
                 "INNER JOIN team.members mb " +
                 "INNER JOIN mb.pk mbpk " +
                 "WHERE mbpk.resource = :resource "+                 
                 "AND c.status.id = :status  ")
   List<Contract> getByResourceAndStatus(@Param("resource")Resource resource, @Param("status")Integer status);
   
   List<Contract> findByTeamContractsPkTeamIn(List<Team> teams);
   List<Contract> findDistinctBySubAreaAreaAndWorkItemsWorkLogsDayYear(Area area, Year year);
   List<Contract> findDistinctBySubAreaAreaInAndWorkItemsWorkLogsDayYear(List<Area> area, Year year);
   List<Contract> findDistinctBySubAreaArea(Area area);
   List<Contract> findBySubAreaAreaIn(List<Area> area);
   List<Contract> findDistinctByWorkItemsWorkLogsCreatedByAndWorkItemsWorkLogsDayDateBetween(Resource resource, Date start, Date end);
   
   @Query(value = "SELECT DISTINCT c " +
                  "FROM Contract c " +
                  "INNER JOIN c.teamContracts tc " +
                  "INNER JOIN c.workItems w " +
                  "INNER JOIN w.workLogs wlog " +
                  "WHERE tc.pk.team in (:teams) "+                 
                  "AND wlog.day.year = :year ")
   List<Contract> findDistinctByTeamsAndWorkItemsWorkLogsDayYearId(@Param("teams") List<Team> teams, @Param("year") Year year);
   
   @Query(value = "SELECT c " +
                  "FROM Contract c " +
                  "INNER JOIN c.properties cp " +
                  "WHERE cp.pk.property.name = :propertyName " + 
                  "AND cp.value = :value")
   Contract findByPropertyNameAndValue(@Param("propertyName") String propertyName, @Param("value") String value);
   
   @Query(value = "SELECT c " +
                  "FROM Contract c " +
                  "INNER JOIN c.properties cp1 " +
                  "INNER JOIN c.properties cp2 " +
                  "WHERE cp1.pk.property.name = :propertyName1 " + 
                  "AND cp1.value = :value1 " +
                  "AND cp2.pk.property.name = :propertyName2 " +
                  "AND cp2.value = :value2")
   Contract findByTwoPropertyNameAndValue(@Param("propertyName1") String propertyName1, @Param("value1") String value1, @Param("propertyName2") String propertyName2, @Param("value2") String value2);
}