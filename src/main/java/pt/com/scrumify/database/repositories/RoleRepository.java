package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
   List<Role> findAllByOrderByNameAsc();
}