package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.ContractProperty;


public interface ContractPropertyRepository extends JpaRepository<ContractProperty, Integer> {
   
   ContractProperty findByPkContractIdAndPkPropertyId(int idContract, int idProperty);
   
   ContractProperty findByPkPropertyNameAndPkContractId(String propertyName, Integer contract);
   
}