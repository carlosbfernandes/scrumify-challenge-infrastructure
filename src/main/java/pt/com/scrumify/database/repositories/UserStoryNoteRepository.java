package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.UserStoryNote;

public interface UserStoryNoteRepository extends JpaRepository<UserStoryNote, Integer> {

}