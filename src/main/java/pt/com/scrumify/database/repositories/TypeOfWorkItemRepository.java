package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pt.com.scrumify.database.entities.TypeOfWorkItem;

public interface TypeOfWorkItemRepository extends JpaRepository<TypeOfWorkItem, Integer> {

   TypeOfWorkItem findByName(String name);
   
   @Query(value = 
           "SELECT t "
         + "FROM TypeOfWorkItem t "
         + "WHERE t.workItem is true "
         + "ORDER BY t.name ")
   List<TypeOfWorkItem> findByWorkItemIsTrueOrderByNameAsc();
   
}