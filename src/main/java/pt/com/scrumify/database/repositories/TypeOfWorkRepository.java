package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.TypeOfWork;

public interface TypeOfWorkRepository extends JpaRepository<TypeOfWork, Integer> {
}