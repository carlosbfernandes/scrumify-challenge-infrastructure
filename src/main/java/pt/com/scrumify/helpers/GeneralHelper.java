package pt.com.scrumify.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class GeneralHelper {
   
   public <T> List<T> castList(Class<? extends T> clazz, Collection<?> c) {
      List<T> r = new ArrayList<>(c.size());
      for(Object o: c)
        r.add(clazz.cast(o));
      return r;
   }
   
   public boolean convertToBool(String value) {
      return "1".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value) || 
            "true".equalsIgnoreCase(value) || "on".equalsIgnoreCase(value);
   }
}
