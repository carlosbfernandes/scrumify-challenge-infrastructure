package pt.com.scrumify.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.services.ScheduleService;

@Service
public class SchedulesHelper {
   
   @Autowired
   private ScheduleService schedulesService;
   
   @Transactional
   public void save(Schedule schedule) {
      schedulesService.save(schedule);
   }   
}