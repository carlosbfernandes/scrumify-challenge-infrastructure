package pt.com.scrumify.helpers;

import java.text.Normalizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Profile;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.services.ProfileService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.entities.ResourceView;
import pt.com.scrumify.entities.ScrumifyUser;
import pt.com.scrumify.services.EmailService;

@Service
public class ResourcesHelper {
   @Autowired
   private EmailService emailService;
   @Autowired
   private ProfileService profilesService;
   @Autowired
   private ResourceService resourcesService;
     
   public static Resource getResource() {
      Resource resource = null;

      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      if (authentication != null) {
         resource = ((ScrumifyUser) authentication.getPrincipal()).getResource();
      }

      return resource;
   }
   
   @Transactional
   public void changePIN(Resource resource) {
      PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

      //
      // Get changed password
      //
      String password = resource.getPassword();

      //
      // Get resource
      //
      Resource saved = getResource();

      //
      // Set new password and last password change date
      //
      if(saved!=null) {
         saved.setPassword(passwordEncoder.encode(password));
         saved.setLastPasswordChange(DatesHelper.now());
         //
         // Save resource
         //
         this.resourcesService.save(saved);
      }

      
   }
   
   @Transactional
   public void personalData(Resource resource) {
      Resource saved = getResource();

      if(saved!=null) {
         saved.setAbbreviation(resource.getAbbreviation());
         saved.setName(resource.getName());
         saved.setEmail(resource.getEmail());
         saved.setAvatar(resource.getAvatar());
         saved.setPhone(resource.getPhone());
         saved.setMobile(resource.getMobile());
         saved.setExtension(resource.getExtension());

         //
         // Save resource
         //
         this.resourcesService.save(saved);
      } 
      
   }
   
   @Transactional
   public void registration(Resource resource) {
      PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

      /*
       * Get default profile (Guests)
       */
      Profile profile = this.profilesService.getOne(ConstantsHelper.DEFAULT_PROFILE);

      /*
       * Save user
       */
      resource.setUsername(this.getUsername(resource.getName()));
      resource.setPassword(passwordEncoder.encode(RandomStringUtils.randomNumeric(ConstantsHelper.DEFAULT_RESOURCE_KEY_LENGTH)));
      resource.setProfile(profile);
      resource.setStartingDay(DatesHelper.now());
      resource.setActive(false);

      this.resourcesService.save(resource);
   }
   
   @Transactional
   public void save(ResourceView resourceView, HttpServletRequest request, HttpServletResponse response) {
      Resource resource = null;
      PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
      
      /*
       * On insert, set default password. On update, get resource from database.
       */
      if (resourceView.getId() == 0) {
         resource = new Resource();
         String code = RandomStringUtils.randomNumeric(ConstantsHelper.DEFAULT_RESOURCE_KEY_LENGTH);
         resource.setPassword(passwordEncoder.encode(code));
         if(resourceView.isApproved() && resourceView.isActive())
            emailService.sendApprovalEmail(resourceView.translate(), code,request,response);
      }
      else {
         resource = this.resourcesService.getOne(resourceView.getId());
         
         if(!resource.isApproved() && resourceView.isApproved()) {
            String code = RandomStringUtils.randomNumeric(ConstantsHelper.DEFAULT_RESOURCE_KEY_LENGTH);
            resource.setPassword(passwordEncoder.encode(code));
            emailService.sendApprovalEmail(resourceView.translate(), code,request,response);
         }            
      }
      
      /*
       * Copy data from resource view to resource entity
       */
      resource.setAbbreviation(resourceView.getAbbreviation());
      resource.setName(resourceView.getName());
      resource.setGender(resourceView.getGender());
      resource.setEmail(resourceView.getEmail());
      resource.setAvatar(resourceView.getAvatar());
      resource.setStartingDay(resourceView.getStartingDay());
      resource.setEndingDay(resourceView.getEndingDay());
      resource.setUsername(resourceView.getUsername());
      resource.setProfile(resourceView.getProfile());
      resource.setCode(resourceView.getCode());
      resource.setApproved(resourceView.isApproved());
      resource.setActive(resourceView.isActive());
      resource.setSchedule(resourceView.getSchedule());
      resource.setExtension(resourceView.getExtension());
      resource.setMobile(resourceView.getMobile());
      resource.setPhone(resourceView.getPhone());
      
      this.resourcesService.save(resource);
   }
   
   private String getUsername(String resourceName) {
      String username;

      if (resourceName.contains(" ")) {
         username = resourceName.substring(0, 1) + resourceName.substring(resourceName.lastIndexOf(' ') + 1, resourceName.length());
      }
      else {
         username = RandomStringUtils.randomAlphabetic(10);
      }

      username = Normalizer.normalize(username, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

      return username.toLowerCase();
   }   
}