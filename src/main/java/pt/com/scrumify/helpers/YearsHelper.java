package pt.com.scrumify.helpers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.services.YearService;

@Service
public class YearsHelper {
   
   @Autowired
   private TimeService timesService;
   @Autowired
   private YearService yearsService;
   @Autowired
   private TimesHelper timesHelper;
   
   @Transactional
   public void save(Year year) {
      
      yearsService.save(year);
      
      for (Time time : timesHelper.getTimes(year.getId())) {
         timesService.save(time);
      }
   }
   
   public Date getMinYear() {      
      Year year = yearsService.getFirstYear();
      
      return getCalendar(year.getId(), 1, 1);
   }
   
   public Date getMinActiveYear() {      
      Year year = yearsService.getFirstActiveYear();
      
      return getCalendar(year.getId(), 1, 1);
   }
   
   public Date getMaxYear() {      
      Year year = yearsService.getLastYear();
      
      return getCalendar(year.getId(), 12, 31);
   }
   
   public Date getMaxActiveYear() {      
      Year year = yearsService.getLastActiveYear();
      
      return getCalendar(year.getId(), 12, 31);
   }
   
   private Date getCalendar(int year, int month, int day) {
      Calendar cal = Calendar.getInstance();
      cal.set(Calendar.YEAR, year);
      cal.set(Calendar.MONTH, month - 1);
      cal.set(Calendar.DAY_OF_MONTH, day);
      cal.set(Calendar.HOUR, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);
      cal.set(Calendar.AM_PM, Calendar.AM);
      
      return cal.getTime();
   }
   
   public List<Year> getYearsBasedOnContract(Year year){
      List<Year> years = this.yearsService.getAllActive();
      if(!year.isActive()) {
         years.add(year);
      }
      return years;      
   }
}