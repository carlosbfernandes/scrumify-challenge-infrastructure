package pt.com.scrumify.helpers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.Wiki;

@Service
public class WikisHelper {
   
   @Autowired
   private EntityManager entityManager;
   
   @Autowired
   private GeneralHelper generalHelper;
   
   @Transactional
   public List<Wiki> fuzzySearch(String searchTerm, Resource resource){
      
      FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
      QueryBuilder qb = fullTextEntityManager.getSearchFactory()
                                             .buildQueryBuilder().forEntity(Wiki.class)
                                             .get();
      /*
       * withEditDistanceUpTo(1) - Checks for 1 distance of fuzzyness.
       * Searching for 'tesr' or 'tes1' will return 'test'  
       */
      Query luceneQuery = qb.keyword().fuzzy().withEditDistanceUpTo(1)
                                      .onFields("name", "pages.title", "pages.content")
                                      .matching(searchTerm).createQuery();
      
      javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, Wiki.class);
      
      List<Wiki> wikis = new ArrayList<>();
      List<Wiki> wikisfilter =new ArrayList<>();
      try {       
         wikis = generalHelper.castList(Wiki.class , jpaQuery.getResultList());
         wikisfilter = filterList(wikis,resource);
      } catch (NoResultException nre) {
         //do nothing.
      }     
      return wikisfilter;
   }
   public List<Wiki> filterList(List<Wiki> wikis , Resource resource){ 
      List<Wiki> wikisfilter = new ArrayList<>();
      for(Wiki wiki: wikis){
         if(wiki.getApplication() == null){
            wikisfilter.add(wiki);
         }else{
            for(Team team : wiki.getApplication().getSubArea().getTeams()){
               filterByMember(wikisfilter, team, resource, wiki);
            }
         }
      }
   return wikisfilter;
   }
   
   private void filterByMember(List<Wiki> wikisfilter, Team team, Resource resource, Wiki wiki) {
      for(TeamMember member : team.getMembers()){
         if(member.getResource().getId().intValue() == resource.getId().intValue()){
            wikisfilter.add(wiki);
            break;
         }
      }
   }
   
   
}