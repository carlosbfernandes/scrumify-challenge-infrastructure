package pt.com.scrumify.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.UserStoryHistory;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.repositories.TypeOfWorkItemRepository;
import pt.com.scrumify.database.services.UserStoryHistoryService;
import pt.com.scrumify.database.services.UserStoryService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.database.services.WorkItemStatusService;
import pt.com.scrumify.entities.ItemStatusView;
import pt.com.scrumify.entities.UserStoryFilterView;
import pt.com.scrumify.entities.UserStoryView;

@Service
public class UserStoriesHelper {
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private ContextHelper contextHelper;
   @Autowired
   private UserStoryService userStoriesService;
   @Autowired
   private WorkItemStatusService itemStatusService;
   @Autowired
   private TypeOfWorkItemRepository typeRepository;
   @Autowired
   private UserStoryHistoryService userStoryHistoryService;
   @Autowired
   private WorkItemService workitemsService;
   
   private static final Logger logger = LoggerFactory.getLogger(UserStoriesHelper.class);

   @Transactional
   public UserStory save(UserStoryView userStoryView) {

      UserStory userStory;
      userStory = userStoryView.translate();

      if (userStoryView.getId() == 0) {
         userStory.setType(typeRepository.findByName("User Story"));
         userStory.setNumber(formatUserStoryNumber(userStory));
         userStory.setStatus(itemStatusService.getOneByTypeOfWorkitem(userStory.getType().getId()));
      }

      return userStoriesService.save(userStory);
   }

   @Transactional
   public void saveTeam(UserStoryView userStoryView, Team oldTeam) {

      UserStory userStory;
      userStory = userStoryView.translate();

      if (userStory.getTeam() != oldTeam) {
         for (WorkItem workitem : userStory.getWorkItems()) {
            workitem.setTeam(userStory.getTeam());
            workitemsService.save(workitem);
         }
      }
      UserStory userStorysaved = userStoriesService.save(userStory);
      if (userStoryView.getId() != 0) {
         userStoryHistoryService.save(new UserStoryHistory(userStorysaved));
      }
   }

   public String formatUserStoryNumber(UserStory userStory) {
      String format = "%06d";
      return userStory.getType().getAbbreviation().substring(0, 3) + DatesHelper.year().toString().substring(2, 4) + String.format(format, userStoriesService.countAll() + 1);
   }

   public List<UserStory> getByFilter(UserStoryFilterView filter) {
      List<UserStory> userstories = new ArrayList<>();
      Resource resource = ResourcesHelper.getResource();

      if (filter.getStatuses() != null && !filter.getStatuses().isEmpty()) {
         userstories = userStoriesService.getByStatusesAndResource(resource, ItemStatusView.getStatusesIds(filter.getStatuses()));
         
         if (filter.getSearchQuery() != null && !filter.getSearchQuery().isEmpty()) {
            List<UserStory> temp = new ArrayList<>();
            for (UserStory userstory : userstories) {
               if (StringUtils.containsIgnoreCase(userstory.getName(), filter.getSearchQuery()) || StringUtils.containsIgnoreCase(userstory.getDescription(), filter.getSearchQuery()) || StringUtils.containsIgnoreCase(userstory.getNumber(), filter.getSearchQuery())) {
                  temp.add(userstory);
               }
            }
            
            userstories = temp;
         }
      }
      else {
         userstories = userStoriesService.getByResource();
      }

      Team activeTeam = contextHelper.getActiveTeam();
      if (activeTeam != null) {
         userstories =  userstories.stream().filter(x -> x.getTeam().getId().equals(activeTeam.getId())).collect(Collectors.toList());
      }
      
      return userstories;
   }

   public void setHours(UserStory story) {
      if (!story.getWorkItems().isEmpty()) {
         Integer sum = 0;
         for (WorkItem workitem : story.getWorkItems())
            sum += workitem.getWorkLogs().stream().mapToInt(WorkItemWorkLog::getTimeSpent).sum();
         story.setHours(sum);
      }
   }

   public UserStoryFilterView getFilterFromCookie(String cookieName) {
      UserStoryFilterView filter = null;
      
      /*
       * Get cookie data from database
       */
      Cookie cookie = cookiesHelper.get(cookieName, ResourcesHelper.getResource());
      if (cookie == null) {
         /*
          * If cookie not exists, create cookie with default values (status = null)
          */
         if (filter == null) {
            filter = new UserStoryFilterView();
            
            cookiesHelper.save(cookieName, ResourcesHelper.getResource(), filter);
         }
      }
      else {
         /*
          * If cookie exists, get data from cookie
          */
         ObjectMapper mapper = new ObjectMapper();
         
         try {
            filter = mapper.readValue(cookie.getData(), UserStoryFilterView.class);
         }
         catch (Exception ex) {
            logger.error("Exception was caught when trying to serialize user story filter.", ex);
         }
      }
      
      return filter;
   }
}