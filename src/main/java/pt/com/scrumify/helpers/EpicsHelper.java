package pt.com.scrumify.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.services.EpicService;
import pt.com.scrumify.database.services.WorkItemStatusService;
import pt.com.scrumify.entities.EpicFilterView;
import pt.com.scrumify.entities.ItemStatusView;

@Service
public class EpicsHelper {
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private ContextHelper contextHelper;
   @Autowired
   private GeneralHelper generalHelper;
   @Autowired
   private EntityManager entityManager;
   @Autowired
   private EpicService epicsService;
   @Autowired
   private WorkItemStatusService itemStatusService;
   
   private static final Logger logger = LoggerFactory.getLogger(EpicsHelper.class);

   /*
    * Try and generalize this one, if not remove it
    */
   public EpicFilterView generateFilter() {
      EpicFilterView filter = new EpicFilterView();

      for (WorkItemStatus status : itemStatusService.getOneByTypeOfWorkitemById(TypeOfWorkItem.EPIC)) {
         filter.addStatus(new ItemStatusView(status));
      }

      return filter;
   }

   public List<Epic> getByFilter(EpicFilterView filter) {
      List<Epic> epics = new ArrayList<>();
      Resource resource = ResourcesHelper.getResource();

      if (filter.getStatuses() != null && !filter.getStatuses().isEmpty()) {
         epics = epicsService.getByStatusesAndResource(resource, getStatusesIds(filter.getStatuses()));
         
         if (filter.getSearchQuery() != null && !filter.getSearchQuery().isEmpty()) {

            List<Epic> temp = new ArrayList<>();
            for (Epic epic : epics) {
               if (StringUtils.containsIgnoreCase(epic.getName(), filter.getSearchQuery()) || StringUtils.containsIgnoreCase(epic.getDescription(), filter.getSearchQuery()) || StringUtils.containsIgnoreCase(epic.getNumber(), filter.getSearchQuery()))
                  temp.add(epic);
            }
            
            epics = temp;
         }
      }
      else {
         epics = epicsService.getAllByResource(ResourcesHelper.getResource());
      }

      Team activeTeam = contextHelper.getActiveTeam();
      if (activeTeam != null) {
         epics =  epics.stream().filter(x -> x.getTeam().getId().equals(activeTeam.getId())).collect(Collectors.toList());
      }
      
      return epics;
   }

   @Transactional
   public List<Epic> fuzzySearch(String searchTerm) {
      FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
      QueryBuilder qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Epic.class).get();

      Query luceneQuery = qb.keyword().fuzzy().withEditDistanceUpTo(1).onFields("name", "description", "number").matching(searchTerm).createQuery();

      javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, Epic.class);

      List<Epic> epics = new ArrayList<>();
      try {
         epics = generalHelper.castList(Epic.class, jpaQuery.getResultList());

      } catch (NoResultException nre) {
         // do nothing.
      }
      return epics;
   }

   public List<Integer> getStatusesIds(List<ItemStatusView> statuses) {
      List<Integer> ids = new ArrayList<>();
      for (ItemStatusView status : statuses) {
         ids.add(status.getId());
      }
      return ids;
   }

   public EpicFilterView getFilterFromCookie(String cookieName) {
      EpicFilterView filter = null;
      
      /*
       * Get cookie data from database
       */
      Cookie cookie = cookiesHelper.get(cookieName, ResourcesHelper.getResource());
      if (cookie == null) {
         /*
          * If cookie not exists, create cookie with default values (status = null)
          */
         if (filter == null) {
            filter = new EpicFilterView();
            
            cookiesHelper.save(cookieName, ResourcesHelper.getResource(), filter);
         }
      }
      else {
         /*
          * If cookie exists, get data from cookie
          */
         ObjectMapper mapper = new ObjectMapper();
         
         try {
            filter = mapper.readValue(cookie.getData(), EpicFilterView.class);
         }
         catch (Exception ex) {
            logger.error("Exception was caught when trying to serialize epic filter.", ex);
         }
      }
      
      return filter;
   }
}