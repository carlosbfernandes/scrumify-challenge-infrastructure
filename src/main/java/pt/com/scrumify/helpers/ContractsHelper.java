package pt.com.scrumify.helpers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.ContractProperty;
import pt.com.scrumify.database.entities.ContractStatus;
import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.ContractPropertyService;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.ContractStatusService;
import pt.com.scrumify.database.services.TimesheetApprovalService;
import pt.com.scrumify.entities.ContractFilterView;
import pt.com.scrumify.entities.ContractStatusView;
import pt.com.scrumify.entities.ContractView;

@Service
public class ContractsHelper {
   @Autowired
   private ContractPropertyService contractsPropertiesService;
   @Autowired
   private ContractService contractsService;
   @Autowired
   private ContractStatusService contractStatusService;
   @Autowired
   private TimesheetApprovalService billablesService;
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private DatesHelper datesHelper;
   @Autowired
   private TimesHelper timesHelper;
   
   private static final Logger logger = LoggerFactory.getLogger(ContractsHelper.class);

   public List<Integer> listOfPropertie(List<ContractProperty> properties) {
      List<Integer> idList =  new ArrayList<>();
      
      for (ContractProperty property : properties){
         idList.add(property.getProperty().getId());   
      }
      return idList;
   }
   
   public List<ContractView> getContractReport(List<Contract> contracts, Date startingDate){
      List<ContractView> views = new ArrayList<>();
      
      for(Contract contract : contracts) {
         ContractView view = new ContractView(contract);
         Year year = new Year(datesHelper.getYearOfDate(startingDate));
         Integer month = datesHelper.getMonthOfDate(startingDate);
         
         view.setHoursSpent(billablesService.getHoursForStatusReportByContract(contract));
         view.setHoursSpentInterval(billablesService.getHoursForStatusReport(contract, year, month));
         
         views.add(view);
      }
      
      
      return views;
   }
   
   public Contract save(ContractView view) {      
      Contract contract = view.translate();
      if(view.getEndingDay() != null) {
         contract.setEndingDay(new Time(timesHelper.getTime(view.getEndingDay())));
      } 
      else {
         contract.setEndingDay(null);
      }
      
      contract.setStartingDay(new Time(timesHelper.getTime(view.getStartingDay())));
      
      return this.contractsService.save(contract);
   }

   public String getContractNumber(Integer contract, String propertyName) {
      ContractProperty property = contractsPropertiesService.getByPropertyNameAndContract(contract, propertyName);
      
      if(property!=null && property.getValue()!=null && !property.getValue().isEmpty())
         return property.getValue() + " - ";
      
      return "";
   }
   
   public ContractFilterView getFilterFromCookie(String cookieName) {
      ContractFilterView filter = null;
      
      /*
       * Get cookie data from database
       */
      Cookie cookie = cookiesHelper.get(cookieName, ResourcesHelper.getResource());
      if (cookie == null) {
         /*
          * If cookie not exists, create cookie with default values (status = all)
          */
         if (filter == null) {
            filter = new ContractFilterView();
            
            for(ContractStatus status : contractStatusService.getAllStatus()) 
               filter.addContractStatus(new ContractStatusView(status));
            
            cookiesHelper.save(cookieName, ResourcesHelper.getResource(), filter);
         }
      }
      else {
         /*
          * If cookie exists, get data from cookie
          */
         ObjectMapper mapper = new ObjectMapper();
         
         try {
            filter = mapper.readValue(cookie.getData(), ContractFilterView.class);
         }
         catch (Exception ex) {
            logger.error("Exception was caught when trying to serialize epic filter.", ex);
         }
      }
      
      return filter;
   }
}