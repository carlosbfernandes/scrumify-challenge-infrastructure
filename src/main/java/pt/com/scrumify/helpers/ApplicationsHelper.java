package pt.com.scrumify.helpers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.ApplicationProperty;

@Service
public class ApplicationsHelper {
   public List<Integer> listOfProperties(List<ApplicationProperty> properties, int environment) {
      List<Integer> result = new ArrayList<>();

      for (ApplicationProperty property : properties) {
         if (environment == property.getPk().getEnvironment().getId())
            result.add(property.getProperty().getId());
      }
      
      return result;
   }
}