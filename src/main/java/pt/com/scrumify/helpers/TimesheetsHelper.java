package pt.com.scrumify.helpers;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.fasterxml.jackson.databind.ObjectMapper;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Holiday;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimeSchedule;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetApproval;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.TimesheetStatus;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.database.services.CompensationDayService;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.HolidayService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.TimeScheduleService;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.services.TimesheetApprovalService;
import pt.com.scrumify.database.services.TimesheetDayService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.database.services.VacationService;
import pt.com.scrumify.database.services.WorkItemWorkLogService;
import pt.com.scrumify.database.services.YearService;
import pt.com.scrumify.entities.ContractApprovalView;
import pt.com.scrumify.entities.TimesheetApprovalView;
import pt.com.scrumify.entities.TimesheetFilterView;
import pt.com.scrumify.entities.TimesheetTotalView;
import pt.com.scrumify.entities.TimesheetView;

@Service
public class TimesheetsHelper {
   @Autowired
   private AbsenceService absenceService;
   @Autowired
   private ContractService contractsService;
   @Autowired
   private TimeService timesService;
   @Autowired
   private TimesheetService timesheetService;
   @Autowired
   private TimeScheduleService timeScheduleService;
   @Autowired
   private WorkItemWorkLogService workItemsWorkLogService;
   @Autowired
   private VacationService vacationsService;
   @Autowired
   private AbsenceService absencesService;
   @Autowired
   private HolidayService holidaysService;
   @Autowired
   private CompensationDayService compensationDaysService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private TeamService teamService;
   @Autowired
   private TimesheetApprovalService timesheetsBillableService;
   @Autowired
   private TimesheetDayService timesheetDayService;
   @Autowired
   private YearService yearsService;
   @Autowired
   private CookiesHelper cookieHelper;
   @Autowired
   private TimesheetApprovalHelper timesheetApprovalHelper;
   @Autowired
   private YearsHelper yearsHelper;
   
   private static final Logger logger = LoggerFactory.getLogger(TimesheetsHelper.class);

   /*
    * TODO: code review
    */
   public List<TimesheetDay> createNewTimesheets1(Resource resource, Year year, Integer month) {
      Schedule schedule = resource.getSchedule();
      List<Time> times = timesService.findByYearAndMonth(year, month);
      List<TimesheetDay> days = new ArrayList<>();
      List<WorkItemWorkLog> workItemWorkLog = workItemsWorkLogService.getByYearAndMonthAndResourceOrderByDate(year, month, resource);
      List<Vacation> vacations = vacationsService.listByResourceYearAndMonth(resource, year, month);
      List<Absence> absences = absencesService.listByResourceYearAndMonth(resource, year, month);
      List<Holiday> holidays = holidaysService.getByYearAndMonth(year, month);
      List<CompensationDay> compensation = compensationDaysService.getByYearAndMonth(year, month);

      Time firstDay = times.get(0);
      Time fifteenthDay = times.get(14);
      Time sixteenthDay = times.get(15);
      Time lastDay = times.get(times.size() - 1);

      List<TimeSchedule> timeSchedules = timeScheduleService.getByYearMonthAndFortnight(year, month, 1, schedule);

      Timesheet q1 = new Timesheet();
      q1.setResource(resource);
      q1.setStartingDay(firstDay);
      q1.setEndingDay(fifteenthDay);
      q1.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
      q1.setSubmitted(false);
      q1.setApproved(false);
      q1.setReviewed(false);

      Timesheet q2 = new Timesheet();
      q2.setResource(resource);
      q2.setStartingDay(sixteenthDay);
      q2.setEndingDay(lastDay);
      q2.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
      q2.setSubmitted(false);
      q2.setApproved(false);
      q2.setReviewed(false);

      for (TimeSchedule timeSchedule : timeSchedules) {
         TimesheetDay day = new TimesheetDay();
         Time time = timeSchedule.getTime();
         day.setDay(time);

         if (time.getDay() <= 15) {
            q1.getDays().add(day);
            day.setTimesheet(q1);
         } else {
            q2.getDays().add(day);
            day.setTimesheet(q2);
         }

         day.setHours(timeSchedule.getHours());
         List<WorkItemWorkLog> workItemWorkLogfilt = workItemWorkLog.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         day.setTimeSpent(workItemWorkLogfilt.stream().mapToInt(WorkItemWorkLog::getTimeSpent).sum());

         List<Vacation> vacationsfilt = vacations.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!vacationsfilt.isEmpty()) {
            day.setHoursVacation(timeSchedule.getHours());
         }

         List<CompensationDay> compensationDaysfilt = compensation.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!compensationDaysfilt.isEmpty()) {
            day.setHoursCompensationDay(timeSchedule.getHours());
         }

         List<Absence> absencesfilt = absences.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!absencesfilt.isEmpty()) {
            day.setHoursAbsence(absencesfilt.stream().mapToInt(Absence::getHours).sum());
         }

         List<Holiday> holidaysFilt = holidays.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!holidaysFilt.isEmpty())
            day.setHoursHoliday(timeSchedule.getHours());

         day.setWeekend(time.getDayOfWeek() >= 6);

         days.add(day);
      }

      timesheetService.save(q1);
      timesheetService.save(q2);

      return days;
   }

   /*
    * TODO: code review, check if this method is used on the project
    */
   public List<TimesheetDay> createTimesheet(Resource resource, Year year, Integer month, Integer fortnight) {
      Schedule schedule = resource.getSchedule();
      List<TimeSchedule> timeSchedules = timeScheduleService.getByYearMonthAndFortnight(year, month, fortnight, schedule);

      List<Absence> absences = absencesService.listByResourceYearAndMonth(resource, year, month);
      List<CompensationDay> compensation = compensationDaysService.getByYearAndMonth(year, month);
      List<Holiday> holidays = holidaysService.getByYearAndMonth(year, month);
      List<Vacation> vacations = vacationsService.listByResourceYearAndMonth(resource, year, month);
      List<WorkItemWorkLog> worklog = workItemsWorkLogService.getByYearAndMonthAndResourceOrderByDate(year, month, resource);
      
      Timesheet ts = new Timesheet();
      ts.setResource(resource);
      ts.setStartingDay(timeSchedules.get(0).getTime());
      ts.setEndingDay(timeSchedules.get(timeSchedules.size() - 1).getTime());
      ts.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
      ts.setSubmitted(false);
      ts.setApproved(false);
      ts.setReviewed(false);
      
      List<TimesheetDay> days = new ArrayList<>();
      
      for (TimeSchedule timeSchedule : timeSchedules) {
         TimesheetDay day = new TimesheetDay();
         Time time = timeSchedule.getTime();
         day.setDay(time);

         ts.getDays().add(day);
         day.setTimesheet(ts);

         List<Absence> filteredAbsences = absences.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!filteredAbsences.isEmpty()) {
            day.setHoursAbsence(filteredAbsences.stream().mapToInt(Absence::getHours).sum());
         }

         List<CompensationDay> filteredCompensationDays = compensation.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!filteredCompensationDays.isEmpty()) {
            day.setHoursCompensationDay(timeSchedule.getHours());
         }

         List<Holiday> filteredHolidays = holidays.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!filteredHolidays.isEmpty()) {
            day.setHoursHoliday(timeSchedule.getHours());
         }

         List<Vacation> filteredVacations = vacations.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         if (!filteredVacations.isEmpty()) {
            day.setHoursVacation(timeSchedule.getHours());
         }
         
         day.setHours(timeSchedule.getHours());
         List<WorkItemWorkLog> filteredWorkLogs = worklog.stream().filter(t -> t.getDay().getId().equals(time.getId())).collect(Collectors.toList());
         day.setTimeSpent(filteredWorkLogs.stream().mapToInt(WorkItemWorkLog::getTimeSpent).sum());

         day.setWeekend(time.getDayOfWeek() >= 6);

         days.add(day);
      }

      timesheetService.save(ts);
      
      return days;
   }
   
   /*
    * TODO: code review
    */
   public String getTabTimesheet(String step, Year year, Integer month, Resource resource, Model model) {
      switch (step) {
         case ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMHOURS :
            List<WorkItemWorkLog> worklogs = workItemsWorkLogService.getByYearAndMonthAndResourceOrderByDate(year, month, resource);
            model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMLOGS, worklogs);
            return ConstantsHelper.VIEW_TIMESHEET_TABHOURS;

         case ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES :
            List<Absence> absences = absencesService.listByResourceYearAndMonth(resource, year, month);
            model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, absences);
            return ConstantsHelper.VIEW_TIMESHEET_TABABSENCES;

         case ConstantsHelper.VIEW_ATTRIBUTE_COMPENSATIONDAYS :
            List<CompensationDay> compensationdays = compensationDaysService.getByYearAndMonth(year, month);
            model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_COMPENSATIONDAYS, compensationdays);
            return ConstantsHelper.VIEW_TIMESHEET_TABCOMPENSATION;

         case ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAYS :
            List<Holiday> holidays = holidaysService.getByYearAndMonth(year, month);
            model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAYS, holidays);
            return ConstantsHelper.VIEW_TIMESHEET_TABHOLIDAYS;

         case ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS :
            List<Vacation> vacations = vacationsService.listByResourceYearAndMonth(resource, year, month);
            model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, vacations);
            return ConstantsHelper.VIEW_TIMESHEET_TABVACATIONS;

         case ConstantsHelper.VIEW_ATTRIBUTE_COMMENTS :
            List<Timesheet> comments = timesheetService.getSheetsWithComments(resource, year, month);
            model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_COMMENTS, comments);
            return ConstantsHelper.VIEW_TIMESHEET_TABCOMMENTS;

         case ConstantsHelper.VIEW_ATTRIBUTE_SUMMARY :
         default :
            List<Timesheet> sheets = timesheetService.getSheets(resource, year, month);
            model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, sheets);
            return ConstantsHelper.VIEW_TIMESHEET_TABSUMMARY;
      }
   }

   public Timesheet findTimeSheetOfToday(List<Timesheet> timeSheets) {
      LocalDate today = LocalDate.now();

      for (Timesheet timeSheet : timeSheets) {
         Time startDay = timeSheet.getStartingDay();
         Time endDay = timeSheet.getEndingDay();
         if (startDay.getYear().getId() == today.getYear() && startDay.getMonth() == today.getMonthValue() && startDay.getDay() <= today.getDayOfMonth()
               && endDay.getYear().getId() == today.getYear() && endDay.getMonth() == today.getMonthValue() && endDay.getDay() >= today.getDayOfMonth()) {
            return timeSheet;
         }
      }
      return null;
   }

   public boolean canSubmit(Timesheet timesheet) {
      
      return true;
      
      //Removed on TSK20000139 - should be validated later on
      /*List<TimesheetDay> days = timesheet.getDays();

      int sumHours = days.stream().mapToInt(TimesheetDay::getHours).sum();
      int sumVacation = days.stream().mapToInt(TimesheetDay::getVacation).sum();
      int sumSpent = days.stream().mapToInt(TimesheetDay::getTimespent).sum();
      int sumAbsence = days.stream().mapToInt(TimesheetDay::getAbsence).sum();
      int sumCompAndHolidays = days.stream().mapToInt(TimesheetDay::getCompensationDay).sum() + days.stream().mapToInt(TimesheetDay::getHoliday).sum();
      
      return sumSpent + sumAbsence + sumCompAndHolidays + sumVacation == sumHours;
      */
      
   }

   public void commentValidation(Timesheet timesheet, TimesheetView timesheetView) {
      if (!timesheetView.getComment().equals(timesheet.getComment())) {
         if (timesheetView.getComment().trim().isEmpty()) {
            timesheet.setCommentedBy(null);
            timesheet.setComment(null);
         } else {
            timesheet.setCommentedBy(ResourcesHelper.getResource());
            timesheet.setComment(timesheetView.getComment());
         }
      }
   }

   /*
    * TODO: code review
    */
//   public void messageInfo(Model model, Timesheet timesheet) {
//      if (timesheet != null && !timesheet.getSubmitted() && timesheet.getReviewedBy() != null && timesheet.getApprovedBy() == null) {
//         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE,
//               messagesHelper.createMessage(ConstantsHelper.ERROR_MESSAGE, ConstantsHelper.ERROR_ICON_MESSAGE,
//                     messageSource.getMessage(
//                           ConstantsHelper.MESSAGE_VALIDATION_TIMESHEET_REJECTBY, new Object[]{timesheet.getStartingDay().getFortnight(),
//                                 ConstantsHelper.MONTHS.get(timesheet.getStartingDay().getMonth() - 1), ConstantsHelper.REVIEWER, timesheet.getReviewedBy().getName()},
//                           LocaleContextHolder.getLocale())));
//      } else if (timesheet != null && !timesheet.getSubmitted() && timesheet.getApprovedBy() != null) {
//         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE,
//               messagesHelper.createMessage(ConstantsHelper.ERROR_MESSAGE, ConstantsHelper.ERROR_ICON_MESSAGE,
//                     messageSource.getMessage(
//                           ConstantsHelper.MESSAGE_VALIDATION_TIMESHEET_REJECTBY, new Object[]{timesheet.getStartingDay().getFortnight(),
//                                 ConstantsHelper.MONTHS.get(timesheet.getStartingDay().getMonth() - 1), ConstantsHelper.APPROVER, timesheet.getApprovedBy().getName()},
//                           LocaleContextHolder.getLocale())));
//      } else if (timesheet != null && timesheet.getSubmitted() && timesheet.getReviewedBy() != null && timesheet.getApprovedBy() == null) {
//         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE,
//               messagesHelper
//                     .createMessage(ConstantsHelper.SUCCESS_MESSAGE, ConstantsHelper.SUCCESS_ICON_MESSAGE,
//                           messageSource.getMessage(
//                                 ConstantsHelper.MESSAGE_VALIDATION_TIMESHEET_REVIEWEDBY, new Object[]{timesheet.getStartingDay().getFortnight(),
//                                       ConstantsHelper.MONTHS.get(timesheet.getStartingDay().getMonth() - 1), timesheet.getReviewedBy().getName()},
//                                 LocaleContextHolder.getLocale())));
//      } else if (timesheet != null && timesheet.getSubmitted() && timesheet.getApprovedBy() != null) {
//         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE,
//               messagesHelper
//                     .createMessage(ConstantsHelper.SUCCESS_MESSAGE, ConstantsHelper.SUCCESS_ICON_MESSAGE,
//                           messageSource.getMessage(
//                                 ConstantsHelper.MESSAGE_VALIDATION_TIMESHEET_APPROVEDBY, new Object[]{timesheet.getStartingDay().getFortnight(),
//                                       ConstantsHelper.MONTHS.get(timesheet.getStartingDay().getMonth() - 1), timesheet.getApprovedBy().getName()},
//                                 LocaleContextHolder.getLocale())));
//      }
//   }

   /*
    * TODO: code review
    */
   public Map<Contract, List<TimesheetApprovalView>> createContractsData(List<TimesheetApprovalView> timeSheetsApproval) {
      Map<Contract, List<TimesheetApprovalView>> contractsTimeSheet = new HashMap<>();
      for (TimesheetApprovalView timeSheetApproval : timeSheetsApproval) {
         List<TimesheetApprovalView> lista = new ArrayList<>();
         if (!contractsTimeSheet.containsKey(timeSheetApproval.getContract())) {
            addContract(lista, timeSheetApproval, contractsTimeSheet);
         }
         
         Iterator<Entry<Contract, List<TimesheetApprovalView>>> it = contractsTimeSheet.entrySet().iterator();
         while (it.hasNext()) {
            Entry<Contract, List<TimesheetApprovalView>> pair = it.next();
            Collections.sort((List<TimesheetApprovalView>) pair.getValue(), new SortbyMonth());
         }
         
         if(!lista.isEmpty()) {
            lista.get(lista.size() - 1).setTotalHours(lista.stream().collect(Collectors.summingInt(TimesheetApprovalView::getHours)));
            
            lista.get(lista.size() - 1)
                  .setBalance((timeSheetApproval.getContract().getYear().getId().equals(timeSheetApproval.getYear().getId())
                        ? timeSheetApproval.getContract().getHours1stYear()
                        : timeSheetApproval.getContract().getHours2ndYear()) - lista.get(lista.size() - 1).getTotalHours());
         }
         
      }
      return contractsTimeSheet;
   }
   
   /*
    * TODO: code review
    */
   public List<ContractApprovalView> getContractsInfo(Year year){
      List<ContractApprovalView> contractsView = new ArrayList<>();
      List<TimesheetApproval> billables = timesheetsBillableService.getContractsInfo(year);
      
      Map<Contract, List<TimesheetApproval>> grouped = billables.stream().collect(
            Collectors.groupingBy(TimesheetApproval::getContract));
      
      for (Map.Entry<Contract, List<TimesheetApproval>> entry : grouped.entrySet()) {
         ContractApprovalView contractView = new ContractApprovalView();
         contractView.setContract(entry.getKey());
         updateYears(contractView, year);
         
         for(TimesheetApproval billable : entry.getValue()) {
            switch (billable.getMonth()) {
               case 1:
                  contractView.setJanuary(billable.getWorkingHours());
                  break;
               case 2:
                  contractView.setFebruary(billable.getWorkingHours());
                  break;
               case 3:
                  contractView.setMarch(billable.getWorkingHours());
                  break;
               case 4:
                  contractView.setApril(billable.getWorkingHours());
                  break;
               case 5:
                  contractView.setMay(billable.getWorkingHours());
                  break;
               case 6:
                  contractView.setJune(billable.getWorkingHours());
                  break;
               case 7:
                  contractView.setJuly(billable.getWorkingHours());
                  break;
               case 8:
                  contractView.setAugust(billable.getWorkingHours());
                  break;
               case 9:
                  contractView.setSeptember(billable.getWorkingHours());
                  break;
               case 10:
                  contractView.setOctober(billable.getWorkingHours());
                  break;
               case 11:
                  contractView.setNovember(billable.getWorkingHours());
                  break;
               case 12:
                  contractView.setDecember(billable.getWorkingHours());
                  break;
               default :
                  break;
            }
         }
         
         contractsView.add(contractView);
      }
      
      return contractsView;
   }
   
   private void updateYears(ContractApprovalView contractView, Year year) {
      if(year.getId().equals(contractView.getContract().getYear().getId())) {
         contractView.setHoursCurrentYear(contractView.getContract().getHours1stYear());
         contractView.setHoursPreviousYear(0);
      } 
      else if(year.getId().intValue()-1 == contractView.getContract().getYear().getId().intValue()) {
         contractView.setHoursCurrentYear(contractView.getContract().getHours2ndYear());
         contractView.setHoursPreviousYear(contractView.getContract().getHours1stYear());
      }      
   }
   
   /*
    * TODO: code review
    */
   public TimesheetTotalView getTotals(List<Timesheet> timesheets) {
      TimesheetTotalView total = new TimesheetTotalView();
      for(Timesheet timesheet : timesheets) {
         total.setTotalHours(total.getTotalHours() + timesheet.getDays().stream().mapToInt(TimesheetDay::getHours).sum());
         total.setTimespent(total.getTimespent() + timesheet.getDays().stream().mapToInt(TimesheetDay::getTimeSpent).sum());
         total.setAbsences(total.getAbsences() + timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursAbsence).sum());
         total.setCompensationDays(total.getCompensationDays() + timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursCompensationDay).sum());
         total.setHolidays(total.getHolidays() + timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursHoliday).sum());
         total.setVacations(total.getVacations() + timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursVacation).sum());
      }
      
      total.setOccupation((int) Math.round((total.getTimespent() + total.getAbsences() + total.getVacations() + total.getCompensationDays() + total.getHolidays()) * 100.0 / (total.getTotalHours())));
      
      return total;
   }
   
//   public TimesheetTotalView getTotals(List<TimesheetView> timesheets) {
//      TimesheetTotalView total = new TimesheetTotalView();
//      
//      for(TimesheetView timesheet : timesheets) {
//         total.setTotalHours(total.getTotalHours() + timesheet.getDays().stream().mapToInt(TimesheetDay::getHours).sum());
//         total.setTimespent(total.getTimespent() + timesheet.getDays().stream().mapToInt(TimesheetDay::getTimespent).sum());
//         total.setAbsences(total.getAbsences() + timesheet.getDays().stream().mapToInt(TimesheetDay::getAbsence).sum());
//         total.setCompensationDays(total.getCompensationDays() + timesheet.getDays().stream().mapToInt(TimesheetDay::getCompensationDay).sum());
//         total.setHolidays(total.getHolidays() + timesheet.getDays().stream().mapToInt(TimesheetDay::getHoliday).sum());
//         total.setVacations(total.getVacations() + timesheet.getDays().stream().mapToInt(TimesheetDay::getVacation).sum());
//      }
//      
//      total.setOccupation((int) Math.round(
//            (total.getTimespent() + total.getAbsences() + total.getVacations() + total.getCompensationDays() + total.getHolidays()) * 100.0
//            / (total.getTotalHours())));
//      
//      return total; 
//   }
   
   /*
    * Placeholder method that creates a row with all the months, hours and balance totals.   
    * Should be replaced when a view is created for the Contracts Tab info. 
    */
   /*
    * TODO: code review
    */
   public int[] getTotalsContractsInfo(Map<Contract, List<TimesheetApprovalView>> view) {
      int[] array = new int[14];

      for (Entry<Contract, List<TimesheetApprovalView>> entry : view.entrySet()) {
         for (int i = 0; i < entry.getValue().size(); i++) {
            if (entry.getValue().get(i).getMonth() == i + 1) {
               array[i] += entry.getValue().get(i).getHours();
               array[12] += entry.getValue().get(i).getTotalHours();
               array[13] += entry.getValue().get(i).getBalance();
            }
         }
      }
      return array;
   }
   
   /*
    * TODO: code review
    */
   private void addContract(List<TimesheetApprovalView> lista, TimesheetApprovalView timeSheetApproval, Map<Contract, List<TimesheetApprovalView>> contractsTimeSheet) {
      List<Contract> contracts = contractsService.getContractsMyTeamsWorkedOnYear(teamService.getByMember(ResourcesHelper.getResource()), timeSheetApproval.getYear());
      for (Contract contract : contracts) {
         for (int i = 1; i <= 12; i++) {
            List<TimesheetApproval> billables = timesheetsBillableService.getbyYearAndMonthAndContract(
                  timeSheetApproval.getYear(), i, contract.getId());
            TimesheetApprovalView timeSheetApprovalView = new TimesheetApprovalView();
            timeSheetApprovalView.setContract(contract);
            timeSheetApprovalView.setYear(timeSheetApproval.getYear());
            timeSheetApprovalView.setMonth(i);
            if(billables!=null && !billables.isEmpty())
               timeSheetApprovalView.setHours(billables.stream().mapToInt(TimesheetApproval::getWorkingHours).sum());
            else
               timeSheetApprovalView.setHours(0);
            
            lista.add(timeSheetApprovalView);
         }
         contractsTimeSheet.put(contract, lista);
      }
   }
   
   /*
    * TODO: code review
    */
   class SortbyMonth implements Comparator<TimesheetApprovalView> {
      public int compare(TimesheetApprovalView a, TimesheetApprovalView b) {
         return a.getMonth() - b.getMonth();
      }
   }

   /*
    * TODO: code review
    */
   public List<TimesheetApproval> createBillables(Timesheet timesheet) {

      List<TimesheetApproval> billables = new ArrayList<>();
      Year year = timesheet.getStartingDay().getYear();
      Integer month = timesheet.getStartingDay().getMonth();
      Resource resource = timesheet.getResource();
      Integer fortnight = timesheet.getStartingDay().getFortnight();

      List<Contract> contracts = contractsService.getContractsWorkedOnThisFortnight(resource, timesheet.getStartingDay().getDate(), timesheet.getEndingDay().getDate());

      if (!contracts.isEmpty()) {
         billables.addAll(getWorkedHours(contracts, resource, year, month, fortnight));
      }
      billables.addAll(getNonWorkingHours(resource, month, year, fortnight, timesheet.getStartingDay(), timesheet.getEndingDay()));

      timesheetsBillableService.saveAll(billables);
      return billables;
   }
   
   /*
    * TODO: code review
    */
   public void deleteBillables(Timesheet timesheet) {
      timesheetsBillableService.delete(
            timesheetsBillableService.getbyTimesheetAndResource(
                  timesheet.getStartingDay().getYear().getId(), timesheet.getStartingDay().getMonth(), timesheet.getStartingDay().getFortnight(), timesheet.getResource().getId()));
   }

   /*
    * TODO: code review
    */
   private List<TimesheetApproval> getNonWorkingHours(Resource resource, Integer month, Year year, Integer fortnight, Time startingDate, Time endingDate) {

      List<TimesheetApproval> billables = new ArrayList<>();
      Integer vacations = vacationsService.getResourceHoursByTimesheet(year.getId(), month, fortnight, resource).intValue();
      if (vacations != 0) {
         TimesheetApproval billable = new TimesheetApproval(resource, year, month, fortnight);
         billable.setOther(vacations);
         billable.setCode(ConstantsHelper.CODE_VACATIONS);
         billable.setDescription("Vacations");
         billables.add(billable);
      }
      List<Absence> absences = absenceService.listByResourceAndTimesheet(resource, startingDate, endingDate);
      for (Absence absence : absences) {
         TimesheetApproval billable = new TimesheetApproval(resource, year, month, fortnight);
         billable.setOther(absence.getHours());
         billable.setCode(absence.getType().getCode());
         billable.setDescription(absence.getType().getName());
         billables.add(billable);
      }

      Integer compensation = compensationDaysService.getResourceHoursByTimesheet(year.getId(), month, fortnight, resource.getSchedule()).intValue();
      if (compensation != 0) {
         TimesheetApproval billable = new TimesheetApproval(resource, year, month, fortnight);
         billable.setOther(compensation);
         billable.setCode(ConstantsHelper.CODE_COMPENSATIONDAYS);
         billable.setDescription("Compensation Days");
         billables.add(billable);
      }

      Integer holidays = holidaysService.getResourceHoursByTimesheet(year.getId(), month, fortnight, resource.getSchedule()).intValue();
      if (holidays != 0) {
         TimesheetApproval billable = new TimesheetApproval(resource, year, month, fortnight);
         billable.setOther(holidays);
         billable.setCode(ConstantsHelper.CODE_HOLIDAYS);
         billable.setDescription("Holidays");
         billables.add(billable);
      }

      return billables;
   }

   /*
    * TODO: code review
    */
   private List<TimesheetApproval> getWorkedHours(List<Contract> contracts, Resource resource, Year year, Integer month, Integer fortnight) {
      List<TimesheetApproval> billables = new ArrayList<>();
      for (Contract contract : contracts) {
         List<WorkItemWorkLog> worklogs = workItemsWorkLogService.getByYearAndMonthAndResourcesAndFortnighAndContractOrderByDate(year, month, resource, fortnight, contract);
         TimesheetApproval billable = new TimesheetApproval(resource, year, month, fortnight);
         billable.setMonth(month);
         billable.setCode(contract.getCode());
         billable.setContract(contract);
         billable.setDescription(contract.getName());

         for (WorkItemWorkLog worklog : worklogs) {
            switch (worklog.getTypeOfWork().getAbbreviation()) {
               case ConstantsHelper.TYPEOFWORK_ANALYSIS_ABR :
                  billable.setAnalysis(billable.getAnalysis() + worklog.getTimeSpent());
                  break;
               case ConstantsHelper.TYPEOFWORK_DESIGN_ABR :
                  billable.setDesign(billable.getDesign() + worklog.getTimeSpent());

                  break;
               case ConstantsHelper.TYPEOFWORK_DEVELOPMENT_ABR :
                  billable.setDevelopment(billable.getDevelopment() + worklog.getTimeSpent());

                  break;
               case ConstantsHelper.TYPEOFWORK_TEST_ABR :
                  billable.setTest(billable.getTest() + worklog.getTimeSpent());

                  break;
               case ConstantsHelper.TYPEOFWORK_OTHER_ABR :
                  billable.setOther(billable.getOther() + worklog.getTimeSpent());
                  break;
               default :
                  break;
            }
         }
         billables.add(billable);
      }

      return billables;
   }
   
   /*
    * TODO: code review
    */
   public Integer getOccupation(Timesheet timesheet) {
      
      int total = timesheet.getDays().stream().mapToInt(TimesheetDay::getHours).sum();
      int sum = timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursAbsence).sum() + 
                timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursCompensationDay).sum() + 
                timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursHoliday).sum() +
                timesheet.getDays().stream().mapToInt(TimesheetDay::getHoursVacation).sum() + 
                timesheet.getDays().stream().mapToInt(TimesheetDay::getTimeSpent).sum();
      
      return (int) Math.round(sum * 100.0/ total);
   }

   /*
    * TODO: code review
    */
   public String getLastDateofTimeSheetApprovedorReviewed() {
      Resource resource = ResourcesHelper.getResource();
      Date dateApprovedorReviewed = timesheetService.dateLastTimeSheetApprovedorReviewed(resource);
      String formattedDate = null;
      if (dateApprovedorReviewed != null) {
         formattedDate = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME).format(dateApprovedorReviewed);
      } else {
         formattedDate = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME).format(resource.getStartingDay());
      }
      return formattedDate;
   }

   /*
    * TODO: code review
    */
   public List<TimesheetApproval> preview(Timesheet timesheet) {
      List<TimesheetApproval> billables = new ArrayList<>();
      Year year = timesheet.getStartingDay().getYear();
      Integer month = timesheet.getStartingDay().getMonth();
      Resource resource = timesheet.getResource();
      Integer fortnight = timesheet.getStartingDay().getFortnight();
      List<Contract> contracts = contractsService.getContractsWorkedOnThisFortnight(resource, timesheet.getStartingDay().getDate(),
            DatesHelper.addDays(timesheet.getEndingDay().getDate(), 1));

      billables.addAll(getWorkedHours(contracts, resource, year, month, fortnight));
      billables.addAll(getNonWorkingHours(resource, month, year, fortnight, timesheet.getStartingDay(), timesheet.getEndingDay()));

      return billables;
   }
   
   public void loadTimesheet(Model model, Integer year, Integer month) {
      /*
       * TODO: code review
       */
      Resource resource = ResourcesHelper.getResource();
      Year yearEntity = new Year(year);
      List<TimesheetDay> days = timesheetDayService.findByYearAndMonthOrderByDate(yearEntity, month,resource);

      if (days.isEmpty()) {
//         days = createNewTimesheets(resource, yearEntity, month);
      }    
      List<Timesheet> sheets = timesheetService.getSheets(resource, yearEntity, month);
      Timesheet timesheet = findTimeSheetOfToday(sheets);
      if(timesheet == null){
         timesheet = new Timesheet();
      }
      timesheet.setMonth(month);
      timesheet.setYear(yearEntity.getId());
            
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_YEAR, yearEntity.getId());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_MONTH, month);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getAllActive());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETDAYS, days);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, sheets);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET, timesheet);
      Date dateApprovedorRevised = timesheetService.dateLastTimeSheetApprovedorReviewed(ResourcesHelper.getResource());
      String formattedDate = null;
      if(dateApprovedorRevised != null){
          formattedDate = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME).format(dateApprovedorRevised);
      }
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATE_LASTDAYAPPROVED, formattedDate);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LIMIT_DATE, yearsHelper.getMaxActiveYear());
   }
   
//   public List<TimesheetView> getBasedOnReviewFilter(TimesheetFilter filter){
//      List<Timesheet> result = new ArrayList<>();
//      Year year = new Year(filter.getYear());
//      Integer month = filter.getMonth();
//      Integer fortnight = filter.getFortnight();
//      String status = filter.getStatus();
//      List<Resource> resources = new ArrayList<>();
//      
//      if (filter.getResource()!=null)
//         resources.add(resourcesService.getOne(filter.getResource()));
//      else
//         resources = resourcesService.findWithTimesheetsToReview(teamService.getByMemberAndReviewerOrApprover(ResourcesHelper.getResource()));
//      
//      for(Resource resource : resources) {
//         List<Timesheet> timesheets = new ArrayList<>();
//         if(fortnight == null)
//            timesheets = timesheetService.getSheets(resource, year, month);
//         else {
//            Timesheet timesheet = timesheetService.getSheetByFortnight(resource, year.getId(), month, fortnight); 
//            if(timesheet!=null)
//               timesheets.add(timesheet);
//         }
//         
//         if (timesheets==null || timesheets.isEmpty()) {
////            createNewTimesheets(resource, year, month);
//            timesheets = timesheetService.getSheets(resource, year, month);
//         }
//         
//         if(status != null && !status.isEmpty()) {
//            timesheets = timesheets.stream()
//               .filter(o -> status.equals(o.getStatus().getName()))
//               .collect(Collectors.toList());
//         }
//         
//         result.addAll(timesheets);
//      }
//      
//      return addOccupationAndStatus(result);
//   }
   
   /*
    * TODO: code review
    */
   public Timesheet acceptReview(Integer id) {
      Timesheet timesheet = timesheetService.getOne(id);
      timesheet.setReviewed(true);
      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.REVIEWED));
      timesheet.setReviewedBy(ResourcesHelper.getResource());
      timesheet.setReviewDate(DatesHelper.now());
      
      return timesheetService.save(timesheet);
   }
   
   /*
    * TODO: code review
    */
   public Timesheet rejectReview(Integer id) {
      Timesheet timesheet = timesheetService.getOne(id);
      timesheet.setSubmitted(false);
      timesheet.setReviewed(false);
      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
      timesheet.setReviewDate(DatesHelper.now());
      
      return timesheetService.save(timesheet);
   }
   
//   public List<TimesheetView> getTimesheetsToReview(List<Resource> resources){
//      List<Timesheet> timesheets = timesheetService.getByResources(resources);
//      
//      return addOccupationAndStatus(timesheets);
//   }
//   
//   public List<TimesheetView> addOccupationAndStatus(List<Timesheet> timesheets){
//      List<TimesheetView> views = new ArrayList<>();
//      for(Timesheet timesheet : timesheets) {
//         TimesheetView view = new TimesheetView(timesheet);
//         view.setOccupation(getOccupation(timesheet));
//         view.setStatus(timesheet.getStatus());
//         
//         views.add(view);
//      }     
//      
//      return views;
//   }
   
   public void load(Model model, TimesheetFilterView filter) {
      /*
       * Load resources
       */
      List<Resource> resources = new ArrayList<>();
      if (filter.getResource() == null) {
         resources = resourcesService.getByTeams(teamService.getByMemberAndReviewer(ResourcesHelper.getResource()));
      }
      else {
         resources.add(new Resource(filter.getResource()));
      }
      
      if (resources.isEmpty()) {
         resources.add(ResourcesHelper.getResource());
      }
      
      /*
       * Get timesheets
       */
      List<Timesheet> timesheets = timesheetService.getByResourcesYearMonthAndFortnight(resources, filter.getYear(), filter.getMonth(), filter.getFortnight());

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheets);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, resources);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getAllActive());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MONTHS, timesService.getMonths());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TOTALS, getTotals(timesheets));
   }
   
   public TimesheetFilterView getFilterFromCookie(String cookieName) {
      TimesheetFilterView filter = null;
      
      /*
       * Get cookie data from database
       */
      Cookie cookie = cookieHelper.get(cookieName, ResourcesHelper.getResource());
      if (cookie == null) {
         /*
          * If cookie not exists, create cookie with default values (current month and year)
          */         
         LocalDate today = LocalDate.now();
         if (filter == null) {
            filter = new TimesheetFilterView(today.getMonthValue(), today.getYear());
            
            cookieHelper.save(cookieName, ResourcesHelper.getResource(), filter);
         }
      }
      else {
         /*
          * If cookie exists, get data from cookie
          */
         ObjectMapper mapper = new ObjectMapper();
         
         try {
            filter = mapper.readValue(cookie.getData(), TimesheetFilterView.class);
         }
         catch (Exception ex) {
            logger.error("Exception was caught when trying to serialize timesheet filter.", ex);
         }
      }
      
      return filter;
   }
   
   public Timesheet approve(Timesheet timesheet) {
      timesheet.setApprovalDate(DatesHelper.now());
      timesheet.setApproved(true);
      timesheet.setApprovedBy(ResourcesHelper.getResource());
      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.APPROVED));
      
      return timesheetService.save(timesheet);      
   }
   
   @Transactional
   public void approveReview(Timesheet timesheet) {
      timesheet.setReviewed(true);
      timesheet.setReviewedBy(ResourcesHelper.getResource());
      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.REVIEWED));
      
      timesheetApprovalHelper.transfer(timesheet, timesheet.getReviews());
      timesheetService.save(timesheet);
   }
   
   public void reject(Timesheet timesheet) {
      timesheet.setSubmitted(false);
      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
      
      timesheet.setReviewed(false);
      timesheet.setReviewedBy(null);
      timesheet.setReviewDate(null);
       
      timesheet.setApproved(false);
      timesheet.setApprovedBy(null);
      timesheet.setApprovalDate(null);
      
      timesheetService.save(timesheet);
   }
   
   public void revert(Timesheet timesheet) {
      if (!timesheet.getStatus().getId().equals(TimesheetStatus.APPROVED)) {
         timesheet.setSubmitted(false);
         timesheet.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));

         timesheet.setReviewed(false);
         timesheet.setReviewedBy(null);
         timesheet.setReviewDate(null);
         
         timesheetService.save(timesheet);
      }
   }
   
   public void submit(Timesheet timesheet) {
      timesheet.setSubmitted(true);
      timesheet.setReviewed(false);
      timesheet.setReviewedBy(null);
      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.SUBMITTED));
      
      timesheetService.save(timesheet);
   }
}