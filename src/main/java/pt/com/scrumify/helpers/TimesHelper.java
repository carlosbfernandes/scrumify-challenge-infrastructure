package pt.com.scrumify.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.entities.Holiday;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimeSchedule;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.CompensationDayService;
import pt.com.scrumify.database.services.HolidayService;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.entities.ScheduleSetting;

@Service
public class TimesHelper {
   @Autowired
   private TimeService timesService;
   @Autowired
   private HolidayService holidaysService;
   @Autowired
   private CompensationDayService  compensationDaysService;
   @Autowired
   private MessageSource messageSource;
   
   private static Locale localeValue = new Locale("");
   
   public static String now() {
      DateFormat dateFormat = new SimpleDateFormat(ConstantsHelper.DEFAULT_TIMES_FORMAT);
      
      return dateFormat.format(DatesHelper.now());
   }
   
   public String getTime(Date date) {
      DateFormat formatter = new SimpleDateFormat(ConstantsHelper.DEFAULT_TIMES_FORMAT);
      return formatter.format(date);
   }
   
   public List<Time> getTimes(Integer year) {
      List<Time> times = new ArrayList<>(); 
      DateTime datetime = new DateTime(year, 1, 1, 0, 0, DateTimeZone.UTC);
                  
      while (datetime.getYear() == year) {
         LocalDate localDate = LocalDate.fromDateFields(datetime.toDate());
         Time time = new Time();
         time.setId(this.getId(datetime));
         time.setDate(localDate.toDate());
         time.setDay(datetime.getDayOfMonth());
         time.setDayOfWeek(datetime.getDayOfWeek());
         time.setWeek(datetime.getWeekOfWeekyear());
         time.setFortnight(this.getFortnight(datetime));
         time.setMonth(datetime.getMonthOfYear());
         time.setYear(new Year(datetime.getYear()));
         time.setQuarter(this.getQuarter(datetime));
         time.setSemester(this.getSemester(datetime));
         time.setHoliday(false);
         time.setCompensationDay(false);
         
         times.add(time);
         datetime = datetime.plusDays(1);
      }
      
      return times;
   }
   
   private Time getEasterDay(int year) {
      int a = year % 19;
      int b = year / 100;
      int c = year % 100;
      int d = b / 4;
      int e = b % 4;
      int g = (8 * b + 13) / 25;
      int h = (19 * a + b - d - g + 15) % 30;
      int j = c / 4;
      int k = c % 4;
      int m = (a + 11 * h) / 319;
      int r = (2 * e + 2 * j - k - h + m + 32) % 7;
      int month = (h - m + r + 90) / 25;
      int day = (h - m + r + month + 19) % 32;

      return timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, month, day, 0));
   }
   
   public void createHolidays(int year) {
       Time easterDay = getEasterDay(year);
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 01, 1, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_NEWYEAR, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(getId(new DateTime(DateUtils.addDays(easterDay.getDate(), -2)))), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_GOODFRIDAY, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(easterDay, messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_EASTER, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 4, 25, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_FREEDOMDAY, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 5, 1, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_LABOURDAY, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 6, 10, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_NATIONALDAY, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 6, 13, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_SANANTONIO, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(getId(new DateTime(DateUtils.addDays(easterDay.getDate(), 60)))), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_CORPUSCHRISTI, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 8, 15, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_ASSUMPTIONDAY, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 10, 05, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_REPUBLICDAY, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 11, 01, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_ALLSAINTSDAY, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 12, 1, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_RESTORATIONINDE, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 12, 8, 0)),messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_IMMACULATECONCEPTION, null, ConstantsHelper.DEFAULT_TITLE, localeValue));
       createHoliday(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 12, 25, 0)), messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_HOLIDAY_CHRISTMAS, null, ConstantsHelper.DEFAULT_TITLE, localeValue));

   }
   
   public void createCompensationDays(int year) {
      Time easterDay = getEasterDay(year);
      getCarnivalCompensationDays(easterDay,localeValue);
      getEasterCompensationDays(easterDay, localeValue);
      getChristmasCompensationDays(timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, year, 12, 25, 0)), localeValue);
   }   

   public void getEasterCompensationDays(Time easterDay, Locale localeValue){
      List<Time> easterCompensationList = timesService.getBetweenDates(DateUtils.addDays(easterDay.getDate(), -6), DateUtils.addDays(easterDay.getDate(), -3));
      for(Time easterCompensationDay :easterCompensationList){
         easterCompensationDay.setCompensationDay(true);
        CompensationDay easterCompensation = new CompensationDay("FY"+easterDay.getYear().getId().toString().substring(2) + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_EASTER, null, ConstantsHelper.DEFAULT_TITLE, localeValue)
              , easterCompensationDay);
        compensationDaysService.save(easterCompensation);
      }
      Time easterCompensationDay = timesService.getOne(getId(new DateTime(DateUtils.addDays(easterDay.getDate(), +1))));
      easterCompensationDay.setCompensationDay(true);
      CompensationDay easterCompensation = new CompensationDay("FY"+easterDay.getYear().getId().toString().substring(2) + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_EASTER, null, ConstantsHelper.DEFAULT_TITLE, localeValue)
            , easterCompensationDay);
      compensationDaysService.save(easterCompensation);
   }
   
   public void getCarnivalCompensationDays(Time easterDay, Locale localeValue){
      Time carnivalCompensationMondayDay = timesService.getOne(getId(new DateTime(DateUtils.addDays(easterDay.getDate(), -48))));
      carnivalCompensationMondayDay.setCompensationDay(true);
        CompensationDay carnivalCompensationMonday = new CompensationDay("FY"+easterDay.getYear().getId().toString().substring(2) + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_CARNIVAL, null, ConstantsHelper.DEFAULT_TITLE, localeValue)
              , carnivalCompensationMondayDay);
        compensationDaysService.save(carnivalCompensationMonday);
        Time carnivalCompensationTuesdayDay = timesService.getOne(getId(new DateTime(DateUtils.addDays(easterDay.getDate(), -47))));
        carnivalCompensationTuesdayDay.setCompensationDay(true);
        CompensationDay carnivalCompensationTuesday = new CompensationDay("FY"+easterDay.getYear().getId().toString().substring(2) + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_CARNIVAL, null, ConstantsHelper.DEFAULT_TITLE, localeValue)
              , carnivalCompensationTuesdayDay);
        compensationDaysService.save(carnivalCompensationTuesday);
      
   }
   
   public void monOrWedChristmas(Time christmasDay) {
      for (int i = christmasDay.getDay() + 1; i <= christmasDay.getDay() + 4; i++) {
         addChristmasCompensationDay(christmasDay, i);
      }
   }
   
   public void tuesdayChristmas(Time christmasDay) {
      Time christmasDayCompensationDay  = timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, christmasDay.getYear().getId(), 1, 2, 0));
      christmasDayCompensationDay.setCompensationDay(true);
      CompensationDay christmasCompensation = new CompensationDay("FY" + (Integer.toString(christmasDay.getYear().getId().intValue() - 1)).substring(2)
            + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_CHRISTMAS, null, ConstantsHelper.DEFAULT_TITLE, localeValue), christmasDayCompensationDay);
      compensationDaysService.save(christmasCompensation);
      for (int i = christmasDay.getDay() - 1; i <= christmasDay.getDay() + 3; i++) {
         if (i != christmasDay.getDay()) {
            addChristmasCompensationDay(christmasDay, i);
         }
      }
      christmasDayCompensationDay = timesService
            .getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, christmasDay.getYear().getId(), christmasDay.getMonth(), christmasDay.getDay() + 6, 0));
      christmasDayCompensationDay.setCompensationDay(true);
      christmasCompensation = new CompensationDay("FY" + christmasDay.getYear().getId().toString().substring(2)
            + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_CHRISTMAS, null, ConstantsHelper.DEFAULT_TITLE, localeValue), christmasDayCompensationDay);
      compensationDaysService.save(christmasCompensation);
   }
   
   public void getChristmasCompensationDays(Time christmasDay, Locale localeValue){
      Time christmasDayCompensationDay = null;
      CompensationDay christmasCompensation = null;
      switch (christmasDay.getDayOfWeek()) {
         case 1 :
            monOrWedChristmas(christmasDay);
            break;
         case 2 :
            tuesdayChristmas(christmasDay);
            break;
         case 3 :
            monOrWedChristmas(christmasDay);
            break;
         case 4 :
            for (int i = christmasDay.getDay() + 4; i <= christmasDay.getDay() + 6; i++) {
               addChristmasCompensationDay(christmasDay, i);
            }
            for (int i = christmasDay.getDay() - 1; i <= christmasDay.getDay() + 1; i++) {
               if (i != christmasDay.getDay()) {
                  addChristmasCompensationDay(christmasDay, i);
               }
            }
            break;
         case 5 :
            for (int i = christmasDay.getDay() + 3; i <= christmasDay.getDay() + 6; i++) {
               addChristmasCompensationDay(christmasDay, i);
            }
            christmasDayCompensationDay = timesService
                  .getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, christmasDay.getYear().getId(), christmasDay.getMonth(), christmasDay.getDay() - 1, 0));
            christmasDayCompensationDay.setCompensationDay(true);
            christmasCompensation = new CompensationDay(
                  "FY" + christmasDay.getYear().getId().toString().substring(2)
                        + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_CHRISTMAS, null, ConstantsHelper.DEFAULT_TITLE, localeValue),
                  christmasDayCompensationDay);
            compensationDaysService.save(christmasCompensation);
            break;
         case 6 :
            for (int i = christmasDay.getDay() + 2; i <= christmasDay.getDay() + 6; i++) {
               addChristmasCompensationDay(christmasDay, i);
            }
            break;
         case 7 :
            for (int i = christmasDay.getDay() + 1; i <= christmasDay.getDay() + 5; i++) {
               addChristmasCompensationDay(christmasDay, i);
            }
            break;
         default :
            break;
      }    
   }
   
   private void addChristmasCompensationDay(Time christmasDay, int i) {
      Time christmasDayCompensationDay = timesService.getOne(String.format(ConstantsHelper.DEFAULT_DATETIME, christmasDay.getYear().getId(), christmasDay.getMonth(), i, 0));
      christmasDayCompensationDay.setCompensationDay(true);
      /*
       * TODO: remove hard-coded code
       */
      CompensationDay christmasCompensation = new CompensationDay(
            "FY" + christmasDay.getYear().getId().toString().substring(2)
                  + messageSource.getMessage(ConstantsHelper.MESSAGE_COMMON_COMPENSATION_CHRISTMAS, null, ConstantsHelper.DEFAULT_TITLE, localeValue),
            christmasDayCompensationDay);
      compensationDaysService.save(christmasCompensation);
   }
   
   public void createHoliday(Time day, String name){
      Holiday holiday = new Holiday();
      day.setHoliday(true);
      holiday.setDay(day);
      holiday.setName(name);
      holidaysService.save(holiday);
   }   
   
   
   public String getId(DateTime datetime) {
      return String.format(ConstantsHelper.DEFAULT_DATETIME, datetime.getYear(), datetime.getMonthOfYear(), datetime.getDayOfMonth(), 0);
   }
   
   public String getId(Date date) {
      /*
       * TODO: convert to constant
       */
      DateFormat dateFormat = new SimpleDateFormat("yyyyMMddKKmmss");
      return dateFormat.format(date);            
   } 
   
   private Integer getSemester(DateTime datetime) {      
      return datetime.getMonthOfYear() >  5 ? 2 : 1;
   }
   
   private Integer getFortnight(DateTime datetime) {
      return datetime.getDayOfMonth() > 15 ? 2 : 1;
   }
   
   private Integer getQuarter(DateTime datetime) {
      int quarter = 1;
      
      if (datetime.getMonthOfYear() > 3 && datetime.getMonthOfYear() <= 6)
         quarter = 2;

      if (datetime.getMonthOfYear() > 6 && datetime.getMonthOfYear() <= 9)
         quarter = 3;

      if (datetime.getMonthOfYear() > 9 && datetime.getMonthOfYear() <= 12)
         quarter = 4;
      
      return quarter;
   }
   
   public List<Time> updateByScheduleSetting(List<Time> times, ScheduleSetting scheduleSetting) {
      Integer hours = 0;
      
      for (Time time : times) {
         switch (time.getDayOfWeek()) {
            case DateTimeConstants.MONDAY:
               hours = scheduleSetting.getMonday();
               break;
            case DateTimeConstants.TUESDAY:
               hours = scheduleSetting.getTuesday();
               break;
            case DateTimeConstants.WEDNESDAY:
               hours = scheduleSetting.getWednesday();
               break;
            case DateTimeConstants.THURSDAY:
               hours = scheduleSetting.getThursday();
               break;
            case DateTimeConstants.FRIDAY:
               hours = scheduleSetting.getFriday();
               break;
            case DateTimeConstants.SATURDAY:
               hours = scheduleSetting.getSaturday();
               break;
            case DateTimeConstants.SUNDAY:
               hours = scheduleSetting.getSunday();
               break;
            default:
               hours = 0;
               break;
         }
         
         if(scheduleSetting.getId()==0) {
            TimeSchedule timeSchedule = new TimeSchedule();
            timeSchedule.setSchedule(scheduleSetting.getSchedule());
            timeSchedule.setTime(time);
            timeSchedule.setHours(hours);
            time.addTimeSchedule(timeSchedule);
         }
         else {
            for (TimeSchedule ts : time.getTimesSchedules()) {
               if (ts.getSchedule().getId().equals(scheduleSetting.getSchedule().getId())) {
                  ts.setHours(hours);
               }
            }
         }
         
         
      }
      
      return times;
   }
  
}