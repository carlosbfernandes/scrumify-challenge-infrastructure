package pt.com.scrumify.helpers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PropertiesHelper {
   
   private static final Logger logger = LoggerFactory.getLogger(PropertiesHelper.class);
   
   @Value(ConstantsHelper.STORAGE_DIRECTORY)
   private String storageDirectory;
   
   public String getAttachmentsDirectory() {
      String path = getStorageDirectory() + "attachments";
      
      Path directory = Paths.get(path);
      if (Files.notExists(directory)){
         try {
            Files.createDirectories(directory);
         } catch (IOException e) {
            logger.error("Exception was caught when trying to get attachments directory. ", e);
         }
      }
      
      return path + ConstantsHelper.MAPPING_SLASH;
   }
   
   public String getStatusReportsDirectory() {
      String path = getStorageDirectory() + "reports/statusreports";
      
      Path directory = Paths.get(path);
      if (Files.notExists(directory)){
         try {
            Files.createDirectories(directory);
         } catch (IOException e) {
            logger.error("Exception was caught when trying to get status report directory. ", e);
         }
      }
      
      return path + ConstantsHelper.MAPPING_SLASH;
   }
   
   public String getStorageDirectory() {
      return this.storageDirectory.endsWith(ConstantsHelper.MAPPING_SLASH) ? this.storageDirectory : this.storageDirectory.concat(ConstantsHelper.MAPPING_SLASH);
   }
}