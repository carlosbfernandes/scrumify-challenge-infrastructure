package pt.com.scrumify.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.services.ResourceService;

@Service("DatesHelper")
public class DatesHelper {
   @Autowired
   private MessageSource messageSource;
   @Autowired
   private ResourceService resourcesService;
   
   @Autowired 
   private YearsHelper yearsHelper;

   public static Date now() {
      Date in = new Date();
      LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());

      return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
   }

   public static Integer year() {
      LocalDate today = LocalDate.now();

      return today.getYear();
   }
   
   public static Integer month() {
      LocalDate today = LocalDate.now();

      return today.getMonthValue();
   }

   public String diff(Date di, Date df) {
      Period period = new Period(new DateTime(di), new DateTime(df));

      if (period.getYears() > 0) {
         return period.getYears() + " "
               + (period.getYears() == 1
                     ? messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_YEAR, null, LocaleContextHolder.getLocale())
                     : messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_YEARS, null, LocaleContextHolder.getLocale()))
               + " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_AGO, null, LocaleContextHolder.getLocale());
      }
      if (period.getMonths() > 0) {
         return period.getMonths() + " "
               + (period.getMonths() == 1
                     ? messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_MONTH, null, LocaleContextHolder.getLocale())
                     : messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_MONTHS, null, LocaleContextHolder.getLocale()))
               + " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_AGO, null, LocaleContextHolder.getLocale());
      }
      if (period.getDays() > 0) {
         return period.getDays() + " "
               + (period.getDays() == 1
                     ? messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_DAY, null, LocaleContextHolder.getLocale())
                     : messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_DAYS, null, LocaleContextHolder.getLocale()))
               + " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_AGO, null, LocaleContextHolder.getLocale());
      }

      PeriodFormatter formatter = new PeriodFormatterBuilder().appendHours()
            .appendSuffix(" " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_HOUR, null, LocaleContextHolder.getLocale()),
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_HOURS, null, LocaleContextHolder.getLocale()))
            .appendSeparator(messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR, null, LocaleContextHolder.getLocale()) + " ",
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR_FINAL, null, LocaleContextHolder.getLocale()) + " ")
            .appendMinutes()
            .appendSuffix(" " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_MINUTE, null, LocaleContextHolder.getLocale()),
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_MINUTES, null, LocaleContextHolder.getLocale()))
            .appendSeparator(messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR, null, LocaleContextHolder.getLocale()) + " ",
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR_FINAL, null, LocaleContextHolder.getLocale()) + " ")
            .appendSeconds()
            .appendSuffix(" " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SECOND, null, LocaleContextHolder.getLocale()),
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SECONDS, null, LocaleContextHolder.getLocale()))
            .appendSeparator(messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR, null, LocaleContextHolder.getLocale()) + " ",
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR_FINAL, null, LocaleContextHolder.getLocale()) + " ")
            .toFormatter();

      return period.toString(formatter) + " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_AGO, null, LocaleContextHolder.getLocale());
   }

   public String duration(Date di) {
      Period period = new Period(new DateTime(di), new DateTime(new Date()));

      PeriodFormatter formatter = new PeriodFormatterBuilder().appendYears()
            .appendSuffix(" " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_YEAR, null, LocaleContextHolder.getLocale()),
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_YEARS, null, LocaleContextHolder.getLocale()))
            .appendSeparator(messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR, null, LocaleContextHolder.getLocale()) + " ",
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR_FINAL, null, LocaleContextHolder.getLocale()) + " ")
            .appendMonths()
            .appendSuffix(" " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_MONTH, null, LocaleContextHolder.getLocale()),
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_MONTHS, null, LocaleContextHolder.getLocale()))
            .appendSeparator(messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR, null, LocaleContextHolder.getLocale()) + " ",
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR_FINAL, null, LocaleContextHolder.getLocale()) + " ")
            .appendDays()
            .appendSuffix(" " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_DAY, null, LocaleContextHolder.getLocale()),
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_DAYS, null, LocaleContextHolder.getLocale()))
            .appendSeparator(messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR, null, LocaleContextHolder.getLocale()) + " ",
                  " " + messageSource.getMessage(ConstantsHelper.COMMON_PERIOD_SEPARATOR_FINAL, null, LocaleContextHolder.getLocale()) + " ")
            .minimumPrintedDigits(2).printZeroAlways().appendHours().appendSuffix("", "").appendSeparator(":", ":").appendMinutes().appendSuffix("", "")
            .appendSeparator(":", ":").appendSeconds().appendSuffix("", "").appendSeparator(":", ":").toFormatter();

      return period.toString(formatter);
   }

   public static String getEndOfCurrentMonth() {

      LocalDate localDate = LocalDate.now();
      LocalDate end = localDate.withDayOfMonth(localDate.lengthOfMonth());

      DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantsHelper.DEFAULT_FORMAT_DATE);
      return end.format(formatter);
   }
   
   public Date getMaxDate() {
      Resource resource = resourcesService.getOne(ResourcesHelper.getResource().getId());
      return DatesHelper.min(resource.getEndingDay(), yearsHelper.getMaxActiveYear());
   }
   
   public Date getMinDate() {
      Resource resource = resourcesService.getOne(ResourcesHelper.getResource().getId());
      return DatesHelper.max(resource.getStartingDay(), yearsHelper.getMinActiveYear());
   }
   
   public static Date addDays(Date date, int days) {
       Calendar cal = Calendar.getInstance();
       cal.setTime(date);
       cal.add(Calendar.DATE, days); //minus number will decrement the days
       cal.set(Calendar.SECOND, 0);
       cal.set(Calendar.MINUTE, 0);
       cal.set(Calendar.HOUR, 0);       
       return cal.getTime();
   }
   
   public String formatDate(Date date) {
      SimpleDateFormat sdf = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATE);
      return sdf.format(date);
   }
   
   public String getMaxDateFormatted() {
      return formatDate(getMaxDate());
   }
   
   public String getMinDateFormatted() {
      return formatDate(getMinDate());
   }
   
   public Date getLastDayOfMaximumYear() {
      return yearsHelper.getMaxActiveYear();
   }
   
   public Date getFirstDayOfMinimumYear() {
      return yearsHelper.getMinActiveYear();
   }
   
   /** 
    * Returns the maximum of two dates. A null date is treated as being less
    * than any non-null date. 
    */
   public static Date max(Date d1, Date d2) {
       if (d1 == null && d2 == null) return null;
       if (d1 == null) return d2;
       if (d2 == null) return d1;
       return (d1.after(d2)) ? d1 : d2;
   }
   
   /** 
    * Returns the minimum of two dates. A null date is treated as being greater
    * than any non-null date. 
    */
   public static Date min(Date d1, Date d2) {
       if (d1 == null && d2 == null) return null;
       if (d1 == null) return d2;
       if (d2 == null) return d1;
       return (d1.before(d2)) ? d1 : d2;
   }
   
   public Integer getYearOfDate(Date date) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      return calendar.get(Calendar.YEAR);
   }
   
   public Integer getMonthOfDate(Date date) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      return calendar.get(Calendar.MONTH) + 1;
   }
   
   /*
    * Returns days between two dates
    */
   public Integer getDaysBetweenDates(String inputString1, String inputString2) throws ParseException {
      SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
      Date date1 = myFormat.parse(inputString1);
      Date date2 = myFormat.parse(inputString2);
      
      return getDaysBetweenDates(date1, date2);
   }
   
   public static Integer getDaysBetweenDates(Date date1, Date date2) {
      long diff = java.lang.Math.abs(date2.getTime() - date1.getTime());
      return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
   }
}