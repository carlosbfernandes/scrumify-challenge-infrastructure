package pt.com.scrumify.helpers;

import java.io.File;
import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.docx4j.jaxb.Context;
import org.docx4j.model.table.TblFactory;
import org.docx4j.openpackaging.contenttype.ContentType;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.WordprocessingML.AlternativeFormatInputPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.Br;
import org.docx4j.wml.CTAltChunk;
import org.docx4j.wml.CTBorder;
import org.docx4j.wml.CTShd;
import org.docx4j.wml.CTVerticalJc;
import org.docx4j.wml.Jc;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.STBorder;
import org.docx4j.wml.STPageOrientation;
import org.docx4j.wml.STShd;
import org.docx4j.wml.STVerticalJc;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.SectPr.PgSz;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblBorders;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TblWidth;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;
import org.docx4j.wml.U;
import org.docx4j.wml.UnderlineEnumeration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.ContractProperty;
import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.WorkItemNoteService;
import pt.com.scrumify.entities.ContractView;
import pt.com.scrumify.entities.ReportView;

@Service
public class ReportsHelper {

   @Autowired
   private ContractsHelper contractsHelper;
   @Autowired
   private DatesHelper datesHelper;

   @Autowired
   private ContractService contractsService;
   @Autowired
   private PropertiesHelper propertiesHelper;
   @Autowired
   private WorkItemNoteService workItemsNotesService;

   private static final Logger logger = LoggerFactory.getLogger(ReportsHelper.class);

   public File generateReport(ReportView reportView) {

      List<Contract> contracts = getContractsBasedOnFilter(reportView);

      try {
         WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(getFileFromResources(ConstantsHelper.REPORT_TEMPLATE));
         ObjectFactory factory = Context.getWmlObjectFactory();

         for (Contract contract : contracts) {
            MainDocumentPart document = wordMLPackage.getMainDocumentPart();
            createParagraph(contractsHelper.getContractNumber(contract.getId(), ContractProperty.PROJECT_NUMBER) + contract.getName(), document, factory, false, true,
                  false);
            createParagraph("Âmbito:", document, factory, true, false, false);
            createParagraph(contract.getDescription(), document, factory, false, false, false);
            createParagraph("Descrição dos Trabalhos:", document, factory, true, false, false);
            createParagraph("Neste período destacaram-se as seguintes tarefas realizadas:", document, factory, false, false, false);

            AlternativeFormatInputPart afiPart = new AlternativeFormatInputPart(new PartName("/hw" + contract.getId() + ".html"));
            afiPart.setBinaryData(createBulletedList(getNotesBasedOnFilter(reportView, contract)));
            afiPart.setContentType(new ContentType("text/html"));
            Relationship altChunkRel = document.addTargetPart(afiPart);

            CTAltChunk ac = factory.createCTAltChunk();
            ac.setId(altChunkRel.getId());
            document.addObject(ac);

            // Create a blank space
            createParagraph("", document, factory, false, false, false);

         }

         // Adds a page break so that the following table will be in a new page
         addPageBreak(wordMLPackage);

         // Create table at the end of the document
         wordMLPackage.getMainDocumentPart().getContent().add(createTable(wordMLPackage, contracts, reportView));

         addLandscape(wordMLPackage);

         wordMLPackage.getContentTypeManager().addDefaultContentType("html", "text/html");
         File report = new java.io.File(createFilename(propertiesHelper.getStatusReportsDirectory()));
         wordMLPackage.save(report);

         return report;

      } catch (Exception e) {
         logger.error("An error occured while generating a new Status Report. ", e);
         return null;
      }

   }

   private List<Contract> getContractsBasedOnFilter(ReportView reportView) {
      List<Contract> contracts = new ArrayList<>();
      if (reportView.getContract() != null) {
         contracts.add(reportView.getContract());
      } else if (reportView.getSubArea() != null) {
         contracts.addAll(contractsService.getBySubarea(reportView.getSubArea()));
      } else if (reportView.getArea() != null) {
         contracts.addAll(contractsService.getByArea(reportView.getArea()));
      } else {
         contracts.addAll(contractsService.getOpenContractsByResource(ResourcesHelper.getResource()));
      }
      return contracts;
   }

   private List<String> getNotesBasedOnFilter(ReportView reportView, Contract contract) {
      List<WorkItemNote> notes = null;
      if (reportView.getResource() != null) {
         notes = workItemsNotesService.getStatusReportsByResource(contract, TypeOfNote.STATUS_REPORT, reportView);
      } else {
         notes = workItemsNotesService.getStatusReports(contract, TypeOfNote.STATUS_REPORT, reportView);
      }

      List<String> strings = new ArrayList<>();
      for (WorkItemNote note : notes)
         strings.add(note.getContent());

      return strings;
   }

   private static void createParagraph(String text, MainDocumentPart mainDocumentPart, ObjectFactory factory, boolean underline, boolean bold, boolean italic) {
      BooleanDefaultTrue b = new BooleanDefaultTrue();
      P p = factory.createP();
      R r = factory.createR();
      Text t = factory.createText();
      t.setValue(text);
      r.getContent().add(t);
      p.getContent().add(r);

      RPr rpr = factory.createRPr();
      if (bold)
         rpr.setB(b);
      if (italic)
         rpr.setI(b);
      if (underline) {
         U u = factory.createU();
         u.setVal(UnderlineEnumeration.SINGLE);
         rpr.setU(u);
      }
      r.setRPr(rpr);

      mainDocumentPart.getContent().add(p);
   }

   private static byte[] createBulletedList(List<String> strings) {
      String xhtml = "<html><body><ul><li></li></ul></body></html>";

      StringBuilder builder = new StringBuilder();
      for (String s : strings) {
         builder.append("<li>" + s + "</li>");
      }
      xhtml = xhtml.replace("<li></li>", builder.toString());

      return xhtml.getBytes();
   }

   private String createFilename(String directory) {
      return directory + "Report_" + ThreadLocalRandom.current().nextInt(0, 99999 + 1) + ".docx";
   }

   // get file from classpath, resources folder
   private File getFileFromResources(String fileName) {

      ClassLoader classLoader = getClass().getClassLoader();

      URL resource = classLoader.getResource(fileName);
      if (resource == null) {
         throw new IllegalArgumentException("File " + fileName + " not found!");
      } else {
         return new File(resource.getFile());
      }

   }

   private Tbl createTable(WordprocessingMLPackage wordPackage, List<Contract> contracts, ReportView reportView) {
      int writableWidthTwips = wordPackage.getDocumentModel().getSections().get(0).getPageDimensions().getWritableWidthTwips();
      int columnNumber = 11;

      int rownumber = contracts.size() + 1;
      Tbl tbl = TblFactory.createTable(rownumber, columnNumber, writableWidthTwips / columnNumber);
      configureTable(tbl);

      List<Object> rows = tbl.getContent();
      Tr trHead = (Tr) rows.get(0);

      List<Object> cellsHeader = trHead.getContent();
      createHeaderCell(cellsHeader.get(0), "Projeto (Nº - Designação)");
      createHeaderCell(cellsHeader.get(1), "Data Início/Fim Prevista");
      createHeaderCell(cellsHeader.get(2), "Data Início/Fim Real");
      createHeaderCell(cellsHeader.get(3), "Situação Projeto");
      createHeaderCell(cellsHeader.get(4), "Estado Projeto");
      createHeaderCell(cellsHeader.get(5), "Nº Contrato");
      createHeaderCell(cellsHeader.get(6), "Data Início/Fim Contrato");
      createHeaderCell(cellsHeader.get(7), "Total Horas Contrato");
      createHeaderCell(cellsHeader.get(8), "Horas Gastas Período Projeto");
      createHeaderCell(cellsHeader.get(9), "Horas Acumuladas Projeto");
      createHeaderCell(cellsHeader.get(10), "Saldo Horas Contrato");

      List<ContractView> views = contractsHelper.getContractReport(contracts, reportView.getStartDay());

      for (int i = 1, j = 0; i < rownumber; i++, j++) {
         Tr tr = (Tr) rows.get(i);
         List<Object> cells = tr.getContent();
         createCell(cells.get(0), contractsHelper.getContractNumber(views.get(j).getId(), ContractProperty.PROJECT_NUMBER) + views.get(j).getName());
         createCell(cells.get(1), getDatePlaceholder(views.get(j)));
         createCell(cells.get(2), " - ");
         createCell(cells.get(3), views.get(j).getStatus().getName());
         createCell(cells.get(4), " - ");
         createCell(cells.get(5), contractsHelper.getContractNumber(views.get(j).getId(), ContractProperty.CONTRACT_NUMBER));
         createCell(cells.get(6), " - ");
         createCell(cells.get(7), views.get(j).getHours().toString());
         createCell(cells.get(8), views.get(j).getHoursSpentInterval().toString());
         createCell(cells.get(9), views.get(j).getHoursSpent().toString());
         createCell(cells.get(10), String.valueOf(views.get(j).getHours() - views.get(j).getHoursSpent()));

      }

      return tbl;
   }

   private Tc createCell(Object cell, String value) {
      Tc td = (Tc) cell;
      ObjectFactory factory = Context.getWmlObjectFactory();
      P p = factory.createP();
      R r = factory.createR();
      Text t = factory.createText();
      t.setValue(value);
      r.getContent().add(t);
      p.getContent().add(r);

      td.getContent().add(p);

      return td;
   }

   private void configureTable(Tbl table) {
      table.setTblPr(new TblPr());
      CTBorder border = new CTBorder();
      border.setColor("auto");
      border.setSz(BigInteger.valueOf(6));
      border.setSpace(BigInteger.valueOf(0));
      border.setVal(STBorder.OUTSET);

      TblBorders borders = new TblBorders();
      borders.setBottom(border);
      borders.setLeft(border);
      borders.setRight(border);
      borders.setTop(border);
      borders.setInsideH(border);
      borders.setInsideV(border);
      table.getTblPr().setTblBorders(borders);

      TblWidth cellSpacing = new TblWidth();
      cellSpacing.setW(BigInteger.valueOf(20));
      cellSpacing.setType("dxa");
      table.getTblPr().setTblCellSpacing(cellSpacing);

      TblWidth width = new TblWidth();
      width.setW(BigInteger.valueOf(14307));
      width.setType("dxa");
      table.getTblPr().setTblW(width);

      Jc jc = new Jc();
      jc.setVal(JcEnumeration.CENTER);
      table.getTblPr().setJc(jc);
   }

   private Tc createHeaderCell(Object cell, String value) {
      Tc tc = createCell(cell, value);
      ObjectFactory factory = Context.getWmlObjectFactory();

      TcPr tcpr = factory.createTcPr();
      CTShd shd = new CTShd();
      shd.setColor("auto");
      shd.setVal(STShd.PCT_12);
      shd.setFill("auto");
      tcpr.setShd(shd);
      TblWidth tblWidth = new TblWidth();
      tblWidth.setW(BigInteger.valueOf(2035));
      tcpr.setTcW(tblWidth);
      CTVerticalJc vJc = new CTVerticalJc();
      vJc.setVal(STVerticalJc.CENTER);
      tcpr.setVAlign(vJc);

      tc.setTcPr(tcpr);

      return tc;

   }

   private void addPageBreak(WordprocessingMLPackage wordPackage) {
      ObjectFactory factory = Context.getWmlObjectFactory();
      /* page break */
      P pa = factory.createP();
      // Create object for r
      R ra = factory.createR();
      pa.getContent().add(ra);
      // Create object for br
      Br bra = factory.createBr();
      ra.getContent().add(bra);
      bra.setType(org.docx4j.wml.STBrType.PAGE);
      wordPackage.getMainDocumentPart().addObject(pa);
   }

   private void addLandscape(WordprocessingMLPackage wordPackage) {
      ObjectFactory factory = Context.getWmlObjectFactory();
      SectPr sectionLandscape = factory.createSectPr();
      PgSz landscape = new PgSz();
      landscape.setOrient(STPageOrientation.LANDSCAPE);
      landscape.setH(BigInteger.valueOf(11906));
      landscape.setW(BigInteger.valueOf(16383));
      sectionLandscape.setPgSz(landscape);

      org.docx4j.wml.P p = factory.createP();
      PPr createPPr = factory.createPPr();
      createPPr.setSectPr(sectionLandscape);
      p.setPPr(createPPr);
      wordPackage.getMainDocumentPart().addObject(p);
   }

   private String getDatePlaceholder(ContractView view) {
      return datesHelper.formatDate(view.getStartingDay()) + " a " + datesHelper.formatDate(view.getEndingDay());
   }
}