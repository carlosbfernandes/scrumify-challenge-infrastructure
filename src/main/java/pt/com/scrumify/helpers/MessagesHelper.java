package pt.com.scrumify.helpers;

import org.springframework.stereotype.Service;

import pt.com.scrumify.entities.Message;

@Service
public class MessagesHelper {
  
   public Message createMessage(String type, String icon, String text) {
     Message message= new Message();
     message.setType(type);
     message.setIcon(icon);
     message.setText(text);
     
     return message;  
   }
   
}