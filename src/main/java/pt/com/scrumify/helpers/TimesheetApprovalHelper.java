 package pt.com.scrumify.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetApproval;
import pt.com.scrumify.database.entities.TimesheetReview;
import pt.com.scrumify.database.entities.TypeOfAbsence;
import pt.com.scrumify.database.entities.TypeOfTimesheetApproval;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.TimesheetApprovalService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.database.services.TypeOfAbsenceService;
import pt.com.scrumify.entities.TimesheetApprovalContractOrAbsenceView;
import pt.com.scrumify.entities.TimesheetApprovalView;
import pt.com.scrumify.entities.TimesheetFilterView;

@Service
public class TimesheetApprovalHelper {
   @Autowired
   private ContractService contractsService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TimesheetService timesheetsService;
   @Autowired
   private TimesheetApprovalService timesheetApprovalService;
   @Autowired
   private TypeOfAbsenceService typesOfAbsenceService;
   
   /*
    * TODO: code review
    */
   public TimesheetApproval getTimesheetBillableTotals(List<TimesheetApproval> billables) {
      TimesheetApproval totals = new TimesheetApproval();
      
      totals.setAnalysis(billables.stream().mapToInt(TimesheetApproval::getAnalysis).sum());
      totals.setDesign(billables.stream().mapToInt(TimesheetApproval::getDesign).sum());
      totals.setDevelopment(billables.stream().mapToInt(TimesheetApproval::getDevelopment).sum());
      totals.setTest(billables.stream().mapToInt(TimesheetApproval::getTest).sum());
      totals.setOther(billables.stream().mapToInt(TimesheetApproval::getOther).sum());
      
      return totals; 
   }
   
   /*
    * TODO: code review
    */
   public TimesheetApproval saveTimeSheetBillable(TimesheetApproval timesheetBillable) {
      return timesheetApprovalService.save(timesheetBillable);
   }
   
   /*
    * TODO: code review
    */
   public TimesheetApproval updateDescription(Integer billableId, String code, String name) {
      TimesheetApproval timesheetBillable = null;
      if(billableId == 0){
          timesheetBillable = new TimesheetApproval();
      }else{
          timesheetBillable = timesheetApprovalService.getOne(billableId);
      }
      if(code.startsWith("99")) {
         TypeOfAbsence absenceType = typesOfAbsenceService.findByCodeAndName(code, name);
         timesheetBillable.setCode(code);
         timesheetBillable.setContract(null);
         timesheetBillable.setDescription(absenceType.getName());         
      } else {
         Contract contract = contractsService.getByCode(code);
         timesheetBillable.setCode(contract.getCode());
         timesheetBillable.setContract(contract);
         timesheetBillable.setDescription(contract.getName());
      }
         
      return timesheetBillable;
   }
   
   /*
    * Load contracts and absences
    */
   public List<TimesheetApprovalContractOrAbsenceView> getContractsAndAbsences(Resource resource){
      List<TimesheetApprovalContractOrAbsenceView> ca = new ArrayList<>();

      for(Contract contract : contractsService.getOpenContractsByResource(resource)) 
         ca.add(new TimesheetApprovalContractOrAbsenceView(contract.getCode(), contract.getId(), contract.getName()));

      for(TypeOfAbsence absenceType : typesOfAbsenceService.getAllTypes()) 
         ca.add(new TimesheetApprovalContractOrAbsenceView(absenceType.getCode(), null, absenceType.getName()));

      return ca;
   }
   
   /*
    * TODO: code review
    */
   public List<Timesheet> getWithSameStatus(List<Timesheet> timesheets, String status){
      if(status != null && !status.isEmpty()) {
         return timesheets.stream()
            .filter(o -> status.equals(o.getStatus().getName()))
            .collect(Collectors.toList());
      }     

      return timesheets;
   }
   
   @Transactional
   public TimesheetApproval addNewRow(TimesheetApprovalView view, TypeOfTimesheetApproval type) {
      Timesheet timesheet = timesheetsService.getSheetByFortnight(view.getResource(), view.getYear().getId(), view.getMonth(), view.getFortnight());
      TimesheetApproval approval = new TimesheetApproval(view.getResource(), timesheet, type, view.getYear(), view.getMonth(), view.getFortnight());
      
      return timesheetApprovalService.save(approval);
   }
   
   public List<Timesheet> search(TimesheetFilterView filter) {
      /*
       * Get resources from resource teams or filter
       */
      List<Resource> resources = new ArrayList<>();
      if (filter.getResource() == null) {
         resources = resourcesService.getByTeams(teamsService.getByMemberAndApprover(ResourcesHelper.getResource()));
         
         if (resources.isEmpty()) {
            resources.add(ResourcesHelper.getResource());
         }
      }
      else {
         resources.add(new Resource(filter.getResource()));
      }
      
      /*
       * Return timesheets based on selected resources, year, month and fortnight
       */
      return timesheetsService.getByResourcesYearMonthAndFortnight(resources, filter.getYear(), filter.getMonth(), filter.getFortnight());
   }
   
   public void transfer(Timesheet timesheet, List<TimesheetReview> reviews) {
      List<TimesheetApproval> approvals = new ArrayList<>();
      
      for (TimesheetReview review : reviews) {
         TimesheetApproval approval = timesheetApprovalService.getOne(timesheet.getResource(), timesheet.getStartingDay().getYear(), timesheet.getStartingDay().getMonth(), timesheet.getStartingDay().getFortnight(), review.getCode(), review.getDescription());
         if (approval == null) {
            approvals.add(new TimesheetApproval(review));
         }
         else {
            approval.setAnalysis(review.getAnalysis());
            approval.setDesign(review.getDesign());
            approval.setDevelopment(review.getDevelopment());
            approval.setTest(review.getTest());
            approval.setOther(review.getOther());
            
            approvals.add(approval);
         }
      }
      
      timesheetApprovalService.saveAll(approvals);
   }
}