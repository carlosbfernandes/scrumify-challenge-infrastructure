package pt.com.scrumify.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.services.CookieService;

@Service
public class CookiesHelper {
   @Autowired
   private CookieService cookieService;
   
   private static final Logger logger = LoggerFactory.getLogger(CookiesHelper.class);
   
   public Cookie get(String name, Resource resource) {
      return cookieService.getOne(ConstantsHelper.COOKIE_NAME + "." + name, resource);
   }
   
   public Integer getInteger(String name, Resource resource) {
      Integer result = null;
      
      Cookie cookie = get(name, resource);
      if (cookie != null) {
         /*
          * Try parse object types
          */
         try {
            result = Integer.valueOf(cookie.getData());
         }
         catch (Exception ex) {
            logger.error("Exception was caught when trying to serialize cookie.", ex);
         }
      }

      return result;
   }
   
   public <T> T getObject(String name, Resource resource, Class<T> type) {
      T result = null;
      
      Cookie cookie = get(name, resource);
      if (cookie != null) {
         ObjectMapper mapper = new ObjectMapper();

         /*
          * Try parse object types
          */
         try {
            result = type.cast(mapper.readValue(cookie.getData(), type.getClass()));
         }
         catch (Exception ex) {
            logger.error("Exception was caught when trying to serialize cookie.", ex);
         }
      }

      return result;
   }
   
   public void save(String name, Resource resource, Object object) {
      ObjectMapper mapper = new ObjectMapper();
      
      Cookie cookie = cookieService.getOne(ConstantsHelper.COOKIE_NAME + "." + name, resource);
      if (cookie == null) {
         cookie = new Cookie();
      }
      
      try {
         cookie.setName(ConstantsHelper.COOKIE_NAME + "." + name);
         cookie.setData(mapper.writeValueAsString(object));
         cookie.setResource(resource);
      }
      catch (JsonProcessingException e) {
         logger.error("Exception was caught when trying to parse the cookie.", e);
      }

      cookieService.save(cookie);
   }
}