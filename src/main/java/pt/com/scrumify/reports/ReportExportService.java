package pt.com.scrumify.reports;

import java.io.ByteArrayInputStream;

import pt.com.scrumify.entities.ReportView;

public interface ReportExportService {
   void addColors();
   void addFonts();
   void addStyles();
   void filters(ReportView view);
   ByteArrayInputStream export(ReportView view);
   void writeHome(ReportView view);
   void writeContent(ReportView view);
}