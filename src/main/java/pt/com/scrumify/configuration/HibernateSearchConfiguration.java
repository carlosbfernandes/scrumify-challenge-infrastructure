package pt.com.scrumify.configuration;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pt.com.scrumify.services.HibernateSearchService;


@Configuration
public class HibernateSearchConfiguration {
   
   @Bean
   HibernateSearchService hibernateSearchService(EntityManagerFactory entityManagerFactory) throws InterruptedException {
      HibernateSearchService hibernateSearchService = new HibernateSearchService(entityManagerFactory.createEntityManager());
      hibernateSearchService.initializeHibernateSearch();
      return hibernateSearchService;
   }
   
}
