package pt.com.scrumify.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import pt.com.scrumify.helpers.ConstantsHelper;

public class AuthenticationFailureHandlerConfiguration implements AuthenticationFailureHandler {
   private final Logger logger = LoggerFactory.getLogger(this.getClass());
   private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
   
   @Override
   public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
      String username = request.getParameter("username");
      
      /*
       * Log authentication failure
       */
      logger.info("Authentication failed : Bad credentials for user `{}`.", username);

      response.setStatus(HttpStatus.UNAUTHORIZED.value());
      
      redirectStrategy.sendRedirect(request, response, String.format("%s?username=%s&error", ConstantsHelper.MAPPING_LOGIN, username));
   }
}