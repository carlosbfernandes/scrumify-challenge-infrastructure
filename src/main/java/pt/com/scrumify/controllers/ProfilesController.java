package pt.com.scrumify.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.services.ProfileService;
import pt.com.scrumify.database.services.RoleService;
import pt.com.scrumify.entities.ProfileView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class ProfilesController {
   @Autowired
   private ProfileService profilesService;
   @Autowired
   private RoleService rolesService;

   /*
    * PROFILES
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROFILES_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_PROFILES)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILES, this.profilesService.getBySystem(false));

      return ConstantsHelper.VIEW_PROFILES_READ;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROFILES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_PROFILES_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILE, new ProfileView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ROLES, this.rolesService.getAllOrderedByName());

      return ConstantsHelper.VIEW_PROFILES_CREATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROFILES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_PROFILES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_PROFILE)
   public String update(Model model, @PathVariable int profile) {
      ProfileView profileView = new ProfileView(this.profilesService.getOne(profile));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILE, profileView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ROLES, this.rolesService.getAllOrderedByName());

      return ConstantsHelper.VIEW_PROFILES_UPDATE;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROFILES_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_PROFILES_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILE) @Valid ProfileView profileView, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILE, profileView);
         
         return ConstantsHelper.VIEW_PROFILES_UPDATE;
      }
      
      this.profilesService.save(profileView.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_PROFILES;
   }
}