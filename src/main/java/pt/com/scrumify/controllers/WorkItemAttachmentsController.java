package pt.com.scrumify.controllers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.WorkItemAttachment;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.WorkItemAttachmentService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.MessagesHelper;
import pt.com.scrumify.helpers.PropertiesHelper;

@Controller
public class WorkItemAttachmentsController {

   private static final Logger logger = LoggerFactory.getLogger(WorkItemAttachmentsController.class);
   private static Pattern fileExtnPtrn = Pattern.compile(ConstantsHelper.PATTERN_ACCEPTANCE_EXTENSION_IMAGE_PDF);

   @Autowired
   private WorkItemService workitemsService;
   @Autowired
   private WorkItemAttachmentService attachmentsService;
   @Autowired
   private PropertiesHelper propertiesHelper;
   @Autowired
   private MessagesHelper messagesHelper;
   @Autowired
   private MessageSource messageSource;

   /*
    * AJAX TAB ATTACHMENTS WORKITEM
    */
   @ApiOperation("Mapping to respond ajax requests for workitem attachments.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_ATTACHMENTS + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String tab(Model model, @PathVariable int workitem) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, new WorkItemView(workitemsService.getOne(workitem)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ATTACHMENTS, attachmentsService.getByWorkItemId(workitem));

      return ConstantsHelper.VIEW_WORKITEMS_ATTACHMENTS + " :: body ";
   }

   @ApiOperation("Mapping to add a new attachment.")
   @PostMapping(value = ConstantsHelper.MAPPING_WORKITEMS_ATTACHMENTS_UPLOAD)
   public String addAttachment(Model model, @RequestParam("file") MultipartFile file, WorkItemView view, RedirectAttributes redirAttrs) {

      WorkItem workitem = workitemsService.getOne(view.getId());
      List<WorkItemAttachment> attachments = new ArrayList<>();
      WorkItemView workitemView = null;
      String filename = file.getOriginalFilename().replaceAll("\\s+", "");

      Matcher mtch = fileExtnPtrn.matcher(filename);
      if (mtch.matches() && (file.getSize() * 0.00000095367432) < 10) {
         try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(propertiesHelper.getAttachmentsDirectory() + workitem.getId() + ConstantsHelper.MAPPING_SLASH + file.getOriginalFilename());
            Files.createDirectories(path.getParent());
            Files.createFile(path);

            Files.write(path, bytes);

            WorkItemAttachment attachment = new WorkItemAttachment();
            attachment.setName(file.getOriginalFilename());
            attachment.setAttachment(UUID.randomUUID().toString());
            attachment.setWorkItem(workitem);
            attachments.add(attachment);

         } catch (Exception ex) {
            logger.error("Exception caught when adding a new attachment. Message: %s ", ex);
            redirAttrs.addFlashAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE,
                  this.messagesHelper.createMessage(ConstantsHelper.ERROR_MESSAGE, ConstantsHelper.ERROR_ICON_MESSAGE,
                        messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_WORKITEMATTACHEMENT_ERROR, null, LocaleContextHolder.getLocale())));

         }

         workitem.setAttachments(attachments);
         workitemView = new WorkItemView(workitemsService.save(workitem));
      } else {
         if (!mtch.matches()) {
            redirAttrs.addFlashAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE,
                  this.messagesHelper.createMessage(ConstantsHelper.ERROR_MESSAGE, ConstantsHelper.ERROR_ICON_MESSAGE,
                        messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_WORKITEMATTACHEMENT_EXTENSION, null, LocaleContextHolder.getLocale())));
         } else {
            redirAttrs.addFlashAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE,
                  this.messagesHelper.createMessage(ConstantsHelper.ERROR_MESSAGE, ConstantsHelper.ERROR_ICON_MESSAGE,
                        messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_WORKITEMATTACHEMENT_SIZE, null, LocaleContextHolder.getLocale())));
         }
      }
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workitemView);

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + workitem.getId()
            + ConstantsHelper.MAPPING_WORKITEMS_ATTACHMENTS;
   }

   @ApiOperation("Mapping to delete an attachment.")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_ATTACHMENTS_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID + "/{workitemId}")
   public String delete(Model model, @PathVariable int id, @PathVariable int workitemId) throws IOException {
      WorkItemAttachment attachment = attachmentsService.getOne(id);
      Path path = Paths.get(
            propertiesHelper.getAttachmentsDirectory() + ConstantsHelper.MAPPING_SLASH + attachment.getWorkItem().getId() + ConstantsHelper.MAPPING_SLASH + attachment.getName());
      if (Files.deleteIfExists(path))
         logger.info("File deleted.");
      attachmentsService.delete(attachment);

      WorkItem workitem = workitemsService.getOne(workitemId);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, new WorkItemView(workitem));

      return ConstantsHelper.VIEW_WORKITEMS_ATTACHMENTS + " :: list";
   }

   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_ATTACHMENTS_PREVIEW + ConstantsHelper.MAPPING_PARAMETER_ID + "/ext")
   public String getFileExt(Model model, @PathVariable int id) {
      WorkItemAttachment attachment = attachmentsService.getOne(id);
      model.addAttribute("ext", FilenameUtils.getExtension(attachment.getName()));

      return ConstantsHelper.VIEW_WORKITEMS_ATTACHMENTS_PREVIEW;
   }

   @ApiOperation("Mapping to download an attachment.")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_ATTACHMENTS + ConstantsHelper.MAPPING_PARAMETER_ID + "/{action}")
   public void download(HttpServletResponse response, @PathVariable int id, @PathVariable String action) throws IOException {
      WorkItemAttachment attachment = attachmentsService.getOne(id);
      String fullname = propertiesHelper.getAttachmentsDirectory() + ConstantsHelper.MAPPING_SLASH + attachment.getWorkItem().getId() + ConstantsHelper.MAPPING_SLASH
            + attachment.getName();
      File file = new File(fullname);
      if (!file.exists()) {
         if (logger.isDebugEnabled())
            logger.info(String.format("File %s not found.", fullname));
         return;
      }

      try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
         String mimeType = URLConnection.guessContentTypeFromName(file.getName());
         if (mimeType == null) {
            logger.info("Mimetype is not detectable, will take default");
            mimeType = "application/octet-stream";
         }

         response.setContentType(mimeType);

         /*
          * "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your
          * browser setting
          */
         if ("download".equals(action)) {
            response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
         } else {
            response.setHeader("Content-Disposition", String.format("inline; filename=\"%s\\", file.getName()));
         }

         response.setContentLength((int) file.length());

         FileCopyUtils.copy(bis, response.getOutputStream());
      } catch (Exception ex) {
         logger.error("Error downloading file.", ex);
      }
   }

}