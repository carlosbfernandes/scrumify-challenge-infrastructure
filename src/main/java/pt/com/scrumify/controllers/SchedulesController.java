package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.ScheduleSetting;
import pt.com.scrumify.database.services.ScheduleSettingService;
import pt.com.scrumify.database.services.ScheduleService;
import pt.com.scrumify.entities.ScheduleSettingView;
import pt.com.scrumify.entities.ScheduleView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ScheduleSettingsHelper;
import pt.com.scrumify.helpers.SchedulesHelper;

@Controller
public class SchedulesController {
   
   @Autowired
   private ScheduleService schedulesService;
   @Autowired
   private ScheduleSettingService settingService;
   
   @Autowired
   private SchedulesHelper helper;
   @Autowired
   private ScheduleSettingsHelper settingsHelper;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }
   
   @ApiOperation("Mapping to list all available schedules.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SCHEDULES)
   public String list(Model model) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULES, schedulesService.getAll());
      
      return ConstantsHelper.VIEW_SCHEDULES_READ;
   }
   
   @ApiOperation("Mapping to create a new schedule.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SCHEDULES_CREATE)
   public String create(Model model) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULE , new ScheduleView());
      
      return ConstantsHelper.VIEW_SCHEDULES_CREATE;
   }
   
   @ApiOperation("Mapping to edit an existing schedule.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SCHEDULES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String update(Model model, @PathVariable int id) {
      
      Schedule schedule = schedulesService.getOne(id);
      ScheduleView view = new ScheduleView(schedule);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULE, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SETTINGS, schedule.getSettings());
      
      return ConstantsHelper.VIEW_SCHEDULES_CREATE;
   }
   
   @ApiOperation("Mapping to save a schedule.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_ANY_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_CREATE + "," + ConstantsHelper.ROLE_SCHEDULES_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_SCHEDULES_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULE) @Valid ScheduleView scheduleView, BindingResult bindingResult) {
      
      helper.save(scheduleView.translate());
      
      return ConstantsHelper.VIEW_SCHEDULES_READ;
   }
   
   @ApiOperation("Mapping to add setting to an existing schedule.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SCHEDULES_SETTINGS_CREATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String addSetting(Model model, @PathVariable int id) {
      ScheduleSettingView setting = new ScheduleSettingView();
      setting.setSchedule(schedulesService.getOne(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SETTING, setting);
      return ConstantsHelper.VIEW_SCHEDULES_SETTINGS;
   }
   
   @ApiOperation("Mapping to edit a setting.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SCHEDULES_SETTINGS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String editSetting(Model model, @PathVariable int id) {
      ScheduleSettingView setting = new ScheduleSettingView(settingService.getOne(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SETTING, setting);
      return ConstantsHelper.VIEW_SCHEDULES_SETTINGS;
   }
   
   @ApiOperation("Mapping to delete a setting.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_SCHEDULES_SETTINGS_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String deleteSetting(Model model, @PathVariable int id) {
      ScheduleSetting setting = settingService.getOne(id);
      settingService.delete(setting);
      
      Schedule schedule = setting.getSchedule();
      ScheduleView view = new ScheduleView(schedule);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULE, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SETTINGS, schedule.getSettings());
      return ConstantsHelper.VIEW_SCHEDULES_CREATE + " :: settings-list";
   }
   
   @ApiOperation("Mapping to save a setting.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SCHEDULES_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_SCHEDULES_SETTINGS_SAVE)
   public String saveSetting(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SETTING) @Valid ScheduleSettingView setting) {
      
      settingsHelper.save(setting.translate());
      
      Schedule schedule = setting.getSchedule();
      ScheduleView view = new ScheduleView(schedule);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULE, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SETTINGS, schedule.getSettings());
      
      return ConstantsHelper.VIEW_SCHEDULES_CREATE + " :: settings-list";
   }
}