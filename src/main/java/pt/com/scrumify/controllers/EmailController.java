package pt.com.scrumify.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.services.EmailService;

@Controller
public class EmailController {

  @Autowired
  private ResourceService resourcesService; 
  @Autowired
  private EmailService emailService; 
  
  @GetMapping(value = "/sendMail")
  public String sendMail(Model model,HttpServletRequest request,HttpServletResponse response) {
     
     Resource resource = resourcesService.getOne(4);
     emailService.sendApprovalEmail(resource, ConstantsHelper.RESOURCES_DEFAULT_KEY,request,response);
     
     model.addAttribute("message", "Done");

     return "common/email";
  }  
  
}