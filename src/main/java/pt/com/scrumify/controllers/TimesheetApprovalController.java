package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetApproval;
import pt.com.scrumify.database.entities.TypeOfTimesheetApproval;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.services.TimesheetApprovalMonthlyService;
import pt.com.scrumify.database.services.TimesheetApprovalService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.database.services.YearService;
import pt.com.scrumify.entities.TimesheetApprovalView;
import pt.com.scrumify.entities.TimesheetFilterView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TimesheetApprovalHelper;
import pt.com.scrumify.helpers.TimesheetsHelper;

@Controller
@RequestMapping(value = {ConstantsHelper.MAPPING_TIMESHEETS_APPROVAL})
public class TimesheetApprovalController {
   @Autowired
   private ContractService contractService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TimeService timeService;
   @Autowired
   private TimesheetService timesheetService;
   @Autowired
   private TimesheetApprovalService timesheetApprovalService;
   @Autowired
   private TimesheetApprovalMonthlyService timesheetApprovalMonthlyService;
   @Autowired
   private YearService yearService;
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private TimesheetApprovalHelper timesheetApprovalHelper;
   @Autowired
   private TimesheetsHelper timesheetsHelper;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);

      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * Index
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @GetMapping(value = {ConstantsHelper.MAPPING_INDEX, ConstantsHelper.MAPPING_SLASH + ConstantsHelper.MAPPING_PARAMETER_TAB})
   public String index(Model model, @PathVariable Optional<String> tab) {
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_APPROVAL);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearService.getAllActive());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MONTHS, timeService.getMonths());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, resourcesService.getByTeams(teamsService.getByMemberAndApprover(ResourcesHelper.getResource())));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheetApprovalHelper.search(filter));
      
      return ConstantsHelper.VIEW_TIMESHEETAPPROVAL;
   }

   /*
    * List resources timesheets to approve
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @GetMapping(value = {ConstantsHelper.MAPPING_AJAX + ConstantsHelper.MAPPING_RESOURCES})
   public String resources(Model model) {
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_APPROVAL);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheetApprovalHelper.search(filter));
      
      return ConstantsHelper.VIEW_TIMESHEETAPPROVAL_RESOURCES;
   }

   /*
    * List approved hours per contracts
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @GetMapping(value = {ConstantsHelper.MAPPING_AJAX + ConstantsHelper.MAPPING_CONTRACTS})
   public String contracts(Model model) {
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_APPROVAL);
      List<Contract> contracts = contractService.getContractsByResource(ResourcesHelper.getResource());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_MONTH, LocalDate.now().getMonthValue());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_YEAR, LocalDate.now().getYear());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPROVED_HOURS, timesheetApprovalMonthlyService.findByContractsAndYear(contracts, filter.getYear()));
      
      return ConstantsHelper.VIEW_TIMESHEETAPPROVAL_CONTRACTS;
   }

   /*
    * Apply filter on timesheetsapproval
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_SEARCH)
   public String search(Model model, @Valid TimesheetFilterView filter) {
      cookiesHelper.save(ConstantsHelper.COOKIE_TIMESHEETS_APPROVAL, ResourcesHelper.getResource(), filter);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearService.getAllActive());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MONTHS, timeService.getMonths());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, resourcesService.getByTeams(teamsService.getByMemberAndApprover(ResourcesHelper.getResource())));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheetApprovalHelper.search(filter));
      
      return ConstantsHelper.VIEW_TIMESHEETAPPROVAL;
   }

   /*
    * Open timesheet for approval
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String review(Model model, @PathVariable int id) {
      Timesheet timesheet = timesheetService.getOne(id);
      TimesheetApprovalView approval = new TimesheetApprovalView(timesheet);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, timesheetApprovalHelper.getContractsAndAbsences(timesheet.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET_APPROVAL, approval);

      return ConstantsHelper.VIEW_TIMESHEET_APPROVALS_UPDATE;
   }

   /*
    * Add new row to timesheet approval
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_ADD)
   public String addNewRow(Model model, @Valid TimesheetApprovalView view) {
      TimesheetApproval approval = timesheetApprovalHelper.addNewRow(view, new TypeOfTimesheetApproval(TypeOfTimesheetApproval.HOURS));
      view.getApprovals().add(approval);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, timesheetApprovalHelper.getContractsAndAbsences(view.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET_APPROVAL, view);

      return ConstantsHelper.VIEW_TIMESHEET_APPROVALS_UPDATE + " :: body";
   }

   /*
    * Remove row from timesheet approval
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REMOVE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String removeRow(Model model, @PathVariable int id) {
      TimesheetApproval approval = timesheetApprovalService.getOne(id);
      TimesheetApprovalView view = new TimesheetApprovalView(approval.getTimesheet());
      
      timesheetApprovalService.delete(id);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, timesheetApprovalHelper.getContractsAndAbsences(approval.getTimesheet().getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET_APPROVAL, view);

      return ConstantsHelper.VIEW_TIMESHEET_APPROVALS_UPDATE + " :: body";
   }

   /*
    * Save timesheet approval row
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_SAVE)
   public String save(Model model, @Valid TimesheetApprovalView view) {
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_APPROVAL);

      timesheetApprovalService.saveAll(view.getApprovals());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheetApprovalHelper.search(filter));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);

      return ConstantsHelper.VIEW_TIMESHEETAPPROVAL_RESOURCES_TABLE;
   }

   /*
    * Approve timesheet approval
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_APPROVE)
   public String approve(Model model, @Valid TimesheetApprovalView view) {
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_APPROVAL);

      timesheetApprovalService.saveAll(view.getApprovals());
      
      timesheetsHelper.approve(timesheetService.getSheetByFortnight(view.getResource(), view.getYear().getId(), view.getMonth(), view.getFortnight()));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheetApprovalHelper.search(filter));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);

      return ConstantsHelper.VIEW_TIMESHEETAPPROVAL_RESOURCES_TABLE;
   }
   
   /*
    * Reject timesheet approval
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_APPROVE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_REJECT)
   public String reject(Model model, @Valid TimesheetApprovalView view) {
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_APPROVAL);

      Timesheet timesheet = timesheetService.getSheetByFortnight(view.getResource(), view.getYear().getId(), view.getMonth(), view.getFortnight());
      timesheetsHelper.reject(timesheet);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheetApprovalHelper.search(filter));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);

      return ConstantsHelper.VIEW_TIMESHEETAPPROVAL_RESOURCES_TABLE;
   }
}