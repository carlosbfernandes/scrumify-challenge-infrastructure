package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.entities.TimesheetFilterView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TimesheetsHelper;

@Controller
@RequestMapping(value = {ConstantsHelper.MAPPING_TIMESHEETS_REVIEW})
public class TimesheetReviewController {
   @Autowired
   private TimesheetService timesheetService;
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private TimesheetsHelper timesheetsHelper;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);

      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * Index
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_REVIEW + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_INDEX)
   public String index(Model model) {
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_REVIEW);

      timesheetsHelper.load(model, filter);

      return ConstantsHelper.VIEW_TIMESHEET_REVIEW;
   }

   /*
    * Preview timesheet
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_REVIEW + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_PREVIEW + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String preview(Model model, @PathVariable int id) {
      Timesheet timesheet = timesheetService.getOne(id);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET, timesheet);

      return ConstantsHelper.VIEW_TIMESHEET_REVIEW_PREVIEW;
   }

   /*
    * Approve timesheet review
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_REVIEW + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_APPROVE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String approve(Model model, @PathVariable Integer id) {
      Timesheet timesheet = timesheetService.getOne(id);
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_REVIEW);

      timesheetsHelper.approveReview(timesheet);
      timesheetsHelper.load(model, filter);
      
      return ConstantsHelper.VIEW_TIMESHEET_REVIEW;
   }

   /*
    * Reject timesheet review
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_REVIEW + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REJECT + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String reject(Model model, @PathVariable Integer id) {
      Timesheet timesheet = timesheetService.getOne(id);
      TimesheetFilterView filter = timesheetsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_TIMESHEETS_REVIEW);

      timesheetsHelper.reject(timesheet);
      timesheetsHelper.load(model, filter);
      
      return ConstantsHelper.VIEW_TIMESHEET_REVIEW;
   }

   /*
    * Apply filter on timesheetsreview
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_REVIEW + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_SEARCH)
   public String search(Model model, @Valid TimesheetFilterView filter) {
      timesheetsHelper.load(model, filter);

      cookiesHelper.save(ConstantsHelper.COOKIE_TIMESHEETS_REVIEW, ResourcesHelper.getResource(), filter);

      return ConstantsHelper.VIEW_TIMESHEET_REVIEW_TABLE;
   }
}