package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.ContractProperty;
import pt.com.scrumify.database.services.ContractPropertyService;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.PropertyService;
import pt.com.scrumify.entities.ContractPropertyView;
import pt.com.scrumify.entities.ContractView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ContractsHelper;

@Controller
public class ContractPropertiesController {
   
   @Autowired
   private ContractService contractsService; 
   @Autowired
   private PropertyService propertiesService;
   @Autowired
   private ContractPropertyService contractsPropertiesService;  
   @Autowired
   private ContractsHelper contractsHelper;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
     
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }
	
   /*
    * TAB CONTRACT PROPERTIES
    */
   @ApiOperation("Mapping to respond ajax requests for Contract properties.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "') and @SecurityService.isMember('Contract', #contract)")
   @GetMapping(value =  ConstantsHelper.MAPPING_CONTRACTS_PROPERTIES_AJAX + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
   public String contractPropertiesTab(Model model, @PathVariable int contract) {
      ContractView contractview = new ContractView(this.contractsService.getOne(contract));
     
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, contractview);

      return ConstantsHelper.CONTRACTS_VIEW_UPDATE_CONTRACT_PROPERTIES;
   }

  /*
   * CREATE PROPERTY ON CONTRACT
   */
   @ApiOperation("Mapping to create property on Contract.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "') and @SecurityService.isMember('Contract', #contract)")  
   @GetMapping(value =  ConstantsHelper.MAPPING_CONTRACT_PROPERTY_CREATE + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
   public String openModalProperty(Model model, @PathVariable int contract ) {
      ContractPropertyView contractPropertyView = new ContractPropertyView();
      contractPropertyView.getPk().setContract(contractsService.getOne(contract));
     
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT_PROPERTY, contractPropertyView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES,propertiesService.getByGroup(ConstantsHelper.DATABASE_TABLE_CONTRACTS));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES, propertiesService.getPropertyGroupNotIn(contractsHelper.listOfPropertie(contractsService.getOne(contract).getProperties()), ConstantsHelper.DATABASE_TABLE_CONTRACTS));

      return ConstantsHelper.CONTRACTS_VIEW_UPDATE_CONTRACT_NEW_PROPERTY;
  }

  /*
   * SAVE PROPERTY ON CONTRACT
   */
   @ApiOperation("Mapping to save property on Contract.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "')") 
   @PostMapping(value = ConstantsHelper.MAPPING_CONTRACT_PROPERTY_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT_PROPERTY) @Valid ContractPropertyView contractPropertyView) {

      Contract contract = contractsService.getOne(contractPropertyView.getPk().getContract().getId());
      contract.addProperty(contractsPropertiesService.save(contractPropertyView.translate()));
      contractsService.save(contract);
      return  ConstantsHelper.MAPPING_REDIRECT +  ConstantsHelper.MAPPING_CONTRACTS + ConstantsHelper.MAPPING_SLASH + contract.getId() + ConstantsHelper.MAPPING_UPDATE  + ConstantsHelper.MAPPING_CONTRACTS_PROPERTIES_AJAX;
  }

  /*
   * UPDATE PROPERTY ON CONTRACT
   */
  @ApiOperation("Mapping to update a property on Contract.")
  @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "') and @SecurityService.isMember('Contract', #contract)")  
  @GetMapping(value = ConstantsHelper.MAPPING_CONTRACT_PROPERTY_UPDATE + ConstantsHelper.MAPPING_SLASH + "{idProperty}" + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
  public String update(Model model, @PathVariable int idProperty, @PathVariable int contract) {
     ContractProperty contractProperty = contractsPropertiesService.getOne(contract, idProperty);
     
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT_PROPERTY, new ContractPropertyView(contractProperty));
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES,propertiesService.getByGroup(ConstantsHelper.DATABASE_TABLE_CONTRACTS));
	      
     return ConstantsHelper.CONTRACTS_VIEW_UPDATE_CONTRACT_NEW_PROPERTY;
  }

  /*
   * DELETE PROPERTY ON CONTRACT
   */
  @ApiOperation("Mapping to delete a property on Contract.")
  @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "') and @SecurityService.isMember('Contract', #contract)")
  @GetMapping(value = ConstantsHelper.MAPPING_CONTRACT_PROPERTY_DELETE + ConstantsHelper.MAPPING_SLASH + "{idProperty}" + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
  public String delete(Model model, @PathVariable int idProperty, @PathVariable int contract) {
     ContractProperty contractProperty = contractsPropertiesService.getOne(contract, idProperty);
     contractsPropertiesService.delete(contractProperty);    
     Contract contractToReturn = contractsService.getOne(contract);
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, new ContractView(contractToReturn));
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES,propertiesService.getByGroup(ConstantsHelper.DATABASE_TABLE_CONTRACTS));
     return ConstantsHelper.CONTRACTS_VIEW_UPDATE_CONTRACT_PROPERTIES + " :: properties-list";
  }  
}