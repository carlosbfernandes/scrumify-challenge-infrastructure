package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.ContractStatus;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.ContractStatusService;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.YearService;
import pt.com.scrumify.entities.ContractFilterView;
import pt.com.scrumify.entities.ContractView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ContractsHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.MessagesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.YearsHelper;

@Controller
public class ContractsController {
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private ContractsHelper contractsHelper;
   @Autowired
   private DatesHelper datesHelper;
   @Autowired
   private MessagesHelper messagesHelper;
   @Autowired
   private YearsHelper yearsHelper;
   
	@Autowired
	private ContractService contractService;
	@Autowired
   private ContractStatusService contractStatusService;
   @Autowired
   private MessageSource messageSource;
   @Autowired
   private SubAreaService subAreaService;
   @Autowired
   private YearService yearsService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
     
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST CONTRACTS
    */
   @ApiOperation("Mapping to list the contracts of a resource.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_CONTRACTS)
   public String list(Model model) {
      ContractFilterView filter = contractsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_CONTRACTS);
      
      List<ContractStatus> statuses = contractStatusService.getAllStatus();
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUSLIST, statuses);
      
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, contractService.getContractsInStatusIdsResource(ResourcesHelper.getResource(), filter.getContractStatusesIds()));        
// xxx        model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, contractService.getContractsByResource(ResourcesHelper.getResource()));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE, messagesHelper.createMessage(ConstantsHelper.INFO_MESSAGE, ConstantsHelper.INFO_ICON_MESSAGE, messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CONTRACTS, null, LocaleContextHolder.getLocale())));
      
		return ConstantsHelper.VIEW_CONTRACTS_READ;
	}

   /*
    * CREATE CONTRACTS
    */  
   @ApiOperation("Mapping to create a Contract.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_CREATE + "')")
	@GetMapping(value = ConstantsHelper.MAPPING_CONTRACTS_CREATE)
	public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, new ContractView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT_STATUS, contractStatusService.getAllStatus());
		model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.listSubareasByResource(ResourcesHelper.getResource()));
		model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, this.yearsService.getAllActive());
		model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.formatDate(yearsHelper.getMinActiveYear()));
		model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.formatDate(yearsHelper.getMaxActiveYear()));
		
		return ConstantsHelper.VIEW_CONTRACTS_SAVE;
	}

   /*
    * UPDATE CONTRACTS
    */
   @ApiOperation("Mapping to update a Contract.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "') and @SecurityService.isMember('Contract', #contract)")
	@GetMapping(value = {ConstantsHelper.MAPPING_CONTRACTS_UPDATE, ConstantsHelper.MAPPING_CONTRACTS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_TAB})
	public String update(Model model, @PathVariable Integer contract) {
      ContractView contractview = new ContractView(this.contractService.getOne(contract));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, contractview);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT_STATUS, this.contractStatusService.getAllStatus());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.listSubareasByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsHelper.getYearsBasedOnContract(contractview.getYear()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.formatDate(yearsHelper.getMinActiveYear()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.formatDate(yearsHelper.getMaxActiveYear()));
      
		return ConstantsHelper.VIEW_CONTRACTS_SAVE;
	}	

   /*
    * SAVE CONTRACTS
    */
   @ApiOperation("Mapping to save a Contract.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "')")
	@PostMapping(value = ConstantsHelper.MAPPING_CONTRACTS_SAVE)
	public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT) @Valid ContractView view, BindingResult bindingResult) {
	   if (bindingResult.hasErrors()) {      
	      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_CONTRACTS_CREATE;
	   }
	   
	   contractsHelper.save(view);
 
	   return ConstantsHelper.MAPPING_CONTRACTS_REDIRECT;
	}
	
   /*
    * TAB CONTRACT INFO
    */
   @ApiOperation("Mapping to respond ajax requests for Contract info.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_CONTRACTS_UPDATE + "') and @SecurityService.isMember('Contract', #contract)")
	@GetMapping(value =  ConstantsHelper.MAPPING_CONTRACTS_AJAX + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
   public String contracInfotTab(Model model, @PathVariable int contract) {
      ContractView contractview = new ContractView(this.contractService.getOne(contract));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, contractview);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT_STATUS, this.contractStatusService.getAllStatus());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.listSubareasByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsHelper.getYearsBasedOnContract(contractview.getYear()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.formatDate(yearsHelper.getMinActiveYear()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.formatDate(yearsHelper.getMaxActiveYear()));

      return ConstantsHelper.CONTRACTS_VIEW_UPDATE_CONTRACT;
   }
   
   /*
    * Open filter
    */
   @GetMapping(value = ConstantsHelper.MAPPING_CONTRACTS_FILTER)
   public String getFilter(Model model) {      
      ContractFilterView filter = contractsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_CONTRACTS);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUSLIST, contractStatusService.getAllStatus());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      
      return ConstantsHelper.VIEW_CONTRACTS_FILTER;
   }
   
   /*
    * Save filter
    */
   @PostMapping(value = ConstantsHelper.MAPPING_CONTRACTS_FILTER)
   public String submitFilter(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER) @Valid ContractFilterView filterView) {      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, contractService.getContractsInStatusIdsResource(ResourcesHelper.getResource(), filterView.getContractStatusesIds()));

      cookiesHelper.save(ConstantsHelper.COOKIE_CONTRACTS, ResourcesHelper.getResource(), filterView);

      return ConstantsHelper.VIEW_CONTRACTS_READ + " :: contracts";
   }
}