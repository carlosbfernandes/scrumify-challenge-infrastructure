package pt.com.scrumify.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.TagService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Controller
public class WorkItemTagsController {
   @Autowired
   private SubAreaService subAreasService;
   @Autowired
   private TagService tagsService;
   @Autowired
   private WorkItemService workItemsService;

   /*
    * AJAX TAB TAGS WORKITEM
    */
   @ApiOperation("Mapping to respond ajax requests for workItem tags.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping( value = ConstantsHelper.MAPPING_WORKITEMS_TAGS_AJAX + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String tagsTab(Model model, @PathVariable int workitem) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, this.workItemsService.getOne(workitem));

      return "workitems/fragments/tags :: tags-list";
   }
   
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_TAGS_CREATE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String openModalTag(Model model, @PathVariable int workitem) {
      WorkItemView view = new WorkItemView(workItemsService.getOne(workitem));
      List<SubArea> subAreas = this.subAreasService.listSubareasByResource(ResourcesHelper.getResource());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAGS, tagsService.getAll(subAreas));
      
      return ConstantsHelper.VIEW_WORKITEMS_TAG;
      
   }
   
   @ApiOperation("Mapping to add a new tag related to epic.")
   @PostMapping(value = ConstantsHelper.MAPPING_WORKITEMS_TAGS_SAVE)
   public String saveTagPost(Model model , @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM) @Valid WorkItemView workItemView) {
      WorkItem workItem = workItemsService.getOne(workItemView.getId());
      
      workItem.setTags(workItemView.getTags());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, new WorkItemView(workItemsService.save(workItem)));
      
      return ConstantsHelper.VIEW_WORKITEMS_TAGS;
   }
}