package pt.com.scrumify.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pt.com.scrumify.database.entities.WorkflowTransition;
import pt.com.scrumify.database.services.WorkItemStatusService;
import pt.com.scrumify.database.services.WorkflowService;
import pt.com.scrumify.database.services.WorkflowTransitionService;
import pt.com.scrumify.entities.WorkflowTransitionView;
import pt.com.scrumify.entities.WorkflowView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class WorkflowTransitionsController {
   
   @Autowired
   private WorkItemStatusService itemStatusService;
   @Autowired
   private WorkflowService workflowsService;
   @Autowired
   private WorkflowTransitionService transitionService;
     
   @GetMapping(value = ConstantsHelper.MAPPING_WORKFLOWS_TRANSITION_NEW + ConstantsHelper.MAPPING_PARAMETER_ID )
   public String create(Model model, @PathVariable int id) {
      WorkflowTransition transition = new WorkflowTransition();
      transition.setWorkflow(workflowsService.getOne(id));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOWTRANSITION, transition);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUSLIST, itemStatusService.getAll());
      
      return ConstantsHelper.VIEW_WORKFLOWS_TRANSITION_FORM;
   }
   
   @GetMapping(value = {ConstantsHelper.MAPPING_WORKFLOWS_TRANSITION_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID})
   public String apiUpdateWorkItemTransition(Model model, @PathVariable int id) {

      WorkflowTransition transition = transitionService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOWTRANSITION, transition);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUSLIST, itemStatusService.getAll());
      
      return ConstantsHelper.VIEW_WORKFLOWS_TRANSITION_FORM;
   }

   @PostMapping(value = ConstantsHelper.MAPPING_WORKFLOWS_TRANSITION_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOWTRANSITION) @Valid WorkflowTransitionView workflowtransitionView) {
      
      WorkflowTransition transition = workflowsService.saveTransition(workflowtransitionView.translate());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOW, new WorkflowView(workflowsService.getOne(transition.getWorkflow().getId())));
      return ConstantsHelper.VIEW_WORKFLOWS_UPDATE + " :: workflowtransitions-list";
   }
   
   @GetMapping(value = {ConstantsHelper.MAPPING_WORKFLOWS_TRANSITION_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID})
   public String apiDeleteWorkItemTransition(Model model, @PathVariable int id) {

      WorkflowTransition transition = transitionService.getOne(id);
      
      transitionService.delete(transition);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOW, workflowsService.getOne(transition.getWorkflow().getId()));
      
      return ConstantsHelper.VIEW_WORKFLOWS_UPDATE + " :: workflowtransitions-list";
   }
}