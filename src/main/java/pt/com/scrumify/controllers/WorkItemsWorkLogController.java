package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Activity;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.services.ActivityService;
import pt.com.scrumify.database.services.TypeOfWorkService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.database.services.WorkItemWorkLogService;
import pt.com.scrumify.entities.WorkItemWorkLogView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TimesheetsHelper;
import pt.com.scrumify.helpers.WorkItemWorkLogsHelper;
import pt.com.scrumify.helpers.YearsHelper;

@Controller
public class WorkItemsWorkLogController {
   @Autowired
   private TimesheetsHelper timeSheetsHelper;
   @Autowired
   private WorkItemWorkLogsHelper workLogHelper;
   @Autowired
   private YearsHelper yearsHelper;
   @Autowired
   private ActivityService activityService;
   @Autowired
   private TypeOfWorkService typesOfWorkService;
   @Autowired
   private WorkItemService workItemService;
   @Autowired
   private WorkItemWorkLogService workItemWorkLogService;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);

      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * AJAX TAB WORKLOG WORKITEM
    */
   @ApiOperation("Mapping to respond ajax requests for workItem worklog.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_WORKLOG + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String worklogTab(Model model, @PathVariable int workitem) {
      WorkItem workItem = this.workItemService.getOne(workitem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workItem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, ResourcesHelper.getResource());

      String formattedDate = timeSheetsHelper.getLastDateofTimeSheetApprovedorReviewed();

      if (formattedDate != null) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATE_LASTDAYAPPROVED, formattedDate);
      } else {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATE_LASTDAYAPPROVED, workItem.getAssigned());
      }

      return ConstantsHelper.VIEW_WORKITEMS_WORKLOG;
   }

   /*
    * CREATE WORKLOG
    */
   @ApiOperation("Mapping to create worklogs.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_NEW + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String create(Model model, @PathVariable int workitem) {
      WorkItemWorkLogView log = new WorkItemWorkLogView();
      log.setWorkItem(workItemService.getOne(workitem));

      List<Activity> activities = this.activityService.getByTechnicalAssistance(log.getWorkItem().getTeam().isTechnicalAssistance());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMWORKLOG, log);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITIES, activities);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKS, typesOfWorkService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ASSIGNED, log.getWorkItem().getAssigned());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LIMIT_DATE, yearsHelper.getMaxActiveYear());

      return ConstantsHelper.VIEW_WORKITEMS_WORKLOG_NEW;
   }

   /*
    * EDIT WORKLOG
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.canDelete('Worklog', #id)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String update(Model model, @PathVariable int id) {
      WorkItemWorkLog log = workItemWorkLogService.getOne(id);

      List<Activity> activities = this.activityService.getByTechnicalAssistance(log.getWorkItem().getTeam().isTechnicalAssistance());

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITIES, activities);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKS, typesOfWorkService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMWORKLOG, new WorkItemWorkLogView(log));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LIMIT_DATE, yearsHelper.getMaxActiveYear());

      return ConstantsHelper.VIEW_WORKITEMS_WORKLOG_NEW;
   }

   /*
    * SAVE WORKLOG
    */
   @ApiOperation("Mapping to save worklogs.")
   @PostMapping(value = ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMWORKLOG) WorkItemWorkLogView view) {
      workLogHelper.add(view);
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + view.getWorkItem().getId() + ConstantsHelper.MAPPING_WORKITEMS_WORKLOG;
   }

   /*
    * DELETE WORKLOG
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.canDelete('Worklog', #id)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String delete(Model model, @PathVariable int id) {
      WorkItem wi = workLogHelper.remove(id);

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_WORKITEMS_WORKLOG + ConstantsHelper.MAPPING_SLASH + wi.getId();
   }
}