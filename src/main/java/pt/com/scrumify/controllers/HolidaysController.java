package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.Holiday;
import pt.com.scrumify.database.services.HolidayService;
import pt.com.scrumify.database.services.YearService;
import pt.com.scrumify.entities.HolidayView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.TimesHelper;

@Controller
public class HolidaysController {

   @Autowired
   private HolidayService holidaysService;
   @Autowired
   private YearService yearsService;
   @Autowired
   private TimesHelper timesHelper;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }
  
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_HOLIDAYS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_HOLIDAYS)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getByHoldiays());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAYS, holidaysService.getAllInMap());

      return ConstantsHelper.VIEW_HOLIDAYS_READ;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_HOLIDAYS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_HOLIDAYS_CREATE)
   public String add(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAY, new HolidayView());

      return ConstantsHelper.VIEW_HOLIDAYS_NEW;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_ANY_AUTHORITY + ConstantsHelper.ROLE_HOLIDAYS_CREATE + "," + ConstantsHelper.ROLE_HOLIDAYS_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_HOLIDAYS_SAVE)
   public String save(Model model,@ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAY) @Valid HolidayView holiday) {
      
      holidaysService.save(holiday.translate());
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_HOLIDAYS;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_HOLIDAYS_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_HOLIDAYS_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String delete(Model model, @PathVariable int id) {

      Holiday holiday = holidaysService.getOne(id);
      holidaysService.delete(holiday) ;  
     
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_HOLIDAYS;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_HOLIDAYS_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_HOLIDAYS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String update(Model model, @PathVariable int id) {

      Holiday holiday = holidaysService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAY, new HolidayView(holiday));
      
      return ConstantsHelper.VIEW_HOLIDAYS_NEW;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_HOLIDAYS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_HOLIDAYS_GENERATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String generate(Model model, @PathVariable int id) {

      timesHelper.createHolidays(id);      
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_HOLIDAYS;
   }


}