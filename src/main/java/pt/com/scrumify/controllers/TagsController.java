package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.TagService;
import pt.com.scrumify.entities.TagView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Controller
public class TagsController {
   @Autowired
   private TagService tagsService;
   @Autowired
   private SubAreaService subAreasService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TAGS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TAGS)
   public String list(Model model) {
      List<SubArea> subAreas = this.subAreasService.listSubareasByResource(ResourcesHelper.getResource());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAGS, tagsService.getAll(subAreas));

      return ConstantsHelper.VIEW_TAGS_READ;
   }
   
   /*
    * CREATE
    */     
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TAGS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TAGS_CREATE)
   public String create(Model model) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAG, new TagView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreasService.listSubareasByResource(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_TAGS_CREATE;
   }
   
   /*
    * UPDATE
    */ 
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TAGS_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TAGS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_TAG)
   public String update(Model model, @PathVariable int tag) {
      TagView tagView = new TagView(tagsService.getOne(tag));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAG, tagView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreasService.listSubareasByResource(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_TAGS_CREATE;
   }
   
   /*
    * DELETE
    */ 
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TAGS_DELETE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TAGS_DELETE + ConstantsHelper.MAPPING_PARAMETER_TAG)
   public String delete(Model model, @PathVariable int tag) {
      
      tagsService.delete(tag);      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TAGS;
   }
   
   /*
    * SAVE
    */ 
   @PostMapping(value = ConstantsHelper.MAPPING_TAGS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAG) @Valid TagView tagView) {
      
      tagsService.save(tagView.translate());
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TAGS;
   }
     
}