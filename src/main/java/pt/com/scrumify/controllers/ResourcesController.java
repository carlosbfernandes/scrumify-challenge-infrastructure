package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.entities.Gender;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.TeamMemberPK;
import pt.com.scrumify.database.services.AvatarService;
import pt.com.scrumify.database.services.GenderService;
import pt.com.scrumify.database.services.ProfileService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.ScheduleService;
import pt.com.scrumify.database.services.TeamMemberService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.entities.ChangePIN;
import pt.com.scrumify.entities.PersonalData;
import pt.com.scrumify.entities.ResourceView;
import pt.com.scrumify.entities.SignUp;
import pt.com.scrumify.entities.TeamMemberView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TeamsHelper;
import pt.com.scrumify.services.SecurityService;

@Controller
public class ResourcesController {
   @Autowired
   private AvatarService avatarsService;
   @Autowired
   private GenderService gendersService;
   @Autowired
   private ProfileService profilesService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private ResourcesHelper resourcesHelper;
   @Autowired
   private ScheduleService schedulesService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TeamMemberService teamMembersService;
   @Autowired
   private TeamsHelper teamsHelper;
   @Autowired
   private SecurityService securityService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LOGIN
    */
   @ApiOperation("Redirect to login view.")
   @GetMapping(produces = ConstantsHelper.MIME_TYPE_HTML, value = ConstantsHelper.MAPPING_LOGIN)
   public String login(Model model, String error, String logout, HttpServletRequest request) {
      String referer = request.getHeader("Referer");
      request.getSession().setAttribute(ConstantsHelper.REDIRECT_URL_SESSION_ATTRIBUTE, referer);
      
      return ConstantsHelper.VIEW_LOGIN;
   }

   /*
    * CHANGE PIN
    */
   @GetMapping(value = ConstantsHelper.MAPPING_RESOURCES_CHANGEPIN)
   public String changePin(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CHANGEPIN, new ChangePIN(ResourcesHelper.getResource()));

      return ConstantsHelper.VIEW_RESOURCES_CHANGEPIN;
   }

   @PostMapping(value = ConstantsHelper.MAPPING_RESOURCES_CHANGEPIN)
   public String changePin(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CHANGEPIN) @Valid ChangePIN changePIN, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CHANGEPIN, changePIN);
         
         return ConstantsHelper.VIEW_RESOURCES_CHANGEPIN;
      }
      
      this.resourcesHelper.changePIN(changePIN.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_ROOT;
   }

   /*
    * PERSONAL DATA
    */
   @GetMapping(produces = ConstantsHelper.MIME_TYPE_HTML, value = {ConstantsHelper.MAPPING_RESOURCES_PERSONALDATA, ConstantsHelper.MAPPING_RESOURCES_PERSONALDATA_PERSONALINFO, ConstantsHelper.MAPPING_RESOURCES_PERSONALDATA_AVATARS})
   public String personalData(Model model) {
      PersonalData personalData = new PersonalData(ResourcesHelper.getResource());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, personalData);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AVATARS, this.avatarsService.getByGender(personalData.getGender()));

      return ConstantsHelper.VIEW_RESOURCES_PERSONALDATA;
   }

   @PostMapping(value = ConstantsHelper.MAPPING_RESOURCES_PERSONALDATA)
   public String personalData(Model model, @Valid PersonalData personalData, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, personalData);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());
         
         return ConstantsHelper.VIEW_FRAGMENT_PERSONALINFO_SIGNUP;
      }

      this.resourcesHelper.personalData(personalData.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_ROOT;
   }

   /*
    * PERSONAL INFO
    */
   @ApiOperation("Mapping to respond ajax requests for personal info (personal data forms.")
   @PostMapping(produces = ConstantsHelper.MIME_TYPE_HTML, value = ConstantsHelper.MAPPING_PUBLIC_PERSONALDATA_AJAX)
   public String personalInfo4PersonalData(Model model, PersonalData personalData) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, personalData);

      return ConstantsHelper.VIEW_FRAGMENT_PERSONALINFO_PERSONALDATA;
   }
   
   @ApiOperation("Mapping to respond ajax requests for personal info (sign up forms.")
   @PostMapping(produces = ConstantsHelper.MIME_TYPE_HTML, value = ConstantsHelper.MAPPING_PUBLIC_SIGNUP_AJAX)
   public String personalInfo4SignUp(Model model, SignUp signUp) {      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, signUp);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());

      return ConstantsHelper.VIEW_FRAGMENT_PERSONALINFO_SIGNUP;
   }
   
   @ApiOperation("Mapping to respond ajax requests for personal info (resource forms.")
   @PostMapping(produces = ConstantsHelper.MIME_TYPE_HTML, value = ConstantsHelper.MAPPING_PUBLIC_RESOURCES_AJAX)
   public String personalInfo4Resources(Model model, ResourceView resourceView) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, resourceView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILES, this.profilesService.getNonSystem());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULES, schedulesService.getAll());

      return ConstantsHelper.VIEW_FRAGMENT_PERSONALINFO_RESOURCES;
   }
   
   @ApiOperation("Mapping to respond ajax requests for teams (resource forms.")
   @GetMapping(produces = ConstantsHelper.MIME_TYPE_HTML, value = ConstantsHelper.MAPPING_PUBLIC_RESOURCES_TEAMS_AJAX + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String teams4Resources(Model model, @PathVariable int id) {
      ResourceView resourceView = new ResourceView(resourcesService.getOne(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, resourceView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILES, this.profilesService.getNonSystem());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MYTEAMS,this.teamsService.getByMember(this.resourcesService.getOne(resourceView.getId())));
      if(resourceView.isActive() && resourceView.isApproved())
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMember(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULES, schedulesService.getAll());
      
      return ConstantsHelper.VIEW_FRAGMENT_TEAMS;
   }
   
   /*
    * RESOURCES
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_RESOURCES_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_RESOURCES)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, this.resourcesService.getNonSystem());

      return ConstantsHelper.VIEW_RESOURCES_READ;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_RESOURCES_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_RESOURCES_CREATE)
   public String create(Model model) {
      Gender gender = new Gender(Gender.MALE);
      ResourceView view = new ResourceView();
      view.setAvatar(avatarsService.getRandomByGender(gender));
      List<Avatar> avatars = this.avatarsService.getByGender(gender);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AVATARS, avatars);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILES, this.profilesService.getNonSystem());

      return ConstantsHelper.VIEW_RESOURCES_CREATE;
   }   

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_RESOURCES_UPDATE + "')")
   @GetMapping(value = {ConstantsHelper.MAPPING_RESOURCES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_RESOURCE, ConstantsHelper.MAPPING_RESOURCES_UPDATE_PERSONALINFO, ConstantsHelper.MAPPING_RESOURCES_UPDATE_AVATARS, ConstantsHelper.MAPPING_RESOURCES_UPDATE_TEAMS })
   public String update(Model model, @PathVariable int resource) { 
      ResourceView resourceView = new ResourceView(this.resourcesService.getOne(resource));
      List<Avatar> avatars = this.avatarsService.getByGender(resourceView.getGender());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, resourceView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AVATARS, avatars);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILES, this.profilesService.getNonSystem());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MYTEAMS,this.teamsService.getByMember(this.resourcesService.getOne(resource)));  
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS,this.teamsHelper.teamsList(resource));  
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULES, schedulesService.getAll());

      return ConstantsHelper.VIEW_RESOURCES_UPDATE;
   }

   @PostMapping(value = ConstantsHelper.MAPPING_RESOURCES_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE) @Valid ResourceView resourceView, BindingResult bindingResult,HttpServletRequest request,HttpServletResponse response) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, resourceView);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROFILES, this.profilesService.getNonSystem());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MYTEAMS,this.teamsService.getByMember(this.resourcesService.getOne(resourceView.getId())));  
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS,this.teamsHelper.teamsList(resourceView.getId()));
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SCHEDULES, schedulesService.getAll());
         
         return ConstantsHelper.VIEW_FRAGMENT_PERSONALINFO_RESOURCES;
      }
      
      this.resourcesHelper.save(resourceView,request,response);

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_RESOURCES;
   }
   
   /*
    * SAVE RESOURCE IN TEAM
    */
   @PostMapping(value = ConstantsHelper.MAPPING_RESOURCE_TEAMS_SAVE)
   public String save(Model model, @Valid TeamMemberView teamMember) {
     this.teamMembersService.save(teamMember.translate());
     
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, teamMember.getPk().getResource());
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MYTEAMS,this.teamsService.getByMember(this.resourcesService.getOne(teamMember.getPk().getResource().getId())));  
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS,this.teamsHelper.teamsList(teamMember.getPk().getResource().getId()));
                 
     return ConstantsHelper.VIEW_FRAGMENT_TEAMS;
  }

   /*
    * REMOVE RESOURCE IN TEAM
    */
   @GetMapping(value = ConstantsHelper.MAPPING_RESOURCE_TEAMS_DELETE + ConstantsHelper.MAPPING_PARAMETER_RESOURCE + ConstantsHelper.MAPPING_PARAMETER_TEAM)
   public String delete(Model model, @PathVariable int resource, @PathVariable int team) {
      TeamMemberPK teamMemberPK = new TeamMemberPK();
      teamMemberPK.setResource(this.resourcesService.getOne(resource));
      teamMemberPK.setTeam(this.teamsService.getOne(team));
      
      TeamMember teamMember = new TeamMember();
      teamMember.setPk(teamMemberPK);
      teamMembersService.delete(teamMember);
     
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, resourcesService.getOne(resource));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MYTEAMS,this.teamsService.getByMember(resourcesService.getOne(resource)));  
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS,this.teamsHelper.teamsList(resource));
                 
     return ConstantsHelper.VIEW_FRAGMENT_TEAMS;
  }

   /*
    * SIGN UP
    */
   @ApiOperation(produces = ConstantsHelper.MIME_TYPE_HTML, value = "Mapping to the sign-up page.")
   @GetMapping(value = {ConstantsHelper.MAPPING_SIGNUP, ConstantsHelper.MAPPING_SIGNUP_PERSONALINFO, ConstantsHelper.MAPPING_SIGNUP_AVATARS})
   public String signUp(Model model) {
      SignUp signUp = new SignUp();
      Gender gender = signUp.getGender();
      signUp.setAvatar(avatarsService.getRandomByGender(gender));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, signUp);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AVATARS, this.avatarsService.getByGender(gender));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());

      return ConstantsHelper.VIEW_SELFSERVICE_SIGNUP;
   }

   @ApiOperation(produces = ConstantsHelper.MIME_TYPE_HTML, value = "Mapping to the sign-up page.")
   @PostMapping(value = ConstantsHelper.MAPPING_SIGNUP)
   public String signUp(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE) @Valid SignUp signUp, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, signUp);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_GENDERS, this.gendersService.getAll());
         
         return ConstantsHelper.VIEW_FRAGMENT_PERSONALINFO_SIGNUP;
      }
      
      this.resourcesHelper.registration(signUp.translate());
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_LOGIN;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_IMPERSONABLE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_IMPERSONATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String impersonate(Model model, HttpServletRequest request, @PathVariable int id) {
      String referer = request.getHeader(ConstantsHelper.HTTP_REFERER);
      Resource resource = this.resourcesService.getOne(id);
      
      if (resource != null) {
         securityService.setSecurityContext(resource);
      }
      
      /*
       * Redirect to original url
       */
      return ConstantsHelper.MAPPING_REDIRECT + referer;
   } 
}