package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.entities.AbsenceView;
import pt.com.scrumify.helpers.AbsencesHelper;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;


@Controller
public class AbsencesController {
   @Autowired
   public AbsenceService absencesService;
   @Autowired
   public TimesheetService timesheetsService; 
   @Autowired
   private AbsencesHelper absencesHelper;
   @Autowired
   private DatesHelper datesHelper;
      
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ABSENCES_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_ABSENCES)
   public String list(Model model){
      
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, absencesService.listByResource(ResourcesHelper.getResource()));
     
     return ConstantsHelper.VIEW_ABSENCES_READ;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ABSENCES_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_ABSENCES_CREATE)
   public String create(Model model) {
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCE, new AbsenceView());
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCESTYPE, absencesService.getAllTypes());
     
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.formatDate(datesHelper.getMinDate()));
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.formatDate(datesHelper.getMaxDate()));
     
     return ConstantsHelper.VIEW_ABSENCES_NEW;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ABSENCES_CREATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_ABSENCES_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCE) @Valid AbsenceView view) {
      absencesHelper.add(view);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, absencesService.listByResource(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_ABSENCES_READ + " :: absences-list";
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ABSENCES_CREATE + "') and @SecurityService.canDelete('Absence', #id)")
   @GetMapping(value = ConstantsHelper.MAPPING_ABSENCES_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String delete(Model model, @PathVariable int id) {
      absencesHelper.remove(id);  
     
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, absencesService.listByResource(ResourcesHelper.getResource()));
      return ConstantsHelper.VIEW_ABSENCES_READ + " :: absences-list";
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ABSENCES_UPDATE + "') and @SecurityService.canDelete('Absence', #id)")
   @GetMapping(value = ConstantsHelper.MAPPING_ABSENCES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String update(Model model, @PathVariable int id) {

      Absence absence = absencesService.getById(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCESTYPE, absencesService.getAllTypes());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCE, new AbsenceView(absence));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.formatDate(datesHelper.getMinDate()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.formatDate(datesHelper.getMaxDate()));
      
      return ConstantsHelper.VIEW_ABSENCES_NEW;
   }
}   