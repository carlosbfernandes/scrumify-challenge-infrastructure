package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.services.CompensationDayService;
import pt.com.scrumify.database.services.YearService;
import pt.com.scrumify.entities.CompensationDaysView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.TimesHelper;
import pt.com.scrumify.helpers.YearsHelper;

@Controller
public class CompensationDaysController {

   @Autowired
   private CompensationDayService compensationDaysService;
   @Autowired
   private YearService yearsService;
   @Autowired
   private TimesHelper timesHelper;
   @Autowired
   private YearsHelper yearsHelper; 

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }
  
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_COMPENSATIONDAYS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_COMPENSATIONDAYS)
   public String list(Model model) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getByCompensationDays());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_COMPENSATIONDAYS, compensationDaysService.getAllInMap());

      return ConstantsHelper.VIEW_COMPENSATIONDAYS_READ;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_COMPENSATIONDAYS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_COMPENSATIONDAYS_CREATE)
   public String add(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_COMPENSATIONDAY, new CompensationDaysView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LIMIT_DATE, yearsHelper.getMaxActiveYear());

      return ConstantsHelper.VIEW_COMPENSATIONDAYS_NEW;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_ANY_AUTHORITY + ConstantsHelper.ROLE_COMPENSATIONDAYS_CREATE + "," + ConstantsHelper.ROLE_COMPENSATIONDAYS_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_COMPENSATIONDAYS_SAVE)
   public String save(Model model,@ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAY) @Valid CompensationDaysView compensationDaysView) {
      
      if(compensationDaysView.getStartDay() != null){
         List<CompensationDay> compensations = compensationDaysService.listdays(compensationDaysView.translate());
         compensationDaysService.saveAll(compensations);
      }else{
         compensationDaysService.save(compensationDaysView.translate());
      }
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_COMPENSATIONDAYS;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_COMPENSATIONDAYS_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_COMPENSATIONDAYS_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String delete(Model model, @PathVariable int id) {

      CompensationDay compensationDay = compensationDaysService.getOne(id);
      compensationDaysService.delete(compensationDay) ;  
     
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_COMPENSATIONDAYS;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_COMPENSATIONDAYS_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_COMPENSATIONDAYS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String update(Model model, @PathVariable int id) {

      CompensationDay compensationDay = compensationDaysService.getOne(id);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_COMPENSATIONDAY, new CompensationDaysView(compensationDay));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_UPDATE,true);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LIMIT_DATE, yearsHelper.getMaxActiveYear());
      
      return ConstantsHelper.VIEW_COMPENSATIONDAYS_NEW;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_COMPENSATIONDAYS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_COMPENSATIONDAYS_GENERATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String generate(Model model, @PathVariable int id) {

      timesHelper.createCompensationDays(id);
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_COMPENSATIONDAYS;
   }
}