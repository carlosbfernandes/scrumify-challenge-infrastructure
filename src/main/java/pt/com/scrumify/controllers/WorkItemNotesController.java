package pt.com.scrumify.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.database.services.TypeOfWorkItemNoteService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.entities.WorkItemNoteView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.WorkItemNotesHelper;

@Controller
public class WorkItemNotesController {
   @Autowired
   private WorkItemNotesHelper workItemsNotesHelper;
   @Autowired
   private TypeOfWorkItemNoteService typesOfWorkItemNoteService;
   @Autowired
   private WorkItemService workItemService;
 
   /*
    * AJAX TAB NOTES WORKITEM
    */
   @ApiOperation("Mapping to respond ajax requests for workItem notes.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping( value = ConstantsHelper.MAPPING_WORKITEMS_NOTES_AJAX + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String notesTab(Model model, @PathVariable int workitem) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_IDWORKITEM, workitem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEMNOTE,typesOfWorkItemNoteService.getByTypeOfWorkItem((workItemService.getOne(workitem)).getType()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMNOTES, (workItemService.getOne(workitem)).getNotes());

      return ConstantsHelper.VIEW_WORKITEMS_NOTES;
   }

   /*
    * CREATE WORKITEM NOTES
    */
   @ApiOperation("Mapping to create workItems notes.")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_NOTES_NEW + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String create(Model model, @PathVariable int workitem, @PathVariable int id) {
      WorkItemNoteView note = new WorkItemNoteView();
      
      note.setWorkItem(workItemService.getOne(workitem));
      note.setTypeOfNote(typesOfWorkItemNoteService.getById(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMNOTE, note);
      
      return ConstantsHelper.VIEW_WORKITEMS_NOTES_NEW;
   }
   
   /*
    * SAVE WORKITEM NOTES
    */
   @ApiOperation("Mapping to save workItems notes.")
   @PostMapping(value = ConstantsHelper.MAPPING_WORKITEMS_NOTES_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMNOTE) @Valid WorkItemNoteView view, HttpServletRequest request, HttpServletResponse response) {
      WorkItemNote note = workItemsNotesHelper.addNote(view, request, response);
      
      return ConstantsHelper.MAPPING_REDIRECT +  ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + note.getWorkItem().getId() + ConstantsHelper.MAPPING_WORKITEMS_NOTES;
   }

}