package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.services.YearService;
import pt.com.scrumify.entities.YearView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.YearsHelper;

@Controller
public class YearsController {
   
   @Autowired
   private TimeService timesService;
   @Autowired
   private YearService yearsService;
   
   @Autowired
   private YearsHelper yearsHelper;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_YEARS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_YEARS)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getAll());

      return ConstantsHelper.VIEW_YEARS_READ;
   }
   
   /*
    * CREATE
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_YEARS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_YEARS_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEAR, new Year());
      
      return ConstantsHelper.VIEW_YEARS_CREATE;
   }
   
   @PostMapping(value = ConstantsHelper.MAPPING_YEARS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE) @Valid YearView yearView) {

      yearsHelper.save(yearView.translate());
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_YEARS;
   }
   
   /*
    * UPDATE - show times list
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_YEARS_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_YEARS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String update(Model model, @PathVariable int id) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMES, timesService.getByYear(id));
            
      return ConstantsHelper.MAPPING_YEARS_TIMES;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_YEARS_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_YEARS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID + ConstantsHelper.MAPPING_PARAMETER_ACTIVEFLAG)
   public String updateActiveFlag(Model model, @PathVariable int id, @PathVariable boolean activeFlag) {
      
      Year year = yearsService.getOne(id);
      year.setActive(activeFlag);
      yearsService.save(year);
            
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getAll());
      
      return ConstantsHelper.VIEW_YEARS_READ + " :: table";
   }
}