package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.Wiki;
import pt.com.scrumify.database.entities.WikiPage;
import pt.com.scrumify.database.services.ApplicationService;
import pt.com.scrumify.database.services.WikiPageService;
import pt.com.scrumify.database.services.WikiService;
import pt.com.scrumify.entities.WikiPageView;
import pt.com.scrumify.entities.WikiView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.WikisHelper;

@Controller
public class WikisController {
   
   @Autowired
   private WikisHelper wikisHelper;
   @Autowired
   private ApplicationService applicationsService;
   @Autowired
   private WikiService wikisService;
   @Autowired
   private WikiPageService pagesService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKIS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS)
   public String list(Model model) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIS, wikisService.getByResource(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_WIKIS_READ;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKIS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS_CREATE)
   public String create(Model model) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKI, new WikiView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, applicationsService.listAll());
      
      return ConstantsHelper.VIEW_WIKIS_SAVE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKIS_UPDATE + "')")
   @GetMapping(value = { ConstantsHelper.MAPPING_WIKIS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID})
   public String edit(Model model, @PathVariable Integer id) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKI, new WikiView(wikisService.getOne(id)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, applicationsService.listAll());
      
      return ConstantsHelper.VIEW_WIKIS_SAVE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKIS_CREATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_WIKIS_SAVE)
   public String save(@ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKI) @Valid WikiView view) {
      wikisService.save(view.translate());
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_WIKIS;
   }
   
   /*
    * Create new page
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKISPAGES_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS_PAGES_CREATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String createPage(Model model, @PathVariable Integer id) {
      
      WikiPage page = new WikiPage();
      page.setWiki(wikisService.getOne(id));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIPAGE, page);
            
      return ConstantsHelper.VIEW_WIKIS_PAGE_SAVE + ConstantsHelper.FRAGMENTS_FORM;
   }
   
   /*
    * Edit page
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKISPAGES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS_PAGES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String editPage(Model model, @PathVariable Integer id) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIPAGE, pagesService.getOne(id));
      
      return ConstantsHelper.VIEW_WIKIS_PAGE_SAVE + ConstantsHelper.FRAGMENTS_FORM;
   }
   
   /*
    * Save page
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_ANY_AUTHORITY + ConstantsHelper.ROLE_WIKISPAGES_CREATE + "," + ConstantsHelper.ROLE_WIKISPAGES_UPDATE + "')")
   @PostMapping(value =  ConstantsHelper.MAPPING_WIKIS_PAGES_SAVE)
   public String savePage(Model model, @ModelAttribute("wikipage") @Valid WikiPageView wikiPageView) {
      
      pagesService.save(wikiPageView.translate());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIS, wikisService.getByResource(ResourcesHelper.getResource()));
      return ConstantsHelper.VIEW_WIKIS_READ ;
   }
   
   /*
    * View page
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKISPAGES_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS_PAGES_AJAX + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String viewPage(Model model, @PathVariable Integer id) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIPAGE, pagesService.getOne(id));
      
      return ConstantsHelper.VIEW_WIKIS_PAGE_VIEW + " :: wikipage";
   }
   
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS_SEARCH + ConstantsHelper.MAPPING_SLASH + "{searchField}")
   public String search(@PathVariable Optional<String> searchField, Model model) {

      List<Wiki> searchResults = null;
       try {
          if(searchField.isPresent()) {
             if(ConstantsHelper.NULL_STRING.equals(searchField.get())){
                searchResults = wikisService.getByResource(ResourcesHelper.getResource());
             }else{
                searchResults = wikisHelper.fuzzySearch(searchField.get(),ResourcesHelper.getResource());
             }
          }

       } catch (Exception ex) {
           // handle exception
       }
       model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIS, searchResults);
       return ConstantsHelper.VIEW_WIKIS_READ + " :: wikis-list" ;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKISPAGES_DELETE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS_PAGES_DELETE_AJAX + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String deletePage(Model model, @PathVariable Integer id) {
      pagesService.delete(pagesService.getOne(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIS, wikisService.getByResource(ResourcesHelper.getResource()));
      return ConstantsHelper.VIEW_WIKIS_LIST;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WIKIS_DELETE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WIKIS_DELETE+ ConstantsHelper.MAPPING_PARAMETER_ID)
   public String deleteWiki(Model model, @PathVariable Integer id) {
      wikisService.delete(wikisService.getOne(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WIKIS, wikisService.getByResource(ResourcesHelper.getResource()));
      return ConstantsHelper.VIEW_WIKIS_LIST;
   }
}

