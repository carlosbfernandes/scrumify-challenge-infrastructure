package pt.com.scrumify.controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.EpicHistory;
import pt.com.scrumify.database.entities.EpicNote;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.services.EpicHistoryService;
import pt.com.scrumify.database.services.EpicNoteService;
import pt.com.scrumify.database.services.EpicService;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.TagService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.TypeOfWorkItemNoteService;
import pt.com.scrumify.database.services.WorkItemStatusService;
import pt.com.scrumify.entities.EpicFilterView;
import pt.com.scrumify.entities.EpicNoteView;
import pt.com.scrumify.entities.EpicView;
import pt.com.scrumify.entities.UserStoryView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ContextHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.EpicsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.UserStoriesHelper;

@Controller
public class EpicsController {
   @Autowired
   private ContextHelper contextHelper;
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private EpicsHelper epicsHelper;
   @Autowired
   private UserStoriesHelper userstoriesHelper;
   @Autowired
   private EpicService epicService;
   @Autowired
   private EpicHistoryService epicHistoryService;
   @Autowired
   private EpicNoteService notesService;
   @Autowired
   private WorkItemStatusService itemStatusService;
   @Autowired
   private SubAreaService subAreasService;
   @Autowired
   private TagService tagsService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TypeOfWorkItemNoteService typesOfWorkItemNoteService;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);

      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_EPICS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS)
   public String list(Model model) throws IOException {
      EpicFilterView filter = epicsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_EPICS);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsHelper.getByFilter(filter));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      
      return ConstantsHelper.VIEW_EPICS_READ;
   }

   /*
    * CREATE
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_EPICS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_CREATE)
   public String create(Model model) {
      EpicView view = new EpicView();
      view.setStatus(itemStatusService.getOne(WorkItemStatus.NEW));
      
      List<Team> teams = new ArrayList<>();
      teams.add(contextHelper.getActiveTeam());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teams);

      return ConstantsHelper.VIEW_EPICS_CREATE;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_EPICS_READ + "') and @SecurityService.isMember('Epic', #epicId)")
   @GetMapping(value = {ConstantsHelper.MAPPING_EPICS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_EPIC,
         ConstantsHelper.MAPPING_EPICS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_EPIC + ConstantsHelper.MAPPING_SLASH + "{steps}"})
   public String update(Model model, @PathVariable int epicId) {
      Epic epic = epicService.getOne(epicId);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, new EpicView(epic));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), epic.getTeam()));

      return ConstantsHelper.VIEW_EPICS_UPDATE;
   }

   /*
    * Tab epic - info
    */
   @ApiOperation("Mapping to respond ajax requests for epic information.")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_AJAX + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String epicInfoTab(Model model, @PathVariable int epicId) {
      EpicView epicView = new EpicView(epicService.getOne(epicId));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epicView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), epicView.getTeam()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(epicView.getType().getWorkflow(), epicView.getStatus()));

      return ConstantsHelper.VIEW_EPICS_TAB_INFO;
   }

   /*
    * Tab epic - update status
    */
   @ApiOperation("Mapping to update status in info tab.")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_STATUS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_EPIC + ConstantsHelper.MAPPING_PARAMETER_STATE)
   public String updateStatus(Model model, @PathVariable int epicId, @PathVariable int state) {
      Epic epic = epicService.getOne(epicId);
      epicHistoryService.save(new EpicHistory(epic));
      epic.setStatus(itemStatusService.getOne(state));

      EpicView epicView = new EpicView(epicService.save(epic));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epicView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), epicView.getTeam()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(epicView.getType().getWorkflow(), epicView.getStatus()));

      return ConstantsHelper.VIEW_EPICS_TAB_INFO;
   }

   /*
    * Tab epic - userstories
    */
   @ApiOperation("Mapping to respond ajax requests for epic information.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_EPICS_READ + "') and @SecurityService.isMember('Epic', #epicId)")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_USERSTORIES_AJAX + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String epicUserstoriesTab(Model model, @PathVariable int epicId) {
      EpicView epicView = new EpicView(epicService.getOne(epicId));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epicView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORIES, epicView.getUserStories());

      return ConstantsHelper.VIEW_EPICS_TAB_USERSTORIES;
   }

   /*
    * Tab epic - notes
    */
   @ApiOperation("Mapping to respond ajax requests for epic notes .")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_EPICS_READ + "') and @SecurityService.isMember('Epic', #epicId)")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_NOTES_AJAX + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String epicNotesTab(Model model, @PathVariable int epicId) {
      Epic epic = epicService.getOne(epicId);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC_ID, epicId);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, new EpicView(epic));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEMNOTE, typesOfWorkItemNoteService.getByTypeOfWorkItem(epic.getType()));
      return ConstantsHelper.VIEW_EPICS_TAB_NOTES;
   }

   /*
    * Tab epic - new note
    */
   @ApiOperation("Mapping to respond ajax requests for epic notes .")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_NOTES_NEW + ConstantsHelper.MAPPING_PARAMETER_EPIC + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String createNote(Model model, @PathVariable int epicId, @PathVariable int id) {

      EpicNote epicNote = new EpicNote();
      epicNote.setEpic(epicService.getOne(epicId));
      epicNote.setTypeOfNote(typesOfWorkItemNoteService.getById(id));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICNOTE, epicNote);

      return ConstantsHelper.VIEW_EPICS_TAB_NOTES_NEW;
   }

   /*
    * Tab epic - save note
    */
   @ApiOperation("Mapping to respond ajax requests for epic notes .")
   @PostMapping(value = ConstantsHelper.MAPPING_EPICS_NOTES_SAVE)
   public String saveNote(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICNOTE) @Valid EpicNoteView epicNoteView) {

      notesService.save(epicNoteView.translate());

      Epic epic = epicNoteView.getEpic();
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC_ID, epic.getId());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, new EpicView(epic));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEMNOTE, typesOfWorkItemNoteService.getByTypeOfWorkItem(epic.getType()));
      return ConstantsHelper.VIEW_EPICS_TAB_NOTES;

   }

   /*
    * Tab epic - tags
    */
   @ApiOperation("Mapping to respond ajax requests for epic information.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_EPICS_READ + "') and @SecurityService.isMember('Epic', #epicId)")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_TAGS_AJAX + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String epicTagsTab(Model model, @PathVariable int epicId) {
      EpicView epicView = new EpicView(epicService.getOne(epicId));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAGS, epicView.getTags());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epicView);

      return ConstantsHelper.VIEW_EPICS_TAB_TAGS;
   }

   /*
    * History epic -
    */
   @ApiOperation("Mapping to respond ajax requests for epic information.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_EPICS_READ + "') and @SecurityService.isMember('Epic', #epicId)")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_HISTORY_AJAX + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String epicHistoryTab(Model model, @PathVariable int epicId) {
      EpicView epicView = new EpicView(epicService.getOne(epicId));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICHISTORY, epicHistoryService.getEpicHistorybyEpic(epicId));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epicView);

      return ConstantsHelper.VIEW_EPICS_TAB_HISTORY;
   }

   @ApiOperation("Mapping to assign epic to a team.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CHANGE_TEAMS + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_TEAM_UPDATE)
   public String updateTeam(Model model, @PathVariable int epicId, @PathVariable int team) {

      Epic epic = epicService.getOne(epicId);
      epicHistoryService.save(new EpicHistory(epic));
      epic.setTeam(teamsService.getOne(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, new EpicView(epicService.save(epic)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(ResourcesHelper.getResource(), epic.getTeam()));

      return ConstantsHelper.VIEW_EPICS_TAB_INFO;
   }

   @ApiOperation("Mapping to add a new userstory related to epic.")
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_STORY_CREATE + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String addUserStory(Model model, @PathVariable int epicId) {

      UserStoryView userstoryView = new UserStoryView();
      Epic epic = epicService.getOne(epicId);
      userstoryView.setEpic(epic);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY, userstoryView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, epic.getTeam());

      return ConstantsHelper.VIEW_EPICS_NEW_USERSTORY;
   }

   @ApiOperation("Mapping to save a userstory related to epic.")
   @PostMapping(value = ConstantsHelper.MAPPING_EPICS_STORY_SAVE)
   public String savestory(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORY) @Valid UserStoryView userStoryView) {

      userstoriesHelper.save(userStoryView);
      Epic epic = userStoryView.getEpic();
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, new EpicView(epic));

      return ConstantsHelper.VIEW_EPICS_TAB_USERSTORIES;
   }

   @PostMapping(value = ConstantsHelper.MAPPING_EPICS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC) @Valid EpicView epicView) {
      Epic epic = epicService.save(epicView.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_EPICS_UPDATE + ConstantsHelper.MAPPING_SLASH + epic.getId();
   }

   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_TAG_ADD + ConstantsHelper.MAPPING_PARAMETER_EPIC)
   public String add(Model model, @PathVariable Integer epicId) {
      EpicView epicView = new EpicView(epicService.getOne(epicId));
      List<SubArea> subAreas = this.subAreasService.listSubareasByResource(ResourcesHelper.getResource());

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epicView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TAGS, tagsService.getAll(subAreas));

      return ConstantsHelper.VIEW_EPICS_MODAL_TAG;
   }

   @ApiOperation("Mapping to add a new tag related to epic.")
   @PostMapping(value = ConstantsHelper.MAPPING_EPICS_TAG_SAVE)
   public String saveTagPost(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC) @Valid EpicView epicView) {
      Epic epic = epicService.getOne(epicView.getId());
      epic.setTags(epicView.getTags());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, new EpicView(epicService.save(epic)));

      return ConstantsHelper.VIEW_EPICS_TAB_TAGS;
   }

   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_INFO_FIELD + ConstantsHelper.MAPPING_PARAMETER_EPIC + ConstantsHelper.MAPPING_PARAMETER_FIELD + ConstantsHelper.MAPPING_PARAMETER_FIELD_HEADER)
   public String updateField(Model model, @PathVariable int epicId, @PathVariable String field, @PathVariable String fieldheader) {
      EpicView epic = new EpicView(epicService.getOne(epicId));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epic);

      return "common/fragments/fieldedit :: text(label = '" + fieldheader + "', field = '" + field + "', required = false, placeholder = '',columnsize = 'sixteen wide'"
            + ", form= 'epicedit-form', object=" + ConstantsHelper.VIEW_ATTRIBUTE_EPIC + ")";
   }

   @PostMapping(value = ConstantsHelper.MAPPING_EPICS_INFO_FIELD_UPDATE + ConstantsHelper.MAPPING_PARAMETER_FIELD)
   public String saveField(Model model, @PathVariable String field, @Valid EpicView epicview) {
      Epic epic = epicService.getOne(epicview.getId());
      EpicView epicviewOld = new EpicView(epicService.getOne(epicview.getId()));
      epicHistoryService.save(new EpicHistory(epic));
      if ("name".equals(field)) {
         epicviewOld.setName(epicview.getName());
      } else {
         epicviewOld.setDescription(epicview.getDescription());
      }
      epicService.save(epicviewOld.translate());

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPIC, epicviewOld);

      return ConstantsHelper.VIEW_EPICS_TAB_INFOUPDATE + field;
   }

   /**
    * Open filter
    */
   @GetMapping(value = ConstantsHelper.MAPPING_EPICS_FILTER)
   public String getFilter(Model model) {
      EpicFilterView filter = epicsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_EPICS);
      
      List<WorkItemStatus> statuses = itemStatusService.getByTypeId(TypeOfWorkItem.EPIC);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUSES, statuses);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      
      return ConstantsHelper.VIEW_EPICS_FILTER;
   }

   /**
    * Save filter
    */
   @PostMapping(value = ConstantsHelper.MAPPING_EPICS_FILTER)
   public String submitFilter(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER) @Valid EpicFilterView filter) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EPICS, epicsHelper.getByFilter(filter));

      cookiesHelper.save(ConstantsHelper.COOKIE_EPICS, ResourcesHelper.getResource(), filter);

      return ConstantsHelper.VIEW_EPICS_READ + " :: list-epics";
   }

}