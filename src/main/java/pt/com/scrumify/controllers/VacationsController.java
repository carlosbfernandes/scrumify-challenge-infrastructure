package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.database.services.VacationService;
import pt.com.scrumify.entities.VacationView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TimesheetsHelper;
import pt.com.scrumify.helpers.VacationsHelper;
import pt.com.scrumify.helpers.YearsHelper;

@Controller
public class VacationsController {   
   @Autowired
   public VacationService vacationsService;
   @Autowired
   public TimesheetService timesheetsService;
   @Autowired
   private DatesHelper datesHelper; 
   @Autowired
   private TimesheetsHelper timesheetsHelper;
   @Autowired
   private VacationsHelper vacationsHelper;
   @Autowired
   public YearsHelper yearsHelper;   
      
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_VACATIONS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_VACATIONS)
   public String list(Model model){
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, vacationsService.listByResource(ResourcesHelper.getResource()));

      return ConstantsHelper.VIEW_VACATIONS_READ;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_VACATIONS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_VACATIONS_CREATE)
   public String create(Model model) {
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATION, new VacationView());
     String formattedDate = timesheetsHelper.getLastDateofTimeSheetApprovedorReviewed();

     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATE_LASTDAYAPPROVED, formattedDate);
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LIMIT_DATE, yearsHelper.getMaxActiveYear());
     
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.getMinDateFormatted());
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.getMaxDateFormatted());
     
     return ConstantsHelper.VIEW_VACATIONS_NEW;
   }
   
   @PostMapping(value = ConstantsHelper.MAPPING_VACATIONS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATION) @Valid VacationView vacationView) {
      List<Vacation> vacations = vacationsService.listDays(vacationView.translate());
      vacationsHelper.add(vacations);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, vacationsService.listByResource(ResourcesHelper.getResource()));
      return ConstantsHelper.VIEW_VACATIONS_READ + " :: vacations-list";
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_VACATIONS_UPDATE + "') and @SecurityService.canDelete('Vacation', #id)")
   @GetMapping(value = ConstantsHelper.MAPPING_VACATIONS_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String delete(Model model, @PathVariable int id) {
      vacationsHelper.remove(id);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, vacationsService.listByResource(ResourcesHelper.getResource()));
      return ConstantsHelper.VIEW_VACATIONS_READ + " :: vacations-list";
   }
}   