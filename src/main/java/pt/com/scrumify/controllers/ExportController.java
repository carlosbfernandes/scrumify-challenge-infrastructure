package pt.com.scrumify.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.services.ExportService;


@Controller
public class ExportController {
   
   @Autowired
   private ExportService exportService;
   
   @GetMapping(value = ConstantsHelper.MAPPING_EXPORT)
   public ResponseEntity<InputStreamResource> export(HttpServletResponse response) throws IOException {
      
      ByteArrayInputStream in = exportService.exportTurnaround(new ReportView());
      
      HttpHeaders headers = new HttpHeaders();
      headers.add("Content-Disposition", "attachment; filename=TurnAround.xlsx");
      
      return ResponseEntity
            .ok()
            .headers(headers)
            .body(new InputStreamResource(in));
   }
}   