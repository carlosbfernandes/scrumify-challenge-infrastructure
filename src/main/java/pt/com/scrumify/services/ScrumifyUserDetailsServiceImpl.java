package pt.com.scrumify.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.helpers.DatesHelper;

@Service
public class ScrumifyUserDetailsServiceImpl implements UserDetailsService {
   private final Logger logger = LoggerFactory.getLogger(this.getClass());

   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private SecurityService securityService;
   
	@Override
	public UserDetails loadUserByUsername(String username) {
	   /*
	    * Check if resource exists, throw exception if not
	    */
		Resource resource = this.resourcesService.getByUsernameAndActive(username, true);
		if (resource == null) {
	      logger.info("Authentication failed : Resource `{}` not found.", username);
	      
		   throw new UsernameNotFoundException(username);
		}
		else
		   logger.info("Authentication succeeded : Resource `{}`.", username);
      
      /*
       * Update last login date
       */
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      if (authentication != null && authentication.isAuthenticated()) {
         resource.setLastLogin(DatesHelper.now());
         this.resourcesService.save(resource);
      }

		return securityService.getSecurityUser(resource);
	}
}
