package pt.com.scrumify.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TimesheetExport;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.database.services.EpicService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.TimesheetExportService;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.entities.ResourceReport;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.reports.BacklogExportService;
import pt.com.scrumify.reports.WorkItemsByEpicAndUserStoryExportService;

@Service
public class ExportService {
   @Autowired
   private BacklogExportService backlogExportService;
   @Autowired
   private DatesHelper datesHelper;  
   @Autowired
   private WorkItemsByEpicAndUserStoryExportService workItemsByEpicAndUserStoryExportService;
   @Autowired
   private AbsenceService absenceService;
   @Autowired
   private EpicService epicsService;
   @Autowired
   private MessageSource messageSource;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private TimesheetExportService timesheetExportService;
   
   public ByteArrayInputStream exportTurnaround(ReportView reportView) throws IOException {
      String[] columns = {"Código Pessoal", "Mês", "Quinzena", "Código Projecto", "Código Tarefa", "Código Área", "Horas Afectas"};
      try (XSSFWorkbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {

         List<Resource> resources = resourcesService.getMembersOfSameTeam(ResourcesHelper.getResource());
         XSSFSheet sheet = workbook.createSheet(ConstantsHelper.TURNAROUND);
         
         createHeaderRow(sheet, columns);

         List<String> team = new ArrayList<>();
         for (Resource resource : resources) {
            team.add(resource.getCode());
         }
         
         List<TimesheetExport> reportData = new ArrayList<>();
         if (reportView.getFortnight() != null) {
            reportData = this.timesheetExportService.findByResourcesYearMonthAndFortnight(team, reportView.getYear().getId(), reportView.getMonth(), reportView.getFortnight());
         }
         else {
            reportData = this.timesheetExportService.findByResourcesYearAndMonth(team, reportView.getYear().getId(), reportView.getMonth());
         }
         
         for (int i = 0; i < reportData.size(); i++) {
            Row row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(reportData.get(i).getPk().getResource());
            row.createCell(1).setCellValue(StringUtils.leftPad(reportData.get(i).getPk().getMonth().toString(), 2, '0'));
            row.createCell(2).setCellValue(reportData.get(i).getPk().getFortnight());
            row.createCell(3).setCellValue(reportData.get(i).getPk().getProject());
            row.createCell(4).setCellValue(reportData.get(i).getPk().getTask());
            row.createCell(5).setCellValue(reportData.get(i).getPk().getTow());
            row.createCell(6).setCellValue(reportData.get(i).getHours());
         }
         
         workbook.write(out);
         
         return new ByteArrayInputStream(out.toByteArray());
      }
   }

   @SuppressWarnings({"rawtypes", "unchecked"})
   public ByteArrayInputStream exportReportByResource(Map<Resource, List<ResourceReport>> report) throws IOException {
      List<String> columns = ConstantsHelper.MONTHS;
      try (XSSFWorkbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {

         XSSFSheet sheet = workbook.createSheet(ConstantsHelper.TURNAROUND);
         Row headerRow = sheet.createRow(0);
         Row subheaderRow = sheet.createRow(1);

         for (int col = 1; col < (columns.size() * 3) + 1; col++) {
            Cell cell = headerRow.createCell(col);
            Cell subcell = subheaderRow.createCell(col);

            if ((col - 1) % 3 == 0) {
               sheet.addMergedRegion(new CellRangeAddress(0, 0, col, col + 2));
               cell.setCellValue(col == 0 ? columns.get(col) : columns.get(col / 3));
               subcell.setCellValue(messageSource.getMessage(ConstantsHelper.COMMON_HOURSSPENT_INITIAL, null, LocaleContextHolder.getLocale()));
            } else if ((col - 2) % 3 == 0) {
               subcell.setCellValue(messageSource.getMessage(ConstantsHelper.COMMON_ABSENCE_INITIAL, null, LocaleContextHolder.getLocale()));
            } else {
               subcell.setCellValue(messageSource.getMessage(ConstantsHelper.COMMON_HOURSLEFT_INITIAL, null, LocaleContextHolder.getLocale()));
            }
         }
         int rowId = 2;
         Iterator it = report.entrySet().iterator();
         while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Row row = sheet.createRow(rowId++);
            Resource resource = (Resource) pair.getKey();
            row.createCell(0).setCellValue(resource.getName());
            List<ResourceReport> reports = (List<ResourceReport>) pair.getValue();
            int i = 0;
            for (ResourceReport resourceReport : reports) {
               row.createCell(1 + (i * 3)).setCellValue(resourceReport.getHoursSpent());
               row.createCell((2 + i) + (i * 2)).setCellValue(resourceReport.getAbsence());
               row.createCell(3 + (i * 3)).setCellValue(resourceReport.getHours());
               i++;
            }
         }
         for (int col = 0; col < (columns.size() * 3) + 1; col++) {
            sheet.autoSizeColumn(col);
         }
         workbook.write(out);
         return new ByteArrayInputStream(out.toByteArray());

      }
   }

   public Integer writeTeamContractHours(List<ResourceReport> reports, int rowId, XSSFSheet sheet) {
      List<Contract> distinctContracts = reports.stream().map(ResourceReport::getContract).distinct().collect(Collectors.toList());
      int rowIdf = rowId;
      for (int j = 0; j < distinctContracts.size(); j++) {
         Row row = sheet.createRow(rowIdf++);
         row.createCell(0).setCellValue("      " + distinctContracts.get(j).getName());
         final Integer contract = j;
         for (int i = 1; i <= ConstantsHelper.MONTHS.size() + 1; i++) {
            if (i != 13) {
               final Integer month = i;
               Integer hours = reports.stream().filter(t -> month == t.getMonth()).filter(t -> distinctContracts.get(contract) == t.getContract())
                     .collect(Collectors.summingInt(ResourceReport::getHoursSpent));
               row.createCell(i).setCellValue(hours);
            } else {
               row.createCell(i).setCellValue(reports.stream().filter(t -> distinctContracts.get(contract) == t.getContract()).mapToInt(ResourceReport::getHoursSpent).sum());
            }
         }
      }
      return rowIdf;

   }

   @SuppressWarnings({"rawtypes", "unchecked"})
   public ByteArrayInputStream exportReportByWorkitem(Map<Contract, List<ResourceReport>> report) throws IOException {
      List<String> columns = ConstantsHelper.MONTHS;

      try (XSSFWorkbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
         XSSFSheet sheet = workbook.createSheet(ConstantsHelper.TURNAROUND);
         Row headerRow = sheet.createRow(0);
         for (int col = 1; col < columns.size() + 1; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(columns.get(col - 1));
         }
         Cell cell = headerRow.createCell(columns.size() + 1);
         cell.setCellValue("Total");

         int rowId = 1;
         Iterator it = report.entrySet().iterator();
         while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Row row = sheet.createRow(rowId++);
            Contract contract = (Contract) pair.getKey();
            row.createCell(0).setCellValue(contract.getName());
            List<ResourceReport> reports = (List<ResourceReport>) pair.getValue();
            for (ResourceReport resourceReport : reports) {
               row.createCell(resourceReport.getMonth()).setCellValue(resourceReport.getHoursSpent());
               if (row.getCell(13) != null) {
                  row.getCell(13).setCellValue(row.getCell(13).getNumericCellValue() + resourceReport.getHoursSpent());
               } else {
                  row.createCell(13).setCellValue(resourceReport.getHoursSpent());
               }
            }
         }
         for (int col = 0; col < columns.size() + 1; col++) {
            sheet.autoSizeColumn(col);
         }

         workbook.write(out);
         return new ByteArrayInputStream(out.toByteArray());
      }
   }
   public ByteArrayInputStream exportReportByEpic(List<Epic> epics) throws IOException {
      try (XSSFWorkbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
         XSSFSheet sheet = workbook.createSheet(ConstantsHelper.TURNAROUND);
         String[] columns = {"UserStory Number", "Story Points", "Total Estimative", "Total Hours Spent"};
         createHeaderRow(sheet, columns);
         
         int rowId = 1;
         for (Epic epic : epics) {
            Row row = sheet.createRow(rowId++);
            row.createCell(0).setCellValue(epic.getName());
            row.createCell(1).setCellValue(epic.getUserStories().size());
            Long storyPoints = epic.getUserStories().stream().mapToLong(UserStory::getEstimate).sum();
            row.createCell(2).setCellValue(epicsService.closestValue(ConstantsHelper.STORYPOINTS, storyPoints == 0 ? 0 : (int) Math.ceil((double) storyPoints.intValue() / epic.getUserStories().size())));
            long estimate = epic.getUserStories().stream().flatMap(t -> t.getWorkItems().stream()).mapToLong(WorkItem::getEstimate).sum();
            long timespent = epic.getUserStories().stream().flatMap(t -> t.getWorkItems().stream()).flatMap(t -> t.getWorkLogs().stream()).mapToLong(WorkItemWorkLog::getTimeSpent)
                  .sum();
            row.createCell(3).setCellValue(estimate);
            row.createCell(4).setCellValue(timespent);   
         }

         for (int col = 0; col < 5; col++) {
            sheet.autoSizeColumn(col);
         }
         workbook.write(out);
         return new ByteArrayInputStream(out.toByteArray());
      }
   }
   public ByteArrayInputStream exportReportByAbsences(ReportView reportView) throws IOException {
      try (
            XSSFWorkbook workbook = new XSSFWorkbook();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
          ) {
         
         XSSFSheet sheet = workbook.createSheet(ConstantsHelper.ABSENCE);
         String[] columns = {"Name", "Date", "Reason", "Hours"};
         createHeaderRow(sheet, columns);
         
         List<Resource> resources = new ArrayList<>();
         if(reportView.getResource()==null) {
            resources = resourcesService.getByTeam(reportView.getTeam());
         } else {
            resources.add(reportView.getResource());
         }
         
         List<Absence> absences = absenceService.getAllBetween(
               reportView.getStartDay(), 
               DatesHelper.addDays(reportView.getEndDay(), 1),
               resources);
         
         int rowId = 1;
         for(Absence absence : absences) {
            Row row = sheet.createRow(rowId++);
            row.createCell(0).setCellValue(absence.getResource().getName());
            row.createCell(1).setCellValue(datesHelper.formatDate(absence.getDay().getDate()));
            row.createCell(2).setCellValue(absence.getType().getName());
            row.createCell(3).setCellValue(absence.getHours());
         }
         
         workbook.write(out);
         return new ByteArrayInputStream(out.toByteArray());
      }
      
   }
   
   public ByteArrayInputStream exportContracts(ReportView view) {
      return workItemsByEpicAndUserStoryExportService.export(view);
   }
   
   public ByteArrayInputStream exportBacklog(ReportView view) {
      return backlogExportService.export(view);
   }
   
   private void createHeaderRow(XSSFSheet sheet, String[] columns) {
      Row headerRow = sheet.createRow(0);
      int count = 0;
      for(String column : columns) {
         Cell cell = headerRow.createCell(count);
         cell.setCellValue(column);
         count++;
      }
   }
}