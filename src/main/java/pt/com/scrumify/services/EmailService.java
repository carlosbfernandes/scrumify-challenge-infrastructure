package pt.com.scrumify.services;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class EmailService {

   private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

   @Autowired
   private JavaMailSender javaMailSender;
   @Autowired
   private MessageSource messageSource;
   @Autowired
   private SpringTemplateEngine templateEngine;

   @Value(ConstantsHelper.PROPERTIES_SPRING_MAIL_ACTIVE)
   private boolean active;

   @Value(ConstantsHelper.PROPERTIES_SPRING_MAIL_SENDER)
   private String sender;

   @Value(ConstantsHelper.PROPERTIES_APPLICATION_PROTOCOL)
   private String protocol;

   @Value(ConstantsHelper.PROPERTIES_APPLICATION_DOMAIN)
   private String domain;

   @Value(ConstantsHelper.PROPERTIES_APPLICATION_PORT)
   private String port;

   @Value(ConstantsHelper.PROPERTIES_APPLICATION_PATH)
   private String path;

   protected String getApplicationUrl() {
      return protocol + "://" + domain + (!port.equals("") ? ":" + port : "") + path;
   }

   protected String getWorkItemUrl(WorkItem workitem) {
      return getApplicationUrl() + ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + workitem.getId();
   }

   protected String getWorkItemNotesUrl(WorkItem workitem) {
      return getApplicationUrl() + ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + workitem.getId() + ConstantsHelper.MAPPING_WORKITEMS_NOTES;
   }

   /**
    * Method that sends and email based on certain variables
    * 
    * @param to
    * @bcc bcc
    * @param subject
    * @param body
    * @param variables
    * @throws MessagingException
    */
   public void send(String to, String[] bcc, String subject, String body, Map<String, Object> variables, HttpServletRequest request, HttpServletResponse response)
         throws MessagingException {
      /*
       * Check configuration, if mails are off this method won't send any messages
       */
      if (!this.active || (bcc == null && to == null))
         return;

      MimeMessage message = javaMailSender.createMimeMessage();
      MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
      WebContext ctx = new WebContext(request, response, request.getServletContext());
      ctx.setVariables(variables);
      ctx.setLocale(LocaleContextHolder.getLocale());

      String html = templateEngine.process(body, ctx);

      if (to != null)
         helper.setTo(to);

      if (bcc != null)
         helper.setBcc(bcc);

      helper.setFrom(sender);
      helper.setSubject(subject);
      helper.setText(html, true);

      javaMailSender.send(message);
   }

   public void sendApprovalEmail(Resource resource, String code, HttpServletRequest request, HttpServletResponse response) {
      String to = resource.getEmail();
      String subject = messageSource.getMessage(ConstantsHelper.MAIL_RESOURCE_APPROVAL_SUBJECT, null, LocaleContextHolder.getLocale());

      Map<String, Object> variables = new HashMap<>();
      variables.put(ConstantsHelper.VIEW_ATTRIBUTE_APPROVER, ResourcesHelper.getResource());
      variables.put(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, resource);
      variables.put(ConstantsHelper.VIEW_ATTRIBUTE_CODE, code);
      variables.put(ConstantsHelper.VIEW_ATTRIBUTE_LINK, getApplicationUrl());

      try {
         send(to, null, subject, ConstantsHelper.MAIL_TEMPLATE_RESOURCE_ACCOUNT_APPROVAL, variables, request, response);

         logger.info("#email #notification #accountapproval sent to {}.", to);
      } catch (MessagingException ex) {
         logger.error(ConstantsHelper.ERROR_EMAIL_LOG_MESSAGE, ex.getMessage());
      }
   }

   public void sendWorkItemsNotificationNote(TypeOfNote typeOfNote, WorkItem wi, WorkItemNote note, List<Resource> members, HttpServletRequest request, HttpServletResponse response) {
      for (Resource member : members) {
         try {
            String subject = messageSource.getMessage(ConstantsHelper.MAIL_WORKITEMS_NOTIFICATION_NOTE_SUBJECT, new Object[]{ typeOfNote.getName(), wi.getNumber(), wi.getName() }, LocaleContextHolder.getLocale());

            Map<String, Object> variables = new HashMap<>();
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, member);
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMNOTE, note);
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_TYPEOFNOTE, typeOfNote);
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_LINK, getWorkItemNotesUrl(wi));
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_NOTES, wi.getNotes());
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_HISTORY, wi.getHistory().size() + 1);

            send(member.getEmail(), null, subject, ConstantsHelper.MAIL_TEMPLATE_WORKITEMS_NOTIFICATION_NOTE, variables, request, response);

            logger.info("#email #notification #workitemnote sent to {}.", member.getEmail());
         } catch (MessagingException ex) {
            logger.error(ConstantsHelper.ERROR_EMAIL_LOG_MESSAGE, ex.getMessage());
         }
      }
   }

   public void sendWorkItemsNotificationStatus(WorkItem wi, List<Resource> members, HttpServletRequest request, HttpServletResponse response) {
      for (Resource member : members) {
         try {
            String subject = messageSource.getMessage(ConstantsHelper.MAIL_WORKITEMS_NOTIFICATION_STATUS_SUBJECT, new Object[]{ wi.getNumber(), wi.getName(), wi.getStatus().getName() }, LocaleContextHolder.getLocale());

            Map<String, Object> variables = new HashMap<>();
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, member);
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_LINK, getWorkItemUrl(wi));
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_NOTES, wi.getNotes());
            variables.put(ConstantsHelper.VIEW_ATTRIBUTE_HISTORY, wi.getHistory().size() + 1);

            send(member.getEmail(), null, subject, ConstantsHelper.MAIL_TEMPLATE_WORKITEMS_NOTIFICATION_STATUS, variables, request, response);

            logger.info("#email #notification #workitemstatus sent to {}.", member.getEmail());
         } catch (MessagingException ex) {
            logger.error(ConstantsHelper.ERROR_EMAIL_LOG_MESSAGE , ex.getMessage());
         }
      }
   }
}
