package pt.com.scrumify.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.Profile;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Role;
import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.database.services.ApplicationService;
import pt.com.scrumify.database.services.EpicService;
import pt.com.scrumify.database.services.MenuService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.SprintService;
import pt.com.scrumify.database.services.TeamMemberService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.VacationService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.database.services.WorkItemWorkLogService;
import pt.com.scrumify.entities.ScrumifyUser;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service("SecurityService")
public class SecurityService {
   private final Logger logger = LoggerFactory.getLogger(this.getClass());

   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private AbsenceService absencesService;
   @Autowired
   private ApplicationService appService;
   @Autowired
   private EpicService epicsService;
   @Autowired
   private MenuService menusService;
   @Autowired
   private ResourceService resourceService;
   @Autowired
   private SprintService sprintService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TeamMemberService teamMembersService;
   @Autowired
   private VacationService vacationsService;
   @Autowired
   private WorkItemService workItemsService;
   @Autowired
   private WorkItemWorkLogService worklogsService;

   public ScrumifyUser getSecurityUser(Resource resource) {
      List<GrantedAuthority> authorities = new ArrayList<>(0);

      /*
       * Get resource roles and inject them to security context and update last login date
       */
      String authorityName;

      Profile profile;
      if (resource.getProfile() != null) {
         profile = resource.getProfile();

         for (Role role : profile.getRoles()) {
            authorityName = ConstantsHelper.ROLE_PREFIX + role.getName().toUpperCase();

            authorities.add(new SimpleGrantedAuthority(authorityName));
            logger.info("Authorization : Added role `{}` to user `{}`.", authorityName, resource.getUsername());
         }
         
         if (resource.getProfile().isImpersonable()) {
            authorities.add(new SimpleGrantedAuthority(ConstantsHelper.ROLE_IMPERSONABLE));
            logger.info("Authorization : Added role `{}` to user `{}`.", ConstantsHelper.ROLE_IMPERSONABLE, resource.getUsername());
         }
      }

      /*
       * User account status
       */
      boolean enabled = resource.isActive();
      boolean accountNonExpired = true;
      boolean credentialsNonExpired = true;
      boolean accountNonLocked = true;

      /*
       * Create scrumify security user
       */
      ScrumifyUser user = new ScrumifyUser(resource.getUsername(), resource.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
      
      /*
       * Set resource on security context
       */
      user.setResource(resource);
      
      /*
       * Set resource menus on security context
       */
      user.setMenu(this.menusService.getByResource(resource));
      
      /*
       * Set resource teams on security context
       */
      List<Team> teams = teamsService.getByMember(resource);
      user.setTeams(teams);
      
      /*
       * Set resource active team on security context
       */
      Integer activeTeam = cookiesHelper.getInteger(ConstantsHelper.COOKIE_CONTEXT_TEAM, resource); 
      if (activeTeam != null) {
         user.setTeam(this.teamsService.getOne(activeTeam));
      }
      else {
         Team team = teams != null && !teams.isEmpty() ? teams.get(0) : null;
         
         user.setTeam(team);
      }
      
      /*
       * Set resource impersonable users on security context
       */
      if (resource.getProfile().isImpersonable()) {
         user.setResources(resourceService.getByTeams(teamsService.getByMember(resource)));
      }

      return user;
   }

   public void setSecurityContext(Resource resource) {
      ScrumifyUser user = this.getSecurityUser(resource);

      Authentication authentication = new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());
      SecurityContextHolder.getContext().setAuthentication(authentication);
   }

   public boolean isMember(String objectType, int objectId) {
      boolean result = false;
      Resource resource = ResourcesHelper.getResource();

      switch (objectType) {
         case ConstantsHelper.APPLICATION :
            result = this.teamsService.isMemberOfApplication(resource, this.appService.getOne(objectId).getSubArea().getId());
            break;
         case ConstantsHelper.CONTRACT :
            result = this.teamsService.isMemberOfContract(resource, objectId);
            break;
         case ConstantsHelper.TEAM :
            result = this.teamMembersService.isMemberOfTeam(resource, objectId);
            break;
         case ConstantsHelper.WORKITEM :
            result = (this.teamsService.isMemberOfWorkItem(resource, objectId) || workItemsService.isCreatedByMe(resource, objectId)
                  || workItemsService.isAssignedToMe(resource, objectId));
            break;
         case ConstantsHelper.EPIC :
            result = !epicsService.getAllByResource(resource).isEmpty();
            break;
         default :
            result = false;
            break;
      }

      if (logger.isInfoEnabled() && result) {
         logger.info(String.format("RESOURCE `%s` ALLOWED TO ACCESS OBJECT %s `%s`!", resource.getUsername(), objectType.toUpperCase(), objectId));
      } else {
         logger.info(String.format("RESOURCE `%s` NOT ALLOWED TO ACCESS OBJECT %s `%s`!", resource.getUsername(), objectType.toUpperCase(), objectId));
      }

      return result;
   }

   public boolean isReviewer(Optional<Integer> idResource) {
      Resource resource = ResourcesHelper.getResource();

      if (idResource.isPresent()) {
         List<Team> teams = teamsService.getByMember(resourceService.getOne(idResource.get()));

         for (Team team : teams) {
            for (TeamMember member : team.getMembers())
               if (member.isReviewer() && member.getResource().getId().equals(ResourcesHelper.getResource().getId()))
                  return true;
         }
      }

      if (logger.isInfoEnabled()) {
         logger.info(String.format("RESOURCE `%s` IS NOT REVIEWER!", resource.getUsername()));
      }

      return false;
   }

   public boolean isApprover(Optional<Integer> idResource) {
      Resource resource = ResourcesHelper.getResource();

      if (idResource.isPresent()) {
         List<Team> teams = teamsService.getByMemberId(idResource.get());

         for (Team team : teams) {
            for (TeamMember member : team.getMembers())
               if (member.isApprover() && member.getResource().getId().equals(resource.getId())) {
                  return true;
               }
         }

         if (logger.isInfoEnabled()) {
            logger.info(String.format("RESOURCE `%s` IS NOT APPROVER!", resource.getUsername()));
         }
      }

      return false;
   }

   public boolean isApproverOrReviewer(Optional<Integer> idResource) {
      Resource resource = ResourcesHelper.getResource();
      if (idResource.isPresent()) {
         List<Team> teams = teamsService.getByMemberId(idResource.get());
         for (Team team : teams) {
            for (TeamMember member : team.getMembers())
               if ((member.isApprover() || member.isReviewer()) && member.getResource().getId().equals(resource.getId()))
                  return true;
         }
         if (logger.isInfoEnabled()) {
            logger.info(String.format("RESOURCE `%s` IS NEITHER REVIEWER NOR APPROVER!", resource.getUsername()));
         }
      } else if (teamMembersService.isApproverOrReviewer(resource)) {
         return true;
      }

      return false;
   }

   /**
    * Method used to check if the object belongs to a previously approved timesheet. If it does, user should not be able
    * to go through with the action.
    * 
    * @param String
    *           objectType - (absences, worklogs, vacations)
    * @param int
    *           objectId
    * @return
    */
   public boolean canDelete(String objectType, int objectId) {
      boolean result = false;
      Resource resource = ResourcesHelper.getResource();

      switch (objectType) {
         case ConstantsHelper.ABSENCE :
            Absence absence = absencesService.getByIdAndResource(objectId, resource);
            result = absence != null && (absence.getTimesheet() == null || absence.getTimesheet() != null && !absence.getTimesheet().getSubmitted());
            break;
         case ConstantsHelper.WORKLOG :
            WorkItemWorkLog workLog = worklogsService.getByIdAndResource(objectId, resource);
            result = workLog != null && (workLog.getTimesheet() == null || workLog.getTimesheet() != null && !workLog.getTimesheet().getSubmitted());
            break;
         case ConstantsHelper.VACATION :
            Vacation vacation = vacationsService.getByIdAndResource(objectId, resource);
            result = vacation != null && (vacation.getTimesheet() == null || vacation.getTimesheet() != null && !vacation.getTimesheet().getSubmitted());
            break;
         default :
            result = false;
            break;
      }

      if (logger.isInfoEnabled() && result) {
         logger.info(String.format("RESOURCE `%s` ALLOWED TO ACCESS OBJECT %s `%s`!", resource.getUsername(), objectType.toUpperCase(), objectId));
      } else {
         logger.info(String.format("RESOURCE `%s` NOT ALLOWED TO ACCESS OBJECT %s `%s`!", resource.getUsername(), objectType.toUpperCase(), objectId));
      }

      return result;
   }
   
   public boolean isMemberOfSprint(Integer sprintId) {
      Resource resource = ResourcesHelper.getResource();
      Sprint sprint = sprintService.getOne(sprintId);
      
      Resource temp = sprint.getSprintMembers().stream()
                                          .filter(member -> resource.getId().equals(member.getId()))
                                          .findAny()
                                          .orElse(null);
      
      return temp != null;
   }
}