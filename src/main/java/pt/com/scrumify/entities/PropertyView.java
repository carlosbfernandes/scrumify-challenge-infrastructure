package pt.com.scrumify.entities;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Property;
import pt.com.scrumify.database.entities.Resource;

public class PropertyView {   
   @Getter
   @Setter
   private int id = 0;   
   
   @Getter
   @Setter
   private String name;
   
   @Getter
   @Setter
   private String group;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   public PropertyView() {
      super();
   }
   
   public PropertyView(Property property) {
      super();
      this.setId(property.getId());
      this.setGroup(property.getGroup());
      this.setName(property.getName());
      this.setCreated(property.getCreated());
      this.setCreatedBy(property.getCreatedBy());
   }

   public Property translate() {
      Property property = new Property();
      property.setId(this.getId());
      property.setGroup(this.getGroup());
      property.setName(this.getName());
      property.setCreated(this.getCreated());
      property.setCreatedBy(this.getCreatedBy());
      
      return property;      
   }
}