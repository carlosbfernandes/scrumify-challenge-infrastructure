package pt.com.scrumify.entities;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Holiday;
import pt.com.scrumify.database.entities.Resource;

public class HolidayView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private String name;
   
   @Getter
   @Setter
   private Date day;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;   
   
   public HolidayView() {
      super();
   }
   
   public HolidayView(Holiday holiday) {
      super();
      
      this.setId(holiday.getId());
      this.setDay(holiday.getDay().getDate());
      this.setName(holiday.getName());
      this.setCreated(holiday.getCreated());
      this.setCreatedBy(holiday.getCreatedBy());
   }

   public Holiday translate() {
      Holiday holiday = new Holiday();
      holiday.setId(this.getId());
      holiday.getDay().setDate(this.getDay());
      holiday.setName(this.getName());
      holiday.setCreated(this.getCreated());
      holiday.setCreatedBy(this.getCreatedBy());
      
      return holiday;      
   }
}