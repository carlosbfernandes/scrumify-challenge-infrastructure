package pt.com.scrumify.entities;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Wiki;
import pt.com.scrumify.helpers.ConstantsHelper;

public class WikiView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;   
   
   @Getter
   @Setter
   private Application application;
   
   @Getter
   @Setter
   private Date created;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private boolean active;
      
   public WikiView() {
      super();
   }
   
   public WikiView(Wiki wiki) {
      super();
      
      this.setId(wiki.getId());
      this.setName(wiki.getName());
      this.setActive(wiki.getActive());
      this.setApplication(wiki.getApplication());
      this.setCreated(wiki.getCreated());
      this.setCreatedBy(wiki.getCreatedBy());
   }

   public Wiki translate() {
      Wiki wiki = new Wiki();
      
      wiki.setId(id);
      wiki.setName(name);     
      wiki.setActive(active);
      wiki.setApplication(application);
      wiki.setCreated(created);
      wiki.setCreatedBy(createdBy);

      return wiki;
   }   
}