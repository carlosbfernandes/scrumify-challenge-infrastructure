package pt.com.scrumify.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Activity;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TypeOfWork;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.database.entities.WorkItem;


public class WorkItemWorkLogView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter 
   private WorkItem workItem;
   
   @Getter
   @Setter
   private Date day;
   
   @Getter
   @Setter
   private TypeOfWork typeOfWork;
   
   @Getter
   @Setter
   private Integer timeSpent;
   
   @Getter
   @Setter
   private Integer etc;
   
   @Getter
   @Setter
   private String description; 
   
   @Getter
   @Setter
   private Activity activity;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   public WorkItemWorkLogView() {
      super();
   }
   
   public WorkItemWorkLogView(WorkItemWorkLog workItemWorkLog) {
      super();
      
      this.setId(workItemWorkLog.getId());
      this.setWorkItem(workItemWorkLog.getWorkItem());
      this.setDay(workItemWorkLog.getDay().getDate());
      this.setDescription(workItemWorkLog.getDescription());
      this.setTypeOfWork(workItemWorkLog.getTypeOfWork());
      this.setTimeSpent(workItemWorkLog.getTimeSpent());
      this.setEtc(workItemWorkLog.getEtc());
      this.setActivity(workItemWorkLog.getActivity());
      this.setCreated(workItemWorkLog.getCreated());
      this.setCreatedBy(workItemWorkLog.getCreatedBy());

   }

   public WorkItemWorkLog translate() {
      DateFormat formatter = new SimpleDateFormat(ConstantsHelper.DEFAULT_TIMES_FORMAT);
      WorkItemWorkLog workItemWorkLog = new WorkItemWorkLog();
      
      workItemWorkLog.setId(this.getId());
      workItemWorkLog.setWorkItem(this.getWorkItem());
      workItemWorkLog.getDay().setId(formatter.format(this.getDay()));
      workItemWorkLog.setDescription(this.getDescription());
      workItemWorkLog.setTypeOfWork(this.getTypeOfWork());
      workItemWorkLog.setTimeSpent(this.getTimeSpent());
      workItemWorkLog.setEtc(this.getEtc());
      workItemWorkLog.setActivity(this.getActivity());
      workItemWorkLog.setCreated(this.getCreated());
      workItemWorkLog.setCreatedBy(this.getCreatedBy());
      
      return workItemWorkLog;
   }   
}