package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

public class Message {
   
   @Getter
   @Setter
   private String type;
   
   @Getter
   @Setter
   private String icon;
   
   @Getter
   @Setter
   private String text;
   
   public Message() {
      super();
   }
   
   public Message(String type, String icon, String text) {
      super();
      
      this.type = type;
      this.type = icon;
      this.text = text;
   }
}   