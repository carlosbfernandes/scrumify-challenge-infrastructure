package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Profile;
import pt.com.scrumify.database.entities.Role;
import pt.com.scrumify.helpers.ConstantsHelper;

public class ProfileView {   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;
   
   @Getter
   @Setter
   private boolean impersonable = false;
   
   @Getter
   @Setter
   private List<Role> roles = new ArrayList<>(0);

   public ProfileView() {
      super();
   }
   
   public ProfileView(Profile profile) {
      super();
      
      if (profile != null) {
         this.setId(profile.getId());
         this.setName(profile.getName());
         this.setImpersonable(profile.isImpersonable());
         this.setRoles(profile.getRoles());
      }
   }

   public Profile translate() {
      Profile profile = new Profile();
      
      profile.setId(this.getId());
      profile.setName(this.getName());
      profile.setImpersonable(this.isImpersonable());
      profile.setRoles(this.getRoles());
      
      return profile;
   }
}