package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class UserStoryFilterView {
   @Getter
   @Setter
   private List<ItemStatusView> statuses;

   @Getter
   @Setter
   String searchQuery;

   public UserStoryFilterView() {
      super();
   }

   public UserStoryFilterView(List<ItemStatusView> statuses) {
      super();

      this.statuses = statuses;
   }

   public void addStatus(ItemStatusView status) {
      if (this.statuses == null)
         this.statuses = new ArrayList<>();

      statuses.add(status);
   }
}