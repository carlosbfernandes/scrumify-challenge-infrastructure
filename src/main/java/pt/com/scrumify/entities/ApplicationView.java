package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.ApplicationProperty;
import pt.com.scrumify.database.entities.ApplicationStatus;
import pt.com.scrumify.database.entities.TypeOfApplication;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.helpers.ConstantsHelper;

public class ApplicationView {
   @Getter
   @Setter
   private Integer id = 0;

   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=80, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String name;

   @Getter
   @Setter
   @Length(max=150, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String description;

   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=50, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String taxonomy;

   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=150, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String fqdn;

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private SubArea subArea = new SubArea();

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Resource responsible = new Resource();

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Resource functionalContact = new Resource();

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Resource technicalContact = new Resource();

   @Getter
   @Setter
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private boolean active = false;

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private ApplicationStatus status = new ApplicationStatus();

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private TypeOfApplication type = new TypeOfApplication();

   @Getter
   @Setter
   private List<ApplicationProperty> properties = new ArrayList<>(0);

   @Getter
   @Setter
   private Date created;

   @Getter
   @Setter
   private Resource createdBy;
   
   public ApplicationView() {
      super();
   }

   public ApplicationView(Application application) {
      super();
      
      if (application != null) {
         this.setId(application.getId());
         this.setActive(application.getActive());
         this.setName(application.getName());
         this.setStatus(application.getStatus());
         this.setType(application.getType());
         this.setDescription(application.getDescription());
         this.setFqdn(application.getFqdn());
         this.setFunctionalContact(application.getFunctionalContact());
         this.setResponsible(application.getResponsible());
         this.setSubArea(application.getSubArea());
         this.setTaxonomy(application.getTaxonomy());
         this.setTechnicalContact(application.getTechnicalContact());
         this.setProperties(application.getProperties());
         this.setCreated(application.getCreated());
         this.setCreatedBy(application.getCreatedBy());
      }
   }

   public Application translate() {
      Application application = new Application();
      
      application.setId(this.getId());
      application.setActive(this.isActive());
      application.setName(this.getName());
      application.setStatus(this.getStatus());
      application.setType(this.getType());      
      application.setDescription(this.getDescription());
      application.setFqdn(this.getFqdn());
      application.setFunctionalContact(this.getFunctionalContact());
      application.setResponsible(this.getResponsible());
      application.setSubArea(this.getSubArea());
      application.setTaxonomy(this.getTaxonomy());
      application.setTechnicalContact(this.getTechnicalContact());
      application.setProperties(this.getProperties());
      application.setCreated(this.getCreated());
      application.setCreatedBy(this.getCreatedBy());
      
      return application;
   }
}