package pt.com.scrumify.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.EpicNote;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Tag;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.UserStory;

public class EpicView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private String name;
   
   @Getter
   @Setter
   private String number;
   
   @Getter
   @Setter
   @Column(name = "description", columnDefinition="CLOB", nullable = false)
   private String description;
   
   @Getter
   @Setter
   private WorkItemStatus status;
   
   @Getter
   @Setter
   private TypeOfWorkItem type;
   
   @Getter
   @Setter
   private List<UserStory> userStories;
   
   @Getter
   @Setter
   private List<EpicNote> notes;
   
   @Getter
   @Setter
   private Team team;
   
   @Getter
   @Setter
   private List<Tag> tags;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   @Getter
   @Setter
   private Resource lastUpdateBy;
   
   @Getter
   @Setter
   private Date lastUpdate;
   
   @Getter
   @Setter
   private Integer hours;
   
   @Getter
   @Setter
   private Integer storyPoints
   ;
   public EpicView() {
      super();
   }
   
   public EpicView(Epic epic) {
      super();
      
      this.setId(epic.getId());
      this.setDescription(epic.getDescription());
      this.setName(epic.getName());
      this.setNumber(epic.getNumber());
      this.setStatus(epic.getStatus());
      this.setType(epic.getType());
      this.setTags(epic.getTags());
      this.setTeam(epic.getTeam());
      this.setHours(epic.getHours());
      this.setStoryPoints(epic.getStoryPoints());
      this.setUserStories(epic.getUserStories());
      this.setNotes(epic.getNotes());
      this.setCreated(epic.getCreated());
      this.setCreatedBy(epic.getCreatedBy());
      this.setLastUpdate(epic.getLastUpdate());
      this.setLastUpdateBy(epic.getLastUpdateBy());
      
   }

   public Epic translate() {
      Epic epic = new Epic();
      
      epic.setId(id);
      epic.setName(name);
      epic.setDescription(description);
      epic.setStatus(status);
      epic.setType(type);
      epic.setTags(tags);
      epic.setTeam(team);
      epic.setHours(hours);
      epic.setStoryPoints(storyPoints);
      epic.setUserStories(userStories);
      epic.setNotes(notes);
      epic.setNumber(number);
      epic.setCreated(created);
      epic.setCreatedBy(createdBy);
      epic.setLastUpdate(lastUpdate);
      epic.setLastUpdateBy(lastUpdateBy);
      
      return epic;
      
   }

}