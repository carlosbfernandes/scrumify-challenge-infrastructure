package pt.com.scrumify.entities;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.database.entities.Workflow;
import pt.com.scrumify.database.entities.WorkflowTransition;

@NoArgsConstructor
public class WorkflowView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private String name;
   
   @Getter
   @Setter
   private Boolean active = true;
   
   @Getter
   @Setter
   private List<WorkflowTransition> transitions;

   public WorkflowView(Workflow workflow) {
      super();
      
      this.setId(workflow.getId());
      this.setName(workflow.getName());
      this.setActive(workflow.getActive());
      this.setTransitions(workflow.getTransitions());
   }
   
   public Workflow translate() {
      Workflow workflow = new Workflow();
      
      workflow.setId(id);
      workflow.setName(name);
      workflow.setActive(active);
      workflow.setTransitions(transitions);
      
      return workflow;
   }
}