package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class WorkItemFilterView {
   @Getter
   @Setter
   private List<TypeOfWorkItemView> types;
   
   @Getter
   @Setter
   private String searchQuery;
   
   @Getter
   @Setter
   private int option;
   
   @Getter
   @Setter
   private Integer team;
   
   @Getter
   @Setter
   private List<Integer> applications;
   
   public WorkItemFilterView() {
      super();
   }
   
   public WorkItemFilterView(List<TypeOfWorkItemView> types) {
      super();
      
      this.types = types; 
   }
   
   public void addType(TypeOfWorkItemView type) {
      if(this.types == null)
         this.types = new ArrayList<>();
      
      types.add(type);
   }
}