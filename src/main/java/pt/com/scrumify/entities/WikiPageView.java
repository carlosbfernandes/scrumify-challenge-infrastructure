package pt.com.scrumify.entities;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Wiki;
import pt.com.scrumify.database.entities.WikiPage;
import pt.com.scrumify.helpers.ConstantsHelper;

public class WikiPageView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String title;
   
   @Getter
   @Setter
   private Wiki wiki;
   
   @Getter
   @Setter
   private String content;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;   
   
   @Getter
   @Setter
   private boolean active;

   public WikiPageView() {
      super();
   }

   public WikiPageView(WikiPage wikiPage) {
      super();

      this.setId(wikiPage.getId());     
      this.setActive(wikiPage.getActive());
      this.setTitle(wikiPage.getTitle());
      this.setContent(wikiPage.getContent());
      this.setWiki(wikiPage.getWiki());
      this.setCreated(wikiPage.getCreated());
      this.setCreatedBy(wikiPage.getCreatedBy());

   }

   public WikiPage translate() {
      WikiPage wikiPage = new WikiPage();

      wikiPage.setId(id);      
      wikiPage.setActive(active);
      wikiPage.setContent(content);
      wikiPage.setTitle(title);
      wikiPage.setWiki(wiki);
      wikiPage.setCreated(created);
      wikiPage.setCreatedBy(createdBy);

      return wikiPage;

   }
}