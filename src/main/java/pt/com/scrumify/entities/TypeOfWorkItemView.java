package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.TypeOfWorkItem;

public class TypeOfWorkItemView {
   
   @Getter
   @Setter
   private Integer id;
   
   @Getter
   @Setter
   private String name;
   
   @Getter
   @Setter
   private List<ItemStatusView> statuses;
   
   public TypeOfWorkItemView() {
      super();
   }

   public TypeOfWorkItemView(TypeOfWorkItem type) {
      super();
      
      this.id = type.getId();
      this.name = type.getName();
      
      List<ItemStatusView> temp = new ArrayList<>();
      for(WorkItemStatus status : type.getStatuses()) {
         temp.add(new ItemStatusView(status));
      }
      
      this.statuses = temp;
   }
}