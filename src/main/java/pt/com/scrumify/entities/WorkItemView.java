package pt.com.scrumify.entities;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Activity;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Priority;
import pt.com.scrumify.database.entities.Release;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Tag;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemAttachment;
import pt.com.scrumify.database.entities.WorkItemHistory;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.helpers.ConstantsHelper;

public class WorkItemView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @Length(min=11, max=11, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String number;
   
   @Getter
   @Setter
   @Length(min=0, max=80, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String name;
   
   @Getter
   @Setter
   private String description;
   
   @Getter
   @Setter
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private Integer estimate = 0;
   
   @Getter
   @Setter
   private Integer hours = 0;
   
   @Getter
   @Setter
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private Integer etc = 0;
   
   @Getter
   @Setter
   private TypeOfWorkItem type;
   
   @Getter
   @Setter
   private WorkItemStatus status;
   
   @Getter
   @Setter
   private Contract contract;
   
   @Getter
   @Setter
   private Team team;
   
   @Getter
   @Setter
   private UserStory userStory;
   
   @Getter
   @Setter
   private Application application;
   
   @Getter
   @Setter
   private Release release;
   
   @Getter
   @Setter
   private Priority priority;
   
   @Getter
   @Setter
   private Resource assignedTo;
   
   @Getter
   @Setter
   private Date assigned;
   
   @Getter
   @Setter
   private Resource closedBy;
   
   @Getter
   @Setter
   private Date closed;
   
   @Getter
   @Setter
   private List<Tag> tags;
   
   @Getter
   @Setter
   private List<WorkItemAttachment> attachments;

   @Getter
   @Setter
   private List<WorkItemHistory> history;
   
   @Getter
   @Setter
   private List<WorkItemNote> notes;

   @Getter
   @Setter
   private List<WorkItemWorkLog> workLogs;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   @Getter
   @Setter
   private Resource lastUpdateBy;
   
   @Getter
   @Setter
   private Date lastUpdate;
   
   public WorkItemView() {
      super();
   }
   
   public WorkItemView(WorkItem workItem) {
      super();
      
      this.setId(workItem.getId());
      this.setNumber(workItem.getNumber());
      this.setName(workItem.getName());
      this.setDescription(workItem.getDescription());
      this.setEstimate(workItem.getEstimate());
      this.setHours(workItem.getHours());
      this.setEtc(workItem.getEtc());
      this.setType(workItem.getType());
      this.setContract(workItem.getContract());
      this.setStatus(workItem.getStatus());
      this.setUserStory(workItem.getUserStory());
      this.setTeam(workItem.getTeam());
      this.setApplication(workItem.getApplication());
      this.setRelease(workItem.getRelease());
      this.setPriority(workItem.getPriority());
      this.setAssigned(workItem.getAssigned());
      this.setAssignedTo(workItem.getAssignedTo());
      this.setClosed(workItem.getClosed());
      this.setClosedBy(workItem.getClosedBy());
      this.setNotes(workItem.getNotes());
      this.setAttachments(workItem.getAttachments());
      this.setHistory(workItem.getHistory());
      this.setWorkLogs(workItem.getWorkLogs());
      this.setTags(workItem.getTags());
      this.setCreated(workItem.getCreated());
      this.setCreatedBy(workItem.getCreatedBy());
      this.setLastUpdate(workItem.getLastUpdate());
      this.setLastUpdateBy(workItem.getLastUpdateBy());
   }

   public WorkItem translate() {
      WorkItem workItem = new WorkItem();
      
      workItem.setId(this.getId());
      workItem.setNumber(this.getNumber());
      workItem.setName(this.getName());
      workItem.setDescription(this.getDescription());
      workItem.setEstimate(this.getEstimate());
      workItem.setHours(this.getHours());
      workItem.setEtc(this.getEtc());
      workItem.setType(this.getType());
      workItem.setContract(this.getContract());
      workItem.setStatus(this.getStatus());
      workItem.setUserStory(this.getUserStory());
      workItem.setTeam(this.getTeam());
      workItem.setApplication(this.application);
      workItem.setRelease(this.release);
      workItem.setPriority(this.priority);      
      workItem.setAssignedTo(this.getAssignedTo());
      workItem.setAssigned(this.getAssigned());
      workItem.setClosedBy(this.getClosedBy());
      workItem.setClosed(this.getClosed());
      workItem.setNotes(this.notes);
      workItem.setAttachments(this.attachments);
      workItem.setHistory(this.history);
      workItem.setWorkLogs(this.workLogs);      
      workItem.setTags(this.tags);
      workItem.setCreated(this.created);
      workItem.setCreatedBy(this.createdBy);
      workItem.setLastUpdate(this.lastUpdate);
      workItem.setLastUpdateBy(this.lastUpdateBy);
      
      return workItem;
   }   
}