package pt.com.scrumify.entities;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Vacation;

public class VacationView {
   
   @Getter
   @Setter
   private Integer id;
   
   @Getter
   @Setter
   private Date startDay;

   @Getter
   @Setter
   private Date endDay;
   
   public VacationView() {
      super();
   }
   
   public VacationView(Vacation vacation) {      
      this.setId(vacation.getId());
      this.setEndDay(vacation.getEndDay());
      this.setStartDay(vacation.getStartDay());
   }

   public Vacation translate() {
      Vacation vacation = new Vacation();
      vacation.setId(this.getId());
      vacation.setEndDay(this.getEndDay());
      vacation.setStartDay(this.getStartDay());
      
      return vacation;      
   }
}