package pt.com.scrumify.entities;

import org.hibernate.validator.constraints.Range;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.helpers.ConstantsHelper;

public class ChangePIN {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private String username;
   
   @Getter
   @Setter
   @Range(min = 1000, max = 9998, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_RANGE + "}")
   private Integer password = 0;
   
   @Getter
   @Setter
   @Range(min = 1000, max = 9998, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_RANGE + "}")
   private Integer passwordConfirmation = 0;

   public ChangePIN(Resource resource) {
      super();

      if (resource != null) {
         this.setId(resource.getId());
         this.setUsername(resource.getUsername());
      }
   }

   public Resource translate() {
      Resource resource = new Resource();
      
      resource.setId(this.getId());
      resource.setPassword(this.getPassword().toString());

      return resource;
   }
}