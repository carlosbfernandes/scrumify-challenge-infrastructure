package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Menu;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;

public class ScrumifyUser extends User {
   private static final long serialVersionUID = -5731620704369901391L;
   
   @Getter
   @Setter
   private Resource resource = new Resource();
   
   @Getter
   @Setter
   private List<Resource> resources = new ArrayList<>(0);
   
   @Getter
   @Setter
   private List<Menu> menu = new ArrayList<>(0);
   
   @Getter
   @Setter
   private Team team;
   
   @Getter
   @Setter
   private List<Team> teams = new ArrayList<>(0);

   public ScrumifyUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
      super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
   }

   @Override
   public boolean equals(Object rhs) {
      return super.equals(rhs);
   }

   @Override
   public int hashCode() {
      return super.hashCode();
   }
}