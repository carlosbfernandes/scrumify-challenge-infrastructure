package pt.com.scrumify.entities;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Tag;
import pt.com.scrumify.helpers.ConstantsHelper;

public class TagView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;
   
   @Getter
   @Setter
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private SubArea subArea;
   
   @Getter
   @Setter
   private Date created;
   
   @Getter
   @Setter
   private Resource createdBy;
      
   public TagView() {
      super();
   }
   
   public TagView(Tag tag) {
      super();
      
      this.setId(tag.getId());
      this.setName(tag.getName());
      this.setSubArea(tag.getSubArea());
      this.setCreated(tag.getCreated());
      this.setCreatedBy(tag.getCreatedBy());
   }

   public Tag translate() {
      Tag tag = new Tag();
      
      tag.setId(id);
      tag.setName(name);
      tag.setSubArea(subArea);
      tag.setCreated(created);
      tag.setCreatedBy(createdBy);
      
      return tag;
   }
}