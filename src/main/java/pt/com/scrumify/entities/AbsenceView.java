package pt.com.scrumify.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.TypeOfAbsence;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;

public class AbsenceView {
   
   @Getter
   @Setter
   private Integer id = 0;

   @Getter
   @Setter
   private Integer hours;

   @Getter
   @Setter
   private String justification;

   @Getter
   @Setter
   private TypeOfAbsence type;

   @Getter
   @Setter
   private Date day;

   @Getter
   @Setter
   private Resource createdBy;

   @Getter
   @Setter
   private Date created;

   
   public AbsenceView() {
      super();
   }
   
   public AbsenceView(Absence absence) {      
      this.setId(absence.getId());
      this.setType(absence.getType());
      this.setDay(absence.getDay().getDate());
      this.setHours(absence.getHours());
      this.setJustification(absence.getJustification());
      this.setCreated(absence.getCreated());
      this.setCreatedBy(absence.getCreatedBy());
      
   }

   public Absence translate() {
      DateFormat formatter = new SimpleDateFormat(ConstantsHelper.DEFAULT_TIMES_FORMAT);
      Time time = new Time(formatter.format(this.getDay()));
      time.setDate(this.getDay());
      
      Absence absence = new Absence();
      absence.setId(this.getId());
      absence.setType(this.getType());
      absence.setDay(time);
      absence.setHours(this.getHours());
      absence.setJustification(this.getJustification());
      absence.setCreated(this.getCreated());
      absence.setCreatedBy(this.getCreatedBy());
      
      return absence;      
   }
}