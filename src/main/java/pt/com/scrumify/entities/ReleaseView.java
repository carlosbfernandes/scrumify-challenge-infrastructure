package pt.com.scrumify.entities;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Release;
import pt.com.scrumify.database.entities.Resource;

@NoArgsConstructor
public class ReleaseView {
   @Getter
   @Setter
   private Integer id;

   @Getter
   @Setter
   private Application application = new Application();

   @Getter
   @Setter
   private Integer version;

   @Getter
   @Setter
   private Integer major;
   
   @Getter
   @Setter
   private Integer minor;
   
   @Getter
   @Setter
   private Integer revision;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   public ReleaseView(Release release) {
      super();

      this.setId(release.getId());
      this.setApplication(release.getApplication());
      this.setVersion(release.getVersion());
      this.setMajor(release.getMajor());
      this.setMinor(release.getMinor());
      this.setRevision(release.getRevision());
      this.setCreated(release.getCreated());
      this.setCreatedBy(release.getCreatedBy());
   }

   public Release translate() {
      Release release = new Release();

      release.setId(this.id);
      release.setApplication(this.application);
      release.setVersion(this.version);
      release.setMajor(this.major);
      release.setMinor(this.minor);
      release.setRevision(this.revision);
      release.setCreated(this.created);
      release.setCreatedBy(this.createdBy);
      
      return release;  
   }
}