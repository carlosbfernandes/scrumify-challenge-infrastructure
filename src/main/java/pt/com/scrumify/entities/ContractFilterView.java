package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ContractFilterView {
   @Getter
   @Setter
   private List<ContractStatusView> contractStatuses;
   
   public ContractFilterView() {
      super();
   }
   
   public List<Integer> getContractStatusesIds() {
      List<Integer> ids = new ArrayList<>();
      for(ContractStatusView status : this.contractStatuses) {
         ids.add(status.getId());
      }
      return ids;
   }
   
   public void addContractStatus(ContractStatusView status) {
      if(this.contractStatuses == null)
         this.contractStatuses = new ArrayList<>();
      
      contractStatuses.add(status);
   }
   
}