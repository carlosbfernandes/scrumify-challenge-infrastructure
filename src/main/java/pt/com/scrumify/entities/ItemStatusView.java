package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.WorkItemStatus;

public class ItemStatusView {
   
   @Getter
   @Setter
   private Integer id;
   
   @Getter
   @Setter
   private String name;
   
   public ItemStatusView() {
      super();
   }
   
   public ItemStatusView(WorkItemStatus status) {
      this.id = status.getId();
      this.name = status.getName();
   }
   
   public static List<Integer> getStatusesIds(List<ItemStatusView> statuses) {
      List<Integer> ids = new ArrayList<>();
      for(ItemStatusView status : statuses) {
         if(status.getId()!=null)
            ids.add(status.getId());
      }
      return ids;
   }
   
}