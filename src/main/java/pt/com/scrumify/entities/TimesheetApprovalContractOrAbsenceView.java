package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

public class TimesheetApprovalContractOrAbsenceView {
   @Getter
   @Setter
   private String code;
   
   @Getter
   @Setter
   private Integer contract;
   
   @Getter
   @Setter
   private String description;

   public TimesheetApprovalContractOrAbsenceView() {
      super();
   }
   
   public TimesheetApprovalContractOrAbsenceView(String code, Integer contract, String description) {
      super();
      
      this.code = code;
      this.contract = contract;
      this.description = description;
   }
}