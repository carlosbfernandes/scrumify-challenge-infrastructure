package pt.com.scrumify.entities;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetApproval;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.TimesheetReview;
import pt.com.scrumify.database.entities.TimesheetStatus;

public class TimesheetView {
   @Getter
   @Setter
   private Integer id = 0;

   @Getter
   @Setter
   private Resource resource;

   @Getter
   @Setter
   private Time startingDay;

   @Getter
   @Setter
   private Time endingDay;

   @Getter
   @Setter
   private Boolean submitted;

   @Getter
   @Setter
   private Boolean reviewed;

   @Getter
   @Setter
   private Resource reviewedBy;

   @Getter
   @Setter
   private Date reviewDate;

   @Getter
   @Setter
   private Boolean approved;

   @Getter
   @Setter
   private Resource approvedBy;

   @Getter
   @Setter
   private Date approvalDate;

   @Getter
   @Setter
   private String comment;

   @Getter
   @Setter
   private Resource commentedBy;

   @Getter
   @Setter
   private Integer month;

   @Getter
   @Setter
   private Integer year;
   
   @Getter
   @Setter
   private Integer occupation;
   
   @Getter
   @Setter
   private TimesheetStatus status;
   
   @Getter
   @Setter
   private List<TimesheetDay> days;

   @Getter
   @Setter
   private List<TimesheetReview> reviews;

   @Getter
   @Setter
   private List<TimesheetApproval> approvals;

   public TimesheetView() {
      super();
   }

   public TimesheetView(Timesheet timesheet) {
      super();

      if (timesheet != null) {
         this.setId(timesheet.getId());
         this.setResource(timesheet.getResource());
         this.setStartingDay(timesheet.getStartingDay());
         this.setEndingDay(timesheet.getEndingDay());
         this.setSubmitted(timesheet.getSubmitted());
         this.setReviewed(timesheet.getReviewed());
         this.setReviewedBy(timesheet.getReviewedBy());
         this.setReviewDate(timesheet.getReviewDate());
         this.setApproved(timesheet.getApproved());
         this.setApprovedBy(timesheet.getApprovedBy());
         this.setApprovalDate(timesheet.getApprovalDate());
         this.setStatus(timesheet.getStatus());
         this.setComment(timesheet.getComment());
         this.setCommentedBy(timesheet.getCommentedBy());
         
         this.setDays(timesheet.getDays());
         this.setReviews(timesheet.getReviews());
         this.setApprovals(timesheet.getApprovals());
      }
   }
}