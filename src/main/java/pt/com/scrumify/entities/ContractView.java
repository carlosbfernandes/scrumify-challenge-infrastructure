package pt.com.scrumify.entities;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.ContractProperty;
import pt.com.scrumify.database.entities.ContractStatus;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.helpers.ConstantsHelper;

public class ContractView {
   @Getter
   @Setter
   private Integer id = 0;

   @Getter
   @Setter
   private Year year;

   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;

   @Getter
   @Setter
   private String description;

   @Getter
   @Setter
   private Date startingDay;

   @Getter
   @Setter
   private Date endingDay;

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private Integer hours = 0;

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private Integer hours1stYear = 0;

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private Integer hours2ndYear = 0;

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private ContractStatus status = new ContractStatus();

   @Getter
   @Setter
   private Boolean technicalAssistence = false;

   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private SubArea subArea = new SubArea();

   @Getter
   @Setter
   private Date created;

   @Getter
   @Setter
   private Resource createdBy;

   @Getter
   @Setter
   private List<ContractProperty> properties;

   @Getter
   @Setter
   @Length(min = 5, max = 6, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String code;

   @Getter
   @Setter
   private Integer hoursSpent = 0;

   @Getter
   @Setter
   private Integer hoursSpentInterval = 0;

   public ContractView() {
      super();
   }

   public ContractView(Contract contract) {
      super();

      this.setId(contract.getId());
      this.setYear(contract.getYear());
      this.setName(contract.getName());
      this.setDescription(contract.getDescription());
      this.setStartingDay(contract.getStartingDay().getDate());
      if (contract.getEndingDay() == null)
         this.setEndingDay(null);
      else
         this.setEndingDay(contract.getEndingDay().getDate());
      this.setHours(contract.getHours());
      this.setHours1stYear(contract.getHours1stYear());
      this.setHours2ndYear(contract.getHours2ndYear());
      this.setStatus(contract.getStatus());
      this.setTechnicalAssistence(contract.getTechnicalAssistance());
      this.setSubArea(contract.getSubArea());
      this.setCreated(contract.getCreated());
      this.setCreatedBy(contract.getCreatedBy());
      this.setCode(contract.getCode());
      this.setProperties(contract.getProperties());

   }

   public Contract translate() {
      Contract contract = new Contract();

      contract.setId(this.getId());
      contract.setYear(this.getYear());
      contract.setName(this.getName());
      contract.setDescription(this.getDescription());
      contract.getStartingDay().setDate(this.getStartingDay());
      contract.getEndingDay().setDate(this.getEndingDay());
      contract.setHours(this.getHours());
      contract.setHours1stYear(this.getHours1stYear());
      contract.setHours2ndYear(this.getHours2ndYear());
      contract.setStatus(this.getStatus());
      contract.setTechnicalAssistance(this.getTechnicalAssistence());
      contract.setSubArea(this.getSubArea());
      contract.setCreated(this.getCreated());
      contract.setCreatedBy(this.getCreatedBy());
      contract.setCode(this.getCode());
      contract.setProperties(this.properties);

      return contract;
   }
}