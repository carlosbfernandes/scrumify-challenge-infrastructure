package pt.com.scrumify.entities;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.database.entities.WorkItem;


public class WorkItemNoteView {   

   @Getter
   @Setter
   private Integer id;
   
   @Getter
   @Setter
   private String content;
   
   @Getter
   @Setter
   private TypeOfNote typeOfNote;   
   
   @Getter
   @Setter
   private WorkItem workItem;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   public WorkItemNoteView() {
      super();  
   }
   
   public WorkItemNoteView(WorkItemNote workItemNote) {
      super();
      
      this.setId(workItemNote.getId());
      this.setContent(workItemNote.getContent());
      this.setTypeOfNote(workItemNote.getTypeOfNote());
      this.setWorkItem(workItemNote.getWorkItem());
      this.setCreatedBy(workItemNote.getCreatedBy());
      this.setCreated(workItemNote.getCreated());
     
   }

   public WorkItemNote translate() {
      WorkItemNote workItemNote = new WorkItemNote();
      
      workItemNote.setId(id);
      workItemNote.setContent(content);
      workItemNote.setTypeOfNote(typeOfNote);
      workItemNote.setWorkItem(workItem);
      workItemNote.setCreatedBy(createdBy);
      workItemNote.setCreated(created);
      
      return workItemNote;
   }
}