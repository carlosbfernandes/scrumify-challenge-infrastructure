$(document).ready(function() {
   $('.ui.checkbox.checkbox').checkbox();
   $('.ui.toggle.checkbox').checkbox();
   
   tab(false, '');
   
   formValidation();
});

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
         name : {
            identifier : 'name',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PROFILES_SAVE}}]];
         ajaxpost(url, $("#profiles-form").serialize(), "body", false);
      }
   });
}