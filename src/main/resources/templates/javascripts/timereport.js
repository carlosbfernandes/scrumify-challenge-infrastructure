$(document).ready(function() {
	initialize();
});

function initialize() {
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	$('.ui.mini').popup({
	       position: 'top center'
	});
	$('.ui.accordion')
	  .accordion()
	;
   
	$("button[type=submit][name='action'][value='reject']").click(function() {
		 $("#timesheet-form").form({
		      on : 'blur',
		      inline : true,
		      fields : {
		    	  comment : {
		            identifier : 'comment',
		            rules : [ {
		               type : 'empty',
		               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
		            } ]
		         }
		      },
		      onSuccess : function(event) {

		      }
		   });
	});
	   
};

function changeresourcetoreview(id){	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_REVIEW}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id;

	ajaxget(url,"div[id='report']", function() {
     	initialize(); 
    }, false);
};

function changeresourcetoapprove(id){	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_APPROVE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id;
		
	ajaxget(url,"div[id='report']", function() {
     	initialize(); 
    }, false);
};

