$(document).ready(function() {
  $('.ui.radio.checkbox').checkbox();
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_CREATE}}]];
   
  if (id != 0) {
    url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  } else {
    CKEDITOR.replace('description', {
      customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
	  toolbar : 'Basic',
	  uiColor : '#CDD1D3'
    });
  }
  
  ajaxtab(true, url, true, function(settings) {
    switch(settings.urlData.tab) {
      case "notes":
        getnotes();
        break;
      case "worklog":
        getworklog();
        break;
      case "tags":
        gettags();
        break;
      case "instructions":
        getinstructions();
        break;
      case "history":
        gethistory();
        break;
      case "attachments":
        getattachments();
        break;	
      case "workitem":
      default:
        getworkitem();
        break;
    }
  });
   
  initialize();
});

function initialize() {
  $('.ui.dropdown').dropdown();
  $('select.dropdown').dropdown();
  $('.ui.radio.checkbox').checkbox();
  $('.ui.mini').popup({
    position: 'top center'
  });
  $('.ui.progress').progress();
  $('.fitted.example').popup({
    on:'click',
    hoverable: true
  });
  
  formValidation();
}

function getworkitem() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	   
  ajaxget(url, "div[data-tab='workitem']", function() {
    initialize();

    $('.ui.mini').popup({
      position: 'top center'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
  }); 
};

function getnotes() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_NOTES_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
   ajaxget(url, "div[data-tab='notes']", function() {           
      initialize();   
   });
};

function getattachments() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_ATTACHMENTS} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
   ajaxget(url, "div[data-tab='attachments']", function() {
      initialize();
	   $('.message .close').on('click', function() {
		   $(this)
		      .closest('.message')
		      .transition('fade')
		      ;
		});
   });
};

function getworklog() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	   
	ajaxget(url, "div[data-tab='worklog']", function() {           
		initialize();   
    });
};

function gettags() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_TAGS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;

	ajaxget(url, "div[data-tab='tags']", function() {           
		initialize();   
	});
};

function getinstructions() {
    var id = $("input[type=hidden][name='id']").val();
    var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_INSTRUCTIONS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;

    ajaxget(url, "div[data-tab='instructions']", function() {           
        initialize();   
    });
}

function addtag(workitem) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_TAGS_CREATE}}]] + '/' + workitem;
  
  ajaxget(url, "div[id='modal']", function() {
    $('.ui.checkbox').checkbox();
    $('.ui.toggle.checkbox').checkbox();
    
    $("#tags-form").form({
      onSuccess : function(event) {
        event.preventDefault();
        
        var urlpost = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_TAGS_SAVE}}]];
        var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_UPDATE}}]] + '/' +  workitem + '/tags';
        ajaxpost(urlpost, $("#tags-form").serialize(), "#tags-list", true, function() {
          initialize();
        }, returnurl);
      }
    })
  }, true);
}

function gethistory() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_HISTORY_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
   ajaxget(url, "div[data-tab='history']", function() {           
      initialize();
  	$('.ui.mini').popup({
	       position: 'top center'
	});
      });
};

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_SAVE}}]];
   
   ajaxpost(url, $("#workitems-form").serialize(), "", false, function() {
	   initialize();
   });
};


function formValidation() {
   $("#workitems-form").form({
      on : 'blur',
      inline : true,
      fields : {
    	  name : {
            identifier : 'name',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         team : {
            identifier : 'team',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         }
      }
   });
};

function updateContract(id, contract) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_CONTRACT}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + contract;
	 
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
  });
};

function updatePriority(id, priority) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_PRIORITY}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + priority;
     
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
  });
};

function updateState(id, state) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_STATUS}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + state;

  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
  });
};

function updateApplication(id, app) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_APPLICATION}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + app;
	 
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
  });
};

function updateRelease(id, release) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_RELEASE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + release;
	 
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
  });
};

function updateTeam(id, team) {
var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_TEAM}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + team;

  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
  });
};

function updateAssignedTo(id, resource) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_RESOURCE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + resource;
	 
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
  });
};

function removeAssignedTo(id) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_REMOVE_RESOURCE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id ;
	 
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
  });
};

function updateStory(id, userstory) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_USERSTORY}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + userstory;
	 
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
  });
};

function removeattachment(id) {
  var workitemId = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_ATTACHMENTS_DELETE}}]] + '/' + id + '/' + workitemId;

  ajaxdelete(url, "div[id='attachments-list']", function() {
    initialize();
  });
};

function filepreview(id){
  $.ajax({
    url : [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_ATTACHMENTS_PREVIEW}}]]+ '/' + id +'/ext',
    success : function(data) {
      $("#modalPreview").html(data);
      $("#content").attr("src",[[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_ATTACHMENTS}}]]+ '/' + id +'/preview');
      $("#modalPreview").modal('show');
    }
  });  
};

function editCampo(campo, header) {
  var workitem = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_INFO_FIELD}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + workitem + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + campo  + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + header;
	
  ajaxget(url, "div[id='modal']", function() {
    formSubmissionedit(campo);
    
    if (campo == 'description'){
      CKEDITOR.replace('description', {
        customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
        toolbar : 'Basic',
        uiColor : '#CDD1D3'
      });
    }	
  }, true);
};

function updateEstimate(id) {
  var estimate = $("#estimate").val();	
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_ESTIMATE}}]] + '/' + id + '/' + estimate;
	
  ajaxget(url, "div[data-tab='workitem']", function() {
    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
    $('.fitted.example').popup({
      on:'click',
      hoverable: true
    });
  });
}

function formSubmissionedit(campo){
	$("#workItemedit-form").form({	
	      on : 'blur',
	      inline : true,
	      fields : {
	    	  name : {
	              identifier : 'name',
	              rules : [ {
	                 type : 'empty',
	                 prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	              } ]
	           }
	      },
	      onSuccess : function(event) {	      
	      event.preventDefault();
	      for (instance in CKEDITOR.instances) {
	    	    CKEDITOR.instances[instance].updateElement();
	    	} 
	      var id = $("input[type=hidden][name='id']").val();
	      var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	      
         	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_INFO_FIELD_UPDATE}}]]  + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + campo;     
             ajaxpost(url, $("#workItemedit-form").serialize(), "label[id='"+campo+"WK']", false, function() {
          	      initialize();
             }, returnurl);
          }
	   });
}

function search(event){
    if(event.key === "Enter"){
      event.preventDefault()
      submitFilter()
    }
}


function filter() {

  var url  = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_FILTER}}]]
      ajaxget(url, "div[id='modal']", function() {
        $('.ui.checkbox').checkbox();
        $('.ui.toggle.checkbox').checkbox();
        $('.ui.radio.checkbox').checkbox();
        $('.ui.dropdown').dropdown();
    	$('select.dropdown').dropdown();
        bindMasterAndChildCheckboxes();
        $('#filter-form').form({
            onSuccess : function(event) {
               event.preventDefault();
               
               ajaxpost(url, $("#filter-form").serialize(), "div[id='list-workitems']", true, function() {
                 initialize();
               });
             }
         });
        }, true);
  }