$(document).ready(function() {
   initialize();
});
        
function initialize() {
	validateDates()
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function formSubmission() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_COMPENSATIONDAYS_SAVE}}]];
   var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_COMPENSATIONDAYS}}]];
   ajaxpost(url, $("#compensationdays-form").serialize(), "", false, function() {
      initialize();
   }, returnurl);
}

function formValidation() {
   $('#compensationdays-form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  startDay : {
            identifier : 'startDay',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         endDay : {
             identifier : 'endDay',
             rules : [ {
                type : 'empty',
                prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
             } ]
          },
          name : {
              identifier : 'name',
              rules : [ {
                 type : 'empty',
                 prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
              } ]
           }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}

function createCompensationDay(){
	
	var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_COMPENSATIONDAYS_CREATE}}]] ;
	   
	ajaxget(url,  "div[id='modal']", function() {
        initialize();
        $( "#input-endDay" ).blur(function() {
        	validateDates();
        	});
          $( "#input-startDay" ).blur(function() {
        	  validateDates();
          	});
    }, true);
}

function editCompensationDay(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_COMPENSATIONDAYS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
        initialize();
    }, true);
}

function deleteCompensationDay(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_COMPENSATIONDAYS_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	
    $("#modal-confirmation")
	.modal({closable: false,
   		onApprove: function() {
   			ajaxget(url, "body", function() {
   	        initialize();
   	       }, false);
   		},
   		onDeny: function() {
   		}
	})
	.modal('show');  
}

function generate(id){
	
	var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_COMPENSATIONDAYS_GENERATE}}]] + '/' + id ;
	   
	ajaxget(url, "", function() {           
		initialize();   
	});
}

function validateDates(){
	var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	
	var date = $("input[type=hidden][id='limit']").val();
	calendar('day', 'date', json, '', date);	

	if(date != null){
		calendar('startDay', 'date', json,'',date);
	}else{
		calendar('startDay', 'date', json,'',$("#endDay").val());
	}
	if(date != null && date > $("#startDay").val()){		
		calendar('endDay', 'date', json, $("#startDay").val(), date );
	}else{
		calendar('endDay', 'date', json,date);
	}
}