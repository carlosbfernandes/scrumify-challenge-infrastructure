$(document).ready(function() {
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
   formValidation();
});

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  abbreviation : {
              identifier : 'abbreviation',
              rules : [ {
                 type : 'minLength[3]',
                 prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_LENGTH}(3)}]]
              } ]
           },
           name : {
               identifier : 'name',
               rules : [ {
                  type : 'empty',
                  prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               } ]
            },
            area : {
                identifier : 'area',
                rules : [ {
                   type : 'empty',
                   prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                } ]
             }
      },           	  
      onSuccess : function(event) {
         event.preventDefault();
         
         var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SUBAREAS_SAVE}}]];
         ajaxpost(url, $("#subareas-form").serialize(), "body", false);
      }
   });
}