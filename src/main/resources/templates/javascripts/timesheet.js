$(document).ready(function () {
  initialize();
});

function initialize() {
	var year = $("select[id='ddl-years']").val();
	var month = $("select[id='ddl-months']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month;
	var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	var limit = $("input[type=hidden][id='limit']").val();

  var date = $("input[type=hidden][id='dateapproved']").val();
  if (date != null) {
    calendar('day', 'date', json, date, limit);
    calendar('startDay', 'date', json, date, limit);
  } else {
    calendar('day', 'date', json, '', limit);
    calendar('startDay', 'date', json, '', $("#endDay").val());
  }
  if (date != null && date > $("#startDay").val()) {
    calendar('endDay', 'date', json, date, limit);
  } else {
    calendar('endDay', 'date', json, $("#startDay").val(), limit);
  }

  $('.ui.radio.checkbox').checkbox();
  $('.ui.toggle.checkbox').checkbox();
  $('select.dropdown').dropdown();
  $('.ui.mini').popup({
    position: 'top center'
  });

  ajaxtab(true, url, false, function (settings) {
      switch(settings.urlData.tab) {
	         case "summary":
	        	 getSummary();
	            break;
	         case "hours":
	        	 getHours();
	            break;
	         case "vacations":
	        	 getVacations();
	            break;
	         case "compensationdays":
	        	 getCompensation();
	            break;
	         case "absences":
	        	 getAbsences();
	            break;	
	         case "holidays":
	        	 getHolidays();
	        	 break;
	         case "comments":
	        	 getComments();
	        	 break;
	         default:	        	 
	            break;
	      }
	   });
  

  formValidationvacation();
  formValidationabsence();
  formValidationworklog();
}

function changeDate() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET}}]];
  var year = $("select[id='ddl-years']").val();
  var month = $("select[id='ddl-months']").val();
  var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month;
  
  ajaxpost(url, $("#timesheet-form").serialize(), "", false, function () {
    initialize();
  }, returnurl);
}

function getSummary() {
   var year = $("select[id='ddl-years']").val();
   var month = $("select[id='ddl-months']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_AJAX_SUMMARY}}]] + '/' + year + '/' + month;
   
   ajaxget(url, "div[data-tab='summary']", function() {           
      initialize();   
   });
};
	
function getHours() {
   var year = $("select[id='ddl-years']").val();
   var month = $("select[id='ddl-months']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_AJAX_HOURS}}]] + '/' + year + '/' + month;
   
   ajaxget(url, "div[data-tab='hours']", function() {           
      initialize();   
   });
};
		

function getVacations() {
   var year = $("select[id='ddl-years']").val();
   var month = $("select[id='ddl-months']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_AJAX_VACATIONS}}]] + '/' + year + '/' + month;
   
   ajaxget(url, "div[data-tab='vacations']", function() {           
      initialize();   
   });
};

function getCompensation() {
   var year = $("select[id='ddl-years']").val();
   var month = $("select[id='ddl-months']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_AJAX_COMPENSATION}}]] + '/' + year + '/' + month;
   
   ajaxget(url, "div[data-tab='compensationdays']", function() {           
      initialize();   
   });
};

function getAbsences() {
   var year = $("select[id='ddl-years']").val();
   var month = $("select[id='ddl-months']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_AJAX_ABSENCES}}]] + '/' + year + '/' + month;
   
   ajaxget(url, "div[data-tab='absences']", function() {           
      initialize();   
   });
};

function getHolidays() {
   var year = $("select[id='ddl-years']").val();
   var month = $("select[id='ddl-months']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_AJAX_HOLIDAYS}}]] + '/' + year + '/' + month;
   
   ajaxget(url, "div[data-tab='holidays']", function() {           
      initialize();   
   });
};
	
function getComments() {
   var year = $("select[id='ddl-years']").val();
   var month = $("select[id='ddl-months']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_AJAX_COMMENTS}}]] + '/' + year + '/' + month;
   
   ajaxget(url, "div[data-tab='comments']", function() {           
      initialize();   
   });
};


function createAbsence() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_CREATE}}]];
  
  ajaxget(url, "div[id='modal']", function () {
    $('select.dropdown').dropdown();
    var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);

    var minDate = $("input[type=hidden][id='minDate']").val();
    var maxDate = $("input[type=hidden][id='maxDate']").val();

    calendar('day', 'date', json, minDate, maxDate);
    
    formValidationabsence();

  }, true);
}

function editAbsence(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
        initialize();
    }, true);
}

function deleteAbsence(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_DELETE}}]] + '/' + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
    var year = $("select[id='ddl-years']").val();
    var month = $("select[id='ddl-months']").val();
	var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month + '/absences';
    $("#modal-confirmation").modal({
    	closable: false,
   		onApprove: function() {
   			ajaxget(url, "div[id='timesheet']", function() {
   				initialize();
   			}, false, returnurl);
   		}
	})
	.modal('show');  
}

function deleteVacation(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_VACATIONS_DELETE}}]] + '/' + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	
	var year = $("select[id='ddl-years']").val();
    var month = $("select[id='ddl-months']").val();
	var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month + '/vacations';
    $("#modal-confirmation").modal({
    	closable: false,
   		onApprove: function() {
   			ajaxget(url, "div[id='timesheet']", function() {
   				initialize();
   			}, false, returnurl);
   		}
	}).modal('show');  
}

function createVacation() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_VACATIONS_CREATE}}]];

  ajaxget(url, "div[id='modal']", function () {	  
  	var localizedtext = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);	
	var minDate = $("input[type=hidden][id='minDate']").val();
	var maxDate = $("input[type=hidden][id='maxDate']").val();
   
	calendar('startDay', 'date', localizedtext, minDate, maxDate, 'endDay');
	
	formValidationvacation();
	  
  }, true);
}

function editWorklog(id) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  $('.ui.dropdown').dropdown();
  $('select.dropdown').dropdown();

  ajaxget(url, "div[id='modal']", function () {
    initialize();
  }, true);
}

function deleteWorklog(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG_DELETE}}]] + '/' + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	var year = $("select[id='ddl-years']").val();
    var month = $("select[id='ddl-months']").val();
	var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month + '/hours';
	
    $("#modal-confirmation")
	.modal({closable: false,
   		onApprove: function() {
   			ajaxget(url, "div[id='worklog']", function() {
   	        initialize();
   	       }, false, returnurl);
   		},
   		onDeny: function() {
   		}
	})
	.modal('show');  
}

function formValidationvacation() {
  $("#vacations-form").form({
    on: 'blur',
    inline: true,
    fields: {
      startDay: {
        identifier: 'startDay',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      timesheet : {
          identifier : 'startDay',
          rules : [ {
	            type : 'timesheet',
	            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
	            value: "input[id='input-startDay']"			            	
      	}]
      }, 
      endDayTimesheet : {
          identifier : 'endDay',
          rules : [ {
	            type : 'timesheet',
	            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
	            value: "input[id='input-endDay']"			            	
      	}]
      } 
    },
    onSuccess: function (event) {
      event.preventDefault();

      formSubmission('vacations');
    }
  });
}

function formValidationabsence() {
  $("#absences-form").form({
    on: 'blur',
    inline: true,
    fields: {
      absenceType: {
        identifier: 'type.id',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      day: {
        identifier: 'day',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      hours: {
        identifier: 'hours',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      }, 
      timesheet : {
          identifier : 'day',
          rules : [ {
	            type : 'timesheet',
	            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
	            value: "input[id='input-day']"			            	
      	}]
      }
    },
    onSuccess: function (event) {
      event.preventDefault();

      formSubmission('absences');
    }
  });
}

function formValidationworklog() {
  $("#workitemworklog-form").form({
    on: 'blur',
    inline: true,
    fields: {
      timeSpent: {
        identifier: 'timeSpent',
        rules: [{
          type: 'integer[1..9]',
          prompt: [[#{validation.empty.range(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_RANGE}}, 1, 9)}]]
        }]
      },
      etc: {
        identifier: 'etc',
        rules: [{
          type: 'integer[0..99]',
          prompt: [[#{validation.empty.range(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_RANGE}}, 0, 99)}]]
        }]
      },
      typeOfWork: {
        identifier: 'typeOfWork.id',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      day: {
        identifier: 'day',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      }, 
      timesheet : {
          identifier : 'day',
          rules : [ {
	            type : 'timesheet',
	            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
	            value: "input[id='input-day']"			            	
      	}]
      }
    },
    onSuccess: function (event) {
      event.preventDefault();
      formSubmission('worklog');
    }
  });
}

function formSubmission(type) {
  if (type == 'absences') {
    var year = $("#day").val().substring(0, 4);
    var month = $("#day").val().substring(5, 7);
    var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_SAVE}}]];
    var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month + '/absences';
    ajaxpost(url, $("#absences-form").serialize(), "div[data-tab='absences']", true, function () {
    	initialize()
    }, returnurl);
    
  } else if (type == 'vacations') {
    var year = $("#input-startDay").val().substring(0, 4);
    var month = $("#input-startDay").val().substring(5, 7);
    var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_VACATIONS_SAVE}}]];
    var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month + '/vacations';
    ajaxpost(url, $("#vacations-form").serialize(), "div[data-tab='vacations']", true, function () {
    	initialize()
    }, returnurl);
    
  } else {
    var year = $("#day").val().substring(0, 4);
    var month = $("#day").val().substring(5, 7);
    var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG_SAVE}}]];
    var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + year + '/' + month + '/hours';
    ajaxpost(url, $("#workitemworklog-form").serialize(), "div[data-tab='hours']", true, function () {
    	initialize()
    }, returnurl);
  }
}

function timesubmit(timesheetId) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SUBMIT} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + timesheetId;
  
  ajaxget(url, "div[id='summary']", function () {
    initialize();
  }, true);
};

function preview(timesheetId){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_PREVIEW}}]] + '/' + timesheetId;
	ajaxget(url, "div[id='modal']", function () {
				
		$("#timesheet-form").form({
		    onSuccess: function (event) {
		      event.preventDefault();     
		      
		      var urlpost = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_SUBMIT}}]];
		      ajaxpost(urlpost, $("#timesheet-form").serialize(), "div[id='page']", false, function () {
		    });
		  }
		})
	} , true);
}

function revertSubmission(timesheetId){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEET_REVERT_SUBMISSION}}]] + '/' + timesheetId;
	ajaxget(url, "div[id='summary']", function () {
	    initialize();
    }, true);
}