$(document).ready(function() {	
	   
	   initializeProperty();
	});

function initializeProperty() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
  formValidationProperty();
}	

function formValidationProperty() {	
        $("#contractproperty-form").form({
            on: 'blur',
            fields: { 
            	value : {
                  identifier : 'value',
                  rules : [{
                     type : 'empty',
                     prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
               },
		        propertyGroup : {
		            identifier : 'pk.property.id',
		            rules : [{
		               type : 'empty',
		               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
		            }]
		         }
            },
            onSuccess : function(event) {
               event.preventDefault();             
               document.forms["contractproperty-form"].submit( function(data){
              	 $('body').html(data);
               });
            }
       });
};


function modaladdproperty(contractId) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACT_PROPERTY_CREATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + contractId;
    ajaxget(url, "div[id='modal']", function() {
    	initializeProperty();
    }, true);
}

function editProperty(idProperty, idContract) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACT_PROPERTY_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + idProperty
	+ [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + idContract;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
    	var from = jQuery("select[name='pk.property.id']");
    	from.attr('disabled', 'disabled');
        initialize();
    }, true);
}


function deleteProperty(idProperty, idContract) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACT_PROPERTY_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + idProperty
	+ [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + idContract;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	ajaxdelete(url, "div[data-tab='properties']", function() {
    	initialize();
     },false);
	 
}