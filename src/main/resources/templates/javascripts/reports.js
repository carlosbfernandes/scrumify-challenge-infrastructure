$(document).ready(function() {
  initialize();
});
	   
function initialize() {
	validateDates();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
	
   $("#area").change(function() {
	   var areaid = $("select[name='area']").val();
	   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_UPDATESEARCHAREA} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + areaid;
	   
	   ajaxget(url, "div[id='divteam']", function() {
			initialize();   
	   });
	 });
}

function changeExportTeam(){
   var teamid = $("select[name='team']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_UPDATESEARCHTEAM} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + teamid;
   
   ajaxget(url, "div[id='divresource']", function() {     
		initialize();   
   });
}

function changeExportContract(){
   var id = $("select[name='contract']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_UPDATESEARCHCONTRACT} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
   ajaxget(url, "div[id='divteam']", function() {     
		initialize();   
   });
}

function initializeDropZone() {
  Dropzone.autoDiscover = false;
    
  $("#dropzone").dropzone({
    acceptedFiles : '.xls, .xlsx, .xlsm',
    addedfile: function(file) {
    	$("div[id='dataTable'").empty();
    },
    autoProcessQueue : true,
    init : function () {
      var myDropzone = this;      
      this.on("uploadprogress", function (file, progress) {
        progress = parseFloat(progress).toFixed(0);
      })      
    },
    maxFiles : 50,
    maxFilesize : 10,
    parallelUploads : 3,
    paramName : "files",
    previewsContainer : ".dropzone-previews",
    uploadMultiple : true,
    url : [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_UPLOAD}}]],
    successmultiple : function(files, response) {    	
    	var tr;
    	if(response.length > 0){    		
    		tbl = $('<br/><table class="ui table blue "> ');
    		theader = $('<thead><tr><th>Name</th><th>Message</th><th>File Size</th><th>Time Elapsed</th><th>Inserted</th><th>Updated</th></tr></thead>');
			tbl.append(theader);
	    	for (var i = 0; i < response.length; i++) {
	            tr = $('<tr/>');
	            tr.append("<td>" + response[i].fileName + "</td>");
	            tr.append("<td>" + response[i].message + "</td>");
	            tr.append("<td>" + response[i].size + " kB</td>");
	            tr.append("<td>" + response[i].time.toFixed(2) + " s</td>");
	            tr.append("<td>" + response[i].entriesInserted + "</td>");
	            tr.append("<td>" + response[i].entriesUpdated + "</td>");
	            tbl.append(tr);
	            $("div[id='dataTable'").append(tbl);
	        }
    	}
    }
  });
}

function changeArea(){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_UPDATE_AREA}}]];
	
	ajaxpost(url, $("#report-form").serialize(), "form[id='report-form']", false, function(){
		initialize();
	}); 
}

function changeSubArea(){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_UPDATE_SUBAREA}}]];
	
	ajaxpost(url, $("#report-form").serialize(), "form[id='report-form']", false, function(){
		initialize();
	}); 
}

function changeContract(){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_UPDATE_CONTRACT}}]];
	ajaxpost(url, $("#report-form").serialize(), "form[id='report-form']", false, function(){
		initialize();
	}); 
}

function formSubmission() {   
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_GENERATE}}]];
	 ajaxpost(url, $("#report-form").serialize(), "form[id='report-form']", false, function(){
		initialize();
	 });
}

function formValidation() {
   $("#report-form").form({
      on : 'blur',
      inline : true,
      fields : {
    	  startDay : {
            identifier : 'startDay',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         endDay : {
             identifier : 'endDay',
             rules : [ {
                type : 'empty',
                prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
             } ]
          }
      }
   });
}

function validateDates(){	
	var localizedtext = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	
	var minDate = $("input[type=hidden][id='minDate']").val();
	var maxDate = $("input[type=hidden][id='maxDate']").val();
   
	calendar('startDay', 'date', localizedtext, minDate, maxDate, 'endDay');
    
}

function getByContract(){	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_GENERATE}}]];
	
	ajaxget(url, "div[id='report-body']", function() {           
		initialize();   
	});
}

function getByType(type){
	var typeChosen = type.getAttribute("data-type");	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_CREATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + typeChosen;
	   
	ajaxget(url, "div[id='report-body']", function() {           
		initialize();   
	});
}

function getMMEReport(){	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REPORTS_MME}}]];
	
	ajaxget(url, "div[id='report-body']", function() {           
		initialize();  
		initializeDropZone();
	});
}