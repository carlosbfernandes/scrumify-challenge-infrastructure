$(document).ready(function() {

   initialize();
});
        
function initialize() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function formSubmission() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKFLOWS_TRANSITION_SAVE}}]];
  
   ajaxpost(url, $("#workflowtransition-form").serialize(), "div[id='workflowtransitions-list']", true, function() {
      initialize();
   });
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  sortOrder : {
            identifier : 'order',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         status : {
             identifier : 'status',
             rules : [ {
                type : 'empty',
                prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
             } ]
          },
          nextStatus : {
              identifier : 'nextStatus',
              rules : [ {
                 type : 'empty',
                 prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
              } ]
           }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}

