$(document).ready(function() {
   var id = $("input[type=hidden][name='id']").val();
   
   initialize();
});
      

function initialize() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
}


function formSubmission() {
	
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_NOTES_SAVE}}]];
   ajaxpost(url, $("#workitemnote-form").serialize(), "", false, function() {
      initialize();
   });
}

function create_workitemnote(id) {
	
	var idworkitem = document.getElementById("idworkitem").value;
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_NOTES_NEW} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + idworkitem + "/" + id;
    $('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
    	CKEDITOR.replace('content', {customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]]});
    	$('#workitemnote-form').form({
    	      on : 'blur',
    	      inline : true,
    	      fields : {
    	    	  content : {
    	            identifier : 'content',
    	            rules : [ {
    	               type : 'ckeditorvalidator',
    	               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]],
    	               value: "textarea[id='content']"
    	            } ]
    	         }
    	      },
    	      onSuccess : function(event) {
    	         event.preventDefault();
    	         
    	         document.forms["workitemnote-form"].submit( function(data){
    	        	 $('body').html(data);
    	         }
    	       );
    	      }
    	   });
    }, true);
}