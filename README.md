# Scrumify Challenge (BETA)

This repository contains the material and gives useful hints about the scrumify challenge from Journey to Cloud - New Joiners KIT

**Note: Please don't assume things, in case of any doubt or inconsistency on this project, reach us.**

## Jenkins/SonarQube VM Setup

[Jenkins](/setup_documentation/jenkins_machine_setup.md)

[SonarQube](/setup_documentation/sonarqube_machine_setup.md)

## Cluster Setup

[Cluster GKE](/setup_documentation/cluster_setup.md)

## Scrumify Deploy

[Scrumify](/setup_documentation/scrumify_setup.md)

## Apigee 

[Apigee](/setup_documentation/apigee_setup.md)


## Others

### Resource Quotas
Resource quotas are a limit defined by google that sets the max number of active VM's under certain ciscunstances.\
Reach us if any problem related with quotas happen to you.\
Quotas & Limits VM problems can me handled by:

    1. Moving VM's to other regions/ zones
    2. Sharing 1 SonarQube VM among colleagues.
        1 Webook for each Jenkins VM
    3. Sharing 1 Jenkins VM among colleagues
    4. Divide the working day into time slots and manage these with new joiners.
