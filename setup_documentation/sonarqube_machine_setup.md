# SonarQube machine setup

The purpose of this document is to help in the process of creating a machine on Google Cloud Platform to run a SonarQube server. This document presents below a set of steps to perform the desired setup.

## Create a Compute Engine

:warning: [Before you start](/setup_documentation/security.md)

## Open port 9000

* By default, the port 9000 (the port used by default by SonarQube server) will not be possible to acess on our browser.
* To solve this, we need to create a __Firewall rule__.
* Steps:
    * On GCP project, go to the __VPC Network__ section
    * On the left menu choose __Firewall__
    * Click on __Create firewall rule__
    * Example of configuration of the rule:
        * Name: `sonarqube-port-9000`
        * Target tags: `sonarqube-port-9000`
        * Source IP ranges: `0.0.0.0/0`
        * Enable *Specified protocols and ports*
        * Enable *tcp* and write `9000` on it
        * After the rule creation, edit the SonarQube VM to add this new tag in the *network tags*.
    * In case of difficulties, the following URL could be useful:
        - https://www.cloudsavvyit.com/4932/how-to-open-firewall-ports-on-a-gcp-compute-engine-instance/

## Configurations inside SonarQube VM

Access the SonarQube VM by __SSH__ and a new window with a bash terminal will be prompted.

### Install Docker

* `sudo apt update`
* `sudo apt install apt-transport-https ca-certificates curl software-properties-common`
* `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
* `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`
* `sudo apt update`
* `apt-cache policy docker-ce` (used to verify if Docker is going to be installed from Docker repository instead of the default Ubuntu repository)
* `sudo apt install docker-ce`
* `sudo systemctl status docker` (to verify if Docker is running)
*  In case of difficulties, the following URL could be useful:
    - https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04

### Install Docker Compose
* ``sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose``
* `sudo chmod +x /usr/local/bin/docker-compose`
* `docker-compose --version` (to verify)
*  In case of difficulties, the following URL could be useful:
    - https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04

### Install SonarQube

To install SonarQube we will use a `docker-compose.yaml` file, justifying the installation of Docker and Docker Compose.

* Create a new folder and access it.
    * `mkdir sonarqube`
    * `cd sonarqube`
* Create a `docker-compose.yaml` file.
    * `nano docker-compose.yaml`
* Insert the code from the file docker-compose.yml into it
* For machine memory issues, the following command may need to be executed.
    * `sudo sysctl -w vm.max_map_count=262144`
* Execute Docker Compose
    * `sudo docker-compose up -d`
    * Other useful commands:
        * `sudo docker-compose logs` (to verify the logs)
        * `sudo docker ps`
        * `sudo docker ps -a`

## Configurations on SonarQube server

Open the SonarQube server in your browser (`http://<sonarqube-vm-ip-address>:9000`)

* The default login is `admin` and the default password is also `admin`.
* After the authentication step, it is a good practice to change the administrator password.
* You can now create a new SonarQube project, for example:
    * Project key: `scrumify`
    * Display name: `scrumify`
    * Generate a token and store the token somewhere  (Token name: `scrumify`)

## Use SonarQube with Jenkins pipeline

### Configurations

* Install *SonarQube Scanner* plugin in your Jenkins server.
* On Jenkins server, go to __Configure System__ and configure __SonarQube servers__.
    * Click to add server
    * Name: `sonarqube`
    * Server URL: `http://<sonarqube-vm-ip-address>:9000`
    * Enable the parameter *Enable injection of SonarQube server configuration as build environment variables*
    * Configure the access token with a Jenkins *Secret Text* (the access token is the token generated on SonarQube)
* On Jenkins server, go to __Global Tool Configuration__ and configure __SonarQube Scanner__.
    * Add scaner
    * Name: `sonarqube`
    * Enable the parameter *Install automatically*

### Use SonarQube Quality Gate on Jenkins pipeline

* To use the Quality Gate on Jenkins pipeline and therefore use the method `waitForQualityGate()` on `Jenkinsfile` it is necessary to configure a webhook on SonarQube that points to `http://<jenkins-vm-ip-address>:8080/sonarqube-webhook/`.
    * On the SonarQube project go to __Project Settings__ and click on __Webhooks__
    * Create a webhook, for example:
        * Name: `Jenkins Webhook`
        * URL: `http://<jenkins-vm-ip-address>:8080/sonarqube-webhook/`
        * The secret is not necessary
