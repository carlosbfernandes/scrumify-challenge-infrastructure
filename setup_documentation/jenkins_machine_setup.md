# Jenkins machine setup

The purpose of this document is to help in the process of creating a machine on Google Cloud Platform to run a Jenkins server to use with the CI/CD pipeline. This document presents below a set of steps to perform the desired setup.

## Create a Compute Engine

:warning: [Before you start](/setup_documentation/security.md)

## Create a Static IP address (Optional)

* It could be useful to reserve a static IP address for our Jenkins VM.
* This is useful if we want to use the Jenkins server URL in other applications (for example to configure GitLab repository to trigger Jenkins pipeline on push events) and we don't want to change those applications everytime the Jenkins VM needs to be restarted.
* Steps:
    * On GCP project, go to the __VPC Network__ section
    * On the left menu choose __External IP addresses__
    * Click on __Reserve static address__
    * On the configuration of the address, in the __Attached to__ option choose the Compute Engine machine created before

## Open port 8080

* By default, the port 8080 (the port used by default by Jenkins server) will not be possible to acess on our browser.
* To solve this, we need to create a __Firewall rule__.
* Steps:
    * On GCP project, go to the __VPC Network__ section
    * On the left menu choose __Firewall__
    * Click on __Create firewall rule__
    * Example of configuration of the rule:
        * Name: `jenkins-port-8080`
        * Target tags: `jenkins-port-8080`
        * Source IP ranges: `0.0.0.0/0`
        * Enable *Specified protocols and ports*
        * Enable *tcp* and write `8080` on it
        * After the rule creation, edit the Jenkins VM to add this new tag in the *network tags*.
    * In case of difficulties, the following URL could be useful:
        - https://www.cloudsavvyit.com/4932/how-to-open-firewall-ports-on-a-gcp-compute-engine-instance/

## Configurations inside Jenkins VM

Access the Jenkins VM by __SSH__ and a new window with a bash terminal will be prompted.

### Install Java

* `sudo apt update`
* `sudo apt install openjdk-11-jdk`
* `dpkg --list | grep -i jdk` (to verify)

### Install Docker

* `sudo apt update`
* `sudo apt install apt-transport-https ca-certificates curl software-properties-common`
* `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
* `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`
* `sudo apt update`
* `apt-cache policy docker-ce` (used to verify if Docker is going to be installed from Docker repository instead of the default Ubuntu repository)
* `curl -O https://download.docker.com/linux/debian/dists/buster/pool/stable/amd64/containerd.io_1.4.3-1_amd64.deb`
* `sudo apt install ./containerd.io_1.4.3-1_amd64.deb`
* `sudo apt install docker-ce`
* `sudo systemctl status docker` (to verify if Docker is running)
*  In case of difficulties, the following URL could be useful:
    - https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04

### Install Jenkins

* `sudo apt install wget`
* `wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -`
* `sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'`
* `sudo apt update`
* `sudo apt install jenkins`
* `sudo systemctl start jenkins` (to initialize Jenkins)
* `sudo systemctl status jenkins` (to verify if Jenkins is running)
*  In case of difficulties, the following URL could be useful:
    - https://www.digitalocean.com/community/tutorials/how-to-install-jenkins-on-ubuntu-18-04-pt

### Install Maven

* `sudo apt update`
* `sudo apt install maven`
* `mvn -version` (to verify if Maven was installed)

### Add Jenkins user to the Docker group

* `sudo usermod -a -G docker jenkins`
* `sudo service jenkins stop` (stop Jenkins)
* `sudo service jenkins start` (start Jenkins)

### Install Kubernetes

* `curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add`
* `sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"`
* `sudo apt update`
* `sudo apt install kubeadm kubelet kubectl`
* In case of difficulties, the following URL could be useful:
    - https://phoenixnap.com/kb/install-kubernetes-on-ubuntu

## Configurations on Jenkins server

Open the Jenkins server in your browser (`http://<jenkins-vm-ip-address>:8080`)

* To unlock Jenkins you should access the file `/var/lib/jenkins/secrets/initialAdminPassword` to obtain the initial administrator password.
    * `sudo cat /var/lib/jenkins/secrets/initialAdminPassword`
* Some other steps can appear, like:
    * Install Jenkins plugins (recommended or selected if you are sure of what you need)
    * Create a new administrator user (you can skip this step)
    * Confirm the Jenkins URL
* A good practice is to change the admin default password.
* Access Jenkins plugins page, and in the *Available* section search for the plugins you want:
    * *Blue Ocean*
    * *SonarQube Scanner*
    * *GitLab*
    * ...
